#!/bin/bash
# NOTE : Quote it else use array to avoid problems #
set -e
cd kube/cron
IMAGE=$1
KUBE_NAMESPACE=$2
for filename in *.yaml
do
  echo "Processing $filename file..."
  [ -e "$filename" ] || continue
  envsubst < "$filename" | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < "$filename" | kubectl create -f - -n "$KUBE_NAMESPACE"
done || exit 1
cd ../..