# Kinship API

Kinship API

## Requirements

- [Python 3.7](https://www.python.org/)
- [Virtualenv](https://virtualenv.pypa.io/en/latest/)
- [Docker](https://docs.docker.com/v17.12/install/)

## Initial Setup

```bash
git clone git@gitlab.com:truthandsystems/kinship/kin-api.git
```

First create and activate a virtualenv, then run the following commands to setup the git pre-commit hook for autoformatting your code using black:

```bash
pip install -r kin-api/requirements-dev.txt
pre-commit install
```

## Running the project

```bash
docker-compose up
```

Then see the section on loading local development data.

## Stopping the project

Hit ctrl-c then

```bash
docker-compose down
```

## Forcing a container build

If you or someone else adds a new dependency, you'll need to first stop
the project (see above), then force a rebuild of the containers by running:

```bash
docker-compose up --build
```

As a final resort, if your containers absolutely will not build when running the above command, you can do a factory reset of Docker. Please note that this will destroy all of your local Docker data (images, containers, volumes).

[Docker Reset to Factory Defaults Instructions](https://docs.docker.com/docker-for-mac/#reset)

## Running Django management commands in API service container

### Examples:

```bash
docker-compose run api manage shell
docker-compose run api manage makemigrations
docker-compose run api manage migrate
```

## Running test suite

```bash
docker-compose run api test
```

## Running tests for a single app
e.g. Notes

```bash
docker-compose run api pytest /usr/src/kin-api/apps/notes/
```

## Loading Local Development Data

```bash
docker-compose run api manage generate_test_data
```

Then you can login with the following credentials:

```
{
  "email": "test@example.com",
  "password": "Password123!"
}
```

[Click here](https://docs.djangoproject.com/en/3.0/ref/django-admin/#available-commands) for a full list of available Django management commands.

###Elastic Search
***This is an ongoing work in progress***

Elastic search is currently using django-elasticsearch-dsl to copy django models into ES. A rebuild can be triggered by running the following yaml (vairables might neeed to be updated):
```bash
kubectl apply -f kube/es/es-rebuild-job.yaml -n "$KUBE_NAMESPACE"
```
