FROM python:3.8-slim

ENV PYTHONUNBUFFERED 1

RUN apt-get update --fix-missing
RUN apt-get install -y \
    libpq-dev \
    libffi-dev \
    libjpeg-dev \
    zlib1g-dev \
    libpng-dev \
    libmagic-dev \
    inotify-tools

# For some reason, this needs to be installed independently and can't be grouped
RUN apt-get install -y software-properties-common

# Add ffmpeg. Should be moved to media service once migrated to Django instead of Flask
RUN add-apt-repository ppa:jonathonf/ffmpeg-4
RUN apt-get install -y ffmpeg

RUN apt-get -q clean -y && rm -rf /var/lib/apt/lists/* && rm -f /var/cache/apt/*.bin

ENV EXPOSED_PORT=8000
EXPOSE ${EXPOSED_PORT}

COPY kin-api/requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

COPY server/entrypoints/* /usr/bin/
COPY kin-api /usr/src/kin-api/

WORKDIR /usr/src/kin-api

CMD ["/usr/bin/run_api"]
