from datetime import datetime
from dateutil.parser import parse, ParserError

from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator
from rest_framework import serializers

from apps.facts.models import Fact
from apps.kin.models import Kin


def get_pinned_facts(kin):
    pinned_facts = Fact.objects.filter(kin=kin, pinned=True).order_by("order")
    if pinned_facts:
        return pinned_facts
    else:
        return None


def fact_email_validator(value):
    try:
        EmailValidator()(value)
    except ValidationError as e:
        raise serializers.ValidationError({"value": e.message})

    return value


def fact_date_validator(value):
    try:
        formatted_value = parse(value, default=datetime(9999, 1, 1)).strftime("%Y-%m-%d")
    except ParserError:
        raise serializers.ValidationError({"value": "Enter a valid date."})

    return formatted_value


def fact_kin_validator(value, user):
    if not Kin.objects.filter(pk=value, owner=user).exists():
        raise serializers.ValidationError({"value": "Enter a valid Kin id"})
