from django_enum_choices.filters import EnumChoiceFilterSetMixin
from django_filters import rest_framework as filters

from apps.facts.models import Fact


class FactFilter(EnumChoiceFilterSetMixin, filters.FilterSet):
    search = filters.CharFilter(method="search_facts")

    class Meta:
        model = Fact
        fields = ["kin", "fact_type"]

    def search_facts(self, queryset, _, value):
        if value:
            queryset = queryset.filter(key__icontains=value)

        return queryset


class FactLabelSortFilter(filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.extra["choices"] += [
            ("kin_count", "kin_count"),
            ("-kin_count", "-kin_count"),
            ("key", "key"),
            ("-key", "-key"),
        ]

    def filter(self, qs, value):
        if not value:
            value = ["-kin_count"]

        if any(v in ["-kin_count"] for v in value):
            qs = qs.order_by("-kin_count").distinct()

            return qs

        if any(v in ["kin_count"] for v in value):
            qs = qs.order_by("kin_count").distinct()

            return qs

        return super().filter(qs, value)


class FactLabelFilter(EnumChoiceFilterSetMixin, filters.FilterSet):
    search = filters.CharFilter(method="search_fact_labels")
    sort = FactLabelSortFilter(
        fields=("key", "key"),
        help_text="Choices are <strong>key</strong>, <strong>-key</strong>,  <strong>kin_count</strong>, or <strong>-kin_count</strong>. Default: <strong>-kin_count</strong>.",
    )

    class Meta:
        model = Fact
        fields = ["kin", "fact_type"]

    def search_fact_labels(self, queryset, _, value):
        if value:
            queryset = queryset.filter(key__icontains=value)

        return queryset
