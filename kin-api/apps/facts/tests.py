from django.conf import settings
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

import redis
import shortuuid

from apps.facts.choices import FactTypeEnum
from apps.facts.models import Fact
from apps.facts.serializers import FactSerializer
from apps.kin.models import Kin
from apps.users.models import User


class FactCreateTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )
        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user2)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_create_fact_no_auth(self):
        response = self.loggedOutClient.post(
            reverse("facts-list"),
            {"key": "Email", "value": "bob@example.com", "kin": self.bob.pk, "fact_type": "EMAIL"},
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_fact(self):
        response = self.loggedInClient1.post(
            reverse("facts-list"),
            {"key": "Email", "value": "bob@example.com", "kin": self.bob.pk, "fact_type": "EMAIL"},
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_fact_with_non_owned_kin(self):
        response = self.loggedInClient1.post(
            reverse("facts-list"),
            {
                "key": "Email",
                "value": "alice@example.com",
                "kin": self.alice.pk,
                "fact_type": "EMAIL",
            },
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class FactUpdateTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user2)

        self.fact = Fact.objects.create(
            key="Email", value="bob@example.com", kin=self.bob, fact_type=FactTypeEnum.EMAIL
        )

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_update_fact_no_auth(self):
        response = self.loggedOutClient.patch(reverse("facts-detail", kwargs={"pk": self.fact.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_fact_by_owner(self):
        response = self.loggedInClient1.patch(
            reverse("facts-detail", kwargs={"pk": self.fact.pk}), {"value": "bob@example.org"}
        )
        self.assertEqual(response.data["value"], "bob@example.org")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_fact_by_non_owner(self):
        response = self.loggedInClient2.patch(
            reverse("facts-detail", kwargs={"pk": self.fact.pk}), {"value": "bob@example.org"}
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_update_owned_fact_with_non_owned_kin(self):
        response = self.loggedInClient1.patch(
            reverse("facts-detail", kwargs={"pk": self.fact.pk}), {"kin": self.alice.id}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class FactDeleteTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user2)

        self.fact1 = Fact.objects.create(
            key="Email", value="bob@example.com", kin=self.bob, fact_type=FactTypeEnum.EMAIL
        )
        self.fact2 = Fact.objects.create(
            key="Phone", value="2025551212", kin=self.bob, fact_type=FactTypeEnum.PHONE
        )

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_delete_fact_no_auth(self):
        response = self.loggedOutClient.delete(
            reverse("facts-detail", kwargs={"pk": self.fact1.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_fact_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            reverse("facts-detail", kwargs={"pk": self.fact1.pk})
        )
        self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

        retrieve_deleted_response = self.loggedInClient1.get(
            reverse("facts-detail", kwargs={"pk": self.fact1.pk})
        )
        self.assertEqual(retrieve_deleted_response.status_code, status.HTTP_404_NOT_FOUND)

        check_list_count_response = self.loggedInClient1.get(reverse("facts-list"))
        check_list_count = check_list_count_response.data["count"] - 1  # account for user kin
        self.assertEqual(check_list_count, 1)

    def test_delete_fact_by_non_owner(self):
        response = self.loggedInClient2.delete(
            reverse("facts-detail", kwargs={"pk": self.fact1.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_bulk_delete_no_auth(self):
        delete_response = self.loggedOutClient.delete(
            f"{reverse('facts-list')}bulk-delete/", {"fact_ids": [self.fact1.pk, self.fact2.pk]}
        )
        self.assertEqual(delete_response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_bulk_delete_with_valid_fact_ids_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            f"{reverse('facts-list')}bulk-delete/", {"fact_ids": [self.fact1.pk, self.fact2.pk]}
        )
        self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

        retrieve_deleted_response_fact1 = self.loggedInClient1.get(
            reverse("facts-detail", kwargs={"pk": self.fact1.pk})
        )
        self.assertEqual(retrieve_deleted_response_fact1.status_code, status.HTTP_404_NOT_FOUND)

        retrieve_deleted_response_fact2 = self.loggedInClient1.get(
            reverse("facts-detail", kwargs={"pk": self.fact2.pk})
        )
        self.assertEqual(retrieve_deleted_response_fact2.status_code, status.HTTP_404_NOT_FOUND)

        check_list_count_response = self.loggedInClient1.get(reverse("facts-list"))
        check_list_count = check_list_count_response.data["count"] - 1  # account for user kin
        self.assertEqual(check_list_count, 0)

    def test_bulk_delete_with_invalid_fact_ids_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            f"{reverse('facts-list')}bulk-delete/", {"fact_ids": [shortuuid.uuid()]}
        )
        self.assertEqual(delete_response.status_code, status.HTTP_400_BAD_REQUEST)


class FactGetTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)

        self.fact1 = Fact.objects.create(
            key="Email", value="bob@example.com", kin=self.bob, fact_type=FactTypeEnum.EMAIL
        )
        self.fact2 = Fact.objects.create(
            key="Phone", value="2025551212", kin=self.bob, fact_type=FactTypeEnum.PHONE
        )
        self.fact3 = Fact.objects.create(
            key="Birthday", value="09/30", kin=self.bob, fact_type=FactTypeEnum.CUSTOM
        )

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_get_fact_list_no_auth(self):
        response = self.loggedOutClient.get(reverse("facts-list"))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_fact_detail_no_auth(self):
        response = self.loggedOutClient.get(reverse("facts-detail", kwargs={"pk": self.fact1.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_fact_list_by_non_owner(self):
        response = self.loggedInClient2.get(reverse("facts-list"))
        results_count = response.data["count"] - 1  # account for user kin
        results = [
            result for result in response.data["results"] if result["kin"] != self.user2.user_kin.id
        ]  # ignore user kin
        self.assertEqual(results_count, 0)
        self.assertEqual(results, [])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_fact_detail_by_non_owner(self):
        response = self.loggedInClient2.get(reverse("facts-detail", kwargs={"pk": self.fact1.pk}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_fact_list_by_owner(self):
        response = self.loggedInClient1.get(reverse("facts-list"))
        results_count = response.data["count"] - 1  # account for user kin
        results = response.data["results"]

        facts = Fact.objects.filter(kin__owner=self.user1)
        serializer = FactSerializer(facts, many=True)

        self.assertEqual(results_count, 3)
        self.assertEqual(serializer.data, results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_fact_detail_by_owner(self):
        response = self.loggedInClient1.get(reverse("facts-detail", kwargs={"pk": self.fact1.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        fact = Fact.objects.get(pk=self.fact1.pk)
        serializer = FactSerializer(fact)

        self.assertEqual(response.data, serializer.data)

    def test_get_fact_list_filtered_by_kin(self):
        response = self.loggedInClient1.get(reverse("facts-list"), {"kin": self.bob.id})
        results_count = response.data["count"]
        results = response.data["results"]

        facts = Fact.objects.filter(kin__owner=self.user1, kin=self.bob.id)
        serializer = FactSerializer(facts, many=True)

        self.assertEqual(results_count, 3)
        self.assertEqual(serializer.data, results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_fact_list_filtered_by_type(self):
        response = self.loggedInClient1.get(reverse("facts-list"), {"fact_type": "EMAIL"})
        results_count = response.data["count"] - 1  # account for user kin
        results = response.data["results"]

        facts = Fact.objects.filter(kin__owner=self.user1, fact_type=FactTypeEnum.EMAIL)
        serializer = FactSerializer(facts, many=True)

        self.assertEqual(results_count, 1)
        self.assertEqual(serializer.data, results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
