from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.facts.choices import FactTypeEnum
from apps.facts.models import Fact


@receiver(post_save, sender=Fact)
def update_primary_metadata(sender, instance, **kwargs):
    if instance.fact_type == FactTypeEnum.PHONE:
        if instance.metadata.get("phone_primary", False):
            existing_primary_phone = sender.objects.filter(
                kin=instance.kin, metadata__phone_primary=True
            ).exclude(id=instance.id)
            for epp in existing_primary_phone:
                epp.metadata["phone_primary"] = False
                epp.save()
    elif instance.fact_type == FactTypeEnum.EMAIL:
        if instance.metadata.get("email_primary", False):
            existing_primary_email = sender.objects.filter(
                kin=instance.kin, metadata__email_primary=True
            ).exclude(id=instance.id)
            for epp in existing_primary_email:
                epp.metadata["email_primary"] = False
                epp.save()
