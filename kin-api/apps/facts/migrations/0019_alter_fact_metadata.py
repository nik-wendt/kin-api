# Generated by Django 3.2.10 on 2022-01-06 23:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("facts", "0018_auto_20211202_1801")]

    operations = [
        migrations.AlterField(
            model_name="fact",
            name="metadata",
            field=models.JSONField(blank=True, default=None, null=True),
        )
    ]
