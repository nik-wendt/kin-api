# Generated by Django 3.0.3 on 2020-08-23 19:17

from django.db import migrations

from dateutil.parser import parse, ParserError


def cleanup_undefined_in_dates(apps, schema_editor):
    Fact = apps.get_model("facts", "Fact")
    facts = Fact.objects.filter(value__contains="/undefined")

    for fact in facts:
        try:
            cleaned_value = fact.value.replace("/undefined", "")
            parse(cleaned_value)  # verifies that the value is a date
            fact.value = cleaned_value
            fact.save()
        except ParserError:
            pass  # value is not a valid date, continue on


class Migration(migrations.Migration):

    dependencies = [("facts", "0006_auto_20200520_1737")]

    operations = [migrations.RunPython(cleanup_undefined_in_dates)]
