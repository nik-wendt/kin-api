from django.db.models import Count
from rest_framework import mixins, permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from drf_yasg.utils import swagger_auto_schema
from rest_framework_bulk import BulkModelViewSet

from apps.facts.choices import FactTypeEnum
from apps.facts.filters import FactFilter, FactLabelFilter
from apps.facts.models import DefaultFact, Fact
from apps.facts.serializers import (
    DefaultFactSerializer,
    FactBulkDeleteSerializer,
    FactChoicesSerializer,
    FactLabelSerializer,
    FactMoveSerializer,
    FactSerializer,
)
from apps.integrations.mixpanel.tasks import (
    mixpanel_fact_deleted_event,
    mixpanel_fact_created_event,
    mixpanel_fact_modified_event,
)
from apps.kin_auth.permissions import IsPaid, IsVerified


class FactViewSet(BulkModelViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = FactSerializer
    filterset_class = FactFilter

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Fact.objects.none()

        qs = Fact.objects.filter(kin__owner=self.request.user)

        return qs

    def create(self, request, *args, **kwargs):
        ret = super().create(request, *args, **kwargs)
        mixpanel_fact_created_event.delay(request.user.id)
        return ret

    def destroy(self, request, *args, **kwargs):
        ret = super().destroy(request, *args, **kwargs)
        mixpanel_fact_deleted_event.delay(request.user.id)
        return ret

    def update(self, request, *args, **kwargs):
        ret = super().update(request, *args, **kwargs)
        mixpanel_fact_modified_event.delay(request.user.id)
        return ret

    @swagger_auto_schema(
        request_body=FactMoveSerializer, responses={status.HTTP_200_OK: FactSerializer}
    )
    @action(detail=True, methods=["POST"])
    def move(self, request, pk=None):
        serializer = FactMoveSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.get_object()
        instance.to(serializer.validated_data["to"])
        output_serializer = FactSerializer(instance)
        return Response(output_serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        request_body=FactBulkDeleteSerializer,
        responses={status.HTTP_204_NO_CONTENT: []},
        operation_id="facts_bulk_delete",
    )
    @action(detail=False, methods=["DELETE"], url_path="bulk-delete")
    def bulk_destroy(self, request):
        serializer = FactBulkDeleteSerializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)

        facts = self.get_queryset().filter(pk__in=serializer.validated_data["fact_ids"])
        count = facts.count()
        facts.delete()
        mixpanel_fact_deleted_event(request.user.id, True, count)
        return Response(status=status.HTTP_204_NO_CONTENT)


class FactChoicesViewSet(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(responses={status.HTTP_200_OK: FactChoicesSerializer})
    def get(self, request):
        fact_type_choices = [{"value": e.name, "option": e.value} for e in FactTypeEnum]

        serializer = FactChoicesSerializer(data={"fact_type_choices": fact_type_choices})
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FactLabelViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = FactLabelSerializer
    filterset_class = FactLabelFilter

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Fact.objects.none()

        qs = (
            Fact.objects.filter(kin__owner=self.request.user)
            .values("key", "fact_type")
            .annotate(kin_count=Count("kin_id", distinct=True))
            .order_by()
        )

        return qs


class DefaultFactViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = DefaultFactSerializer

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return DefaultFact.objects.none()

        qs = DefaultFact.objects.filter(owner=self.request.user)

        return qs

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
