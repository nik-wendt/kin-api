from django_enum_choices.serializers import EnumChoiceModelSerializerMixin
from rest_framework import serializers
from rest_framework_bulk import BulkListSerializer

from apps.activity_log.choices import ActivityTypeEnum
from apps.activity_log.models import LogActivity
from apps.facts.choices import FactTypeEnum
from apps.facts.models import DefaultFact, Fact
from apps.facts.utils import (
    fact_date_validator,
    fact_email_validator,
    get_pinned_facts,
    fact_kin_validator,
)
from apps.kin.models import Kin
from apps.kin_extensions.serializers import ChoicesItemSerializer
from apps.kin_extensions.validators import json_schema_to_drf_validate

PHONE_METADATA_SCHEMA = {
    "type": "object",
    "properties": {"phone_primary": {"type": "boolean"}, "phone_accepts_sms": {"type": "boolean"}},
    "required": ["phone_primary", "phone_accepts_sms"],
    "additionalProperties": False,
}

EMAIL_METADATA_SCHEMA = {
    "type": "object",
    "properties": {"email_primary": {"type": "boolean"}},
    "required": ["email_primary"],
    "additionalProperties": False,
}

DATE_METADATA_SCHEMA = {
    "type": "object",
    "properties": {"date_notify": {"type": "boolean"}},
    "required": ["date_notify"],
    "additionalProperties": False,
}

EMPTY_METADATA_SCHEMA = {"type": "object", "additionalProperties": False}


class FactSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = Fact
        exclude = ("value_pre_migration",)
        list_serializer_class = BulkListSerializer

    def validate_kin(self, value):
        if value:
            user = self._context[
                "request"
            ].user  # TODO: need to try and not get user off of request object as it makes this flow not work outside of a request context

            try:
                # need to ensure the kin is owned by the logged in user
                Kin.objects.get(pk=value.id, owner=user)
            except Kin.DoesNotExist:
                raise serializers.ValidationError("Invalid kin")

        return value

    def validate(self, attrs):
        if attrs.get("metadata", None):
            if attrs.get("fact_type", None) == FactTypeEnum.PHONE:
                json_schema_to_drf_validate(attrs["metadata"], "metadata", PHONE_METADATA_SCHEMA)
            elif attrs.get("fact_type", None) == FactTypeEnum.EMAIL:
                json_schema_to_drf_validate(attrs["metadata"], "metadata", EMAIL_METADATA_SCHEMA)
            elif attrs.get("fact_type", None) == FactTypeEnum.DATE:
                json_schema_to_drf_validate(attrs["metadata"], "metadata", DATE_METADATA_SCHEMA)
            else:
                json_schema_to_drf_validate(attrs["metadata"], "metadata", EMPTY_METADATA_SCHEMA)

        if attrs.get("fact_type", None) == FactTypeEnum.EMAIL:
            fact_email_validator(attrs["value"])
        elif attrs.get("fact_type", None) == FactTypeEnum.DATE:
            formatted_date = fact_date_validator(attrs["value"])
            attrs["value"] = formatted_date
        elif attrs.get("fact_type", None) == FactTypeEnum.KIN:
            user = self.context["request"].user
            fact_kin_validator(attrs["value"], user)

        return attrs

    def create(self, validated_data):
        fact = super().create(validated_data)

        pinned_facts = get_pinned_facts(validated_data["kin"])
        if pinned_facts:
            pinned_facts = list(pinned_facts)
            order = pinned_facts[-1].order
            fact.to(int(order) + 1)
        else:
            fact.top()

        LogActivity.objects.generate_log_activity(
            ActivityTypeEnum.PEEP_ADD_FACT, obj=fact, target=fact.kin
        )
        return fact

    def update(self, instance, validated_data):
        fact = super().update(instance, validated_data)

        kin = validated_data.get("kin", fact.kin)

        pinned_facts = get_pinned_facts(kin)
        if pinned_facts:
            pinned_facts = list(pinned_facts)
            order = pinned_facts[-1].order
            if fact.pinned:
                fact.top()
            else:
                fact.to(int(order) + 1)
        else:
            fact.top()

        return fact


class FactLabelSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    kin_count = serializers.CharField()

    class Meta:
        model = Fact
        fields = ("key", "fact_type", "kin_count")


class FactMoveSerializer(serializers.Serializer):
    to = serializers.IntegerField(
        min_value=0, help_text="The position in the 0-based array you want the fact moved to"
    )


class FactChoicesSerializer(serializers.Serializer):
    fact_type_choices = ChoicesItemSerializer(many=True)


class FactBulkDeleteSerializer(serializers.Serializer):
    fact_ids = serializers.ListField(
        child=serializers.CharField(max_length=50), help_text="An array of fact ids", required=True
    )

    def validate_fact_ids(self, value):
        user = self._context["request"].user

        for fact_id in value:
            if not Fact.objects.filter(pk=fact_id, kin__owner=user).exists():
                raise serializers.ValidationError("Invalid fact")

        return value


class DefaultFactSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = DefaultFact
        exclude = ("owner",)

    def validate_key(self, value):
        user = self._context["request"].user

        if DefaultFact.objects.filter(key__iexact=value, owner=user).exists():
            raise serializers.ValidationError("A default fact with this key already exists.")

        return value
