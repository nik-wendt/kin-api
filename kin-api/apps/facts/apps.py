from django.apps import AppConfig


class FactsConfig(AppConfig):
    name = "apps.facts"
    verbose_name = "Facts"

    def ready(self):
        import apps.facts.signals
