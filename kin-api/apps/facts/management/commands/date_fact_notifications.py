import logging
from datetime import datetime

from django.core.management.base import BaseCommand

import arrow
import pytz

from apps.facts.choices import FactTypeEnum
from apps.facts.models import Fact
from apps.notifications.utils import send_notification

logger = logging.getLogger(__name__)


def get_timezones_by_hour(hour_lookup):
    tzs = []

    for tz in pytz.all_timezones:
        if datetime.now(pytz.timezone(tz)).hour == hour_lookup:
            tzs.append(tz)

    return tzs


def get_date_by_timezone(tz):
    return datetime.now(pytz.timezone(tz))


def get_month_day_by_timezone(tz):
    return datetime.now(pytz.timezone(tz)).strftime("%m-%d")


class Command(BaseCommand):
    help = "Sends special occassion notifications when the user asked to be notified."

    def handle(self, *args, **options):
        logger.info("Starting sending special occassion notifications...")

        tzs = get_timezones_by_hour(7)
        current_month_day = get_month_day_by_timezone(tzs[0])

        facts_to_notify = Fact.objects.filter(
            fact_type=FactTypeEnum.DATE,
            metadata__date_notify=True,
            value__endswith=current_month_day,
            kin__owner__preferences__timezone_setting__in=tzs,
        ).exclude(kin__owner__preferences__timezone_setting="UTC")
        for fact in facts_to_notify:
            fact_date = arrow.get(fact.value)

            if fact_date.year != 9999:
                if fact_date.year > get_date_by_timezone(tzs[0]).year:
                    continue

            message = f"There’s a {fact.key} today for {fact.kin.friendly_name}"

            send_notification(message, fact.kin.owner.id, tz_aware=True)

        logger.info("Finished sending special occassion notifications.")
