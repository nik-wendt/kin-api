from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from elasticsearch_dsl import FacetedSearch, RangeFacet

from apps.facts.models import Fact
from apps.search.utils import (
    trigram_analyzer,
    ImgUrlField,
    search_date_ranges,
    location_properties,
    prepare_location_city_country,
)


@registry.register_document
class FactDocument(Document):
    id = fields.TextField()
    kin = fields.ObjectField(
        properties={
            "id": fields.TextField(),
            "owner": fields.ObjectField(properties={"id": fields.KeywordField()}),
            "friendly_name": fields.TextField(),
            "profile_pic": ImgUrlField(),
        }
    )
    fact_type = fields.TextField(attr="type_to_string")
    location = fields.NestedField(properties=location_properties)
    city_country = fields.KeywordField()
    metadata = fields.ObjectField()
    key = fields.TextField(analyzer=trigram_analyzer)
    value = fields.TextField(analyzer=trigram_analyzer)
    created = fields.DateField()

    class Index:
        name = "facts"
        settings = {"number_of_shards": 1, "number_of_replicas": 0}

    class Django:
        model = Fact
        fields = ["pinned", "modified"]

    def prepare_type(self, instance):
        if instance.fact_type:
            return instance.fact_type.value
        return ""

    def prepare_city_country(self, instance):
        return prepare_location_city_country(instance)


class FactFacetedSearch(FacetedSearch):
    index = "facts"
    doc_types = [FactDocument]
    fields = [
        "value",
    ]

    facets = {
        "created": RangeFacet(
            field="created",
            ranges=search_date_ranges,
        )
    }

    def __init__(self, query=None, filters={}, sort=(), user=None):
        self._model = Fact
        self._user = user
        super().__init__(query, filters, sort)

    def search(self):
        s = super().search()
        return s.filter("term", kin__owner__id=self._user.id)

    def highlight(self, search):
        """
        Add highlighting for all the fields
        """
        return search.highlight(
            *(f if "^" not in f else f.split("^", 1)[0] for f in self.fields),
            pre_tags=["|"],
            post_tags=["|"]
        )


class FactKeyFacetedSearch(FactFacetedSearch):
    fields = ["key"]
