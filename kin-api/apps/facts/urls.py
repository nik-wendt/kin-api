from django.urls import path
from rest_framework import routers

from apps.facts.views import DefaultFactViewSet, FactChoicesViewSet, FactLabelViewSet, FactViewSet


router = routers.DefaultRouter()
router.register("labels", FactLabelViewSet, basename="fact_labels")
router.register("defaults", DefaultFactViewSet, basename="default_facts")
router.register("", FactViewSet, basename="facts")


urlpatterns = [path("choices/", FactChoicesViewSet.as_view())]

urlpatterns += router.urls
