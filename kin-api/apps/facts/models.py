from django.db.models import JSONField
from django.db import models

from django_enum_choices.fields import EnumChoiceField
from django_extensions.db.fields import ShortUUIDField
from ordered_model.models import OrderedModel

from apps.facts.choices import FactTypeEnum
from apps.kin.models import Kin
from apps.kin_extensions.fields import LocationField
from apps.users.models import User


class Fact(OrderedModel):
    id = ShortUUIDField(primary_key=True, editable=False)
    kin = models.ForeignKey(
        Kin, on_delete=models.CASCADE, related_name="facts", db_column="peep_id"
    )
    key = models.CharField(max_length=50)
    value = models.TextField()
    value_pre_migration = models.TextField(blank=True)
    fact_type = EnumChoiceField(FactTypeEnum, default=FactTypeEnum.CUSTOM)
    metadata = JSONField(blank=True, null=True, default=None)
    pinned = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    location = LocationField(null=True, blank=True)

    order_with_respect_to = ("kin",)

    class Meta(OrderedModel.Meta):
        db_table = "peeps_fact"

    def save(self, *args, **kwargs):
        # set default metadata if none is provided
        # if metadata is provided, validation will occur at the serializer level
        if self._state.adding:
            if not self.metadata:
                if self.fact_type == FactTypeEnum.PHONE:
                    existing_primary_phone = Fact.objects.filter(
                        kin=self.kin, metadata__phone_primary=True
                    ).exists()

                    self.metadata = {
                        "phone_primary": not existing_primary_phone,  # if the kin already has a primary phone, set this to false
                        "phone_accepts_sms": False,
                    }
                elif self.fact_type == FactTypeEnum.EMAIL:
                    existing_primary_email = Fact.objects.filter(
                        kin=self.kin, metadata__email_primary=True
                    ).exists()

                    self.metadata = {
                        "email_primary": not existing_primary_email  # if the kin already has a primary email, set this to false
                    }
                elif self.fact_type == FactTypeEnum.DATE:
                    self.metadata = {"date_notify": True}
                else:
                    self.metadata = {}

            self.full_clean()
        super().save(*args, **kwargs)


class DefaultFact(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="default_facts")
    key = models.CharField(max_length=50)
    fact_type = EnumChoiceField(FactTypeEnum, default=FactTypeEnum.CUSTOM)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ["owner", "key"]
