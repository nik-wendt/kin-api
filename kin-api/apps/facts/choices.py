from enum import Enum


class FactTypeEnum(Enum):
    CUSTOM = "CUSTOM"
    PHONE = "PHONE"
    EMAIL = "EMAIL"
    ADDRESS = "ADDRESS"
    DATE = "DATE"
    SOCIAL_PROFILE = "SOCIAL_PROFILE"
    KIN = "KIN"
