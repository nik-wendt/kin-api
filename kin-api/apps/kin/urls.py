from django.urls import path
from rest_framework import routers

from apps.kin.views import (
    KinSourceIDListView,
    KinViewSet,
    UserKinShareViewset,
    KinFrequentConnectionsViewSet,
)

router = routers.DefaultRouter()
router.register(r"share-template", UserKinShareViewset, basename="share-template")
router.register(
    r"frequent-connections", KinFrequentConnectionsViewSet, basename="frequent-connections"
)
router.register("", KinViewSet, basename="kin")

urlpatterns = [
    path("source-ids/", KinSourceIDListView.as_view(), name="source_ids"),
]

urlpatterns += router.urls
