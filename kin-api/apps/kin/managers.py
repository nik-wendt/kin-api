from datetime import timedelta

from django.apps import apps
from django.db import models
from django.db.models import Count, Exists, F, Max, OuterRef
from django.utils import timezone


class KinQuerySet(models.QuerySet):
    def popular(self):
        Reminder = apps.get_model("reminders", "Reminder")

        qs = self

        in_three_days = timezone.now() + timedelta(days=3)

        soon_reminders = Reminder.objects.filter(
            kin=OuterRef("pk"), next_notification__lte=in_three_days
        )

        active_weekly_kin_exist = qs.filter(
            notes__created__gte=timezone.now() - timedelta(days=7)
        ).exists()

        if active_weekly_kin_exist:
            qs = qs.annotate(
                last_active_date=Max("notes__created"), soon_reminders=Exists(soon_reminders)
            ).order_by("-soon_reminders", F("last_active_date").desc(nulls_last=True), "id")
        else:
            qs = qs.annotate(
                note_count=Count("notes"), soon_reminders=Exists(soon_reminders)
            ).order_by("-soon_reminders", "-note_count", "id")

        return qs


class KinManager(models.Manager):
    def get_queryset(self):
        return KinQuerySet(self.model).select_related("reminder")
