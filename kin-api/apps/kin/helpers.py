import json

from django.conf import settings
from django.utils import timezone
from rest_framework import status

import redis

from apps.kin.choices import KinSourceEnum
from apps.kin.models import Kin
from apps.kin.serializers import AllKinResultsSerializer
from apps.notifications.utils import get_pusher_client


def _get_and_cache_all_kin_serialized(user):
    kin = Kin.objects.filter(owner=user).exclude(userkin__user=user).order_by("friendly_name")
    serializer = AllKinResultsSerializer(
        {"count": kin.count(), "cached_at": timezone.now(), "results": kin}
    )

    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_key = f"kin:all_kin:{user.id}"

    redis_client.set(redis_key, json.dumps(serializer.data))

    pc = get_pusher_client()
    pc.trigger(
        f"private-encrypted-{user.id}",
        "all-kin-cache",
        {"status_code": status.HTTP_200_OK, "message": "", "timestamp": str(timezone.now())},
    )

    return serializer.data


def get_all_kin(user):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_key = f"kin:all_kin:{user.id}"

    serialized_results = redis_client.get(redis_key)
    if serialized_results:
        return json.loads(serialized_results)

    return _get_and_cache_all_kin_serialized(user)


def remove_sample_data(user):
    Kin.objects.filter(owner=user, source=KinSourceEnum.SYSTEM).delete()
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    for key in redis_client.scan_iter(f"spotlight:*:{user.id}"):
        redis_client.delete(key)
