from django.conf import settings
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

import redis
import shortuuid

from apps.facts.models import Fact
from apps.kin.choices import KinSourceEnum
from apps.kin.models import Kin
from apps.kin.serializers import KinListSerializer, KinSerializer
from apps.notes.models import Note
from apps.reminders.choices import FrequencyEnum
from apps.reminders.models import Reminder
from apps.users.models import User


class KinCreateTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            email="test@user.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient = APIClient()
        self.loggedInClient.force_authenticate(user=self.user)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_create_kin_no_auth(self):
        response = self.loggedOutClient.post(
            reverse("kin-list"), {"friendly_name": "Bob"}, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_kin(self):
        response = self.loggedInClient.post(
            reverse("kin-list"), {"friendly_name": "Bob"}, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_kin_missing_required_fields(self):
        response = self.loggedInClient.post(
            reverse("kin-list"), {"first_name": "Bob"}, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class KinUpdateTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user1)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_update_kin_no_auth(self):
        response = self.loggedOutClient.patch(reverse("kin-detail", kwargs={"pk": self.bob.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_kin_by_owner(self):
        response = self.loggedInClient1.patch(
            reverse("kin-detail", kwargs={"pk": self.bob.pk}),
            {"first_name": "Bob", "last_name": "Johnson"},
        )
        self.assertEqual(response.data["first_name"], "Bob")
        self.assertEqual(response.data["last_name"], "Johnson")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_kin_by_non_owner(self):
        response = self.loggedInClient2.patch(
            reverse("kin-detail", kwargs={"pk": self.bob.pk}),
            {"first_name": "Bob", "last_name": "Johnson"},
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class KinDeleteTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user1)
        self.jim = Kin.objects.create(
            friendly_name="Jim", source=KinSourceEnum.IOS, owner=self.user1
        )
        self.sue = Kin.objects.create(
            friendly_name="Sue", source=KinSourceEnum.IOS, owner=self.user1
        )
        self.john = Kin.objects.create(
            friendly_name="John", source=KinSourceEnum.ANDROID, owner=self.user1
        )

        Note.objects.create(
            text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            raw_text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            kin=self.bob,
            owner=self.user1,
        )
        Fact.objects.create(key="Email", value="bob@kin.com", kin=self.bob)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_delete_kin_no_auth(self):
        response = self.loggedOutClient.delete(reverse("kin-detail", kwargs={"pk": self.bob.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_kin_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            reverse("kin-detail", kwargs={"pk": self.bob.pk})
        )
        self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

        retrieve_deleted_response = self.loggedInClient1.get(
            reverse("kin-detail", kwargs={"pk": self.bob.pk})
        )
        self.assertEqual(retrieve_deleted_response.status_code, status.HTTP_404_NOT_FOUND)

        check_list_count_response = self.loggedInClient1.get(reverse("kin-list"))
        check_list_count = check_list_count_response.data["count"]
        self.assertEqual(check_list_count, 4)

    def test_delete_kin_by_non_owner(self):
        response = self.loggedInClient2.delete(reverse("kin-detail", kwargs={"pk": self.alice.pk}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_bulk_delete_no_auth(self):
        delete_response = self.loggedOutClient.delete(
            f"{reverse('kin-list')}bulk-delete/", {"kin_ids": [self.bob.pk, self.alice.pk]}
        )
        self.assertEqual(delete_response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_bulk_delete_with_valid_kin_ids_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            f"{reverse('kin-list')}bulk-delete/", {"kin_ids": [self.bob.pk, self.alice.pk]}
        )
        self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

        retrieve_deleted_response_bob = self.loggedInClient1.get(
            reverse("kin-detail", kwargs={"pk": self.bob.pk})
        )
        self.assertEqual(retrieve_deleted_response_bob.status_code, status.HTTP_404_NOT_FOUND)

        retrieve_deleted_response_alice = self.loggedInClient1.get(
            reverse("kin-detail", kwargs={"pk": self.alice.pk})
        )
        self.assertEqual(retrieve_deleted_response_alice.status_code, status.HTTP_404_NOT_FOUND)

        check_list_count_response = self.loggedInClient1.get(reverse("kin-list"))
        check_list_count = check_list_count_response.data["count"]
        self.assertEqual(check_list_count, 3)

    def test_bulk_delete_with_invalid_kin_ids_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            f"{reverse('kin-list')}bulk-delete/", {"kin_ids": [shortuuid.uuid()]}
        )
        self.assertEqual(delete_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_bulk_delete_with_valid_source_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            f"{reverse('kin-list')}bulk-delete/", {"source": "IOS"}
        )
        self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

        retrieve_deleted_response_jim = self.loggedInClient1.get(
            reverse("kin-detail", kwargs={"pk": self.jim.pk})
        )
        self.assertEqual(retrieve_deleted_response_jim.status_code, status.HTTP_404_NOT_FOUND)

        retrieve_deleted_response_sue = self.loggedInClient1.get(
            reverse("kin-detail", kwargs={"pk": self.sue.pk})
        )
        self.assertEqual(retrieve_deleted_response_sue.status_code, status.HTTP_404_NOT_FOUND)

        check_list_count_response = self.loggedInClient1.get(reverse("kin-list"))
        check_list_count = check_list_count_response.data["count"]
        self.assertEqual(check_list_count, 3)

    def test_bulk_delete_with_invalid_source_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            f"{reverse('kin-list')}bulk-delete/", {"source": "FOO"}
        )
        self.assertEqual(delete_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_bulk_delete_with_valid_kin_ids_and_valid_source_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            f"{reverse('kin-list')}bulk-delete/",
            {"kin_ids": [self.bob.pk, self.alice.pk], "source": "IOS"},
        )
        self.assertEqual(delete_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_bulk_delete_with_valid_kin_ids_by_non_owner(self):
        delete_response = self.loggedInClient2.delete(
            f"{reverse('kin-list')}bulk-delete/", {"kin_ids": [self.bob.pk, self.alice.pk]}
        )
        self.assertEqual(delete_response.status_code, status.HTTP_400_BAD_REQUEST)


class KinGetTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        Fact.objects.create(key="Email", value="bob@kin.com", kin=self.bob)
        Reminder.objects.create(kin=self.bob, frequency=FrequencyEnum.MONTHLY)

        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user1)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_get_kin_list_no_auth(self):
        response = self.loggedOutClient.get(reverse("kin-list"))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_kin_detail_no_auth(self):
        response = self.loggedOutClient.get(reverse("kin-detail", kwargs={"pk": self.bob.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_kin_list_by_non_owner(self):
        response = self.loggedInClient2.get(reverse("kin-list"))
        results_count = response.data["count"]
        results = response.data["results"]
        self.assertEqual(results_count, 0)
        self.assertEqual(results, [])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_kin_detail_by_non_owner(self):
        response = self.loggedInClient2.get(reverse("kin-detail", kwargs={"pk": self.bob.pk}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_kin_list_by_owner(self):
        response = self.loggedInClient1.get(reverse("kin-list"))
        results_count = response.data["count"]
        results = response.data["results"]

        kin = Kin.objects.filter(owner=self.user1).exclude(id=self.user1.user_kin.id).popular()
        serializer = KinListSerializer(kin, many=True)

        self.assertEqual(results_count, 2)
        self.assertEqual(serializer.data, results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_kin_detail_by_owner(self):
        response = self.loggedInClient1.get(reverse("kin-detail", kwargs={"pk": self.bob.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        bob = Kin.objects.get(pk=self.bob.pk)
        serializer = KinSerializer(bob)

        self.assertEqual(response.data, serializer.data)

    def test_get_kin_list_search(self):
        response = self.loggedInClient1.get(reverse("kin-list"), {"search": "bob"})
        results_count = response.data["count"]

        self.assertEqual(results_count, 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_kin_list_filtered_by_existing_fact(self):
        response = self.loggedInClient1.get(reverse("kin-list"), {"fact": "email"})
        results_count = response.data["count"]

        self.assertEqual(results_count, 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_kin_list_filtered_by_non_existing_fact(self):
        response = self.loggedInClient1.get(reverse("kin-list"), {"fact": "phone"})
        results_count = response.data["count"]

        self.assertEqual(results_count, 0)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_kin_list_filtered_by_has_reminders(self):
        response = self.loggedInClient1.get(reverse("kin-list"), {"has_reminder": True})
        results_count = response.data["count"]

        self.assertEqual(results_count, 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_kin_list_filtered_by_no_reminders(self):
        response = self.loggedInClient1.get(reverse("kin-list"), {"has_reminder": False})
        results_count = response.data["count"]

        self.assertEqual(results_count, 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_kin_list_by_latest(self):
        response = self.loggedInClient1.get(reverse("kin-list"), {"sort": "latest"})
        results_count = response.data["count"]
        results = response.data["results"]

        kin = (
            Kin.objects.filter(owner=self.user1)
            .exclude(id=self.user1.user_kin.id)
            .order_by("-modified", "id")
            .distinct()
        )
        serializer = KinListSerializer(kin, many=True)

        self.assertEqual(results_count, 2)
        self.assertEqual(serializer.data, results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_kin_list_by_latest_desc(self):
        response = self.loggedInClient1.get(reverse("kin-list"), {"sort": "-latest"})
        results_count = response.data["count"]
        results = response.data["results"]

        kin = (
            Kin.objects.filter(owner=self.user1)
            .exclude(id=self.user1.user_kin.id)
            .order_by("modified", "id")
            .distinct()
        )
        serializer = KinListSerializer(kin, many=True)

        self.assertEqual(results_count, 2)
        self.assertEqual(serializer.data, results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class KinMergeTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob1 = Kin.objects.create(friendly_name="Bob1", owner=self.user1)
        self.bob2 = Kin.objects.create(
            friendly_name="Bob2", first_name="Bob", last_name="Johnson", owner=self.user1
        )
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user1)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_merge_kin_by_owner(self):
        merge_response = self.loggedInClient1.post(
            f"{reverse('kin-list')}merge/",
            {"from_kin": self.bob2.id, "to_kin": self.bob1.id, "friendly_name": "Bob"},
        )

        self.assertEqual(merge_response.status_code, status.HTTP_200_OK)

        bob = Kin.objects.get(pk=self.bob1.id)
        serializer = KinSerializer(bob)

        self.assertEqual(merge_response.data, serializer.data)
        self.assertEqual(bob.friendly_name, "Bob")
        self.assertEqual(bob.first_name, "Bob")
        self.assertEqual(bob.last_name, "Johnson")

        from_kin_response = self.loggedInClient1.get(
            reverse("kin-detail", kwargs={"pk": self.bob2.pk})
        )
        self.assertEqual(from_kin_response.status_code, status.HTTP_404_NOT_FOUND)

    def test_merge_kin_by_non_owner(self):
        merge_response = self.loggedInClient2.post(
            f"{reverse('kin-list')}merge/",
            {"from_kin": self.bob2.id, "to_kin": self.bob1.id, "friendly_name": "Bob"},
        )

        self.assertEqual(merge_response.status_code, status.HTTP_400_BAD_REQUEST)
