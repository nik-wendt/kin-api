from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from django.utils import timezone

from apps.connections.models import Connection
from apps.facts.models import Fact
from apps.kin.models import Kin
from apps.kin.tasks import update_all_kin_cache_task
from apps.notes.models import Note


@receiver(post_save, sender=Note)
@receiver(post_delete, sender=Note)
@receiver(post_save, sender=Fact)
@receiver(post_delete, sender=Fact)
@receiver(post_save, sender=Connection)
@receiver(post_delete, sender=Connection)
def update_kin_modified(sender, instance, **kwargs):
    try:
        if instance.kin:
            Kin.objects.filter(pk=instance.kin.id).update(
                modified=timezone.now()
            )  # Need to ensure kin modified timestamp gets updated for efficient latest kin sort, don't want to call save because it'll trigger more signals
    except Kin.DoesNotExist:  # Account for the times where a kin is deleted, causing notes/facts to be deleted, and note/fact delete triggers this signal again
        return


@receiver(post_save, sender=Kin)
@receiver(post_delete, sender=Kin)
def update_all_kin_cache(sender, instance, **kwargs):
    update_all_kin_cache_task.delay(instance.owner.id)
