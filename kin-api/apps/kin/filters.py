from django.db.models import Q

from django_enum_choices.filters import EnumChoiceFilterSetMixin
from django_filters import rest_framework as filters

from apps.kin.models import Kin


class KinSortFilter(filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.extra["choices"] += [
            ("popularity", "popularity"),
            ("latest", "latest"),
            ("-latest", "-latest"),
            ("created", "created"),
            ("-created", "-created"),
        ]

    def filter(self, qs, value):
        if not value:
            value = ["popularity"]

        if any(v in ["latest"] for v in value):
            qs = qs.order_by("-modified", "id").distinct()

            return qs

        if any(v in ["-latest"] for v in value):
            qs = qs.order_by("modified", "id").distinct()

            return qs

        if any(v in ["created"] for v in value):
            qs = qs.order_by("-created", "id").distinct()

            return qs

        if any(v in ["-created"] for v in value):
            qs = qs.order_by("created", "id").distinct()

            return qs

        if any(v in ["popularity"] for v in value):
            qs = qs.popular()

            return qs

        return super().filter(qs, value)


class KinFilter(EnumChoiceFilterSetMixin, filters.FilterSet):
    search = filters.CharFilter(method="search_kin")
    group = filters.CharFilter(method="filter_by_group")
    fact = filters.CharFilter(method="filter_by_fact")
    has_reminder = filters.BooleanFilter(
        method="filter_by_has_reminder", help_text="<strong>true</strong> or <strong>false</strong>"
    )
    sort = KinSortFilter(
        fields=("friendly_name", "friendly_name"),
        help_text="Choices are <strong>friendly_name</strong>, <strong>-friendly_name</strong>,  <strong>latest</strong>, <strong>-latest</strong>, <strong>created</strong>, <strong>-created</strong>, or <strong>popularity</strong>. Default: <strong>popularity</strong>.",
    )
    modified_start = filters.DateFilter(field_name="modified", lookup_expr="gte")
    modified_end = filters.DateFilter(
        field_name="modified",
        lookup_expr="lte",
        help_text="Add one day to the last date you want to see. e.g. 2021-03-20 if the last day you want to see is 2021-03-19.",
    )

    class Meta:
        model = Kin
        fields = ["search", "group", "fact", "modified_start", "modified_end", "source"]

    def search_kin(self, queryset, _, value):
        if value:
            queryset = queryset.filter(
                Q(first_name__icontains=value)
                | Q(last_name__icontains=value)
                | Q(friendly_name__icontains=value)
            )

        return queryset

    def filter_by_group(self, queryset, _, value):
        if value:
            queryset = queryset.filter(groups__name__iexact=value)

        return queryset

    def filter_by_fact(self, queryset, _, value):
        if value:
            queryset = queryset.filter(facts__key__icontains=value)

        return queryset

    def filter_by_has_reminder(self, queryset, _, value):

        if isinstance(value, bool):
            is_null = not value
            queryset = queryset.filter(reminder__isnull=is_null)

        return queryset
