import json
import logging

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.db import transaction
from django_enum_choices.choice_builders import attribute_value
from django_enum_choices.serializers import EnumChoiceModelSerializerMixin
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_bulk import BulkListSerializer

import redis
from dateutil.parser import parse, ParserError

from apps.activity_log.choices import ActivityTypeEnum
from apps.activity_log.models import LogActivity
from apps.facts.models import Fact
from apps.facts.utils import fact_date_validator, fact_email_validator
from apps.groups.models import Group
from apps.kin.choices import KinSourceEnum, ShareContentTypeEnum
from apps.kin.models import Kin, UserKin, UserKinShare
from apps.kin_extensions.serializers import CustomEnumChoiceField
from apps.notes.choices import NoteSourceEnum
from apps.notes.models import Note
from apps.notes.serializers import NoteWriteSerializer
from apps.reminders.choices import FrequencyEnum, RecencyEnum
from apps.reminders.models import Reminder

logger = logging.getLogger(__name__)


class InlineFactsSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = Fact
        fields = ("id", "key", "value", "fact_type", "order", "pinned", "metadata", "created")


class InlineReminderSerializer(serializers.ModelSerializer):
    frequency = CustomEnumChoiceField(FrequencyEnum, choice_builder=attribute_value, read_only=True)
    recency = CustomEnumChoiceField(RecencyEnum, choice_builder=attribute_value, read_only=True)

    class Meta:
        model = Reminder
        fields = ("id", "frequency", "recency", "next_notification", "created")
        read_only_fields = ("id", "frequency", "recency", "next_notification", "created")
        ref_name = None


class InlineGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ("id", "name")


class InlineNoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        fields = ("id", "plain_text")


class KinListSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    reminder = InlineReminderSerializer(read_only=True)
    is_favorite = serializers.BooleanField(read_only=True)

    class Meta:
        model = Kin
        exclude = ("owner", "groups")


class KinSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kin
        fields = ("id", "friendly_name", "profile_pic", "created", "modified", "is_favorite")


class KinFrequentSerializer(serializers.ModelSerializer):
    note_score = serializers.IntegerField()
    connection_score = serializers.IntegerField()
    total_score = serializers.IntegerField()

    class Meta:
        model = Kin
        fields = ("id", "note_score", "connection_score", "total_score")


class AllKinResultsSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    cached_at = serializers.DateTimeField()
    results = KinSimpleSerializer(many=True)


class KinSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    facts = InlineFactsSerializer(many=True, read_only=True)
    groups = InlineGroupSerializer(many=True, read_only=True)
    group_objects = InlineGroupSerializer(
        source="groups", many=True, read_only=True
    )  # TODO: Deprecated. Remove after one App Store Cycle
    reminder = InlineReminderSerializer(read_only=True)
    notes = InlineNoteSerializer(many=True, read_only=True)
    last_connection_date = serializers.DateField(read_only=True)
    has_had_profile_pic = serializers.BooleanField(read_only=True)
    is_favorite = serializers.BooleanField(read_only=True)

    class Meta:
        model = Kin
        exclude = ("owner",)
        read_only_fields = (
            "facts",
            "groups",
            "group_objects",
            "reminder",
            "notes",
            "source",
            "source_id",
            "last_connection_date",
            "has_had_profile_pic",
            "is_favorite",
        )

    def create(self, validated_data):
        kin = super().create(validated_data)
        LogActivity.objects.generate_log_activity(ActivityTypeEnum.PEEP_CREATE, obj=kin)

        return kin


class KinBulkDeleteSerializer(serializers.Serializer):
    kin_ids = serializers.ListField(
        child=serializers.CharField(max_length=50), help_text="An array of kin ids", required=False
    )
    source = CustomEnumChoiceField(KinSourceEnum, required=False)

    def validate(self, attrs):
        if attrs.get("kin_ids", None) and attrs.get("source", None):
            raise serializers.ValidationError(
                "Only one of either kin_ids or source must be set, but not both."
            )

        if not attrs.get("kin_ids", None) and not attrs.get("source", None):
            raise serializers.ValidationError("You must set one of either kin_ids or source.")

        return attrs

    def validate_kin_ids(self, value):
        user = self._context["request"].user
        try:
            for kin_id in value:
                Kin.objects.get(pk=kin_id, owner=user)
        except Kin.DoesNotExist:
            raise serializers.ValidationError("Invalid kin")

        return value


class KinPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        user = self.context["request"].user
        queryset = Kin.objects.filter(owner=user)
        return queryset


class KinMergeSerializer(serializers.Serializer):
    from_kin = KinPrimaryKeyRelatedField()
    to_kin = KinPrimaryKeyRelatedField()
    friendly_name = serializers.CharField(max_length=50)

    def _generate_unique_fact_key(self, key, value):
        for d in self.validated_data["to_kin"].facts.all():
            if d.key == key:
                if d.value != value:
                    key_split = key.split("(")
                    if len(key_split) > 1:
                        try:
                            key_number = key_split.pop()
                            key_number = int(key_number[0 : len(key_number) - 1])
                            key = ("(".join(key_split)) + "(" + str(key_number + 1) + ")"
                            return self._generate_unique_fact_key(key, value)
                        except ValueError:
                            key = key + " (2)"
                            return self._generate_unique_fact_key(key, value)
                    else:
                        key = key + " (2)"
                        return self._generate_unique_fact_key(key, value)
                else:
                    return None
        return key

    def _merge_notes(self, to_kin, from_kin):
        for note in from_kin.notes.all():
            note.kin = to_kin
            note.save()

    def _merge_facts(self, to_kin, from_kin):
        next_fact_order = to_kin.facts.count()

        for fact in from_kin.facts.all():
            new_key = self._generate_unique_fact_key(fact.key, fact.value)
            if new_key:
                fact.key = new_key
                fact.kin = to_kin
                fact.save()
                fact.to(next_fact_order)
                next_fact_order += 1
            else:
                fact.delete()

    def _merge_groups(self, to_kin, from_kin):
        for group in from_kin.groups.all():
            to_kin.groups.add(group)

    def _merge_names(self, to_kin, from_kin):
        to_kin.friendly_name = self.validated_data["friendly_name"]

        if not to_kin.first_name:
            to_kin.first_name = from_kin.first_name
        if not to_kin.last_name:
            to_kin.last_name = from_kin.last_name
        if not to_kin.middle_name:
            to_kin.middle_name = from_kin.middle_name
        if not to_kin.suffix:
            to_kin.suffix = from_kin.suffix
        if not to_kin.profile_pic:
            to_kin.profile_pic = from_kin.profile_pic

        to_kin.save()

    def validate(self, attrs):
        from_kin_id = attrs["from_kin"].id
        to_kin_id = attrs["to_kin"].id

        if from_kin_id == to_kin_id:
            raise serializers.ValidationError("From and to kin must be different.")

        return attrs

    def save(self, **kwargs):
        to_kin = self.validated_data["to_kin"]
        from_kin = self.validated_data["from_kin"]

        self._merge_notes(to_kin, from_kin)
        self._merge_facts(to_kin, from_kin)
        self._merge_groups(to_kin, from_kin)
        self._merge_names(to_kin, from_kin)

        from_kin.delete()
        to_kin.refresh_from_db()

        return to_kin


class KinProfilPicSerializer(serializers.Serializer):
    profile_pic = serializers.FileField(required=True)


class FactImportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fact
        exclude = ("kin", "value_pre_migration")

    def validate(self, attrs):
        try:
            if attrs.get("fact_type", None) == "EMAIL":
                fact_email_validator(attrs["value"])
            elif attrs.get("fact_type", None) == "DATE":
                formatted_date = fact_date_validator(attrs["value"])
                attrs["value"] = formatted_date
        except (ParserError, serializers.ValidationError):
            attrs["fact_type"] = "CUSTOM"

        return attrs

    def validate_value(self, value):
        # clean up undefined in date data
        if "/undefined" in value:
            try:
                cleaned_value = value.replace("/undefined", "")
                parse(cleaned_value)  # verifies that the value is a date
                return cleaned_value
            except ParserError:
                pass  # value is not a valid date, continue on and return the original value

        return value


class KinImportFactSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = Fact
        fields = "__all__"
        list_serializer_class = BulkListSerializer

    def create(self, validated_data):
        fact = super().create(validated_data)
        LogActivity.objects.generate_log_activity(
            ActivityTypeEnum.PEEP_ADD_FACT, obj=fact, target=fact.kin
        )
        return fact


class KinImportSerializer(KinSerializer):
    facts = FactImportSerializer(many=True, required=False)
    notes = NoteWriteSerializer(many=True, required=False)

    class Meta:
        model = Kin
        exclude = ("owner", "profile_pic")

    def create(self, validated_data):
        try:
            facts_data = validated_data.pop("facts")
        except KeyError:
            facts_data = []  # Facts aren't required

        try:
            notes_data = validated_data.pop("notes")
        except KeyError:
            notes_data = []  # Notes aren't required

        with transaction.atomic():
            kin = super().create(validated_data)

            for fact_data in facts_data:
                fact_data["kin"] = kin.id
                fact_serializer = KinImportFactSerializer(data=fact_data)
                fact_serializer.is_valid(raise_exception=True)
                fact_serializer.save()

            for note_data in notes_data:
                note_data["kin"] = kin.id
                note_data["source"] = note_data["source"].value
                note_serializer = NoteWriteSerializer(
                    data=note_data, context={"request": self._context["request"]}
                )
                note_serializer.is_valid(raise_exception=True)
                note_serializer.save(owner=self._context["request"].user)

            return kin


class KinSourceIDQuerySerializer(serializers.Serializer):
    source = CustomEnumChoiceField(KinSourceEnum)


class KinTokenImportSerializer(serializers.Serializer):
    token = serializers.CharField()

    def validate_token(self, value):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        decoded_token = redis_client.get(value)

        if not decoded_token:
            raise serializers.ValidationError(
                "The token is expired or invalid. Please request a new one."
            )

        return json.loads(decoded_token)

    def save(self, **kwargs):
        save_serializer = KinImportSerializer(
            data=self.validated_data["token"]["kin"],
            context={"request": self._context["request"]},
            many=True,
        )
        save_serializer.is_valid(raise_exception=True)
        kin = save_serializer.save(owner=self._context["request"].user)

        if self.validated_data["token"]["group_name"]:
            group, _ = Group.objects.get_or_create(
                name=self.validated_data["token"]["group_name"], owner=self._context["request"].user
            )
            group.kin.add(*kin)

        if (
            len(kin) == 1
        ):  # This is temporary, but don't quite know how to go about matching kin with profile pics when dealing with multiple kin in a 'many' supported serializer
            share_kin_profile_pic = self.validated_data["token"]["kin"][0].get("profile_pic", None)
            if share_kin_profile_pic:
                file = default_storage.open(share_kin_profile_pic, "rb")
                new_file = ContentFile(file.read())
                kin[0].profile_pic.save(share_kin_profile_pic.split("/")[-1], new_file)
                kin[0].refresh_from_db()

        # TODO: Fix this, only doing due in the interest of time due to a circular import.
        from apps.kin.helpers import remove_sample_data

        remove_sample_data(self._context["request"].user)

        return kin


class KinTokenBulkImportResultSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    group = InlineGroupSerializer(required=False)
    kin = KinSerializer(many=True)
    sharer_name = serializers.CharField(required=False)


class KinShareURLGenerationSerializer(serializers.Serializer):
    share_name = serializers.CharField(max_length=50)
    kin_fields = serializers.MultipleChoiceField(
        choices=["first_name", "middle_name", "suffix", "last_name", "profile_pic"],
        required=True,
        allow_empty=False,
    )
    fact_ids = serializers.ListField(
        child=serializers.CharField(max_length=50), help_text="An array of fact ids"
    )

    note_ids = serializers.ListField(
        child=serializers.CharField(max_length=22), help_text="An array of note ids"
    )
    content_type = CustomEnumChoiceField(
        ShareContentTypeEnum, required=False, allow_null=True, choice_builder=attribute_value
    )
    template_name = serializers.CharField(max_length=18, required=False, allow_blank=True)

    def validate_field(self, value, model):
        user = self.context["request"].user
        unique_values = set(value)
        qs_values = model.objects.filter(id__in=unique_values, kin__owner=user).values_list(
            "id", flat=True
        )
        if qs_values.count() != len(unique_values):
            bad_ids = [i for i in unique_values if i not in qs_values]
            raise ValidationError(f"{bad_ids} are not valid {model._meta.object_name} ids")
        return list(qs_values)

    def validate_fact_ids(self, value):
        return self.validate_field(value, Fact)

    def validate_note_ids(self, value):
        return self.validate_field(value, Note)

    def validate_note_ids(self, value):
        unique_values = set(value)
        qs_values = Note.objects.filter(id__in=unique_values).values_list("id", flat=True)
        if qs_values.count() != len(unique_values):
            bad_ids = [i for i in unique_values if i not in qs_values]
            raise ValidationError(f"{bad_ids} are not valid note ids")

        return list(qs_values)


def generate_kin_share_dict(kin, fields, share_name, fact_ids, note_ids, template_name=None):
    share_dict = {field: getattr(kin, field) for field in fields if field != "profile_pic"}
    share_dict["friendly_name"] = share_name

    if "profile_pic" in fields:
        share_dict["profile_pic"] = kin.profile_pic.name

    share_dict["facts"] = [
        {
            "key": fact.key,
            "value": fact.value,
            "fact_type": fact.fact_type.value,
            "metadata": fact.metadata,
        }
        for fact in Fact.objects.filter(id__in=fact_ids, kin=kin)
    ]
    share_dict["notes"] = [
        {"text": note.plain_text, "source": NoteSourceEnum.SHARE_TOKEN.value}
        for note in Note.objects.filter(id__in=note_ids, kin=kin)
    ]

    # Storing the data from the original request for use in refreshing for wallets. Also it's not that expensive.
    share_dict["meta"] = {
        "fact_ids": fact_ids,
        "note_ids": note_ids,
        "kin_fields": [x for x in fields],
        "kin_id": kin.id,
        "template_name": template_name,
    }

    return share_dict


class KinBulkShare(serializers.Serializer):
    kin = KinPrimaryKeyRelatedField()
    share_data = KinShareURLGenerationSerializer()


class KinBulkShareURLGenerationSerializer(serializers.Serializer):
    kin_list = KinBulkShare(many=True)
    group_name = serializers.CharField(
        max_length=50, required=False, allow_blank=True, allow_null=True
    )
    content_type = CustomEnumChoiceField(
        ShareContentTypeEnum, required=False, allow_null=True, choice_builder=attribute_value
    )


class KinShareURLSerializer(serializers.Serializer):
    token = serializers.CharField()


class UserKinShareSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserKinShare
        fields = ("token", "template_name", "created", "id")

    def validate(self, attrs):
        qs = UserKinShare.objects.filter(owner=self.owner, token=attrs["token"]).exists()
        if qs:
            raise ValidationError(
                {
                    "token": f"A template already exists for this user with the token {attrs['token']}"
                }
            )
        return attrs

    def validate_token(self, value):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        decoded_token = redis_client.get(value)

        if not decoded_token:
            raise serializers.ValidationError(
                "The token is expired or invalid. Please request a new one."
            )

        token_data = json.loads(decoded_token)
        kin_id = token_data["kin"][0]["meta"].get("kin_id")

        if not UserKin.objects.filter(kin_id=kin_id).exists():
            raise serializers.ValidationError(
                "This token does not contain user Kin data. Please use only tokens sharing the user's Kin."
            )

        return value

    def persist_token(self, value):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.persist(value)


class UserKinShareListSerializer(serializers.ModelSerializer):
    token = serializers.CharField(read_only=True)
    template_name = serializers.CharField(read_only=True)

    class Meta:
        model = UserKinShare
        fields = ("token", "template_name")


class UserKinShareBulkDeleteSerializer(serializers.Serializer):
    template_ids = serializers.ListField(
        child=serializers.CharField(max_length=50),
        help_text="An array of template ids",
        required=True,
    )

    def validate_template_ids(self, value):
        user = self._context["request"].user

        for template_id in value:
            if not UserKinShare.objects.filter(pk=template_id, owner=user).exists():
                raise serializers.ValidationError(f"Invalid template_id {template_id}")

        return value
