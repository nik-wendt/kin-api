from django.contrib.contenttypes.fields import GenericRelation
from django.core.files.storage import default_storage
from django.db import models
from django.utils.functional import cached_property

from django_enum_choices.fields import EnumChoiceField
from django_extensions.db.fields import ShortUUIDField

from apps.favorites.models import Favorite
from apps.groups.models import Group
from apps.kin.choices import KinSourceEnum
from apps.kin.managers import KinManager
from apps.kin_extensions.fields import LocationField
from apps.users.models import User


def kin_profile_pic_path(instance, filename):
    return f"profile_pics/owner-{instance.owner_id}/{instance.id}/{filename}"


class Kin(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="kin")
    friendly_name = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50, blank=True)
    middle_name = models.CharField(max_length=50, blank=True)
    suffix = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    profile_pic = models.ImageField(upload_to=kin_profile_pic_path, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    groups = models.ManyToManyField(Group, through="KinGroup", related_name="kin")
    source = EnumChoiceField(KinSourceEnum, default=KinSourceEnum.APP)
    source_id = models.CharField(max_length=50, blank=True)
    modified = models.DateTimeField(auto_now=True)
    location = LocationField(null=True, blank=True)
    favorite = GenericRelation(Favorite)

    objects = KinManager()

    def __str__(self):
        return f"{self.owner.id}'s kin {self.id}:{self.friendly_name}"

    @cached_property
    def is_favorite(self):
        return Favorite.objects.filter(object_id=self.id).exists()

    @cached_property
    def primary_email(self):
        email_fact = self.facts.filter(metadata__email_primary=True).first()

        if email_fact:
            return email_fact.value

    @cached_property
    def primary_phone(self):
        phone_fact = self.facts.filter(metadata__phone_primary=True).first()

        if phone_fact:
            return phone_fact.value

    @cached_property
    def primary_sms(self):
        sms_fact = self.facts.filter(
            metadata__phone_primary=True, metadata__phone_accepts_sms=True
        ).first()

        if sms_fact:
            return sms_fact.value

    @cached_property
    def last_connection_date(self):
        last_connection = self.connections.order_by("-connection_date", "-created").first()

        if last_connection:
            return last_connection.connection_date

    @cached_property
    def has_had_profile_pic(self):
        try:
            return (
                len(default_storage.listdir(f"/profile_pics/owner-{self.owner.id}/{self.id}/")[1])
                > 0
            )
        except:
            return False

    def save(self, *args, **kwargs):
        if self._state.adding:
            self.full_clean()
        return super().save(**kwargs)

    class Meta:
        db_table = "peeps_peep"
        ordering = ["-created"]


class KinGroup(models.Model):
    kin = models.ForeignKey(Kin, on_delete=models.CASCADE, db_column="peep_id")
    group = models.ForeignKey(Group, on_delete=models.CASCADE, db_column="tag_id")

    class Meta:
        db_table = "peeps_peep_tags"


class UserKin(models.Model):
    user = models.OneToOneField(
        User, primary_key=True, on_delete=models.CASCADE, related_name="userkin"
    )
    kin = models.OneToOneField(Kin, on_delete=models.PROTECT, related_name="userkin")


class UserKinShare(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="userkinshare")
    token = models.CharField(max_length=24)
    template_name = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ["owner", "token"]
