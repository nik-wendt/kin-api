# Generated by Django 3.2.10 on 2022-05-12 04:09

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("kin", "0013_userkinshare"),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name="userkinshare",
            unique_together={("user", "token")},
        ),
    ]
