# Generated by Django 3.0.3 on 2020-07-14 02:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("groups", "0002_auto_20200428_0137"), ("kin", "0004_remove_kin_is_archived")]

    operations = [
        migrations.AlterField(
            model_name="kin",
            name="groups",
            field=models.ManyToManyField(
                related_name="kin", through="kin.KinGroup", to="groups.Group"
            ),
        )
    ]
