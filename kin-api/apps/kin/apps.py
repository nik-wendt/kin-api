from django.apps import AppConfig


class KinConfig(AppConfig):
    name = "apps.kin"
    verbose_name = "Kin"

    def ready(self):
        import apps.kin.signals
