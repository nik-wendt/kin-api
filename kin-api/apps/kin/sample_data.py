from datetime import timedelta

from django.utils import timezone


SAMPLE_GROUPS = [
    "Networkers",
    "Wine Lovers",
    "New Yorkers",
    "Miami Realtors",
    "Clients",
    "Family",
    "Friends",
    "Netflix Recos",
]


SAMPLE_KIN_NOTES_AND_FACTS = [
    {
        "friendly_name": "Foxy",
        "first_name": "Fletcher",
        "middle_name": "Rodman",
        "last_name": "Fox",
        "profile_pic": "Fox@2x.png",
        "facts": [
            {
                "key": "Phone",
                "value": "(646) 555-1212",
                "fact_type": "PHONE",
                "metadata": {"phone_primary": True, "phone_accepts_sms": True},
            },
            {
                "key": "Email",
                "value": "frf@example.com",
                "fact_type": "EMAIL",
                "metadata": {"email_primary": True},
            },
            {
                "key": "Birthday",
                "value": (timezone.now() + timedelta(days=3)).strftime("%Y-%m-%d"),
                "fact_type": "DATE",
                "metadata": {"date_notify": True},
            },
            {
                "key": "Introduced By",
                "value": "Lilian Benson",
                "fact_type": "CUSTOM",
                "metadata": {},
            },
            {
                "key": "LinkedIn",
                "value": "www.linkedin.com/in/fletcherfox",
                "fact_type": "SOCIAL_PROFILE",
                "metadata": {},
            },
            {"key": "Favorite Wine", "value": "Pauillac", "fact_type": "CUSTOM", "metadata": {}},
            {
                "key": "Kid's Names",
                "value": "Lisa, Angela, Pamela, Renee",
                "fact_type": "CUSTOM",
                "metadata": {},
            },
            {"key": "Pets", "value": "Duke, Badger", "fact_type": "CUSTOM", "metadata": {}},
        ],
        "notes": ["Foxy's door code is 2112", "Lovely sunset, send photos to Foxy."],
    },
    {
        "friendly_name": "Horse",
        "first_name": "Horst",
        "middle_name": "P.",
        "last_name": "Horse",
        "profile_pic": "Horse@2x.png",
        "facts": [
            {
                "key": "Phone",
                "value": "(646) 555-5012",
                "fact_type": "PHONE",
                "metadata": {"phone_primary": True, "phone_accepts_sms": True},
            },
            {
                "key": "Email",
                "value": "hph@example.com",
                "fact_type": "EMAIL",
                "metadata": {"email_primary": True},
            },
            {
                "key": "Birthday",
                "value": (timezone.now() + timedelta(days=7)).strftime("%Y-%m-%d"),
                "fact_type": "DATE",
                "metadata": {"date_notify": True},
            },
            {"key": "Introduced By", "value": "Foxy", "fact_type": "CUSTOM", "metadata": {}},
            {
                "key": "LinkedIn",
                "value": "www.linkedin.com/in/hphorse",
                "fact_type": "SOCIAL_PROFILE",
                "metadata": {},
            },
            {
                "key": "Favorite Beer",
                "value": "Imperial IPA",
                "fact_type": "CUSTOM",
                "metadata": {},
            },
            {
                "key": "Move in date",
                "value": "9999-03-21",
                "fact_type": "DATE",
                "metadata": {"date_notify": True},
            },
            {"key": "Wife", "value": "Fiona", "fact_type": "CUSTOM", "metadata": {}},
        ],
        "notes": [
            "Selling his wife's house in Mayfair.",
            "Lucky number 15 and favorite color yellow.",
        ],
    },
    {
        "friendly_name": "Birdie",
        "first_name": "Sandra",
        "middle_name": "",
        "last_name": "Pruitt",
        "profile_pic": "Chicken@2x.png",
        "facts": [
            {
                "key": "Phone",
                "value": "(646) 555-9191",
                "fact_type": "PHONE",
                "metadata": {"phone_primary": True, "phone_accepts_sms": True},
            },
            {
                "key": "Email",
                "value": "spsp@example.com",
                "fact_type": "EMAIL",
                "metadata": {"email_primary": True},
            },
            {
                "key": "Birthday",
                "value": (timezone.now() + timedelta(days=125)).strftime("%Y-%m-%d"),
                "fact_type": "DATE",
                "metadata": {"date_notify": True},
            },
            {
                "key": "LinkedIn",
                "value": "www.linkedin.com/in/sandybirdie",
                "fact_type": "SOCIAL_PROFILE",
                "metadata": {},
            },
            {"key": "Allergy", "value": "nuts, gluten", "fact_type": "CUSTOM", "metadata": {}},
            {
                "key": "Kids' names",
                "value": "Bernice, Justin",
                "fact_type": "CUSTOM",
                "metadata": {},
            },
            {"key": "Pets", "value": "Duffy, Gabby", "fact_type": "CUSTOM", "metadata": {}},
            {"key": "Husband", "value": "William", "fact_type": "CUSTOM", "metadata": {}},
        ],
        "notes": [
            "She and her partner Derek are starting a baby toy company based in Sweden.",
            "Refrigerator model Samsung RF263BEAESR - set Amazon order for water filter",
        ],
    },
    {
        "friendly_name": "Porky",
        "first_name": "George",
        "middle_name": "Napoleon",
        "last_name": "Pig",
        "profile_pic": "Pig@2x.png",
        "facts": [
            {
                "key": "Phone",
                "value": "(646) 555-5588",
                "fact_type": "PHONE",
                "metadata": {"phone_primary": True, "phone_accepts_sms": True},
            },
            {
                "key": "Email",
                "value": "gnp@example.com",
                "fact_type": "EMAIL",
                "metadata": {"email_primary": True},
            },
            {
                "key": "Birthday",
                "value": (timezone.now() + timedelta(days=135)).strftime("%Y-%m-%d"),
                "fact_type": "DATE",
                "metadata": {"date_notify": True},
            },
            {
                "key": "Wife's Birthday",
                "value": (timezone.now() + timedelta(days=36)).strftime("%Y-%m-%d"),
                "fact_type": "DATE",
                "metadata": {"date_notify": True},
            },
            {
                "key": "LinkedIn",
                "value": "www.linkedin.com/in/gnpporky",
                "fact_type": "SOCIAL_PROFILE",
                "metadata": {},
            },
            {"key": "Dislikes", "value": "cats, olives", "fact_type": "CUSTOM", "metadata": {}},
            {"key": "Daughter", "value": "Josephine", "fact_type": "CUSTOM", "metadata": {}},
            {"key": "Wife", "value": "Olivia", "fact_type": "CUSTOM", "metadata": {}},
        ],
        "notes": ["Will visit NYC to see apartments soon."],
    },
]
