from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from elasticsearch_dsl import FacetedSearch, TermsFacet, RangeFacet

from apps.kin.models import Kin
from apps.search.utils import (
    ImgUrlField,
    TermSetFacet,
    search_date_ranges,
    location_properties,
    prepare_location_city_country,
    trigram_analyzer,
)


@registry.register_document
class KinDocument(Document):
    id = fields.TextField()
    owner = fields.ObjectField(
        properties={"id": fields.KeywordField(), "email": fields.TextField()}
    )
    location = fields.NestedField(properties=location_properties)
    city_country = fields.KeywordField()
    profile_pic = ImgUrlField()
    # I may want to add the trigram analyzer to the name fields, but partial searching is working for now
    groups = fields.ListField(fields.KeywordField())
    created = fields.DateField()
    friendly_name = fields.TextField(analyzer=trigram_analyzer)
    first_name = fields.TextField(analyzer=trigram_analyzer)
    last_name = fields.TextField(analyzer=trigram_analyzer)

    class Index:
        name = "kin"
        settings = {"number_of_shards": 1, "number_of_replicas": 0}

    class Django:
        model = Kin

        fields = [
            "middle_name",
            "suffix",
            "source_id",
            "modified",
        ]

    def prepare_groups(self, instance):
        return list(instance.groups.values_list("name", flat=True))

    def prepare_city_country(self, instance):
        return prepare_location_city_country(instance)


class KinFacetedSearch(FacetedSearch):
    index = "kin"
    doc_types = [KinDocument]
    fields = ["first_name", "last_name", "friendly_name"]

    facets = {
        "groups": TermSetFacet(field="groups"),
        "location": TermsFacet(field="city_country"),
        "created": RangeFacet(
            field="created",
            ranges=search_date_ranges,
        ),
    }

    def __init__(self, query=None, filters={}, sort=(), user=None):
        self._model = Kin
        self._user = user
        super().__init__(query, filters, sort)

    def search(self):
        s = super().search()
        return s.filter("term", owner__id=self._user.id)

    def highlight(self, search):
        """
        Add highlighting for all the fields
        """
        return search.highlight(
            *(f if "^" not in f else f.split("^", 1)[0] for f in self.fields),
            pre_tags=["|"],
            post_tags=["|"]
        )
