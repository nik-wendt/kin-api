from enum import Enum


class StatusSentimentEnum(Enum):
    POSITIVE = "POSITIVE"
    NEGATIVE = "NEGATIVE"
    NEUTRAL = "NEUTRAL"


class KinSourceEnum(Enum):
    NYLAS = "NYLAS"
    APP = "APP"
    IOS = "IOS"
    ANDROID = "ANDROID"
    CSV = "CSV"
    SYSTEM = "SYSTEM"
    SALESFORCE = "SALESFORCE"


class ShareContentTypeEnum(Enum):
    BASIC = "BASIC"
    EVERYTHING = "EVERYTHING"
    CUSTOM = "CUSTOM"
