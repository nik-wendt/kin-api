import json
import logging

from django.core.management.base import BaseCommand

from apps.facts.models import Fact
from apps.kin.models import Kin
from apps.notes.models import Note

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = """takes a user ids as an arguments and returns a json of their kin for sharing.
    --users = takes a list of users
    """

    def add_arguments(self, parser):
        parser.add_argument("--users", nargs="+", type=str)
        parser.add_argument("others", nargs="+", type=str)

    def handle(self, *args, **options):
        kin_list = []
        kin_fields_enum = ["first_name", "middle_name", "suffix", "last_name"]
        for user_id in options["users"]:
            # the plural of kin is kin... that's annoying
            kins = Kin.objects.filter(owner_id=user_id)
            for kin in kins:
                if kin.first_name and kin.last_name:
                    share_name = f"{kin.first_name} {kin.last_name}"
                else:
                    share_name = kin.friendly_name
                fact_ids = list(Fact.objects.filter(kin=kin).values_list("pk", flat=True))
                note_ids = list(Note.objects.filter(kin=kin).values_list("pk", flat=True))
                kin_list.append(
                    {
                        "kin": kin.id,
                        "share_data": {
                            "share_name": share_name,
                            "kin_fields": kin_fields_enum,
                            "fact_ids": fact_ids,
                            "note_ids": note_ids,
                        },
                    }
                )
            logger.info(f"Outputting list of kin for user {user_id}:")
            logger.info(json.dumps(kin_list))
            logger.info("DONE")
