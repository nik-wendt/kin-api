import base64
import json
import logging
from datetime import timedelta

from django.conf import settings
from django.core.files.storage import default_storage
from django.db.models import ProtectedError, Count, Q
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.utils import timezone
from rest_framework import permissions, serializers, status, viewsets, mixins
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response


from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
import magic
import qrcode
import qrcode.image.svg
import redis
import shortuuid

from apps.groups.models import Group
from apps.integrations.mixpanel.tasks import mixpanel_shared_kin
from apps.kin.choices import KinSourceEnum
from apps.kin.filters import KinFilter
from apps.kin.helpers import get_all_kin, remove_sample_data
from apps.kin.models import Kin, UserKin, UserKinShare
from apps.kin.serializers import (
    AllKinResultsSerializer,
    generate_kin_share_dict,
    KinBulkDeleteSerializer,
    KinBulkShareURLGenerationSerializer,
    KinImportSerializer,
    KinListSerializer,
    KinMergeSerializer,
    KinProfilPicSerializer,
    KinSerializer,
    KinShareURLGenerationSerializer,
    KinShareURLSerializer,
    KinSourceIDQuerySerializer,
    KinTokenBulkImportResultSerializer,
    KinTokenImportSerializer,
    UserKinShareSerializer,
    UserKinShareBulkDeleteSerializer,
    KinFrequentSerializer,
)
from apps.kin.tasks import bulk_import
from apps.kin_auth.permissions import IsPaid, IsVerified
from apps.kin_extensions.renderers import vCardRenderer, QRCodeRenderer

logger = logging.getLogger(__name__)


class KinViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    filterset_class = KinFilter
    renderer_classes = (
        JSONRenderer,
        vCardRenderer,
        QRCodeRenderer,
    )

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Kin.objects.none()

        qs = Kin.objects.filter(owner=self.request.user)

        if not self.action in [
            "share_kin",
            "retrieve",
            "update",
            "partial_update",
            "upload_profile_pic",
        ]:
            user_kin_kin_ids = UserKin.objects.all().values_list("kin__id", flat=True)
            qs = qs.exclude(id__in=user_kin_kin_ids)

        if self.action == "retrieve":
            qs = qs.prefetch_related("groups", "facts", "notes")

        return qs

    def get_serializer_class(self):
        if self.action == "list":
            return KinListSerializer
        elif self.action == "all":
            return AllKinResultsSerializer
        else:
            return KinSerializer

    def create(self, request, *args, **kwargs):
        resp = super().create(request, *args, **kwargs)
        remove_sample_data(request.user)
        return resp

    @swagger_auto_schema(responses={status.HTTP_200_OK: AllKinResultsSerializer})
    @action(detail=False, methods=["GET"], pagination_class=None, filterset_class=None)
    def all(self, request, *args, **kwargs):
        kin_results = get_all_kin(request.user)
        return Response(kin_results)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    @swagger_auto_schema(
        request_body=KinMergeSerializer,
        responses={status.HTTP_200_OK: KinSerializer},
        operation_id="kin_merge",
    )
    @action(detail=False, methods=["post"], url_path="merge")
    def merge_kin(self, request):
        serializer = KinMergeSerializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        kin = serializer.save()
        kin_serializer = KinSerializer(kin)
        return Response(kin_serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        method="POST",
        request_body=KinProfilPicSerializer,
        responses={status.HTTP_200_OK: KinSerializer},
        operation_id="kin_create_profile_pic",
    )
    @swagger_auto_schema(
        method="DELETE",
        responses={status.HTTP_200_OK: KinSerializer},
        operation_id="kin_delete_profile_pic",
    )
    @action(
        detail=True,
        methods=["POST", "DELETE"],
        url_path="profile-pic",
        parser_classes=(FormParser, MultiPartParser),
    )
    def upload_profile_pic(self, request, pk=None):
        instance = self.get_object()

        keep_file = f"/profile_pics/owner-{instance.owner.id}/{instance.id}/.keep"  # Used to check if kin has ever been assigned a profile pic for tracking sync/delete
        if not default_storage.exists(keep_file):
            file = default_storage.open(keep_file, "w")
            file.write(" ")
            file.close()

        if request.method == "POST":
            instance.profile_pic = request.data["profile_pic"]
            instance.save()
        else:
            instance.profile_pic.delete()

        serializer = KinSerializer(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def perform_destroy(self, instance):
        try:
            instance.delete()
        except ProtectedError:
            raise serializers.ValidationError("Unable to delete a user's own kin.")

    @swagger_auto_schema(
        request_body=KinBulkDeleteSerializer,
        responses={status.HTTP_204_NO_CONTENT: []},
        operation_id="kin_bulk_delete",
    )
    @action(detail=False, methods=["DELETE"], url_path="bulk-delete")
    def bulk_destroy(self, request):
        serializer = KinBulkDeleteSerializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)

        if serializer.validated_data.get("kin_ids", None):
            kin = self.get_queryset().filter(pk__in=serializer.validated_data["kin_ids"])
        else:
            kin = self.get_queryset().filter(source=serializer.validated_data["source"])

        try:
            kin.delete()
        except ProtectedError:
            raise serializers.ValidationError("Unable to delete a user's own kin.")

        return Response(status=status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(
        request_body=KinImportSerializer(many=True),
        responses={status.HTTP_200_OK: serializers.Serializer()},
        operation_id="kin_import",
    )
    @action(detail=False, methods=["POST"], url_path="import")
    def bulk_import(self, request):
        serializer = KinImportSerializer(data=request.data, many=True)

        if serializer.is_valid(raise_exception=False):
            bulk_import.delay(request.data, request.user.id)

            return Response(status=status.HTTP_200_OK)
        else:
            logger.info("Error importing kin: %s", serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(
        request_body=KinTokenImportSerializer,
        responses={
            status.HTTP_201_CREATED: KinSerializer,
            status.HTTP_200_OK: KinTokenBulkImportResultSerializer,
        },
        operation_id="kin_token_import",
    )
    @action(detail=False, methods=["POST"], url_path="import/token")
    def token_import(self, request):
        serializer = KinTokenImportSerializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        kin = serializer.save()

        # This used to have an if else for when there was only 1 kin to go through the KinSerializer instead.
        output_serializer = KinTokenBulkImportResultSerializer(
            {
                "count": len(kin),
                "group": Group.objects.filter(
                    name=serializer.validated_data["token"]["group_name"], owner=request.user
                ).first(),
                "kin": kin,
                "sharer_name": serializer.validated_data["token"]["sharer_name"],
            }
        )
        status_code = status.HTTP_200_OK

        return Response(output_serializer.data, status=status_code)

    @swagger_auto_schema(
        method="POST",
        request_body=KinShareURLGenerationSerializer(),
        responses={status.HTTP_200_OK: KinShareURLSerializer},
        operation_id="kin_share",
    )
    @action(detail=True, methods=["POST"], url_path="share")
    def share_kin(self, request, pk=None):
        instance = self.get_object()
        serializer = KinShareURLGenerationSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        renderer_format = request.accepted_renderer.format

        share_dict = generate_kin_share_dict(
            instance,
            serializer.validated_data["kin_fields"],
            serializer.validated_data["share_name"],
            serializer.validated_data["fact_ids"],
            serializer.validated_data["note_ids"],
            serializer.validated_data.get("template_name", None),
        )

        token = shortuuid.uuid()
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.setex(
            token,
            60 * 60 * 24 * 7,
            json.dumps(
                {
                    "group_name": "",
                    "sharer_name": f"{request.user.first_name} {request.user.last_name}",
                    "kin": [share_dict],
                }
            ),
        )  # good for 1 week

        if renderer_format == "qrcode":
            qr = qrcode.QRCode(
                version=1, error_correction=qrcode.constants.ERROR_CORRECT_L, box_size=10, border=4
            )
            qr.add_data(token)
            img = qr.make_image(fill_color="#2F2B73", back_color="white")
            resp = HttpResponse(content_type="image/png")
            img.save(resp, "PNG")
            return resp

        output_serializer = KinShareURLSerializer(data={"token": token})
        output_serializer.is_valid(raise_exception=True)

        is_user_kin = UserKin.objects.filter(kin__id=instance.id).exists()
        number_details_shared = len(serializer.validated_data["fact_ids"])
        number_notes_shared = len(serializer.validated_data["note_ids"])

        content_type = serializer.validated_data.get("content_type", None)

        mixpanel_shared_kin.delay(
            instance.id,
            is_user_kin,
            renderer_format,
            content_type.value if content_type else None,
            number_details_shared=number_details_shared,
            number_notes_shared=number_notes_shared,
        )

        return Response(output_serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        method="POST",
        request_body=KinBulkShareURLGenerationSerializer,
        responses={status.HTTP_200_OK: KinShareURLSerializer},
        operation_id="kin_bulk_share",
    )
    @action(detail=False, methods=["POST"], url_path="bulk-share")
    def share_bulk_kin(self, request):
        serializer = KinBulkShareURLGenerationSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        renderer_format = request.accepted_renderer.format

        share_dicts = [
            generate_kin_share_dict(
                k["kin"],
                k["share_data"]["kin_fields"],
                k["share_data"]["share_name"],
                k["share_data"]["fact_ids"],
                k["share_data"]["note_ids"],
            )
            for k in serializer.validated_data["kin_list"]
        ]

        token = shortuuid.uuid()
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.setex(
            token,
            60 * 60 * 24 * 7,
            json.dumps(
                {
                    "group_name": serializer.validated_data.get("group_name", ""),
                    "kin": share_dicts,
                    "sharer_name": f"{request.user.first_name} {request.user.last_name}",
                }
            ),
        )  # good for 1 week

        if renderer_format == "qrcode":
            qr = qrcode.QRCode(
                version=1, error_correction=qrcode.constants.ERROR_CORRECT_L, box_size=10, border=4
            )
            qr.add_data(token)
            img = qr.make_image(fill_color="#2F2B73", back_color="white")
            resp = HttpResponse(content_type="image/png")
            img.save(resp, "PNG")
            return resp

        output_serializer = KinShareURLSerializer(data={"token": token})
        output_serializer.is_valid(raise_exception=True)

        content_type = serializer.validated_data.get("content_type", None)

        mixpanel_shared_kin.delay(
            request.user.id,
            False,
            renderer_format,
            content_type.value if content_type else None,
            number_kin_shared=len(serializer.validated_data["kin_list"]),
        )

        return Response(output_serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        method="get",
        manual_parameters=[
            openapi.Parameter(
                "format",
                openapi.IN_QUERY,
                description="set to 'json' or 'vcard'",
                type=openapi.TYPE_STRING,
            ),
            openapi.Parameter(
                "token",
                openapi.IN_QUERY,
                description="the kin share token",
                type=openapi.TYPE_STRING,
            ),
        ],
        responses={200: {}},
    )
    @action(
        detail=False,
        methods=["GET"],
        url_path="vcard",
        permission_classes=[],
    )
    def token_vcard_exchange(self, request):
        raw_token = request.query_params.get("token")

        serializer = KinTokenImportSerializer(
            data={"token": raw_token}, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)

        profile_pic_base64 = ""
        if len(serializer.validated_data["token"]["kin"]) == 1:
            if serializer.validated_data["token"]["kin"][0].get("profile_pic", ""):
                file = default_storage.open(
                    serializer.validated_data["token"]["kin"][0]["profile_pic"], "rb"
                )
                mime_type = magic.from_buffer(file.read(), mime=True)
                file.seek(0)
                profile_pic_base64 = base64.b64encode(file.read()).decode("utf-8")
                file.close()
                serializer.validated_data["token"]["kin"][0]["profile_pic"] = {
                    "base64": profile_pic_base64,
                    "mime_type": mime_type,
                }

        if request.accepted_renderer.format == "vcard":
            vcard_content = render_to_string(
                "vcard.vcf", {"kin": serializer.validated_data["token"]["kin"]}
            )
            filename = f"{raw_token}.vcf"
            response = HttpResponse(vcard_content, content_type="text/x-vcard")
            response["Content-Disposition"] = "attachment; filename={0}".format(filename)
            return response

        json_response = {
            "kin": [
                {
                    "friendly_name": k.get("friendly_name", ""),
                    "first_name": k.get("first_name", ""),
                    "last_name": k.get("last_name", ""),
                    "profile_pic": f"data:{mime_type};base64,{profile_pic_base64}"
                    if profile_pic_base64
                    else "",
                }
                for k in serializer.validated_data["token"]["kin"]
            ],
            "vcard": f"{request.build_absolute_uri(request.path)}?format=vcard&token={raw_token}",
        }

        return Response(json_response)


class KinFrequentConnectionsViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = KinFrequentSerializer
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)

    def get_queryset(self):
        note_score = Count(
            "notes",
            distinct=True,
            filter=Q(notes__modified__gte=(timezone.now() - timedelta(days=30))),
        )
        connection_score = Count(
            "connections",
            distinct=True,
            filter=Q(connections__connection_date__gte=(timezone.now() - timedelta(days=30))),
        )

        qs = (
            Kin.objects.filter(owner=self.request.user)
            .exclude(userkin__isnull=False)
            .annotate(note_score=note_score)
            .annotate(connection_score=connection_score)
            .annotate(total_score=connection_score + note_score)
            .order_by("-total_score")
        )
        return qs


class KinSourceIDListView(GenericAPIView):
    """
    Returns an array of source ids filtered by a provided source.
    """

    pagination_class = None

    @swagger_auto_schema(query_serializer=KinSourceIDQuerySerializer, operation_id="kin_source_ids")
    def get(self, request):
        try:
            source = KinSourceEnum[request.GET.get("source")]
        except KeyError:
            raise serializers.ValidationError("Invalid source.")
        source_ids = Kin.objects.filter(
            source=source, owner=request.user, userkin=None
        ).values_list("source_id", flat=True)
        return Response(source_ids, status=status.HTTP_200_OK)


class UserKinShareViewset(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = UserKinShareSerializer

    def get_queryset(self):
        return UserKinShare.objects.filter(owner=self.request.user).order_by("-created")

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.owner = self.request.user
        serializer.is_valid(raise_exception=True)
        serializer.persist_token(serializer.validated_data["token"])
        serializer.save(owner=self.request.user)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @swagger_auto_schema(
        request_body=UserKinShareBulkDeleteSerializer,
        responses={status.HTTP_204_NO_CONTENT: []},
        operation_id="kin_template_bulk_delete",
    )
    @action(detail=False, methods=["DELETE"], url_path="bulk-delete")
    def bulk_destroy(self, request):
        serializer = UserKinShareBulkDeleteSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)

        connections = self.get_queryset().filter(pk__in=serializer.validated_data["template_ids"])
        connections.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
