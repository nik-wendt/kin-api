import logging

from celery import shared_task
from django.db import transaction
from django.utils import timezone
from rest_framework import status

import shortuuid

from apps.facts.choices import FactTypeEnum
from apps.facts.models import Fact
from apps.kin.helpers import _get_and_cache_all_kin_serialized, remove_sample_data
from apps.kin.models import Kin, UserKinShare
from apps.kin.serializers import KinImportSerializer
from apps.notifications.utils import get_pusher_client, send_notification
from apps.spotlight.tasks import (
    update_recent_kin_cache_task,
    update_popular_kin_cache_task,
    update_special_occasions_cache_task,
)
from apps.users.models import User


logger = logging.getLogger(__name__)


@shared_task
def bulk_import(data, user_id):
    new_kin = []
    new_facts = []

    try:
        user = User.objects.get(pk=user_id)

        serializer = KinImportSerializer(data=data, many=True)
        serializer.is_valid(raise_exception=True)

        with transaction.atomic():
            for k in serializer.validated_data:
                primary_phone_set = False
                primary_email_set = False

                kin_id = shortuuid.uuid()
                k["id"] = kin_id
                k["owner_id"] = user_id
                facts_data = k.pop("facts", [])

                new_kin.append(Kin(**k))

                for f in facts_data:
                    fact_id = shortuuid.uuid()
                    f["id"] = fact_id
                    f["kin_id"] = kin_id

                    try:
                        f["fact_type"] = FactTypeEnum[f["fact_type"]]
                    except KeyError:
                        f["fact_type"] = FactTypeEnum["CUSTOM"]

                    if f["fact_type"] == FactTypeEnum.PHONE:
                        f["metadata"] = {
                            "phone_primary": not primary_phone_set,  # if the kin already has a primary phone, set this to false
                            "phone_accepts_sms": False,
                        }
                        primary_phone_set = True
                    elif f["fact_type"] == FactTypeEnum.EMAIL:
                        f["metadata"] = {
                            "email_primary": not primary_email_set  # if the kin already has a primary email, set this to false
                        }
                        primary_email_set = True
                    elif f["fact_type"] == FactTypeEnum.DATE:
                        f["metadata"] = {"date_notify": True}
                    else:
                        f["metadata"] = {}

                    new_facts.append(Fact(**f))

            remove_sample_data(user)
            Kin.objects.bulk_create(new_kin)
            Fact.objects.bulk_create(new_facts)

        logger.info(
            "Successfully imported %s kin for user %s",
            len(new_kin),
            user_id,
            extra={"user_id": user_id},
        )

        update_all_kin_cache_task.delay(user_id)
        update_recent_kin_cache_task.delay(user_id)
        update_popular_kin_cache_task.delay(user_id)
        update_special_occasions_cache_task.delay(user_id)

        pc = get_pusher_client()
        pc.trigger(
            f"private-encrypted-{user.id}",
            "kin-import",
            {
                "status_code": status.HTTP_200_OK,
                "message": f"Successfully imported {len(new_kin)} kin.",
                "timestamp": str(timezone.now()),
            },
        )
    except Exception as e:
        send_notification("There was an error importing your contacts. Please try again.", user_id)

        logger.error(
            "Error importing kin for user %s. %s", user_id, str(e), extra={"user_id": user_id}
        )

        pc = get_pusher_client()
        pc.trigger(
            f"private-encrypted-{user.id}",
            "kin-import",
            {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": "",
                "timestamp": str(timezone.now()),
            },
        )


@shared_task
def update_all_kin_cache_task(user_id):
    try:
        user = User.objects.get(pk=user_id)
        _ = _get_and_cache_all_kin_serialized(user)
    except User.DoesNotExist:
        logger.info("Failed to update_all_kin_cache. No such user: %s", user_id)


@shared_task
def delete_userkin_templates(user_id):
    UserKinShare.objects.filter(owner_id=user_id).delete()
