import re
import string
from functools import reduce
from operator import and_


from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import Count, Q
from django_enum_choices.serializers import EnumChoiceField
from rest_framework import serializers


from apps.facts.documents import FactFacetedSearch, FactKeyFacetedSearch
from apps.facts.models import Fact
from apps.groups.documents import GroupFacetedSearch
from apps.groups.models import Group
from apps.kin.documents import KinFacetedSearch
from apps.kin.models import Kin
from apps.notes.documents import NotesFacetedSearch
from apps.notes.models import Note
from apps.search.choices import OnlySearchChoices, CreatedRangeChoices
from apps.search.utils import to_queryset


class ESSearchQuerySerializer(serializers.Serializer):
    query = serializers.CharField(required=False)
    only_search = EnumChoiceField(
        OnlySearchChoices,
        required=False,
        help_text="accepts: 'facts', 'fact_keys', 'groups', 'kin', 'notes', and will only return those results.",
    )
    created = EnumChoiceField(
        CreatedRangeChoices,
        required=False,
    )

    def _search_object(self, faceted_search_class, *, query, user, filters):
        # Make sure we don't break each search with facets from other objects.
        filters = {k: v for k, v in filters.items() if k in faceted_search_class.facets}

        fs = faceted_search_class(query=query, user=user, filters=filters)
        total = fs.count()
        response = fs[0:total].execute()
        facets = response.facets
        return response, facets

    def _search_kin_friendly_name(self, **kwargs):
        kin_response, facets = self._search_object(KinFacetedSearch, **kwargs)
        return to_queryset(kin_response), facets

    def _search_kin(self, **kwargs):
        kin_response, facets = self._search_object(KinFacetedSearch, **kwargs)
        return to_queryset(kin_response), facets

    def _search_groups(self, **kwargs):
        group_response, facets = self._search_object(GroupFacetedSearch, **kwargs)
        groups = to_queryset(group_response)
        return groups.annotate(count=Count("kin")), facets

    def _search_facts(self, **kwargs):
        facts_response, facets = self._search_object(FactFacetedSearch, **kwargs)
        facts = to_queryset(facts_response)
        return facts.exclude(kin__userkin__user=kwargs["user"]), facets

    def _search_fact_keys(self, **kwargs):
        fact_keys_response, facets = self._search_object(FactKeyFacetedSearch, **kwargs)
        fact_keys = to_queryset(fact_keys_response)
        fact_keys = (
            fact_keys.exclude(kin__userkin__user=kwargs["user"])
            .values("key", "score", "highlight")
            .annotate(count=Count("kin", distinct=True))
            .annotate(kin=ArrayAgg("kin_id", distinct=True))
            .order_by()
        )

        return fact_keys, facets

    def _search_notes(self, **kwargs):
        notes_response, facets = self._search_object(NotesFacetedSearch, **kwargs)
        return to_queryset(notes_response), facets

    def search(self, only_search="", filters={}):
        query = self.validated_data.get("query", "")
        user = self._context["request"].user

        response = {
            "kin": Kin.objects.none(),
            "groups": Group.objects.none(),
            "facts": Fact.objects.none(),
            "fact_keys": Fact.objects.none(),
            "notes": Note.objects.none(),
            "facets": {},
        }

        # Allow blanks to go through for filtered searches.
        if 1 < len(query) < 3:
            kin_results, facets = self._search_kin_friendly_name(
                query=query, user=user, filters=filters
            )
            response["kin"] = kin_results
            response["facets"]["kin"] = facets
            return response

        funcs = {
            "kin": self._search_kin,
            "groups": self._search_groups,
            "facts": self._search_facts,
            "fact_keys": self._search_fact_keys,
            "notes": self._search_notes,
        }

        if not only_search:
            for k, v in funcs.items():
                results, facets = v(query=query, user=user, filters=filters)
                response[k] = results
                response["facets"][k] = facets
                if response["facets"][k].created:
                    response["facets"][k]["created"].reverse()
        else:
            results, facets = funcs[only_search](query=query, user=user, filters=filters)
            response[only_search] = results
            response["facets"][only_search] = facets
            if response["facets"][only_search].created:
                response["facets"][only_search]["created"].reverse()

        return response

    def facets(self):
        user = self._context["request"].user
        funcs = {
            "kin": KinFacetedSearch,
            "groups": GroupFacetedSearch,
            "facts": FactFacetedSearch,
            "fact_keys": FactKeyFacetedSearch,
            "notes": NotesFacetedSearch,
        }

        response = {"facets": {}}
        for object_name, faceted_search_class in funcs.items():
            _, facets = self._search_object(faceted_search_class, query="", user=user, filters={})
            response["facets"][object_name] = facets.to_dict()
            if response["facets"][object_name].get("created"):
                response["facets"][object_name]["created"].reverse()
        return response


class KinSearchResultsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kin
        fields = ("id", "friendly_name", "first_name", "last_name", "profile_pic")
        read_only_fields = ("id", "friendly_name", "first_name", "last_name", "profile_pic")


class GroupSearchResultsSerializer(serializers.ModelSerializer):
    count = serializers.IntegerField()

    class Meta:
        model = Group
        fields = ("id", "name", "count")
        read_only_fields = ("id", "name", "count")


class InlineKinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kin
        fields = ("id", "friendly_name", "profile_pic")
        read_only_fields = ("id", "friendly_name", "profile_pic")


class FactKeyKinInlineSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        # TODO: this feels jank. Check with Glenn for better way. This just works so I'm leaving it like this to unblock scott
        instances = Kin.objects.filter(id__in=instance)
        return InlineKinSerializer(instances, many=True).data

    class Meta:
        model = Kin
        fields = ("id", "friendly_name", "profile_pic")
        read_only_fields = ("id", "friendly_name", "profile_pic")


class FactSearchResultsSerializer(serializers.ModelSerializer):
    kin = InlineKinSerializer()

    class Meta:
        model = Fact
        fields = ("id", "key", "value", "kin")
        read_only_fields = ("id", "key", "value", "kin")


class FactKeySearchResultsSerializer(serializers.ModelSerializer):
    kin_ids = serializers.ListField()
    count = serializers.IntegerField()

    class Meta:
        model = Fact
        fields = ("key", "kin_ids", "count")
        read_only_fields = ("key", "kin_ids", "count")


class ESKinSearchResultsSerializer(serializers.ModelSerializer):
    score = serializers.FloatField()
    highlight = serializers.DictField()

    class Meta:
        model = Kin
        fields = (
            "id",
            "friendly_name",
            "first_name",
            "last_name",
            "profile_pic",
            "score",
            "highlight",
        )
        read_only_fields = (
            "id",
            "friendly_name",
            "first_name",
            "last_name",
            "profile_pic",
            "score",
            "highlight",
        )


class ESFactSearchResultsSerializer(serializers.ModelSerializer):
    kin = InlineKinSerializer()
    score = serializers.FloatField()
    highlight = serializers.DictField()

    class Meta:
        model = Fact
        fields = ("id", "key", "value", "kin", "score", "highlight")
        read_only_fields = ("id", "key", "value", "kin", "score", "highlight")


class ESFactKeySearchResultsSerializer(serializers.ModelSerializer):
    kin = FactKeyKinInlineSerializer()
    count = serializers.IntegerField()
    score = serializers.FloatField()
    highlight = serializers.DictField()

    class Meta:
        model = Fact
        fields = ("key", "kin", "count", "score", "highlight")
        read_only_fields = ("key", "kin", "count", "score", "highlight")


class ESNoteSearchResultsSerializer(serializers.ModelSerializer):
    kin = InlineKinSerializer()
    result_text = serializers.SerializerMethodField()
    score = serializers.FloatField()
    highlight = serializers.DictField()

    class Meta:
        model = Note
        fields = ("id", "kin", "result_text", "score", "highlight")
        read_only_fields = ("id", "kin", "result_text", "score", "highlight")

    def get_result_text(self, obj):
        """
        Limits the nugget search result text to
        a subset of text surrounding the matching
        query.
        """
        # TODO: make it match the comment above. Currently only getting a left substring.

        return f"{obj.text[:30]}..."


class ESFacetsResultsSerializer(serializers.Serializer):
    facets = serializers.SerializerMethodField()

    def get_facets(self, obj):
        return obj


class ESGroupSearchResultsSerializer(serializers.ModelSerializer):
    count = serializers.IntegerField()
    score = serializers.FloatField()
    highlight = serializers.DictField()

    class Meta:
        model = Group
        fields = ("id", "name", "count", "score", "highlight")
        read_only_fields = ("id", "name", "count", "score", "highlight")


class NoteSearchResultsSerializer(serializers.ModelSerializer):
    kin = InlineKinSerializer()
    result_text = serializers.SerializerMethodField()

    class Meta:
        model = Note
        fields = ("id", "kin", "result_text")
        read_only_fields = ("id", "kin", "result_text")

    def get_result_text(self, obj):
        """
        Limits the nugget search result text to
        a subset of text surrounding the matching
        query.
        """
        query = self.context["query"]
        words = list(filter(None, obj.text.lower().split(" ")))

        if len(query.split(" ")) == 1:
            for x in words:
                if re.search(self.context["query"].lower(), x, re.IGNORECASE):
                    full_word_query_match = x
        else:
            for x in words:
                if re.search(self.context["query"][0].lower(), x, re.IGNORECASE):
                    full_word_query_match = x

        query_match_index = words.index(full_word_query_match)
        match = ""

        if query_match_index == 0:
            match = f"{' '.join(obj.text.split(' ')[0:3])}..."
        elif query_match_index == len(words) - 1:
            match = (
                f"...{' '.join(obj.text.split(' ')[query_match_index - 2:query_match_index + 1])}"
            )
        else:
            match = (
                f"...{' '.join(obj.text.split(' ')[query_match_index : query_match_index +4])}..."
            )

        return match


class SearchResultsSerializer(serializers.Serializer):
    kin = KinSearchResultsSerializer(many=True)
    groups = GroupSearchResultsSerializer(many=True)
    facts = FactSearchResultsSerializer(many=True)
    fact_keys = FactKeySearchResultsSerializer(many=True)
    notes = NoteSearchResultsSerializer(many=True)


class SearchQuerySerializer(serializers.Serializer):
    query = serializers.CharField(required=False)

    def _search_kin_friendly_name(self, query, user):
        kin = Kin.objects.filter(friendly_name__istartswith=query, owner=user).exclude(
            userkin__user=user
        )

        return kin

    def _search_kin(self, query, user):
        kin = Kin.objects.filter(
            Q(first_name__icontains=query)
            | Q(last_name__icontains=query)
            | Q(friendly_name__icontains=query),
            owner=user,
        ).exclude(userkin__user=user)

        return kin

    def _search_groups(self, query, user):
        groups = Group.objects.filter(name__icontains=query, owner=user).annotate(
            count=Count("kin")
        )
        return groups

    def _search_facts(self, query, user):
        facts = Fact.objects.filter(value__icontains=query, kin__owner=user).exclude(
            kin__userkin__user=user
        )
        return facts

    def _search_fact_keys(self, query, user):
        facts = (
            Fact.objects.filter(key__icontains=query, kin__owner=user)
            .exclude(kin__userkin__user=user)
            .values("key")
            .annotate(kin_ids=ArrayAgg("kin_id", distinct=True), count=Count("kin", distinct=True))
            .order_by()
        )
        return facts

    def _search_notes(self, query, user):
        cleaned_query = re.sub(r"[" + string.punctuation + r"][^\s]*\s", " ", query)
        cleaned_query = cleaned_query.split(" ")

        notes_condition = reduce(and_, [Q(text__icontains=string) for string in cleaned_query])
        notes = Note.objects.filter(notes_condition, owner=user)
        return notes

    def search(self):
        query = self.validated_data.get("query", "")
        user = self._context["request"].user

        if not query:
            return {"kin": [], "groups": [], "facts": [], "fact_keys": [], "notes": []}

        if len(query) < 3:
            kin_results = self._search_kin_friendly_name(query, user)
            return {"kin": kin_results, "groups": [], "facts": [], "fact_keys": [], "notes": []}

        kin_results = self._search_kin(query, user)
        group_results = self._search_groups(query, user)
        fact_results = self._search_facts(query, user)
        fact_key_results = self._search_fact_keys(query, user)
        note_results = self._search_notes(query, user)

        return {
            "kin": kin_results[:10],
            "groups": group_results[:10],
            "facts": fact_results[:10],
            "fact_keys": fact_key_results[:10],
            "notes": note_results[:10],
        }
