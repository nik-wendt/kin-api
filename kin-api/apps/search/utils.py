import json

from django.db.models import Case, When, Value
from django.db.models.fields import IntegerField, FloatField
from django.db.models.fields.json import JSONField

from django_elasticsearch_dsl import fields
from elasticsearch_dsl import analyzer, tokenizer, TermsFacet
from elasticsearch_dsl.query import TermsSet

from apps.search.choices import CreatedRangeChoices

trigram_analyzer = analyzer(
    "trigram_analyzer",
    tokenizer=tokenizer("trigram", "ngram", min_gram=3, max_gram=3),
    filter="lowercase",
)


class TermSetFacet(TermsFacet):
    def add_filter(self, filter_values):
        if filter_values:
            params = {
                self._params["field"]: {
                    "terms": filter_values,
                    "minimum_should_match_script": {"source": "params.num_terms"},
                }
            }
            t = TermsSet(_expand__to_dot=False, **params)
            return t


class ImgUrlField(fields.TextField):
    def get_value_from_instance(self, instance, field_value_to_ignore=None):
        img = super().get_value_from_instance(instance, field_value_to_ignore)
        if img:
            return img.url
        return ""


def to_queryset(response):
    """
    Takes an Elasticsearch FacetedResponse and returns a django qs.
    """
    # This might not work if I send a regular search response, but if i'm doing that I probably won't need this func.
    model = response._faceted_search._model
    pks = [hit.id for hit in response.hits]
    meta_dict = [
        {"score": hit.get("_score"), "highlight": hit.get("highlight")}
        for hit in response.hits.hits._l_
    ]
    qs = model.objects.filter(id__in=pks)
    preserved_order = Case(
        *[When(pk=pk, then=pos) for pos, pk in enumerate(pks)], output_field=IntegerField()
    )
    score_cases = Case(
        *[When(pk=pk, then=meta_dict[pos]["score"]) for pos, pk in enumerate(pks)],
        output_field=FloatField(),
    )
    # I'm really not sure about these joins. Seems inelegant and needs testing.
    highlight_cases = Case(
        *[
            When(pk=pk, then=Value(json.dumps(meta_dict[pos]["highlight"])))
            for pos, pk in enumerate(pks)
        ],
        output_field=JSONField(),
    )
    qs = (
        qs.order_by(preserved_order).annotate(score=score_cases).annotate(highlight=highlight_cases)
    )
    return qs


# To be used in a FacetedSearch definition
search_date_ranges = [
    (CreatedRangeChoices.OLDER_THAN_YEAR.value, (None, "now-12M")),
    (CreatedRangeChoices.SIX_MONTHS_TO_YEAR.value, ("now-12M", "now-6M")),
    (CreatedRangeChoices.MONTH_TO_SIX_MONTHS.value, ("now-6M", "now-30d")),
    (CreatedRangeChoices.WEEK_TO_MONTH.value, ("now-30d", "now-7d")),
    (CreatedRangeChoices.WEEK.value, ("now-7d", "now")),
]

# For use in an elastic search Document
location_properties = {
    "street_number": fields.KeywordField(),
    "street_name": fields.KeywordField(),
    "city": fields.KeywordField(),
    "state": fields.KeywordField(),
    "country": fields.KeywordField(),
    "postal_code": fields.KeywordField(),
    "natural_feature": fields.KeywordField(),
}


def prepare_location_city_country(instance):
    if instance.location:
        city = instance.location.get("city")
        country = instance.location.get("country")
        if city and country:
            return f"{city}, {country}"
        elif city or country:
            return f"{city}{country}"
    return None
