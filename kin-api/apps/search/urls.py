from django.urls import path

from apps.search.views import SearchView, ESSearchView, FacetsView

urlpatterns = [
    path("es/", ESSearchView.as_view(), name="es-search"),
    path("facets/", FacetsView.as_view(), name="facets"),
    path("", SearchView.as_view(), name="search"),
]
