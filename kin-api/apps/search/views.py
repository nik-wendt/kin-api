import logging

from rest_framework import permissions, status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from elasticsearch.exceptions import ConnectionError

from apps.kin_auth.permissions import IsPaid, IsVerified
from apps.kin_extensions.pagination import ESPaginator
from apps.search.serializers import (
    SearchResultsSerializer,
    SearchQuerySerializer,
    ESSearchQuerySerializer,
    ESKinSearchResultsSerializer,
    ESGroupSearchResultsSerializer,
    ESFactSearchResultsSerializer,
    ESFactKeySearchResultsSerializer,
    ESNoteSearchResultsSerializer,
    ESFacetsResultsSerializer,
)

logger = logging.getLogger(__name__)


class SearchView(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = SearchQuerySerializer

    @swagger_auto_schema(
        query_serializer=SearchQuerySerializer,
        responses={status.HTTP_200_OK: SearchResultsSerializer},
        operation_id="search",
    )
    def get(self, request):
        serializer = self.get_serializer(data=request.GET)
        serializer.is_valid(raise_exception=True)
        try:
            search_results = serializer.search()
            search_results_serializer = SearchResultsSerializer(
                search_results, context={"query": serializer.validated_data.get("query", "")}
            )
        except ConnectionError as e:
            logger.error(e)
            return Response(
                "There's an issue with search at the moment",
                status=status.HTTP_503_SERVICE_UNAVAILABLE,
            )

        return Response(search_results_serializer.data, status=status.HTTP_200_OK)


class ESSearchView(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = ESSearchQuerySerializer
    pagination_class = ESPaginator

    @swagger_auto_schema(
        query_serializer=ESSearchQuerySerializer,
        responses={status.HTTP_200_OK: SearchResultsSerializer},
        operation_id="search-es",
        manual_parameters=[
            openapi.Parameter(
                "groups",
                openapi.IN_QUERY,
                "Filter based on groups. Only returns matches for all groups.",
                type=openapi.TYPE_ARRAY,
                items=openapi.Items(type=openapi.TYPE_STRING),
                required=False,
                collection_format="multi",
            ),
            openapi.Parameter(
                "location",
                openapi.IN_QUERY,
                "Filter based on city location. Will include results matching ANY location",
                type=openapi.TYPE_ARRAY,
                items=openapi.Items(type=openapi.TYPE_STRING),
                required=False,
                collection_format="multi",
            ),
            openapi.Parameter(
                "created",
                openapi.IN_QUERY,
                "Filter based on creation ranges. Accepts: 'Past Week', '1-week to a Month Ago', '1-month to 6-months Ago', '6-months to a Year Ago', 'Older than a year'",
                type=openapi.TYPE_ARRAY,
                items=openapi.Items(type=openapi.TYPE_STRING),
                required=False,
                collection_format="multi",
            ),
        ],
    )
    def get(self, request):
        serializer = self.get_serializer(data=request.GET)
        serializer.is_valid(raise_exception=True)
        try:
            only_search = request.query_params.get("only_search")

            # Handle query param filters.
            filters = {}
            for key, val in dict(request.query_params).items():
                if key not in ["query", "page", "page_size", "only_search"]:
                    filters[key] = val
            if "groups" in filters:
                only_search = "kin"
            search_results = serializer.search(only_search, filters)
            facets = search_results.pop("facets")
            response_object = {"facets": {}}

            for kin_object, object_facets in facets.items():
                response_object["facets"][kin_object] = {}
                if object_facets:
                    for facet_name, facet in object_facets.to_dict().items():
                        if not response_object["facets"][kin_object].get(facet_name):
                            response_object["facets"][kin_object][facet_name] = []
                        for value, count, is_active in facet:
                            if count > 0:
                                response_object["facets"][kin_object][facet_name].append(
                                    {"value": value, "count": count, "is_active": is_active}
                                )

            for object_name, data in search_results.items():
                page = self.paginator.paginate_queryset(data, request)
                if page is not None:
                    response_serializer = self.get_results_serializer(
                        object_name,
                        page,
                        many=True,
                        context={"query": serializer.validated_data.get("query", "")},
                    )
                    paginated_data = self.paginator.get_paginated_response(
                        response_serializer.data, object_name
                    )
                    response_object[object_name] = paginated_data.data
                else:
                    response_object[object_name] = {}
        except ConnectionError as e:
            logger.error(e)
            return Response(
                "There's an issue with search at the moment",
                status=status.HTTP_503_SERVICE_UNAVAILABLE,
            )

        return Response(response_object, status=status.HTTP_200_OK)

    def get_results_serializer(self, object_name, *args, **kwargs):
        results_serializers = {
            "kin": ESKinSearchResultsSerializer,
            "groups": ESGroupSearchResultsSerializer,
            "facts": ESFactSearchResultsSerializer,
            "fact_keys": ESFactKeySearchResultsSerializer,
            "notes": ESNoteSearchResultsSerializer,
            "facets": ESFacetsResultsSerializer,
        }
        serializer_class = results_serializers[object_name]
        kwargs.setdefault("context", self.get_serializer_context())
        return serializer_class(*args, **kwargs)


class FacetsView(GenericAPIView):
    serializer_class = ESSearchQuerySerializer
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)

    def get(self, request):
        serializer = self.get_serializer(data=request.GET)
        facets_dict = {"kin": {}, "groups": {}, "facts": {}, "fact_keys": {}, "notes": {}}
        serializer_facets = serializer.facets()
        for kin_object, facets_groups in serializer_facets["facets"].items():
            if facets_groups:
                for facet_name, facets in facets_groups.items():
                    facets_dict[kin_object][facet_name] = []
                    for value, count, is_active in facets:
                        if count > 0:
                            facets_dict[kin_object][facet_name].append(
                                {"value": value, "count": count, "is_active": is_active}
                            )
        return Response(facets_dict, status=status.HTTP_200_OK)
