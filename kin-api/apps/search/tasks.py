import logging

from celery import shared_task
from django_elasticsearch_dsl.signals import RealTimeSignalProcessor
from django_elasticsearch_dsl.registries import registry
from django.apps import apps as dapps

logger = logging.getLogger(__name__)


def get_model(app_label, model_name):
    return dapps.get_model(app_label, model_name)


def get_instance(model, instance_pk):
    try:
        instance = model.objects.get(pk=instance_pk)
        return instance
    except Exception as e:
        raise Exception(f"Instance pk = {instance_pk} -- \n {e}")


@shared_task
def handle_save(app_label, model_name, instance_pk):
    model = get_model(app_label, model_name)
    if model in registry.get_models():
        instance = get_instance(model, instance_pk)
        registry.update(instance)
        registry.update_related(instance)


@shared_task
def handle_delete(app_label, model_name, instance_pk):
    model = get_model(app_label, model_name)
    if model in registry.get_models():
        instance = get_instance(app_label, model_name, instance_pk)
        registry.delete(instance, raise_on_error=False)


@shared_task
def handle_pre_delete(app_label, model_name, instance_pk):
    model = get_model(app_label, model_name)
    if model in registry.get_models():
        instance = get_instance(app_label, model_name, instance_pk)
        registry.delete_related(instance)


class CelerySignalProcessor(RealTimeSignalProcessor):
    def handle_m2m_changed(self, sender, instance, action, **kwargs):
        super().handle_m2m_changed(sender, instance, action, **kwargs)

    def handle_save(self, sender, instance, **kwargs):
        handle_save.delay(sender._meta.app_label, sender._meta.object_name, instance.pk)

    def handle_delete(self, sender, instance, **kwargs):
        handle_delete.delay(sender._meta.app_label, sender._meta.object_name, instance.pk)

    def handle_pre_delete(self, sender, instance, **kwargs):
        handle_pre_delete.delay(sender._meta.app_label, sender._meta.object_name, instance.pk)
