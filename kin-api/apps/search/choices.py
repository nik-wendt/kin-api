from enum import Enum


class OnlySearchChoices(Enum):
    FACTS = "facts"
    FACT_KEYS = "fact_keys"
    GROUPS = "groups"
    KIN = "kin"
    NOTES = "notes"


class CreatedRangeChoices(Enum):
    WEEK = "Past Week"
    WEEK_TO_MONTH = "1-week to a Month Ago"
    MONTH_TO_SIX_MONTHS = "1-month to 6-months Ago"
    SIX_MONTHS_TO_YEAR = "6-months to a Year Ago"
    OLDER_THAN_YEAR = "Older than a year"
