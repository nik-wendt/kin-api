from rest_framework import routers

from apps.connections.views import ConnectionViewSet


router = routers.DefaultRouter()
router.register("", ConnectionViewSet, basename="connection")


urlpatterns = router.urls
