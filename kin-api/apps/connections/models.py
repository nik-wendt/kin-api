from datetime import date

from django.db import models

from django_enum_choices.fields import EnumChoiceField
from django_extensions.db.fields import ShortUUIDField

from apps.connections.choices import ConnectionTypeEnum
from apps.kin.models import Kin
from apps.reminders.choices import RecencyEnum
from apps.reminders.models import Reminder


class Connection(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    kin = models.ForeignKey(Kin, on_delete=models.CASCADE, related_name="connections")
    connection_type = EnumChoiceField(ConnectionTypeEnum)
    connection_date = models.DateField(default=date.today)
    note = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-connection_date", "-created"]

    @property
    def has_reminder_soon(self):
        reminder = Reminder.objects.filter(kin=self.kin).first()

        if reminder and reminder.recency == RecencyEnum.SOON:
            return True

        return False
