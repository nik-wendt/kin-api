from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers

from django_enum_choices.serializers import EnumChoiceModelSerializerMixin

from apps.connections.models import Connection
from apps.kin.models import Kin


class KinPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        user = self.context["request"].user
        queryset = Kin.objects.filter(owner=user)
        return queryset


class ConnectionSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    kin = KinPrimaryKeyRelatedField()

    class Meta:
        model = Connection
        fields = "__all__"

    def create(self, validated_data):
        instance = super().create(validated_data)

        try:
            reminder = instance.kin.reminder

            reminder.set_next_reminder_date(save=False)
            reminder.notification_sent = False
            reminder.save()
        except ObjectDoesNotExist:
            return instance

        return instance


class ConnectionUpdateSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = Connection
        exclude = ("kin",)


class ConnectionBulkDeleteSerializer(serializers.Serializer):
    connection_ids = serializers.ListField(
        child=serializers.CharField(max_length=50),
        help_text="An array of connection ids",
        required=True,
    )

    def validate_connection_ids(self, value):
        user = self._context["request"].user

        for connection_id in value:
            if not Connection.objects.filter(pk=connection_id, kin__owner=user).exists():
                raise serializers.ValidationError("Invalid connection")

        return value
