from rest_framework import mixins, permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from drf_yasg.utils import swagger_auto_schema
from rest_framework_bulk.drf3.mixins import BulkCreateModelMixin

from apps.connections.filters import ConnectionFilter
from apps.connections.models import Connection
from apps.connections.serializers import (
    ConnectionBulkDeleteSerializer,
    ConnectionSerializer,
    ConnectionUpdateSerializer,
)
from apps.kin_auth.permissions import IsPaid, IsVerified


class ConnectionViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    BulkCreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = ConnectionSerializer
    filterset_class = ConnectionFilter

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Connection.objects.none()

        qs = Connection.objects.filter(kin__owner=self.request.user)

        return qs

    def get_serializer_class(self):
        if self.action in ["update", "partial_update"]:
            return ConnectionUpdateSerializer

        return ConnectionSerializer

    def perform_update(self, serializer):
        connection = serializer.save()
        return connection

    @swagger_auto_schema(
        request_body=ConnectionUpdateSerializer,
        responses={status.HTTP_200_OK: ConnectionSerializer},
    )
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        connection = self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        output_serializer = ConnectionSerializer(connection)
        return Response(output_serializer.data)

    @swagger_auto_schema(
        request_body=ConnectionUpdateSerializer,
        responses={status.HTTP_200_OK: ConnectionSerializer},
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @swagger_auto_schema(
        request_body=ConnectionBulkDeleteSerializer,
        responses={status.HTTP_204_NO_CONTENT: []},
        operation_id="connections_bulk_delete",
    )
    @action(detail=False, methods=["DELETE"], url_path="bulk-delete")
    def bulk_destroy(self, request):
        serializer = ConnectionBulkDeleteSerializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)

        connections = self.get_queryset().filter(pk__in=serializer.validated_data["connection_ids"])
        connections.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
