from django.apps import AppConfig


class ConnectionsConfig(AppConfig):
    name = "apps.connections"
    verbose_name = "Connections"
