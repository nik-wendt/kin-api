# Generated by Django 3.0.13 on 2021-04-21 19:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [("connections", "0002_connection_confirmed")]

    operations = [migrations.RemoveField(model_name="connection", name="confirmed")]
