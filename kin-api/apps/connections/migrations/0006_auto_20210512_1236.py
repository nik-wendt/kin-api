# Generated by Django 3.0.13 on 2021-05-12 12:36

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("connections", "0005_auto_20210507_2043")]

    operations = [
        migrations.AlterModelOptions(name="connection", options={"ordering": ["-connection_date"]}),
        migrations.AddField(
            model_name="connection",
            name="connection_date",
            field=models.DateField(default=datetime.date.today),
        ),
    ]
