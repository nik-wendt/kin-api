from enum import Enum


class ConnectionTypeEnum(Enum):
    EMAIL = "EMAIL"
    PHONE = "PHONE"
    SMS = "SMS"
    MEETING = "MEETING"
    PERSON = "PERSON"
    OTHER = "OTHER"
