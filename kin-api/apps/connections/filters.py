from django_enum_choices.filters import EnumChoiceFilter, EnumChoiceFilterSetMixin
from django_filters import rest_framework as filters

from apps.connections.choices import ConnectionTypeEnum
from apps.connections.models import Connection


class ConnectionFilter(EnumChoiceFilterSetMixin, filters.FilterSet):
    connection_type = EnumChoiceFilter(
        ConnectionTypeEnum, help_text="Choices are EMAIL, PHONE, SMS, MEETING, PERSON, or OTHER"
    )

    class Meta:
        model = Connection
        fields = ["kin"]
