from django.conf import settings
from django.utils import timezone
from rest_framework import permissions, serializers, status, viewsets
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

import redis
from drf_yasg.utils import swagger_auto_schema

from apps.facts.models import Fact
from apps.kin.models import Kin
from apps.kin_admin.models import MaintenanceMode
from apps.kin_admin.serializers import (
    MaintenanceModeImmediateSerializer,
    MaintenanceModeScheduleSerializer,
    MaintenanceModeSerializer,
    SetPaymentStatusResultSerializer,
    SetPaymentStatusSerializer,
    TriggerNotificationSerializer,
)
from apps.kin_admin.tasks import remove_orphan_users
from apps.kin_auth.permissions import IsVerified
from apps.notes.models import Note
from apps.users.models import User


class FlushSpotlightCaches(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser, IsVerified)
    serializer_class = None

    @swagger_auto_schema(
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="admin_spotlight_cache_flush",
    )
    def post(self, request):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        for key in redis_client.scan_iter("spotlight:*"):
            redis_client.delete(key)

        return Response(status=status.HTTP_200_OK)


class FlushGroupMailtoCaches(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser, IsVerified)
    serializer_class = None

    @swagger_auto_schema(
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="admin_group_mailto_cache_flush",
    )
    def post(self, request):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        for key in redis_client.scan_iter("groups:*:members:mailto"):
            redis_client.delete(key)

        return Response(status=status.HTTP_200_OK)


class Analytics(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser, IsVerified)
    serializer_class = None

    def get(self, request):
        user_count = User.objects.count()
        kin_count = Kin.objects.count()
        note_count = Note.objects.count()
        fact_count = Fact.objects.count()

        return Response(
            {
                "user_count": user_count,
                "kin_count": kin_count,
                "note_count": note_count,
                "fact_count": fact_count,
            },
            status=status.HTTP_200_OK,
        )


class MaintenanceModeViewSet(viewsets.ViewSet):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser, IsVerified)
    serializer_class = MaintenanceModeSerializer

    @swagger_auto_schema(responses={status.HTTP_200_OK: MaintenanceModeSerializer})
    @action(detail=False, methods=["GET"])
    def current(self, request, *args, **kwargs):
        current_maintenance_mode = MaintenanceMode.objects.filter(active=True).first()

        if current_maintenance_mode:
            serializer = MaintenanceModeSerializer(current_maintenance_mode)
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_200_OK)

    @swagger_auto_schema(
        request_body=MaintenanceModeImmediateSerializer,
        responses={status.HTTP_201_CREATED: MaintenanceModeSerializer},
    )
    @action(detail=False, methods=["POST"])
    def immediate(self, request, *args, **kwargs):
        _ = MaintenanceMode.objects.all().update(active=False)
        serializer = MaintenanceModeImmediateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        maintenance_mode = serializer.save(start=timezone.now(), active=True)

        output_serializer = MaintenanceModeSerializer(maintenance_mode)
        return Response(output_serializer.data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(responses={status.HTTP_200_OK: serializers.Serializer})
    @action(detail=False, methods=["POST"])
    def end(self, request, *args, **kwargs):
        _ = MaintenanceMode.objects.all().update(active=False)

        return Response(status=status.HTTP_200_OK)

    @swagger_auto_schema(
        request_body=MaintenanceModeScheduleSerializer,
        responses={status.HTTP_201_CREATED: MaintenanceModeSerializer},
    )
    @action(detail=False, methods=["POST"])
    def schedule(self, request, *args, **kwargs):
        serializer = MaintenanceModeScheduleSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        scheduled_maintenance_mode = serializer.save()

        output_serializer = MaintenanceModeSerializer(scheduled_maintenance_mode)
        return Response(output_serializer.data, status=status.HTTP_201_CREATED)


class TriggerNotificationView(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser, IsVerified)
    serializer_class = None

    @swagger_auto_schema(
        request_body=TriggerNotificationSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="admin_trigger_notification",
    )
    def post(self, request):
        serializer = TriggerNotificationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_200_OK)


class SetPaymentStatusView(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser, IsVerified)
    serializer_class = None

    @swagger_auto_schema(
        request_body=SetPaymentStatusSerializer,
        responses={status.HTTP_200_OK: SetPaymentStatusResultSerializer},
        operation_id="admin_set_payment_status",
    )
    def post(self, request):
        serializer = SetPaymentStatusSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()

        output_serializer = SetPaymentStatusResultSerializer(user)

        return Response(output_serializer.data, status=status.HTTP_200_OK)


class RemoveOrphanUsersView(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser, IsVerified)
    serializer_class = None

    @swagger_auto_schema(
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="admin_remove_orphan_users",
    )
    def post(self, request):
        remove_orphan_users.delay()

        return Response(status=status.HTTP_200_OK)
