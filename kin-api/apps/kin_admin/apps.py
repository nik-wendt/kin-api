from django.apps import AppConfig


class KinAdminConfig(AppConfig):
    name = "apps.kin_admin"
    verbose_name = "Kinship Admin"
