from django.urls import path
from rest_framework import routers

from apps.kin_admin.views import (
    Analytics,
    FlushGroupMailtoCaches,
    FlushSpotlightCaches,
    MaintenanceModeViewSet,
    RemoveOrphanUsersView,
    SetPaymentStatusView,
    TriggerNotificationView,
)

router = routers.DefaultRouter()
router.register("maintenance", MaintenanceModeViewSet, basename="maintenance_mode")

urlpatterns = [
    path("flush-spotlight-caches/", FlushSpotlightCaches.as_view(), name="flush_spotlight_caches"),
    path(
        "flush-group-mailto-caches/",
        FlushGroupMailtoCaches.as_view(),
        name="flush_group_mailto_caches",
    ),
    path("analytics/", Analytics.as_view(), name="analytics"),
    path("trigger-notification/", TriggerNotificationView.as_view(), name="trigger_notification"),
    path(
        "set-user-payment-status/", SetPaymentStatusView.as_view(), name="set_user_payment_status"
    ),
    path("remove-orphan-users/", RemoveOrphanUsersView.as_view(), name="remove_orphan_users"),
]

urlpatterns += router.urls
