from django.db import models
from django.utils import timezone

from django_extensions.db.fields import ShortUUIDField


class MaintenanceMode(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    start = models.DateTimeField(default=timezone.now)
    end_eta = models.DateTimeField(null=True)
    active = models.BooleanField(default=False)
