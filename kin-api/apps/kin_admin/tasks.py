import logging
from datetime import timedelta

from celery import shared_task
from django.utils import timezone

from apps.users.models import User


logger = logging.getLogger(__name__)


@shared_task
def remove_orphan_users():
    logger.info("Starting orphaned user cleanup...")
    some_day_last_week = timezone.now() - timedelta(days=7)
    users = User.objects.filter(
        is_active=False, is_verified=False, date_joined__lt=some_day_last_week
    )

    orphaned_user_ids = ", ".join(users.values_list("id", flat=True))
    logging_msg = f"Deleting orphaned users: {orphaned_user_ids}"
    logger.info(logging_msg)

    users.update(user_kin=None)
    users.delete()

    logger.info("Finished orphaned user cleanup.")
