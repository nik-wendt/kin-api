from django.http import HttpResponse

from apps.kin_admin.models import MaintenanceMode


class MaintenanceModeMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if MaintenanceMode.objects.filter(active=True).exists():
            if not "maintenance" in request.path:
                response = HttpResponse()
                response.status_code = 503
                return response

        response = self.get_response(request)

        return response
