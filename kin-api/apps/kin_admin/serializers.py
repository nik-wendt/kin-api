from rest_framework import serializers

from django_enum_choices.serializers import EnumChoiceField, EnumChoiceModelSerializerMixin

from apps.kin_admin.models import MaintenanceMode
from apps.notifications.utils import send_notification
from apps.users.choices import PaymentProviderEnum, PaymentStatusEnum
from apps.users.models import User


class MaintenanceModeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MaintenanceMode
        fields = "__all__"


class MaintenanceModeScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = MaintenanceMode
        exclude = ("active",)


class MaintenanceModeImmediateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MaintenanceMode
        exclude = ("start", "active")


class TriggerNotificationSerializer(serializers.Serializer):
    user = serializers.PrimaryKeyRelatedField(
        required=False, queryset=User.objects.all(), help_text="A user id"
    )
    message = serializers.CharField()

    def save(self, **kwargs):
        user = self.validated_data.get("user", None)
        message = self.validated_data.get("message", "")

        send_notification(message, user.id, tz_aware=False)


class SetPaymentStatusSerializer(serializers.Serializer):
    user = serializers.PrimaryKeyRelatedField(
        required=True, queryset=User.objects.all(), help_text="A user id"
    )
    payment_status = EnumChoiceField(
        PaymentStatusEnum,
        help_text="Choices are <strong>PAID</strong>, <strong>TRIAL</strong>, <strong>FOUNDING_MEMBER</strong>, <strong>CANCELLED</strong>, or <strong>PAST_DUE</strong>. Default: <strong>TRIAL</strong>.",
    )
    payment_expiration = serializers.DateTimeField(allow_null=True)

    def validate(self, attrs):
        if (
            attrs["payment_status"] == PaymentStatusEnum.FOUNDING_MEMBER
            and attrs["payment_expiration"] is not None
        ):
            raise serializers.ValidationError(
                "Expiration date must be null for a payment status of founding member"
            )

        return attrs

    def save(self, **kwargs):
        user = self.validated_data["user"]
        payment_status = self.validated_data["payment_status"]
        payment_expiration = self.validated_data["payment_expiration"]

        user.payment_status = payment_status
        user.payment_expiration = payment_expiration
        user.payment_provider = PaymentProviderEnum.MANUAL
        user.save()

        return user


class SetPaymentStatusResultSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "email",
            "first_name",
            "last_name",
            "payment_provider",
            "payment_status",
            "payment_expiration",
            "payment_subscription_type",
        )
