from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class HealthcheckTestCase(APITestCase):
    def test_heartbeat(self):
        url = reverse("heartbeat")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_healthcheck(self):
        url = "/"
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
