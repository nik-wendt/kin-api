from django.urls import path

from apps.healthcheck.views import HeartbeatView


urlpatterns = [path("heartbeat/", HeartbeatView.as_view(), name="heartbeat")]
