import logging

import redis
from health_check.backends import BaseHealthCheckBackend
from health_check.exceptions import HealthCheckException

from kin_api_project import settings

logger = logging.getLogger(__name__)


class RedisMemCheck(BaseHealthCheckBackend):

    critical_service = False

    def __init__(self):
        self.memory_human_readable = None
        super().__init__()

    def check_status(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        self.memory_human_readable = redis_client.info("Memory").get("used_memory_human")
        if not self.memory_human_readable:
            raise HealthCheckException("Could not get used memory")

    def pretty_status(self):
        if self.errors:
            return "\n".join(str(e) for e in self.errors)
        return f"Memory Used:{self.memory_human_readable}"


class RedisTestKeyCheck(BaseHealthCheckBackend):

    critical_service = False

    def check_status(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        testkey = redis_client.get("testkey")
        if testkey:
            testkey = testkey.decode("utf-8")
        if testkey != "working":
            raise HealthCheckException(f"Redis Test Key Invalid: {testkey}")

    def pretty_status(self):
        if self.errors:
            return "\n".join(str(e) for e in self.errors)
        return f"Test Key is valid"
