from django.apps import AppConfig

from health_check.plugins import plugin_dir


class HealthCheckConfig(AppConfig):
    name = "apps.healthcheck"
    verbose_name = "Health Check"

    def ready(self):
        from apps.healthcheck.backends import RedisMemCheck, RedisTestKeyCheck

        plugin_dir.register(RedisMemCheck)
        plugin_dir.register(RedisTestKeyCheck)
        super().ready()
