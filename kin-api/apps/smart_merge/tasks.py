import logging
from difflib import SequenceMatcher
from itertools import combinations, product

from celery import shared_task
from django.db.models import Subquery, OuterRef, Prefetch

from apps.facts.choices import FactTypeEnum
from apps.facts.models import Fact
from apps.kin.models import Kin
from apps.smart_merge.models import Merge
from apps.users.models import User


logger = logging.getLogger(__name__)


def _name_compare(k1, k2):

    # Sanitize and setup names for comparison
    k1_fname = k1.first_name.lower()
    k1_lname = k1.last_name.lower()
    k1_friendly = k1.friendly_name.lower()
    k1_suffix = k1.suffix.lower()
    k2_fname = k2.first_name.lower()
    k2_lname = k2.last_name.lower()
    k2_friendly = k2.friendly_name.lower()
    k2_suffix = k2.suffix.lower()

    # has firstname and last name. Compare those
    if (k1_fname and k1_lname) and (k2_fname and k2_lname):
        if SequenceMatcher(None, f"{k1_fname}{k1_lname}", f"{k2_fname}{k2_lname}").ratio() >= 0.8:
            # but not if suffixes don't match
            return bool(k1_suffix == k2_suffix)
    # has only last name and maybe suffix
    elif (k1_lname and not (k1_friendly or k1_fname)) and (
        k2_lname and not (k2_friendly or k2_fname)
    ):
        if SequenceMatcher(None, f"{k1_lname} {k1_suffix}", f"{k2_lname} {k2_suffix}"):
            return bool(k1_suffix == k2_suffix)
    # has friendly and lname
    elif (k1_friendly and k1_lname) and (k2_friendly and k2_lname):
        if (
            SequenceMatcher(None, f"{k1_friendly}{k1_lname}", f"{k2_friendly}{k2_lname}").ratio()
            >= 0.8
        ):
            return bool(k1_suffix == k2_suffix)
    return False


@shared_task
def generate_merge_suggestions_task(owner_id):
    logger.info("Starting duplicate search for user: %s", owner_id)
    user = User.objects.get(pk=owner_id)
    duplicates_identified = 0
    email_facts = Subquery(
        Fact.objects.filter(kin=OuterRef("kin"), fact_type=FactTypeEnum.EMAIL).values_list(
            "id", flat=True
        )
    )
    phone_facts = Subquery(
        Fact.objects.filter(kin=OuterRef("kin"), fact_type=FactTypeEnum.PHONE).values_list(
            "id", flat=True
        )
    )
    all_kin = Kin.objects.filter(owner=user).prefetch_related(
        Prefetch("facts", queryset=Fact.objects.filter(id__in=email_facts), to_attr="emails"),
        Prefetch(
            "facts", queryset=Fact.objects.filter(id__in=phone_facts), to_attr="phone_numbers"
        ),
        Prefetch("merges", queryset=Merge.objects.all()),
    )
    for k1, k2 in combinations(all_kin, 2):
        if k1.merges.filter(kin__in=[k1, k2]).exists():
            continue
        confidence = 0
        if _name_compare(k1, k2):
            confidence += 1
        for p1, p2 in product(k1.phone_numbers, k2.phone_numbers):
            sanitized_p1 = "".join(e for e in p1.value if e.isalnum())
            sanitized_p2 = "".join(e for e in p2.value if e.isalnum())
            if sanitized_p2 == sanitized_p1:
                confidence += 1
        for e1, e2 in product(k1.emails, k2.emails):
            if e1.value == e2.value:
                confidence += 1

        if confidence > 0:
            logger.info("identified kin pair %s : %s", k1.id, k2.id)
            merge = Merge(owner=user, confidence_score=confidence)
            merge.save()
            merge.kin.add(k1, k2)
            duplicates_identified += 1
    logger.info("%s duplicates identified", duplicates_identified)
