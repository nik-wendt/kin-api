from django_enum_choices.serializers import EnumChoiceField, EnumChoiceModelSerializerMixin
from rest_framework import serializers

from apps.smart_merge.choices import MergeStatusEnum
from apps.smart_merge.models import Merge
from apps.smart_merge.tasks import generate_merge_suggestions_task


class MergeResolveSerializer(serializers.ModelSerializer):
    status = EnumChoiceField(
        MergeStatusEnum,
        default=MergeStatusEnum.RESOLVED,
        help_text="Choices are <strong>RESOLVED</strong>, <strong>UNRESOLVED</strong>",
    )

    class Meta:
        model = Merge
        fields = ("status",)


class GenerateMergeSerialier(serializers.Serializer):
    def save(self, **kwargs):
        generate_merge_suggestions_task.delay(kwargs["owner"].id)


class MergeListSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = Merge
        fields = "__all__"
