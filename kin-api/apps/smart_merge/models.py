from django.db import models
from django_enum_choices.fields import EnumChoiceField
from shortuuid.django_fields import ShortUUIDField

from apps.smart_merge.choices import MergeStatusEnum
from apps.kin.models import Kin
from apps.users.models import User


class Merge(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="merges")
    kin = models.ManyToManyField(Kin, through="MergeKin", related_name="merges")
    status = EnumChoiceField(MergeStatusEnum, default=MergeStatusEnum.UNRESOLVED)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    confidence_score = models.IntegerField()


class MergeKin(models.Model):
    merge = models.ForeignKey(Kin, on_delete=models.CASCADE)
    kin = models.ForeignKey(Merge, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ["merge", "kin"]
