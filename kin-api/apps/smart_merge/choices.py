from enum import Enum


class MergeStatusEnum(Enum):
    UNRESOLVED = "UNRESOLVED"
    RESOLVED = "RESOLVED"
