import logging

from django.core.management.base import BaseCommand
from django.utils import timezone

from apps.kin.models import Kin
from apps.users.models import User
from apps.smart_merge.tasks import generate_merge_suggestions_task

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = """gathers all users with activity from today and runs the dedup suggestions celery task for each. 
    """

    # def add_arguments(self, parser):

    def handle(self, *args, **options):
        users = (
            User.objects.filter(kin__modified__gt=timezone.now() - timezone.timedelta(days=1))
            .values_list("id", flat=True)
            .distinct()
        )
        logger.info("Identified %s users to dedup", users.count())
        for user in users:
            """
            This can be run with generate_merge_suggestions_task.delay(user),
            however since it's a job there's no need to queue it at the moment.
            """
            generate_merge_suggestions_task(user)
