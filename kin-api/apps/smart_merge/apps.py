from django.apps import AppConfig


class SmartMergeConfig(AppConfig):
    name = "apps.smart_merge"
    verbose_name = "SmartMerge"

    def ready(self):
        import apps.smart_merge.signals
