from rest_framework import mixins, viewsets, permissions, status
from rest_framework.decorators import action
from rest_framework.response import Response

from drf_yasg.utils import swagger_auto_schema

from apps.kin_auth.permissions import IsVerified, IsPaid
from apps.smart_merge.models import Merge
from apps.smart_merge.serializers import (
    GenerateMergeSerialier,
    MergeResolveSerializer,
    MergeListSerializer,
)


class KinMergeSuggestViewSet(
    mixins.UpdateModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet
):
    # TODO: open this up to non-admin once we know it's safe to do so.
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)

    def get_serializer_class(self, *args, **kwargs):
        if self.action == "list":
            return MergeListSerializer
        if self.action == "partial_update":
            return MergeResolveSerializer
        else:
            return GenerateMergeSerialier

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Merge.objects.none()
        return Merge.objects.filter(owner=self.request.user)

    @swagger_auto_schema(responses={status.HTTP_200_OK: MergeListSerializer(many=True)})
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = MergeListSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = MergeListSerializer(queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=GenerateMergeSerialier, responses={status.HTTP_201_CREATED: {}}
    )
    @action(methods=["POST"], detail=False, url_path="dedup")
    def generate_suggestions(self, request, *args, **kwargs):
        serializer = self.get_serializer()
        serializer.save(owner=self.request.user)
        return Response("Dedup Started", status=status.HTTP_201_CREATED)
