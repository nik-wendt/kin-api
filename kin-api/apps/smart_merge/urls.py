from django.urls import path
from rest_framework import routers

from apps.smart_merge.views import KinMergeSuggestViewSet

router = routers.DefaultRouter()
router.register("", KinMergeSuggestViewSet, basename="merge")

urlpatterns = []

urlpatterns += router.urls
