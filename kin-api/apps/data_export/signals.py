from django.core.files.storage import default_storage
from django.db.models.signals import post_delete
from django.dispatch import receiver

from apps.data_export.models import DataExport


@receiver(post_delete, sender=DataExport)
def delete_associated_media(sender, instance, **kwargs):
    file_name = instance.export_file

    if file_name and default_storage.exists(str(file_name)):
        default_storage.delete(str(file_name))
