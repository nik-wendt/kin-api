from django.db import models

from django_extensions.db.fields import ShortUUIDField

from apps.users.models import User


def export_file_path(instance, _):
    return f"exports/{instance.owner.id}.zip"


class DataExport(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    owner = models.OneToOneField(User, on_delete=models.CASCADE, related_name="exports")
    export_file = models.FileField(upload_to=export_file_path, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
