import csv
import logging
import os
import shutil

from celery import shared_task
from django.conf import settings
from django.core.files.base import File
from django.core.mail import EmailMessage
from storages.backends.s3boto3 import S3Boto3Storage

from itsdangerous.url_safe import URLSafeTimedSerializer

from apps.data_export.models import DataExport
from apps.data_export.serializers import DataExportSerializer
from apps.facts.models import Fact
from apps.integrations.mixpanel.tasks import mixpanel_data_export_event
from apps.kin_extensions.db_functions import GroupConcat
from apps.users.models import User


logger = logging.getLogger(__name__)


def _send_download_email(user):
    data_export = DataExport.objects.filter(owner=user).first()

    if data_export:
        url = data_export.export_file.url
        s = URLSafeTimedSerializer(settings.SECRET_KEY)
        token = s.dumps({"user_id": user.id})

        message = EmailMessage(
            subject=f"Your kinship data export is ready, {user.first_name}",
            to=[f"{user.first_name} {user.last_name} <{user.email}>"],
        )
        message.template_id = "Data export transactional template"
        message.merge_data = {
            user.email: {"FNAME": user.first_name, "LINK": f"{url}?token={token}"}
        }
        message.send()


def _download_media(user, export_dir):
    storage = S3Boto3Storage()
    objs = storage.bucket.objects.filter(Prefix=f"nuggets/owner-{user.id}")
    for obj in objs:
        file_path = "/".join(obj.key.split("/")[2:]).replace("nugget", "note")

        if not "thumbnails" in file_path:
            save_path = f"{export_dir}{file_path}"

            os.makedirs(os.path.dirname(save_path), exist_ok=True)
            storage.bucket.meta.client.download_file(
                settings.AWS_STORAGE_BUCKET_NAME, obj.key, save_path
            )


def _save_kin_as_csv(user, export_dir):
    kin_columns = [
        "id",
        "friendly_name",
        "first_name",
        "middle_name",
        "last_name",
        "suffix",
        "group_names",
        "created",
    ]

    kin = user.kin.annotate(group_names=GroupConcat("groups__name", ", ")).values(
        "id",
        "friendly_name",
        "first_name",
        "middle_name",
        "last_name",
        "suffix",
        "group_names",
        "created",
    )

    filename = f"{export_dir}/kin.csv"
    with open(filename, "w") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=kin_columns)
        writer.writeheader()
        for data in kin:
            writer.writerow(data)


def _save_facts_as_csv(user, export_dir):
    fact_columns = ["id", "kin", "key", "value", "created"]

    facts = (
        Fact.objects.filter(kin__owner=user)
        .values("id", "kin", "key", "value", "created")
        .order_by("kin_id")
    )

    filename = f"{export_dir}/facts.csv"
    with open(filename, "w") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fact_columns)
        writer.writeheader()
        for data in facts:
            writer.writerow(data)


def _save_notes_as_csv(user, export_dir):
    note_columns = ["id", "kin", "text", "raw_text", "created"]

    notes = user.notes.values("id", "kin", "text", "raw_text", "created").order_by("kin_id")

    filename = f"{export_dir}/notes.csv"
    with open(filename, "w") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=note_columns)
        writer.writeheader()
        for data in notes:
            writer.writerow(data)


@shared_task
def as_csv(user_id):
    try:
        user = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        logger.error("Unable to perform data export. User with id: %s does not exist", user.id)
        return

    DataExport.objects.filter(owner=user).delete()
    export = DataExport.objects.create(owner=user)

    export_archive_filename = f"/tmp/archives/{user.id}"
    os.makedirs(os.path.dirname(export_archive_filename), exist_ok=True)

    export_dir = f"/tmp/{user.id}/"
    export_data_dir = f"{export_dir}data/"
    os.makedirs(os.path.dirname(export_data_dir), exist_ok=True)
    export_media_dir = f"{export_dir}media/"
    os.makedirs(os.path.dirname(export_media_dir), exist_ok=True)

    try:
        _save_kin_as_csv(user, export_data_dir)
        _save_facts_as_csv(user, export_data_dir)
        _save_notes_as_csv(user, export_data_dir)
        _download_media(user, export_media_dir)

        shutil.make_archive(export_archive_filename, "zip", export_dir)

        fh = open(f"{export_archive_filename}.zip", "rb")
        save_file = File(fh)

        data_export_serializer = DataExportSerializer(export, data={"export_file": save_file})
        data_export_serializer.is_valid(raise_exception=True)
        data_export_serializer.save()

        os.remove(f"{export_archive_filename}.zip")
        shutil.rmtree(export_dir)

        _send_download_email(user)

        mixpanel_data_export_event(user_id)
    except Exception as e:
        logger.error("Unable to perform data export for user with id: %s. %s", user.id, str(e))
