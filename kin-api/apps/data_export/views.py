import logging

from rest_framework import serializers, status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from drf_yasg.utils import swagger_auto_schema

from apps.data_export.serializers import DataExportTokenVerificationSerializer
from apps.data_export.tasks import as_csv
from apps.integrations.mixpanel.tasks import mixpanel_data_export_event
from apps.kin_auth.permissions import IsVerified

logger = logging.getLogger(__name__)


class DataExportView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsVerified]

    @swagger_auto_schema(
        request_body=serializers.Serializer, responses={status.HTTP_200_OK: serializers.Serializer}
    )
    def post(self, request, *args, **kwargs):
        as_csv.delay(request.user.id)

        return Response(status=status.HTTP_200_OK)


class DataExportTokenVerificationView(APIView):
    authentication_classes = []
    permission_classes = []

    @swagger_auto_schema(
        request_body=DataExportTokenVerificationSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
    )
    def post(self, request, *args, **kwargs):
        serializer = DataExportTokenVerificationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_id = serializer.data["token"]

        return Response({"user_id": user_id}, status=status.HTTP_200_OK)
