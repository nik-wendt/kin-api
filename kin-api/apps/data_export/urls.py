from django.urls import path

from apps.data_export.views import DataExportTokenVerificationView, DataExportView


urlpatterns = [
    path("", DataExportView.as_view(), name="export"),
    path("verify-token/", DataExportTokenVerificationView.as_view(), name="verify-token"),
]
