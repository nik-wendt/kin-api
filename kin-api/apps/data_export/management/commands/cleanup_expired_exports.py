import logging
from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils import timezone

from apps.data_export.models import DataExport
from apps.kin.models import UserKin
from apps.kin_auth.utils import cleanup_account_delete_data


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Cleans up orphaned inactive and unverified users after a week"

    def handle(self, *args, **options):
        logger.info("Starting expired data export cleanup...")
        yesterday = timezone.now() - timedelta(days=1)
        data_exports = DataExport.objects.filter(created__lt=yesterday)

        expired_data_export_ids = ", ".join(data_exports.values_list("id", flat=True))
        logging_msg = f"Deleting expired data exports: {expired_data_export_ids}"
        logger.info(logging_msg)

        for data_export in data_exports:
            user = data_export.owner
            if (
                not user.is_active and not user.is_verified
            ):  # user requested account deletion, but we had to wait until their data export expired
                user_kin = UserKin.objects.get(user=user)
                cleanup_account_delete_data(user.email)
                user_kin.delete()
                user.delete()

            data_export.delete()

        logger.info("Finshed expired data export cleanup.")
