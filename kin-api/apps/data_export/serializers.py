from django.conf import settings
from rest_framework import serializers

from itsdangerous.url_safe import URLSafeTimedSerializer

from apps.data_export.models import DataExport
from apps.users.models import User


class DataExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataExport
        exclude = ("owner",)


class DataExportTokenVerificationSerializer(serializers.Serializer):
    token = serializers.CharField()

    def validate_token(self, value):
        try:
            s = URLSafeTimedSerializer(settings.SECRET_KEY)

            decoded_token = s.loads(value, max_age=24 * 60 * 60)
            user = User.objects.get(id=decoded_token["user_id"])

            if not DataExport.objects.filter(owner=user).exists():
                raise Exception
        except Exception:
            raise serializers.ValidationError("Invalid token")

        return user.id
