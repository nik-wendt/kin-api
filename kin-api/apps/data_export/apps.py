from django.apps import AppConfig


class DataExportConfig(AppConfig):
    name = "apps.data_export"
    verbose_name = "Data Export"

    def ready(self):
        import apps.data_export.signals
