from rest_framework import serializers

import phonenumbers
from django_enum_choices.serializers import EnumChoiceModelSerializerMixin
from timezone_field.rest_framework import TimeZoneSerializerField

from apps.users.models import User, UserPhone, UserPreferences


class InlineUserPreferencesSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    setup = serializers.SerializerMethodField()
    timezone_setting = TimeZoneSerializerField()

    class Meta:
        model = UserPreferences
        exclude = ("user",)
        read_only_fields = (
            "mode",
            "use_biometrics",
            "receive_push_notifications",
            "sync_calendar_data",
            "access_contacts",
            "app_settings",
        )

    def get_setup(self, obj):
        return {
            "add_kin_complete": obj.setup_add_kin_complete,
            "import_contacts_complete": obj.setup_import_contacts_complete,
            "calendar_connect_complete": obj.setup_calendar_connect_complete,
            "create_note_complete": obj.setup_create_note_complete,
            "text_to_kinship_complete": obj.setup_text_to_kinship_complete,
        }


class InlineUserPhoneSerializer(serializers.ModelSerializer):
    phone = serializers.SerializerMethodField()
    phone_country_flag = serializers.SerializerMethodField()
    phone_country = serializers.SerializerMethodField()
    is_verified = serializers.SerializerMethodField()

    class Meta:
        model = UserPhone
        fields = ("phone", "phone_country", "phone_country_flag", "is_verified")
        read_only_fields = ("phone", "phone_country", "phone_country_flag", "is_verified")

    def get_phone(self, obj):
        try:
            return phonenumbers.format_number(
                phonenumbers.parse(obj.phone, obj.phone_country),
                phonenumbers.PhoneNumberFormat.INTERNATIONAL,
            )
        except AttributeError as e:
            user_phone = obj.first()

            if not user_phone:
                return

            return phonenumbers.format_number(
                phonenumbers.parse(user_phone.phone, user_phone.phone_country),
                phonenumbers.PhoneNumberFormat.INTERNATIONAL,
            )

    def get_phone_country_flag(self, obj):
        try:
            return self.context["request"].build_absolute_uri(obj.phone_country_flag)
        except AttributeError as e:
            user_phone = obj.first()

            if not user_phone:
                return

            return self.context["request"].build_absolute_uri(user_phone.phone_country_flag)

    def get_phone_country(self, obj):
        try:
            return obj.phone_country
        except AttributeError as e:
            user_phone = obj.first()

            if not user_phone:
                return

            return user_phone.phone_country

    def get_is_verified(self, obj):
        try:
            return obj.is_verified
        except AttributeError as e:
            user_phone = obj.first()

            if not user_phone:
                return

            return user_phone.is_verified


class UserSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    preferences = InlineUserPreferencesSerializer(read_only=True)
    phone_numbers = InlineUserPhoneSerializer(read_only=True, allow_null=True)
    phone_numbers_list = InlineUserPhoneSerializer(
        source="phone_numbers", read_only=True, many=True
    )
    user_kin = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = [
            "id",
            "email",
            "first_name",
            "last_name",
            "is_verified",
            "is_active",
            "is_staff",
            "is_superuser",
            "payment_status",
            "payment_provider",
            "payment_expiration",
            "payment_subscription_type",
            "preferences",
            "phone_numbers",
            "phone_numbers_list",
            "email_to_kinship_address",
            "user_kin",
        ]

    def to_representation(self, instance):
        ret = super().to_representation(instance)

        if not instance.phone_numbers.exists():
            ret["phone_numbers"] = None

        return ret

    def get_user_kin(self, obj):
        return obj.userkin.kin.id


class UserUpdateSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "first_name", "last_name", "email", "is_verified", "is_active")
        read_only_fields = ("id", "email", "is_verified", "is_active")


class UserPreferencesSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    timezone_setting = TimeZoneSerializerField()

    class Meta:
        model = UserPreferences
        exclude = ("user",)
