from enum import Enum


class PaymentStatusEnum(Enum):
    PAID = "PAID"
    TRIAL = "TRIAL"
    FOUNDING_MEMBER = "FOUNDING_MEMBER"
    LIFETIME_SUBSCRIPTION = "LIFETIME_SUBSCRIPTION"
    CANCELLED = "CANCELLED"
    PAST_DUE = "PAST_DUE"


class PaymentProviderEnum(Enum):
    APPLE = "APPLE"
    MANUAL = "MANUAL"


class PaymentSubscriptionTypeEnum(Enum):
    MONTHLY = "MONTHLY"
    ANNUAL = "ANNUAL"


class UserModeEnum(Enum):
    DARK = "DARK"
    LIGHT = "LIGHT"
