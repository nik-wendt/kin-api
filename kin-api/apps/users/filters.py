from django.db.models import Q
from django_filters import rest_framework as filters

from apps.users.models import User


class UserAdminFilter(filters.FilterSet):
    search = filters.CharFilter(method="search_users")
    is_staff = filters.CharFilter(method="is_staff")

    class Meta:
        model = User
        fields = ["search", "is_staff"]

    def search_users(self, queryset, _, value):
        if value:
            for term in value.split():
                queryset = User.objects.filter(
                    Q(email__icontains=term)
                    | Q(first_name__icontains=term)
                    | Q(last_name__icontains=term)
                    | Q(id=term)
                )
        return queryset

    def is_staff(self, queryset, _, value):
        if value:
            queryset.filter(is_staff=value)
        return queryset
