from datetime import timedelta

from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db.models import JSONField
from django.db import models
from django.db.models import Q
from django.utils import timezone

from django_enum_choices.fields import EnumChoiceField
from django_extensions.db.fields import ShortUUIDField
from timezone_field import TimeZoneField

from apps.kin.choices import KinSourceEnum
from apps.notes.choices import NoteSourceEnum
from apps.users.choices import (
    PaymentProviderEnum,
    PaymentStatusEnum,
    PaymentSubscriptionTypeEnum,
    UserModeEnum,
)
from apps.users.managers import UserManager


def payment_expiration_default():
    return timezone.now() + timedelta(days=30)


class User(AbstractBaseUser, PermissionsMixin):
    id = ShortUUIDField(primary_key=True, editable=False)
    email = models.EmailField(verbose_name="email address", max_length=255, unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    is_verified = models.BooleanField(
        default=False, help_text="Designates whether the user's email is verified."
    )
    is_staff = models.BooleanField(
        "staff status",
        default=False,
        help_text="Designates whether the user can log into this admin site.",
    )
    is_active = models.BooleanField(
        default=False,
        help_text="Designates whether this user should be treated as active. "
        "Unselect this instead of deleting accounts.",
    )
    date_joined = models.DateTimeField("date joined", default=timezone.now)
    payment_status = EnumChoiceField(PaymentStatusEnum, default=PaymentStatusEnum.TRIAL)
    payment_provider = EnumChoiceField(PaymentProviderEnum, null=True)
    payment_expiration = models.DateTimeField(default=payment_expiration_default, null=True)
    payment_subscription_type = EnumChoiceField(PaymentSubscriptionTypeEnum, null=True)

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "email"

    objects = UserManager()

    def __str__(self):
        return self.email

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    @property
    def email_to_kinship_address(self):
        parts = settings.EMAIL_TO_KINSHIP_ADDRESS.split("@")
        return f"{parts[0]}+{self.id}@{parts[1]}"

    @property
    def user_kin(self):
        kin = self.userkin.kin
        return kin


class UserPhone(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="phone_numbers")
    phone = models.CharField(max_length=15, blank=True, unique=True)
    phone_country = models.CharField(max_length=2, blank=True)
    is_verified = models.BooleanField(
        default=False, help_text="Designates whether the user's phone # is verified."
    )
    created = models.DateTimeField(auto_now_add=True)

    @property
    def phone_country_flag(self):
        return f"{settings.STATIC_URL}flags/{self.phone_country.lower()}.gif"

    class Meta:
        ordering = ["created"]


class UserPreferences(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True, related_name="preferences"
    )
    mode = EnumChoiceField(UserModeEnum, default=UserModeEnum.DARK)
    use_biometrics = models.BooleanField(default=False)
    receive_push_notifications = models.BooleanField(null=True)
    sync_calendar_data = models.BooleanField(null=True)
    access_contacts = models.BooleanField(null=True)
    show_onboarding = models.BooleanField(default=True)
    show_setup = models.BooleanField(default=True)
    timezone_setting = TimeZoneField(default="UTC")
    app_settings = JSONField(null=True, default=None)

    @property
    def setup_add_kin_complete(self):
        return self.user.kin.filter(source=KinSourceEnum.APP).exists()

    @property
    def setup_import_contacts_complete(self):
        return self.user.kin.filter(
            Q(source=KinSourceEnum.IOS) | Q(source=KinSourceEnum.ANDROID)
        ).exists()

    @property
    def setup_calendar_connect_complete(self):
        return self.user.calendars.exists()

    @property
    def setup_create_note_complete(self):
        return self.user.notes.filter(source=NoteSourceEnum.STASH).exists()

    @property
    def setup_text_to_kinship_complete(self):
        return UserPhone.objects.filter(user=self.user).exists()
