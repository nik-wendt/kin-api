from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.users.models import User
from apps.users.serializers import UserPreferencesSerializer, UserSerializer, UserUpdateSerializer


class UserViewSet(mixins.UpdateModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return User.objects.none()

        return User.objects.filter(pk=self.request.user.id)

    def get_serializer_class(self):
        if self.action in ["update", "partial_update"]:
            return UserUpdateSerializer
        elif self.action == "preferences":
            return UserPreferencesSerializer
        else:
            return UserSerializer

    @swagger_auto_schema(
        request_body=UserUpdateSerializer,
        responses={status.HTTP_200_OK: UserSerializer()},
        operation_id="users_partial_update",
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @swagger_auto_schema(
        request_body=UserUpdateSerializer,
        responses={status.HTTP_200_OK: UserSerializer()},
        operation_id="users_update",
    )
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @action(detail=False)
    def me(self, request, *args, **kwargs):
        self.kwargs.update(pk=request.user.id)
        instance = self.get_object()

        serializer = self.get_serializer(instance)

        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=UserPreferencesSerializer,
        responses={status.HTTP_200_OK: UserSerializer()},
        operation_id="users_preferences",
    )
    @action(detail=False, methods=["PATCH"])
    def preferences(self, request, *args, **kwargs):
        user = request.user
        user_prefs = user.preferences
        serializer = self.get_serializer(user_prefs, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        user.refresh_from_db()

        success_serializer = UserSerializer(user, context={"request": request})
        return Response(success_serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        responses={status.HTTP_200_OK: UserSerializer()}, operation_id="users_remove_phone_number"
    )
    @action(detail=False, methods=["DELETE"], url_path="remove-phone-number")
    def remove_phone_number(self, request, *args, **kwargs):
        user = request.user
        user.phone_numbers.first().delete()

        user.refresh_from_db()
        success_serializer = UserSerializer(user, context={"request": request})

        return Response(success_serializer.data, status=status.HTTP_200_OK)
