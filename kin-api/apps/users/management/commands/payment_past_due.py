import logging

from django.core.management.base import BaseCommand
from django.db.models import Q
from django.utils import timezone

from apps.users.choices import PaymentProviderEnum, PaymentStatusEnum
from apps.users.models import User


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Marks users (MANUAL payment provider) whose payment expiration date has passed"

    def handle(self, *args, **options):
        logger.info("Starting update of payment expired users...")

        users = (
            User.objects.filter(payment_expiration__lte=timezone.now())
            .filter(Q(payment_provider=PaymentProviderEnum.MANUAL) | Q(payment_provider=None))
            .filter(
                Q(payment_status=PaymentStatusEnum.PAID) | Q(payment_status=PaymentStatusEnum.TRIAL)
            )
        )

        past_due_user_ids = ", ".join(users.values_list("id", flat=True))
        logging_msg = f"Users marked past due: {past_due_user_ids}"
        logger.info(logging_msg)

        users.update(payment_status=PaymentStatusEnum.PAST_DUE)

        logger.info("Finshed update of payment expired users.")
