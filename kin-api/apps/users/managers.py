from django.contrib.auth.models import BaseUserManager


class UserManager(BaseUserManager):
    def create_user(self, email, password, is_verified=False, is_active=False):
        """
        This method only exists to appease django management commands for creating
        users from the command line, and shouldn't be used in other code.
        """
        if not email:
            raise ValueError("Users must have an email address")

        from apps.kin_auth.utils import create_user_kin

        email = self.normalize_email(email)
        user = self.model(
            email=email,
            is_staff=False,
            is_superuser=False,
            is_verified=is_verified,
            is_active=is_active,
        )
        user.set_password(password)
        user.save()
        user = create_user_kin(user.id)
        return user

    def create_superuser(self, email, password, is_verified=False, is_active=False):
        """
        This method only exists to appease django management commands for creating
        users from the command line, and shouldn't be used in other code.
        """

        if not email:
            raise ValueError("Users must have an email address")

        from apps.kin_auth.utils import create_user_kin

        email = self.normalize_email(email)
        user = self.model(email=email, is_staff=True, is_superuser=True)
        user.set_password(password)
        user.save()
        user = create_user_kin(user.id)
        return user
