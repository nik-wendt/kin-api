from django.db.models import Count
from rest_framework import mixins, permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response

from drf_yasg.utils import swagger_auto_schema

from apps.groups.filters import GroupFilter, GroupMemberFilter
from apps.groups.helpers import get_group_members_contact
from apps.groups.models import Group
from apps.groups.serializers import (
    GroupBulkDeleteSerializer,
    GroupKinSerializer,
    GroupMemberMailtoResultsSerializer,
    GroupMemberSerializer,
    GroupSerializer,
)
from apps.integrations.mixpanel.tasks import add_or_remove_kin_to_group_mixpanel_event
from apps.kin.models import Kin
from apps.kin_auth.permissions import IsPaid, IsVerified


class GroupViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    filterset_class = GroupFilter
    serializer_class = GroupSerializer

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Group.objects.none()

        return Group.objects.filter(owner=self.request.user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @swagger_auto_schema(
        request_body=GroupBulkDeleteSerializer,
        responses={status.HTTP_204_NO_CONTENT: []},
        operation_id="groups_bulk_delete",
    )
    @action(
        detail=False,
        methods=["DELETE"],
        url_path="bulk-delete",
    )
    def bulk_destroy(self, request):
        serializer = GroupBulkDeleteSerializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)

        groups = self.get_queryset().filter(pk__in=serializer.validated_data["group_ids"])
        groups.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(
        request_body=GroupKinSerializer,
        responses={status.HTTP_200_OK: GroupSerializer(many=True)},
        operation_id="groups_add_kin",
    )
    @action(detail=False, methods=["POST"], url_path="add-kin")
    def add_kin_to_groups(self, request):
        group_kin_serializer = GroupKinSerializer(data=request.data, context={"request": request})

        if not group_kin_serializer.is_valid():
            return Response(group_kin_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        group_ids = group_kin_serializer.data["group_ids"]
        kin_ids = group_kin_serializer.data["kin_ids"]
        source = group_kin_serializer.data.get("source")

        kins = Kin.objects.filter(pk__in=kin_ids)
        for k in kins:
            k.groups.add(*group_ids)
            k.save()

        groups = (
            Group.objects.filter(pk__in=group_ids)
            .annotate(kin_count=Count("kin"))
            .order_by("-kin_count")
            .distinct()
        )
        output_serializer = GroupSerializer(groups, many=True)

        for g in groups:
            add_or_remove_kin_to_group_mixpanel_event.delay(g.id, kins.count(), source)

        return Response(output_serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        request_body=GroupKinSerializer,
        responses={status.HTTP_200_OK: GroupSerializer(many=True)},
        operation_id="groups_remove_kin",
    )
    @action(detail=False, methods=["POST"], url_path="remove-kin")
    def remove_kin_from_groups(self, request):
        group_kin_serializer = GroupKinSerializer(data=request.data, context={"request": request})

        if not group_kin_serializer.is_valid():
            return Response(group_kin_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        group_ids = group_kin_serializer.data["group_ids"]
        kin_ids = group_kin_serializer.data["kin_ids"]
        source = group_kin_serializer.data.get("source")

        kins = Kin.objects.filter(pk__in=kin_ids)
        for k in kins:
            k.groups.remove(*group_ids)
            k.save()

        groups = (
            Group.objects.filter(pk__in=group_ids)
            .annotate(kin_count=Count("kin"))
            .order_by("-kin_count")
            .distinct()
        )
        output_serializer = GroupSerializer(groups, many=True)

        for g in groups:
            add_or_remove_kin_to_group_mixpanel_event.delay(g.id, -kins.count(), source)

        return Response(output_serializer.data, status=status.HTTP_200_OK)


class GroupMemberViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = GroupMemberSerializer
    filterset_class = GroupMemberFilter

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Kin.objects.none()

        return Kin.objects.filter(owner=self.request.user, groups=self.kwargs["group_pk"])

    @swagger_auto_schema(
        responses={status.HTTP_200_OK: GroupMemberMailtoResultsSerializer},
        operation_id="groups_members_mailto",
        deprecated=True,
    )
    @action(
        detail=False,
        methods=["GET"],
        url_path="mailto",
        pagination_class=None,
        filterset_class=None,
    )
    def mailto(self, request, *args, **kwargs):
        mailto_results = get_group_members_contact(request.user, self.kwargs["group_pk"])
        return Response(mailto_results, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        responses={status.HTTP_200_OK: GroupMemberMailtoResultsSerializer},
        operation_id="groups_members_contact",
    )
    @action(
        detail=False,
        methods=["GET"],
        url_path="contact",
        pagination_class=None,
        filterset_class=None,
    )
    def contact(self, request, *args, **kwargs):
        contact_results = get_group_members_contact(request.user, self.kwargs["group_pk"])
        return Response(contact_results, status=status.HTTP_200_OK)


class GroupNonMemberViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = GroupMemberSerializer
    filterset_class = GroupMemberFilter

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Kin.objects.none()

        return (
            Kin.objects.filter(owner=self.request.user)
            .exclude(groups=self.kwargs["group_pk"])
            .exclude(userkin__user=self.request.user)
        )
