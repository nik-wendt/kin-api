from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from elasticsearch_dsl import TermsFacet, FacetedSearch, RangeFacet

from apps.groups.models import Group
from apps.search.utils import (
    trigram_analyzer,
    search_date_ranges,
    location_properties,
    prepare_location_city_country,
)


@registry.register_document
class GroupDocument(Document):
    id = fields.TextField()
    owner = fields.ObjectField(
        properties={"id": fields.KeywordField(), "email": fields.TextField()}
    )
    location = fields.NestedField(properties=location_properties)
    city_country = fields.KeywordField()
    name = fields.TextField(analyzer=trigram_analyzer)
    created = fields.DateField()

    class Index:
        name = "groups"
        settings = {"number_of_shards": 1, "number_of_replicas": 0}

    class Django:
        model = Group
        fields = []

    def prepare_city_country(self, instance):
        return prepare_location_city_country(instance)


class GroupFacetedSearch(FacetedSearch):
    index = "groups"
    doc_types = [GroupDocument]
    fields = [
        "name",
    ]

    facets = {
        "location": TermsFacet(field="city_country"),
        "created": RangeFacet(
            field="created",
            ranges=search_date_ranges,
        ),
    }

    def __init__(self, query=None, filters={}, sort=(), user=None):
        self._model = Group
        self._user = user
        super().__init__(query, filters, sort)

    def search(self):
        s = super().search()
        return s.filter("term", owner__id=self._user.id)

    def highlight(self, search):
        """
        Add highlighting for all the fields
        """
        return search.highlight(
            *(f if "^" not in f else f.split("^", 1)[0] for f in self.fields),
            pre_tags=["|"],
            post_tags=["|"]
        )
