from django.db.models import OuterRef, Prefetch, Subquery

from apps.facts.choices import FactTypeEnum
from apps.facts.models import Fact
from apps.groups.serializers import GroupMemberMailtoResultsSerializer
from apps.kin.models import Kin


def get_group_members_contact(user, group_id):
    email_facts = Subquery(
        Fact.objects.filter(kin=OuterRef("kin"), fact_type=FactTypeEnum.EMAIL).values_list(
            "id", flat=True
        )
    )
    phone_facts = Subquery(
        Fact.objects.filter(kin=OuterRef("kin"), fact_type=FactTypeEnum.PHONE).values_list(
            "id", flat=True
        )
    )

    members = Kin.objects.filter(owner=user, groups=group_id).prefetch_related(
        Prefetch("facts", queryset=Fact.objects.filter(id__in=email_facts), to_attr="emails"),
        Prefetch(
            "facts", queryset=Fact.objects.filter(id__in=phone_facts), to_attr="phone_numbers"
        ),
    )

    results = [
        {
            "id": member.id,
            "friendly_name": member.friendly_name,
            "profile_pic": member.profile_pic,
            "emails": [
                {"email": f.value, "primary": f.metadata["email_primary"]} for f in member.emails
            ],
            "phones": [
                {
                    "phone_number": f.value,
                    "primary": f.metadata["phone_primary"],
                    "accepts_sms": f.metadata["phone_accepts_sms"],
                }
                for f in member.phone_numbers
            ],
        }
        for member in members
    ]

    serializer = GroupMemberMailtoResultsSerializer({"count": members.count(), "results": results})

    return serializer.data
