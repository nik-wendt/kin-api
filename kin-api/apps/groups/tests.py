from django.conf import settings
from django.db.models import Count
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

import redis
import shortuuid

from apps.groups.models import Group
from apps.groups.serializers import GroupSerializer
from apps.kin.models import Kin
from apps.users.models import User


class GroupCreateTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )
        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.group = Group.objects.create(name="Bar", owner=self.user1)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_create_group_no_auth(self):
        response = self.loggedOutClient.post(reverse("group-list"), {"name": "Foo"})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_group(self):
        response = self.loggedInClient1.post(reverse("group-list"), {"name": "Foo"})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_group_existing(self):
        response = self.loggedInClient1.post(reverse("group-list"), {"name": "Bar"})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class GroupUpdateTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.group = Group.objects.create(name="Bar", owner=self.user1)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_update_group_no_auth(self):
        response = self.loggedOutClient.patch(reverse("group-detail", kwargs={"pk": self.group.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_group_by_owner(self):
        response = self.loggedInClient1.patch(
            reverse("group-detail", kwargs={"pk": self.group.pk}), {"name": "Foo"}
        )
        self.assertEqual(response.data["name"], "Foo")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_group_by_non_owner(self):
        response = self.loggedInClient2.patch(
            reverse("group-detail", kwargs={"pk": self.group.pk}), {"name": "Foo"}
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GroupDeleteTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.group1 = Group.objects.create(name="Foo", owner=self.user1)
        self.group2 = Group.objects.create(name="Bar", owner=self.user1)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_delete_group_no_auth(self):
        response = self.loggedOutClient.delete(
            reverse("group-detail", kwargs={"pk": self.group1.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_group_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            reverse("group-detail", kwargs={"pk": self.group1.pk})
        )
        self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

        retrieve_deleted_response = self.loggedInClient1.get(
            reverse("group-detail", kwargs={"pk": self.group1.pk})
        )
        self.assertEqual(retrieve_deleted_response.status_code, status.HTTP_404_NOT_FOUND)

        check_list_count_response = self.loggedInClient1.get(reverse("group-list"))
        check_list_count = check_list_count_response.data["count"]
        self.assertEqual(check_list_count, 1)

    def test_delete_group_by_non_owner(self):
        response = self.loggedInClient2.delete(
            reverse("group-detail", kwargs={"pk": self.group1.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_bulk_delete_no_auth(self):
        delete_response = self.loggedOutClient.delete(
            f"{reverse('group-list')}bulk-delete/", {"group_ids": [self.group1.pk, self.group2.pk]}
        )
        self.assertEqual(delete_response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_bulk_delete_with_valid_group_ids_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            f"{reverse('group-list')}bulk-delete/", {"group_ids": [self.group1.pk, self.group2.pk]}
        )
        self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

        retrieve_deleted_response_group1 = self.loggedInClient1.get(
            reverse("group-detail", kwargs={"pk": self.group1.pk})
        )
        self.assertEqual(retrieve_deleted_response_group1.status_code, status.HTTP_404_NOT_FOUND)

        retrieve_deleted_response_group2 = self.loggedInClient1.get(
            reverse("group-detail", kwargs={"pk": self.group2.pk})
        )
        self.assertEqual(retrieve_deleted_response_group2.status_code, status.HTTP_404_NOT_FOUND)

        check_list_count_response = self.loggedInClient1.get(reverse("group-list"))
        check_list_count = check_list_count_response.data["count"]
        self.assertEqual(check_list_count, 0)

    def test_bulk_delete_with_invalid_group_ids_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            f"{reverse('group-list')}bulk-delete/", {"group_ids": [shortuuid.uuid()]}
        )
        self.assertEqual(delete_response.status_code, status.HTTP_400_BAD_REQUEST)


class GroupGetTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.group1 = Group.objects.create(name="Foo", owner=self.user1)
        self.group2 = Group.objects.create(name="Bar", owner=self.user1)
        self.group3 = Group.objects.create(name="Baz", owner=self.user1)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_get_group_list_no_auth(self):
        response = self.loggedOutClient.get(reverse("group-list"))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_group_detail_no_auth(self):
        response = self.loggedOutClient.get(reverse("group-detail", kwargs={"pk": self.group1.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_group_list_by_non_owner(self):
        response = self.loggedInClient2.get(reverse("group-list"))
        results_count = response.data["count"]
        results = response.data["results"]
        self.assertEqual(results_count, 0)
        self.assertEqual(results, [])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_group_detail_by_non_owner(self):
        response = self.loggedInClient2.get(reverse("group-detail", kwargs={"pk": self.group1.pk}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_group_list_by_owner(self):
        response = self.loggedInClient1.get(reverse("group-list"))
        results_count = response.data["count"]
        results = response.data["results"]

        groups = (
            Group.objects.filter(owner=self.user1)
            .annotate(kin_count=Count("kin"))
            .order_by("-kin_count")
            .distinct()
        )
        serializer = GroupSerializer(groups, many=True)

        self.assertEqual(results_count, 3)
        self.assertEqual(serializer.data, results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_group_detail_by_owner(self):
        response = self.loggedInClient1.get(reverse("group-detail", kwargs={"pk": self.group1.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        group = Group.objects.annotate(kin_count=Count("kin")).get(pk=self.group1.pk)
        serializer = GroupSerializer(group)

        self.assertEqual(response.data, serializer.data)


class GroupAddKinTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user2)

        self.group1 = Group.objects.create(name="Foo", owner=self.user1)
        self.group2 = Group.objects.create(name="Bar", owner=self.user1)
        self.group3 = Group.objects.create(name="Baz", owner=self.user1)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_add_kin_to_group_no_auth(self):
        response = self.loggedOutClient.post(
            f"{reverse('group-list')}add-kin/",
            {"group_ids": [self.group1.pk, self.group2.pk], "kin_ids": [self.bob.pk]},
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_add_kin_to_group_by_non_owned_group_and_kin(self):
        response = self.loggedInClient2.post(
            f"{reverse('group-list')}add-kin/",
            {"group_ids": [self.group1.pk, self.group2.pk], "kin_ids": [self.bob.pk]},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_kin_to_group_by_owned_group_and_kin(self):
        response = self.loggedInClient1.post(
            f"{reverse('group-list')}add-kin/",
            {"group_ids": [self.group1.pk, self.group2.pk], "kin_ids": [self.bob.pk]},
        )

        self.assertEqual(len(response.data), 2)
        for g in response.data:
            self.assertEqual(g["kin_count"], 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_remove_kin_from_group_no_auth(self):
        response = self.loggedOutClient.post(
            f"{reverse('group-list')}remove-kin/",
            {"group_ids": [self.group1.pk, self.group2.pk], "kin_ids": [self.bob.pk]},
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_remove_kin_from_group_by_non_owned_group_and_kin(self):
        response = self.loggedInClient2.post(
            f"{reverse('group-list')}remove-kin/",
            {"group_ids": [self.group1.pk, self.group2.pk], "kin_ids": [self.bob.pk]},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_remove_kin_from_group_by_owned_group_and_kin(self):
        self.group1.kin.add(self.bob.pk)
        self.group1.refresh_from_db()
        self.assertEqual(self.group1.kin.count(), 1)

        response = self.loggedInClient1.post(
            f"{reverse('group-list')}remove-kin/",
            {"group_ids": [self.group1.pk], "kin_ids": [self.bob.pk]},
        )

        self.assertEqual(response.data[0]["kin_count"], 0)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GroupMemberTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        self.john = Kin.objects.create(friendly_name="John", owner=self.user1)
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user2)

        self.group1 = Group.objects.create(name="Foo", owner=self.user1)
        self.group2 = Group.objects.create(name="Bar", owner=self.user1)
        self.group3 = Group.objects.create(name="Baz", owner=self.user1)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_get_group_members_no_auth(self):
        response = self.loggedOutClient.get(f"{reverse('group-list')}{self.group1.pk}/members/")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_group_members_by_non_owner(self):
        self.group1.kin.add(self.bob)

        response = self.loggedInClient2.get(f"{reverse('group-list')}{self.group1.pk}/members/")
        results_count = response.data["count"]
        results = response.data["results"]
        self.assertEqual(results_count, 0)
        self.assertEqual(results, [])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_group_members_by_owner(self):
        self.group1.kin.add(self.bob)

        response = self.loggedInClient1.get(f"{reverse('group-list')}{self.group1.pk}/members/")

        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_group_nonmembers_no_auth(self):
        response = self.loggedOutClient.get(f"{reverse('group-list')}{self.group1.pk}/nonmembers/")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_group_nonmembers_by_non_owner(self):
        self.group1.kin.add(self.bob)

        response = self.loggedInClient2.get(f"{reverse('group-list')}{self.group1.pk}/nonmembers/")
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_group_nonmembers_by_owner(self):
        self.group1.kin.add(self.bob)

        response = self.loggedInClient1.get(f"{reverse('group-list')}{self.group1.pk}/nonmembers/")

        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
