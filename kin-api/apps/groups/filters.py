from django.db.models import Count, Q

from django_filters import rest_framework as filters

from apps.groups.models import Group
from apps.kin.filters import KinSortFilter
from apps.kin.models import Kin


class GroupSortFilter(filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.extra["choices"] += [
            ("kin_count", "kin_count"),
            ("-kin_count", "-kin_count"),
            ("name", "name"),
            ("-name", "-name"),
        ]

    def filter(self, qs, value):
        if not value:
            value = ["-kin_count"]

        if any(v in ["-kin_count"] for v in value):
            qs = qs.annotate(kin_count=Count("kin")).order_by("-kin_count").distinct()

            return qs

        if any(v in ["kin_count"] for v in value):
            qs = qs.annotate(kin_count=Count("kin")).order_by("kin_count").distinct()

            return qs

        return super().filter(qs, value)


class GroupFilter(filters.FilterSet):
    search = filters.CharFilter(method="search_group")
    sort = GroupSortFilter(
        fields=("name", "name"),
        help_text="Choices are <strong>name</strong>, <strong>-name</strong>,  <strong>kin_count</strong>, or <strong>-kin_count</strong>. Default: <strong>-kin_count</strong>.",
    )

    class Meta:
        model = Group
        fields = ["search"]

    def search_group(self, queryset, _, value):
        if value:
            queryset = queryset.filter(name__icontains=value)

        return queryset


class GroupMemberFilter(filters.FilterSet):
    search = filters.CharFilter(method="search_kin")
    has_reminder = filters.BooleanFilter(
        method="filter_by_has_reminder", help_text="<strong>true</strong> or <strong>false</strong>"
    )
    sort = KinSortFilter(
        fields=("friendly_name", "friendly_name"),
        help_text="Choices are <strong>friendly_name</strong>, <strong>-friendly_name</strong>,  <strong>latest</strong>, <strong>-latest</strong>, or <strong>popularity</strong>. Default: <strong>popularity</strong>.",
    )

    class Meta:
        model = Kin
        fields = ["search", "has_reminder", "sort"]

    def search_kin(self, queryset, _, value):
        if value:
            queryset = queryset.filter(
                Q(first_name__icontains=value)
                | Q(last_name__icontains=value)
                | Q(friendly_name__icontains=value)
            )

        return queryset

    def filter_by_has_reminder(self, queryset, _, value):

        if isinstance(value, bool):
            is_null = not value
            queryset = queryset.filter(reminder__isnull=is_null)

        return queryset
