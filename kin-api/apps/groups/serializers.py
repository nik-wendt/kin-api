from rest_framework import serializers

from apps.activity_log.choices import ActivityTypeEnum
from apps.activity_log.models import LogActivity
from apps.groups.models import Group
from apps.kin.models import Kin
from apps.kin.serializers import KinPrimaryKeyRelatedField, InlineReminderSerializer


class GroupSerializer(serializers.ModelSerializer):
    kin_count = serializers.IntegerField(read_only=True)
    is_favorite = serializers.BooleanField(read_only=True)

    class Meta:
        model = Group
        fields = ("id", "name", "kin_count", "location", "is_favorite")

    def validate_name(self, value):
        user = self._context["request"].user

        if Group.objects.filter(name__iexact=value, owner=user).exists():
            raise serializers.ValidationError("A group with this name already exists.")

        return value

    def create(self, validated_data):
        group = super().create(validated_data)
        LogActivity.objects.generate_log_activity(ActivityTypeEnum.TAG_CREATE, obj=group)
        return group


class GroupBulkDeleteSerializer(serializers.Serializer):
    group_ids = serializers.ListField(
        child=serializers.CharField(max_length=50), help_text="An array of group ids", required=True
    )

    def validate_group_ids(self, value):
        user = self._context["request"].user

        for group_id in value:
            if not Group.objects.filter(pk=group_id, owner=user).exists():
                raise serializers.ValidationError("Invalid group")

        return value


class GroupPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        user = self.context["request"].user
        queryset = Group.objects.filter(owner=user)
        return queryset


class GroupKinSerializer(serializers.Serializer):
    group_ids = GroupPrimaryKeyRelatedField(
        queryset=Group.objects.all(), many=True, help_text="An array of group ids"
    )
    kin_ids = KinPrimaryKeyRelatedField(
        queryset=Kin.objects.all(), many=True, help_text="An array of kin ids"
    )
    source = serializers.CharField(required=False)


class GroupMemberSerializer(serializers.ModelSerializer):
    reminder = InlineReminderSerializer(read_only=True)

    class Meta:
        model = Kin
        fields = ("id", "friendly_name", "profile_pic", "reminder")


class GroupMemberMailtoSerializer(serializers.Serializer):
    id = serializers.CharField()
    friendly_name = serializers.CharField()
    profile_pic = serializers.ImageField()
    emails = serializers.ListField(child=serializers.DictField())
    phones = serializers.ListField(child=serializers.DictField())


class GroupMemberMailtoResultsSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    results = GroupMemberMailtoSerializer(many=True)
