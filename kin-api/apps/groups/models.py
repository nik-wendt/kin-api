from django.db import models
from django.utils.functional import cached_property

from django_extensions.db.fields import ShortUUIDField

from apps.favorites.models import Favorite
from apps.kin_extensions.fields import LocationField
from apps.users.models import User


class Group(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    name = models.CharField(max_length=50, db_column="tag")
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="kin_groups")
    created = models.DateTimeField(auto_now_add=True)
    location = LocationField(null=True, blank=True)

    class Meta:
        db_table = "tags_tag"
        unique_together = ("name", "owner")

    def __str__(self):
        return f"{self.name}: {self.owner.id}"

    def save(self, *args, **kwargs):
        if self._state.adding:
            self.full_clean()
        return super().save(**kwargs)

    def kin_count(self):
        return self.kin.count()

    @cached_property
    def is_favorite(self):
        return Favorite.objects.filter(object_id=self.id).exists()
