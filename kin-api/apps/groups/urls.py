from rest_framework_nested import routers

from apps.groups.views import GroupMemberViewSet, GroupNonMemberViewSet, GroupViewSet


router = routers.DefaultRouter()
router.register("", GroupViewSet, basename="group")

group_member_router = routers.NestedDefaultRouter(router, "", lookup="group")
group_member_router.register("members", GroupMemberViewSet, basename="group-member")
group_member_router.register("nonmembers", GroupNonMemberViewSet, basename="group-nonmember")

urlpatterns = router.urls + group_member_router.urls
