import logging
import re
import uuid

from celery import shared_task
from django.db import transaction

from apps.calendar.models import Calendar, Event
from apps.facts.choices import FactTypeEnum
from apps.facts.models import Fact
from apps.kin.models import Kin
from apps.kin_extensions.constants import EMAIL_REGEX
from apps.spotlight.tasks import update_upcoming_events_cache_task
from apps.users.models import User


logger = logging.getLogger(__name__)


@shared_task
def sync_calendars(*, user_id, calendar_sync_data):
    user = User.objects.get(pk=user_id)

    logger.info(
        "Started syncing %s calendar(s) for user with id: %s.",
        len(calendar_sync_data),
        user.id,
    )

    try:
        with transaction.atomic():
            # Deleting stale calendar data. This is our 'brute force' approach,
            # until we can use EventKit to make it more event (pubsub) driven.
            Calendar.objects.filter(owner=user).delete()

            for calendar_data in calendar_sync_data:
                calendar = Calendar.objects.create(
                    source_id=calendar_data["source_id"],
                    title=calendar_data["title"],
                    owner=user,
                )

                for event_data in calendar_data["events"]:
                    attendee_data_dict = {
                        attendee["email"].lower(): attendee["name"]
                        for attendee in event_data["attendees"]
                        if attendee["email"] and attendee["email"].lower() != user.email
                    }

                    for attendee in event_data["attendees"]:
                        if not attendee["email"]:
                            attendee_data_dict[str(uuid.uuid4())] = attendee["name"]

                    if attendee_data_dict:
                        attendee_emails_iregex = r"(" + "|".join(attendee_data_dict.keys()) + ")"

                        kin = Kin.objects.filter(
                            facts__value__iregex=attendee_emails_iregex,
                            facts__fact_type=FactTypeEnum.EMAIL,
                            owner=user,
                        )
                        non_kin_attendee_emails = set(attendee_data_dict.keys()).difference(
                            set(
                                [
                                    f.lower()
                                    for f in Fact.objects.filter(
                                        kin__in=kin, fact_type=FactTypeEnum.EMAIL
                                    ).values_list("value", flat=True)
                                ]
                            )
                        )
                        non_kin_attendees = [
                            {
                                "email": attendee_email
                                if re.match(EMAIL_REGEX, attendee_email)
                                else None,
                                "name": attendee_data_dict[attendee_email],
                            }
                            for attendee_email in non_kin_attendee_emails
                        ]

                        event = Event.objects.create(
                            source_id=event_data["source_id"],
                            title=event_data["title"],
                            start_date=event_data["start_date"],
                            end_date=event_data["end_date"],
                            all_day=event_data.get("all_day", False),
                            location=event_data.get("location", ""),
                            calendar=calendar,
                            non_kin_attendees=non_kin_attendees,
                        )
                        event.attendees.add(*kin)

        update_upcoming_events_cache_task.delay(user.id)

        logger.info(
            "Finished syncing %s calendar(s) for user with id: %s.",
            len(calendar_sync_data),
            user.id,
        )
    except Exception as e:
        logger.error("Unable to perform calendar sync for user with id: %s. %s", user.id, str(e))
