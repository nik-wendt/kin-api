from django.apps import AppConfig


class CalendarConfig(AppConfig):
    name = "apps.calendar"
    verbose_name = "Calendar"
