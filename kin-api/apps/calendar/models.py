from django.db.models import JSONField
from django.db import models

from django_extensions.db.fields import ShortUUIDField

from apps.kin.models import Kin
from apps.users.models import User


class Calendar(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    source_id = models.UUIDField()
    title = models.CharField(max_length=255)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="calendars")
    last_synced = models.DateTimeField(auto_now_add=True)


class Event(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    source_id = models.UUIDField()
    calendar = models.ForeignKey(Calendar, on_delete=models.CASCADE, related_name="events")
    title = models.CharField(max_length=255)
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)
    all_day = models.BooleanField(default=False)
    location = models.TextField(blank=True)
    attendees = models.ManyToManyField(Kin, related_name="events")
    non_kin_attendees = JSONField(blank=True, null=True, default=None)
