import logging

from rest_framework import permissions, serializers, status
from rest_framework.response import Response
from rest_framework.views import APIView

from drf_yasg.utils import swagger_auto_schema

from apps.calendar.serializers import CalendarSyncSerializer
from apps.calendar.tasks import sync_calendars
from apps.kin_auth.permissions import IsPaid, IsVerified


logger = logging.getLogger(__name__)


class CalendarSyncView(APIView):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)

    @swagger_auto_schema(
        request_body=CalendarSyncSerializer(many=True),
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="calendars_sync",
    )
    def post(self, request, *args, **kwargs):
        serializer = CalendarSyncSerializer(data=request.data, many=True)

        if serializer.is_valid(raise_exception=False):
            sync_calendars.delay(user_id=request.user.id, calendar_sync_data=serializer.data)

            return Response(status=status.HTTP_200_OK)
        else:
            logger.info("Error syncing calendars: %s", serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
