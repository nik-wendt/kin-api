from rest_framework import serializers


class AttendeeSyncNestedSerializer(serializers.Serializer):
    email = serializers.EmailField(allow_null=True)
    name = serializers.CharField(max_length=255, allow_blank=True, required=False)


class EventSyncNestedSerializer(serializers.Serializer):
    source_id = serializers.UUIDField()
    title = serializers.CharField(max_length=255)
    start_date = serializers.DateTimeField()
    end_date = serializers.DateTimeField()
    all_day = serializers.BooleanField(required=False)
    location = serializers.CharField(allow_blank=True, required=False)
    attendees = AttendeeSyncNestedSerializer(allow_null=True, many=True)


class CalendarSyncSerializer(serializers.Serializer):
    source_id = serializers.UUIDField()
    title = serializers.CharField(max_length=255)
    events = EventSyncNestedSerializer(allow_null=True, required=False, many=True)
