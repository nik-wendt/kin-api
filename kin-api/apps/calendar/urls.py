from django.urls import path

from apps.calendar.views import CalendarSyncView


urlpatterns = [path("sync/", CalendarSyncView.as_view(), name="sync")]
