from enum import Enum


class WebhookEventEnum(Enum):
    KIN_CREATED = "kin_created"
    KIN_UPDATED = "kin_updated"
    GROUP_CREATED = "group_created"
    GROUP_UPDATED = "group_updated"
    CONNECTION_CREATED = "connection_created"
    CONNECTION_UPDATED = "connection_updated"
