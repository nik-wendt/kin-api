from rest_framework import serializers

from django_enum_choices.serializers import EnumChoiceModelSerializerMixin
from oauth2_provider.models import get_application_model

from apps.developers.models import WebhookSubscription


class ApplicationRegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_application_model()
        fields = (
            "id",
            "name",
            "client_type",
            "authorization_grant_type",
            "redirect_uris",
            "algorithm",
        )


class ApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_application_model()
        fields = "__all__"


class WebhookSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = WebhookSubscription
        exclude = ("owner",)
