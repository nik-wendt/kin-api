from django.contrib.postgres.fields import ArrayField
from django.db import models

from django_enum_choices.fields import EnumChoiceField
from django_extensions.db.fields import ShortUUIDField

from apps.developers.choices import WebhookEventEnum
from apps.users.models import User


class WebhookSubscription(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    target = models.URLField()
    events = ArrayField(base_field=EnumChoiceField(WebhookEventEnum))
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="webhook_subscriptions")
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
