from rest_framework import routers

from apps.developers.views import ApplicationViewSet, WebhookSubscriptionViewSet


router = routers.DefaultRouter()
router.register("applications", ApplicationViewSet, basename="applications")
router.register("webhooks", WebhookSubscriptionViewSet, basename="webhooks")


urlpatterns = router.urls
