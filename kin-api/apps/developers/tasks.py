import requests
from celery import shared_task


@shared_task
def send_webhook(target, event, data):
    resp = requests.post(target, json={"event": event, "data": data})

    # TODO Need to handle error retry logic based on response status code
