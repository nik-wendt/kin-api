from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.connections.models import Connection
from apps.connections.serializers import ConnectionSerializer
from apps.developers.choices import WebhookEventEnum
from apps.developers.models import WebhookSubscription
from apps.developers.tasks import send_webhook
from apps.facts.models import Fact
from apps.groups.models import Group
from apps.groups.serializers import GroupSerializer
from apps.kin.models import Kin
from apps.kin.serializers import KinSerializer


@receiver(post_save, sender=Kin)
def call_kin_webhooks(sender, instance, created, **kwargs):
    hooks = WebhookSubscription.objects.filter(owner=instance.owner)

    if not hooks.exists():
        return

    kin_serializer = KinSerializer(instance)

    if created:
        event = "kin_created"
        hooks = hooks.filter(events__contains=[WebhookEventEnum.KIN_CREATED,], owner=instance.owner)
    else:
        event = "kin_updated"
        hooks = hooks.filter(events__contains=[WebhookEventEnum.KIN_UPDATED,], owner=instance.owner)

    for hook in hooks:
        send_webhook.delay(hook.target, event, kin_serializer.data)


@receiver(post_save, sender=Fact)
def call_kin_fact_webhooks(sender, instance, *args, **kwargs):
    hooks = WebhookSubscription.objects.filter(owner=instance.kin.owner)

    if not hooks.exists():
        return

    kin_serializer = KinSerializer(instance.kin)

    hooks = hooks.filter(events__contains=[WebhookEventEnum.KIN_UPDATED,], owner=instance.kin.owner)
    for hook in hooks:
        send_webhook.delay(hook.target, "kin_updated", kin_serializer.data)


@receiver(post_save, sender=Group)
def call_group_webhooks(sender, instance, created, **kwargs):
    hooks = WebhookSubscription.objects.filter(owner=instance.owner)

    if not hooks.exists():
        return

    group_serializer = GroupSerializer(instance)

    if created:
        event = "group_created"
        hooks = hooks.filter(
            events__contains=[WebhookEventEnum.GROUP_CREATED,], owner=instance.owner
        )
    else:
        event = "group_updated"
        hooks = hooks.filter(
            events__contains=[WebhookEventEnum.GROUP_UPDATED,], owner=instance.owner
        )

    for hook in hooks:
        send_webhook.delay(hook.target, event, group_serializer.data)


@receiver(post_save, sender=Connection)
def call_connection_webhooks(sender, instance, created, **kwargs):
    hooks = WebhookSubscription.objects.filter(owner=instance.kin.owner)

    if not hooks.exists():
        return

    connection_serializer = ConnectionSerializer(instance)

    if created:
        event = "connection_created"
        hooks = hooks.filter(
            events__contains=[WebhookEventEnum.CONNECTION_CREATED,], owner=instance.kin.owner
        )
    else:
        event = "connection_updated"
        hooks = hooks.filter(
            events__contains=[WebhookEventEnum.CONNECTION_UPDATED,], owner=instance.kin.owner
        )

    for hook in hooks:
        send_webhook.delay(hook.target, event, connection_serializer.data)
