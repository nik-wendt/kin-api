from django.apps import AppConfig


class DevelopersConfig(AppConfig):
    name = "apps.developers"
    verbose_name = "Developers"

    def ready(self):
        import apps.developers.signals
