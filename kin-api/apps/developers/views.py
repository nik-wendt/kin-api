from rest_framework import permissions, viewsets

from oauth2_provider.models import get_application_model

from apps.developers.models import WebhookSubscription
from apps.developers.serializers import (
    ApplicationRegistrationSerializer,
    ApplicationSerializer,
    WebhookSerializer,
)
from apps.kin_auth.permissions import IsPaid, IsVerified


class ApplicationViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return get_application_model().objects.none()

        qs = get_application_model().objects.filter(user=self.request.user)

        return qs

    def get_serializer_class(self):
        if self.action == "create":
            return ApplicationRegistrationSerializer

        return ApplicationSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class WebhookSubscriptionViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = WebhookSerializer

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return WebhookSubscription.objects.none()

        qs = WebhookSubscription.objects.filter(owner=self.request.user)

        return qs

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
