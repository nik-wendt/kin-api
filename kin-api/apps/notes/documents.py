from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from elasticsearch_dsl import FacetedSearch, TermsFacet, RangeFacet

from apps.notes.models import Note
from apps.search.utils import (
    trigram_analyzer,
    ImgUrlField,
    search_date_ranges,
    location_properties,
    prepare_location_city_country,
)


@registry.register_document
class NoteDocument(Document):
    id = fields.TextField()
    owner = fields.ObjectField(
        properties={"id": fields.KeywordField(), "email": fields.TextField()}
    )
    location = fields.NestedField(properties=location_properties)
    city_location = fields.KeywordField()
    kin = fields.ObjectField(
        properties={
            "id": fields.TextField(),
            "friendly_name": fields.TextField(),
            "profile_pic": ImgUrlField(),
        }
    )
    source = fields.TextField()
    text = fields.TextField(analyzer=trigram_analyzer)
    created = fields.DateField()

    class Index:
        name = "notes"
        settings = {"number_of_shards": 1, "number_of_replicas": 0}

    class Django:
        model = Note
        fields = [
            "modified",
        ]

    def prepare_source(self, instance):
        if instance.source:
            return instance.source.value
        return ""

    def prepare_city_country(self, instance):
        return prepare_location_city_country(instance)


class NotesFacetedSearch(FacetedSearch):
    index = "notes"
    doc_types = [NoteDocument]
    fields = [
        "text",
    ]

    facets = {
        "location": TermsFacet(field="city_country"),
        "created": RangeFacet(
            field="created",
            ranges=search_date_ranges,
        ),
    }

    def __init__(self, query=None, filters={}, sort=(), user=None):
        self._model = Note
        self._user = user
        super().__init__(query, filters, sort)

    def search(self):
        s = super().search()
        return s.filter("term", owner__id=self._user.id)

    def highlight(self, search):
        """
        Add highlighting for all the fields
        """
        return search.highlight(
            *(f if "^" not in f else f.split("^", 1)[0] for f in self.fields),
            pre_tags=["|"],
            post_tags=["|"]
        )
