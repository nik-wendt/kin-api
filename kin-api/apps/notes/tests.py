import os
import shutil
from unittest.mock import MagicMock

from django.conf import settings
from django.core.files import File
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

import redis
import shortuuid

from apps.kin.models import Kin
from apps.notes.models import Note, NoteMedia
from apps.notes.serializers import NoteMediaSerializer, NoteReadSerializer
from apps.users.models import User


TEST_FILE_DIR = "test_data"


class NoteCreateTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )
        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user2)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_create_note_no_auth(self):
        response = self.loggedOutClient.post(
            reverse("note-list"),
            {"text": "Bacon ipsum dolor amet fatback ball tip chicken meatloaf."},
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_unassigned_note(self):
        response = self.loggedInClient1.post(
            reverse("note-list"),
            {"text": "Bacon ipsum dolor amet fatback ball tip chicken meatloaf."},
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_assigned_note(self):
        response = self.loggedInClient1.post(
            reverse("note-list"),
            {
                "text": "Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
                "kin": self.bob.pk,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_note_with_non_owned_kin(self):
        response = self.loggedInClient1.post(
            reverse("note-list"),
            {
                "text": "Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
                "kin": self.alice.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class NoteUpdateTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user2)

        self.note1 = Note.objects.create(
            text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            raw_text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            kin=self.bob,
            owner=self.user1,
        )
        self.note2 = Note.objects.create(
            text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            raw_text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            owner=self.user1,
        )

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_update_note_no_auth(self):
        response = self.loggedOutClient.patch(reverse("note-detail", kwargs={"pk": self.note1.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_note_by_owner(self):
        response = self.loggedInClient1.patch(
            reverse("note-detail", kwargs={"pk": self.note1.pk}),
            {"text": "Short loin pork alcatra."},
        )
        self.assertEqual(response.data["text"], "Short loin pork alcatra.")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_note_by_non_owner(self):
        response = self.loggedInClient2.patch(
            reverse("note-detail", kwargs={"pk": self.note1.pk}),
            {"text": "Short loin pork alcatra."},
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_update_owned_note_with_non_owned_kin(self):
        response = self.loggedInClient1.patch(
            reverse("note-detail", kwargs={"pk": self.note2.pk}),
            {"text": "Short ribs tongue swine.", "kin": self.alice.id},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class NoteDeleteTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user2)

        self.note1 = Note.objects.create(
            text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            raw_text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            kin=self.bob,
            owner=self.user1,
        )
        self.note2 = Note.objects.create(
            text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            raw_text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            owner=self.user1,
        )

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_delete_note_no_auth(self):
        response = self.loggedOutClient.delete(reverse("note-detail", kwargs={"pk": self.note1.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_note_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            reverse("note-detail", kwargs={"pk": self.note1.pk})
        )
        self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

        retrieve_deleted_response = self.loggedInClient1.get(
            reverse("note-detail", kwargs={"pk": self.note1.pk})
        )
        self.assertEqual(retrieve_deleted_response.status_code, status.HTTP_404_NOT_FOUND)

        check_list_count_response = self.loggedInClient1.get(reverse("note-list"))
        check_list_count = check_list_count_response.data["count"]
        self.assertEqual(check_list_count, 1)

    def test_delete_note_by_non_owner(self):
        response = self.loggedInClient2.delete(reverse("note-detail", kwargs={"pk": self.note2.pk}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class NoteGetTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)

        self.note1 = Note.objects.create(
            text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            raw_text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            kin=self.bob,
            owner=self.user1,
        )
        self.note2 = Note.objects.create(
            text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            raw_text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            owner=self.user1,
        )
        self.note3 = Note.objects.create(
            text="Salami ham turkey capicola shoulder ribeye pork chop andouille.",
            raw_text="Salami ham turkey capicola shoulder ribeye pork chop andouille.",
            owner=self.user1,
        )

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_get_note_list_no_auth(self):
        response = self.loggedOutClient.get(reverse("note-list"))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_note_detail_no_auth(self):
        response = self.loggedOutClient.get(reverse("note-detail", kwargs={"pk": self.note1.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_note_list_by_non_owner(self):
        response = self.loggedInClient2.get(reverse("note-list"))
        results_count = response.data["count"]
        results = response.data["results"]
        self.assertEqual(results_count, 0)
        self.assertEqual(results, [])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_note_detail_by_non_owner(self):
        response = self.loggedInClient2.get(reverse("note-detail", kwargs={"pk": self.note1.pk}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_note_list_by_owner(self):
        response = self.loggedInClient1.get(reverse("note-list"))
        results_count = response.data["count"]
        results = response.data["results"]

        notes = Note.objects.filter(owner=self.user1)
        serializer = NoteReadSerializer(notes, many=True)

        self.assertEqual(results_count, 3)
        self.assertEqual(serializer.data, results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_note_detail_by_owner(self):
        response = self.loggedInClient1.get(reverse("note-detail", kwargs={"pk": self.note1.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        note = Note.objects.get(pk=self.note1.pk)
        serializer = NoteReadSerializer(note)

        self.assertEqual(response.data, serializer.data)

    def test_get_note_list_filtered_by_kin(self):
        response = self.loggedInClient1.get(reverse("note-list"), {"kin": self.bob.id})
        results_count = response.data["count"]
        results = response.data["results"]

        notes = Note.objects.filter(owner=self.user1, kin=self.bob.id)
        serializer = NoteReadSerializer(notes, many=True)

        self.assertEqual(results_count, 1)
        self.assertEqual(serializer.data, results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_note_list_filtered_by_unassigned(self):
        response = self.loggedInClient1.get(reverse("note-list"), {"unassigned": True})
        results_count = response.data["count"]
        results = response.data["results"]

        notes = Note.objects.filter(owner=self.user1, kin__isnull=True)
        serializer = NoteReadSerializer(notes, many=True)

        self.assertEqual(results_count, 2)
        self.assertEqual(serializer.data, results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_note_list_filtered_by_assigned(self):
        response = self.loggedInClient1.get(reverse("note-list"), {"unassigned": False})
        results_count = response.data["count"]
        results = response.data["results"]

        notes = Note.objects.exclude(owner=self.user1, kin__isnull=True)
        serializer = NoteReadSerializer(notes, many=True)

        self.assertEqual(results_count, 1)
        self.assertEqual(serializer.data, results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class NoteMediaCreateTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)

        self.note1 = Note.objects.create(
            text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            raw_text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            kin=self.bob,
            owner=self.user1,
        )
        self.note2 = Note.objects.create(
            text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            raw_text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            owner=self.user2,
        )

        self.file_mock = MagicMock(spec=File, name="FileMock")
        self.file_mock.name = "test1.jpg"
        os.mkdir(TEST_FILE_DIR)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()
        shutil.rmtree(TEST_FILE_DIR)

    def test_create_note_media_no_auth(self):
        with self.settings(
            DEFAULT_FILE_STORAGE="django.core.files.storage.FileSystemStorage",
            MEDIA_ROOT=TEST_FILE_DIR,
        ):
            response = self.loggedOutClient.post(
                reverse("note-media-list", kwargs={"note_pk": self.note1.pk}),
                {"media": self.file_mock},
            )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_note_media(self):
        with self.settings(
            DEFAULT_FILE_STORAGE="django.core.files.storage.FileSystemStorage",
            MEDIA_ROOT=TEST_FILE_DIR,
        ):
            response = self.loggedInClient1.post(
                reverse("note-media-list", kwargs={"note_pk": self.note1.pk}),
                {"media": self.file_mock},
                format="multipart",
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_note_media_with_non_owned_note(self):
        with self.settings(
            DEFAULT_FILE_STORAGE="django.core.files.storage.FileSystemStorage",
            MEDIA_ROOT=TEST_FILE_DIR,
        ):
            response = self.loggedInClient1.post(
                reverse("note-media-list", kwargs={"note_pk": self.note2.pk}),
                {"media": self.file_mock},
            )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class NoteMediaUpdateTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user2)

        self.note1 = Note.objects.create(
            text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            raw_text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            kin=self.bob,
            owner=self.user1,
        )
        self.note2 = Note.objects.create(
            text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            raw_text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            owner=self.user2,
        )

        self.file_mock = MagicMock(spec=File, name="FileMock")
        self.file_mock.name = "test1.jpg"

        with self.settings(
            DEFAULT_FILE_STORAGE="django.core.files.storage.FileSystemStorage",
            MEDIA_ROOT=TEST_FILE_DIR,
        ):
            self.note_media_1 = NoteMedia.objects.create(
                media=self.file_mock, note=self.note1, mime_type="image/jpeg"
            )
            self.note_media_2 = NoteMedia.objects.create(
                media=self.file_mock, note=self.note1, mime_type="image/jpeg", is_cover=True
            )

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_update_note_media_no_auth(self):
        response = self.loggedOutClient.patch(
            reverse(
                "note-media-detail", kwargs={"note_pk": self.note1.pk, "pk": self.note_media_1.pk}
            ),
            {"is_cover": True},
        )
        self.assertEqual(response.status_code, status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)

    def test_update_note_media_by_owner(self):
        response = self.loggedInClient1.patch(
            reverse(
                "note-media-detail", kwargs={"note_pk": self.note1.pk, "pk": self.note_media_1.pk}
            ),
            {"is_cover": True},
            format="json",
        )
        self.assertEqual(response.data["is_cover"], True)

        note_is_cover_count = NoteMedia.objects.filter(note=self.note1.pk, is_cover=True).count()
        self.assertEqual(note_is_cover_count, 1)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_note_media_by_non_owner(self):
        response = self.loggedInClient2.patch(
            reverse(
                "note-media-detail", kwargs={"note_pk": self.note1.pk, "pk": self.note_media_1.pk}
            ),
            {"is_cover": True},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class NoteMediaDeleteTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)
        self.alice = Kin.objects.create(friendly_name="Alice", owner=self.user2)

        self.note1 = Note.objects.create(
            text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            raw_text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            kin=self.bob,
            owner=self.user1,
        )
        self.note2 = Note.objects.create(
            text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            raw_text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            owner=self.user2,
        )

        self.file_mock = MagicMock(spec=File, name="FileMock")
        self.file_mock.name = "test1.jpg"

        with self.settings(
            DEFAULT_FILE_STORAGE="django.core.files.storage.FileSystemStorage",
            MEDIA_ROOT=TEST_FILE_DIR,
        ):
            self.note_media_1 = NoteMedia.objects.create(
                media=self.file_mock, note=self.note1, mime_type="image/jpeg"
            )
            self.note_media_2 = NoteMedia.objects.create(
                media=self.file_mock, note=self.note1, mime_type="image/jpeg", is_cover=True
            )
            self.note_media_3 = NoteMedia.objects.create(
                media=self.file_mock, note=self.note1, mime_type="image/jpeg"
            )

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()

    def test_delete_note_media_no_auth(self):
        with self.settings(
            DEFAULT_FILE_STORAGE="django.core.files.storage.FileSystemStorage",
            MEDIA_ROOT=TEST_FILE_DIR,
        ):
            response = self.loggedOutClient.delete(
                reverse(
                    "note-media-detail",
                    kwargs={"note_pk": self.note1.pk, "pk": self.note_media_1.pk},
                )
            )
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_note_media_by_owner(self):
        with self.settings(
            DEFAULT_FILE_STORAGE="django.core.files.storage.FileSystemStorage",
            MEDIA_ROOT=TEST_FILE_DIR,
        ):
            delete_response = self.loggedInClient1.delete(
                reverse(
                    "note-media-detail",
                    kwargs={"note_pk": self.note1.pk, "pk": self.note_media_2.pk},
                )
            )
            self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

            retrieve_deleted_response = self.loggedInClient1.get(
                reverse(
                    "note-media-detail",
                    kwargs={"note_pk": self.note1.pk, "pk": self.note_media_2.pk},
                )
            )
            self.assertEqual(retrieve_deleted_response.status_code, status.HTTP_404_NOT_FOUND)

            check_list_count_response = self.loggedInClient1.get(
                reverse("note-media-list", kwargs={"note_pk": self.note1.pk})
            )
            check_list_count = check_list_count_response.data["count"]
            self.assertEqual(check_list_count, 2)

            # should_be_cover_media = NoteMedia.objects.get(pk=self.note_media_1.pk)
            # self.assertEqual(should_be_cover_media.is_cover, True)

    def test_delete_note_media_by_non_owner(self):
        with self.settings(
            DEFAULT_FILE_STORAGE="django.core.files.storage.FileSystemStorage",
            MEDIA_ROOT=TEST_FILE_DIR,
        ):
            response = self.loggedInClient2.delete(
                reverse(
                    "note-media-detail",
                    kwargs={"note_pk": self.note1.pk, "pk": self.note_media_1.pk},
                )
            )
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_bulk_delete_note_media_no_auth(self):
        with self.settings(
            DEFAULT_FILE_STORAGE="django.core.files.storage.FileSystemStorage",
            MEDIA_ROOT=TEST_FILE_DIR,
        ):
            delete_response = self.loggedOutClient.delete(
                f"{reverse('note-media-list', kwargs={'note_pk': self.note1.pk})}bulk-delete/",
                {"note_media_ids": [self.note_media_1.pk, self.note_media_2.pk]},
            )
            self.assertEqual(delete_response.status_code, status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)

    def test_bulk_delete_with_valid_note_media_ids_by_owner(self):
        with self.settings(
            DEFAULT_FILE_STORAGE="django.core.files.storage.FileSystemStorage",
            MEDIA_ROOT=TEST_FILE_DIR,
        ):
            delete_response = self.loggedInClient1.delete(
                f"{reverse('note-media-list', kwargs={'note_pk': self.note1.pk})}bulk-delete/",
                {"note_media_ids": [self.note_media_1.pk, self.note_media_2.pk]},
                format="json",
            )
            self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

            retrieve_deleted_response_note_media_1 = self.loggedInClient1.get(
                reverse(
                    "note-media-detail",
                    kwargs={"note_pk": self.note1.pk, "pk": self.note_media_1.pk},
                )
            )
            self.assertEqual(
                retrieve_deleted_response_note_media_1.status_code, status.HTTP_404_NOT_FOUND
            )

            retrieve_deleted_response_note_media_2 = self.loggedInClient1.get(
                reverse(
                    "note-media-detail",
                    kwargs={"note_pk": self.note1.pk, "pk": self.note_media_2.pk},
                )
            )
            self.assertEqual(
                retrieve_deleted_response_note_media_2.status_code, status.HTTP_404_NOT_FOUND
            )

            check_list_count_response = self.loggedInClient1.get(
                reverse("note-media-list", kwargs={"note_pk": self.note1.pk})
            )
            check_list_count = check_list_count_response.data["count"]
            self.assertEqual(check_list_count, 1)

            should_be_cover_media = NoteMedia.objects.get(pk=self.note_media_3.pk)
            self.assertEqual(should_be_cover_media.is_cover, True)

    def test_bulk_delete_with_invalid_note_media_ids_by_owner(self):
        delete_response = self.loggedInClient1.delete(
            f"{reverse('note-media-list', kwargs={'note_pk': self.note1.pk})}bulk-delete/",
            {"note_media_ids": [shortuuid.uuid()]},
            format="json",
        )
        self.assertEqual(delete_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_bulk_delete_with_valid_note_media_ids_by_non_owner(self):
        delete_response = self.loggedInClient2.delete(
            f"{reverse('note-media-list', kwargs={'note_pk': self.note1.pk})}bulk-delete/",
            {"note_media_ids": [self.note_media_1.pk, self.note_media_2.pk]},
            format="json",
        )
        self.assertEqual(delete_response.status_code, status.HTTP_400_BAD_REQUEST)


class NoteMediaGetTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            email="test@userone.com", password="Password123!", is_verified=True, is_active=True
        )

        self.user2 = User.objects.create_user(
            email="test@usertwo.com", password="Password123!", is_verified=True, is_active=True
        )

        self.loggedInClient1 = APIClient()
        self.loggedInClient1.force_authenticate(user=self.user1)

        self.loggedInClient2 = APIClient()
        self.loggedInClient2.force_authenticate(user=self.user2)

        self.loggedOutClient = APIClient()
        self.loggedOutClient.force_authenticate(user=None)

        self.bob = Kin.objects.create(friendly_name="Bob", owner=self.user1)

        self.note1 = Note.objects.create(
            text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            raw_text="Bacon ipsum dolor amet fatback ball tip chicken meatloaf.",
            kin=self.bob,
            owner=self.user1,
        )
        self.note2 = Note.objects.create(
            text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            raw_text="Beef ribs jerky capicola, buffalo prosciutto turducken ham hock bresaola pork belly chislic.",
            owner=self.user2,
        )

        self.file_mock = MagicMock(spec=File, name="FileMock")
        self.file_mock.name = "test1.jpg"

        with self.settings(
            DEFAULT_FILE_STORAGE="django.core.files.storage.FileSystemStorage",
            MEDIA_ROOT=TEST_FILE_DIR,
        ):
            self.note_media_1 = NoteMedia.objects.create(media=self.file_mock, note=self.note1)

    def tearDown(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.flushdb()
        shutil.rmtree(TEST_FILE_DIR)

    def test_get_note_media_list_no_auth(self):
        response = self.loggedOutClient.get(
            reverse("note-media-list", kwargs={"note_pk": self.note1.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_note_detail_no_auth(self):
        response = self.loggedOutClient.get(
            reverse(
                "note-media-detail", kwargs={"note_pk": self.note1.pk, "pk": self.note_media_1.pk}
            )
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_note_media_list_by_non_owner(self):
        response = self.loggedInClient2.get(
            reverse("note-media-list", kwargs={"note_pk": self.note1.pk})
        )
        results_count = response.data["count"]
        results = response.data["results"]
        self.assertEqual(results_count, 0)
        self.assertEqual(results, [])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_note_media_detail_by_non_owner(self):
        response = self.loggedInClient2.get(
            reverse(
                "note-media-detail", kwargs={"note_pk": self.note1.pk, "pk": self.note_media_1.pk}
            )
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_note_media_list_by_owner(self):
        response = self.loggedInClient1.get(
            reverse("note-media-list", kwargs={"note_pk": self.note1.pk})
        )
        results_count = response.data["count"]
        results = response.data["results"]

        note_media = NoteMedia.objects.filter(note__owner=self.user1)
        serializer = NoteMediaSerializer(note_media, many=True)

        self.assertEqual(results_count, 1)
        self.assertEqual(serializer.data, results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_note_media_detail_by_owner(self):
        response = self.loggedInClient1.get(
            reverse(
                "note-media-detail", kwargs={"note_pk": self.note1.pk, "pk": self.note_media_1.pk}
            )
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        note_media = NoteMedia.objects.get(pk=self.note_media_1.pk)
        serializer = NoteMediaSerializer(note_media)

        self.assertEqual(response.data, serializer.data)
