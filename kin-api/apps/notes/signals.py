from django.core.files.storage import default_storage
from django.db.models.signals import post_delete
from django.dispatch import receiver

from apps.notes.models import NoteMedia
from apps.notes.tasks import delete_media


@receiver(post_delete, sender=NoteMedia)
def delete_associated_media(sender, instance, **kwargs):
    file_name = instance.media

    if file_name and default_storage.exists(str(file_name)):
        delete_media.delay(str(file_name))
