import logging
import os
import uuid

from celery import shared_task
from django.conf import settings
from django.core.files.base import File
from django.core.files.storage import default_storage
from storages.backends.s3boto3 import S3Boto3Storage

import ffmpeg

from apps.notes.models import NoteMedia
from apps.notifications.utils import send_notification


logger = logging.getLogger(__name__)


@shared_task
def delete_media(file_name):
    default_storage.delete(file_name)


@shared_task
def transcode_video(note_media_id):
    note_media = NoteMedia.objects.get(pk=note_media_id)
    original_file_name = note_media.media.name
    user_id = note_media.note.owner.id

    storage = S3Boto3Storage()

    file_path, ext = os.path.splitext(original_file_name)
    base_file_name = os.path.basename(file_path)
    temp_filename = uuid.uuid4()
    transcoding_file_path = f"/tmp/{temp_filename}_processing{ext}"
    transcoded_file_path = f"/tmp/{temp_filename}.mp4"

    storage.bucket.meta.client.download_file(
        settings.AWS_STORAGE_BUCKET_NAME, original_file_name, transcoding_file_path
    )

    try:
        ffmpeg.input(transcoding_file_path).output(
            transcoded_file_path, **{"crf": 28}
        ).overwrite_output().run()
    except Exception as e:
        send_notification("Unfortunately we were unable to process your uploaded video.", user_id)
        logger.error(
            "Error processing video %s. %s", note_media.id, str(e), extra={"user_id": user_id}
        )
        note_media.delete()
        return

    fh = open(transcoded_file_path, "rb")
    out_file = File(fh)

    note_media.is_processing = False
    note_media.mime_type = "video/mp4"
    note_media.media.save(f"{base_file_name}.mp4", out_file)

    send_notification("Good news! Your video is done processing and is now available.", user_id)

    # cleanup
    os.remove(transcoding_file_path)
    os.remove(transcoded_file_path)
    default_storage.delete(original_file_name)


@shared_task
def transcode_audio(note_media_id):
    note_media = NoteMedia.objects.get(pk=note_media_id)
    original_file_name = note_media.media.name
    user_id = note_media.note.owner.id

    storage = S3Boto3Storage()

    file_path, ext = os.path.splitext(original_file_name)
    base_file_name = os.path.basename(file_path)
    temp_filename = uuid.uuid4()
    transcoding_file_path = f"/tmp/{temp_filename}_processing{ext}"
    transcoded_file_path = f"/tmp/{temp_filename}.mp3"

    storage.bucket.meta.client.download_file(
        settings.AWS_STORAGE_BUCKET_NAME, original_file_name, transcoding_file_path
    )

    try:
        ffmpeg.input(transcoding_file_path).output(transcoded_file_path).overwrite_output().run()
    except Exception as e:
        send_notification("Unfortunately we were unable to process your uploaded audio.", user_id)
        logger.error(
            "Error processing audio %s. %s", note_media.id, str(e), extra={"user_id": user_id}
        )
        note_media.delete()
        return

    fh = open(transcoding_file_path, "rb")
    out_file = File(fh)

    note_media.is_processing = False
    note_media.mime_type = "audio/mpeg"
    note_media.media.save(f"{base_file_name}.mp3", out_file)

    send_notification("Good news! Your audio is done processing and is now available.", user_id)

    # cleanup
    os.remove(transcoding_file_path)
    os.remove(transcoded_file_path)
    default_storage.delete(original_file_name)
