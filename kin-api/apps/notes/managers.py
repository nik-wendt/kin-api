from django.db import models
from django.db.models import Prefetch


class NoteManager(models.Manager):
    def get_queryset(self):
        from apps.notes.models import NoteMedia

        return (
            super()
            .get_queryset()
            .prefetch_related(
                Prefetch("note_media", queryset=NoteMedia.objects.exclude(is_processing=True))
            )
        )
