import logging
from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils import timezone

from apps.notes.models import Note


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Cleans up empty notes older than 24 hours."

    def handle(self, *args, **options):
        logger.info("Starting empty note cleanup...")
        one_day_ago = timezone.now() - timedelta(hours=24)
        notes = Note.objects.filter(note_media__isnull=True, raw_text="", created__lte=one_day_ago)

        empty_note_ids = ", ".join(notes.values_list("id", flat=True))
        logging_msg = f"Deleting empty notes: {empty_note_ids}"
        logger.info(logging_msg)

        notes.delete()

        logger.info("Finshed empty note cleanup.")
