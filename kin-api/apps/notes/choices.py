from enum import Enum


class MediaTypeEnum(Enum):
    IMAGE = "IMAGE"
    FILE = "FILE"


class NoteSourceEnum(Enum):
    STASH = "STASH"
    KIN_DETAIL = "KIN_DETAIL"
    SMS = "SMS"
    SHARE = "SHARE"
    SHARE_TOKEN = "SHARE_TOKEN"
    EMAIL = "EMAIL"
    SYSTEM = "SYSTEM"
