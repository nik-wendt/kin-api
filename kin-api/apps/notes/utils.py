import re

from django.core.validators import URLValidator, ValidationError

from bs4 import BeautifulSoup


def markup_text_to_raw_text(markup_string):
    markup_string = re.sub(r">\s*<", "><", markup_string)
    content = BeautifulSoup(markup_string, "html.parser")
    empty_tags = content.findAll(lambda tag: not tag.contents)
    [empty_tag.extract() for empty_tag in empty_tags]

    for p_tag in content.find_all("p"):
        new_tag = content.new_tag("p")
        if p_tag.contents:
            if p_tag.get_text() != "":
                if ":" in p_tag.get_text():
                    new_tag.string = p_tag.get_text()
                else:
                    new_tag.string = p_tag.get_text() + " "
        p_tag.replace_with(new_tag)

    for ul_tag in content.find_all("ul"):
        if ul_tag.findPrevious():
            if ul_tag.findPrevious().name == "p":
                new_tag = content.new_tag("p")
                if ul_tag.findPrevious().string:
                    new_tag.string = ul_tag.findPrevious().string + ": "
                    ul_tag.findPrevious().replace_with(new_tag)
        if ul_tag.contents:
            for li_tag in ul_tag.find_all("li"):
                if len(ul_tag.find_all("li")) != 1:
                    new_tag = content.new_tag("p")
                    if li_tag.string:
                        new_tag.string = li_tag.string.strip() + ", "
                    li_tag.replace_with(new_tag)
            new_tag = content.new_tag("p")
            new_tag.string = "; "
            ul_tag.insert_after(new_tag)

    for ol_tag in content.find_all("ol"):
        if ol_tag.findPrevious():
            if ol_tag.findPrevious().name == "p":
                new_tag = content.new_tag("p")
                if ol_tag.findPrevious().string:
                    new_tag.string = ol_tag.findPrevious().string + ": "
                    ol_tag.findPrevious().replace_with(new_tag)
        if ol_tag.contents:
            for li_tag in ol_tag.find_all("li"):
                if len(ol_tag.find_all("li")) != 1:
                    new_tag = content.new_tag("p")
                    if li_tag.string:
                        new_tag.string = li_tag.string.strip() + ", "
                    li_tag.replace_with(new_tag)
            new_tag = content.new_tag("p")
            new_tag.string = "; "
            ol_tag.insert_after(new_tag)

    for tag in content.find_all():
        new_tag = content.new_tag("p")
        if tag.contents:
            if tag.string is not None:
                new_tag.string = tag.string.strip() + " "
                tag.replace_with(new_tag)

    return content.get_text()


def markup_text_to_plain_text(markup_string):
    content = BeautifulSoup(markup_string, features="html.parser")

    validate_url = URLValidator()

    for a_tag in content.find_all("a", href=True):
        a_tag_text = a_tag.get_text()

        try:
            validate_url(a_tag_text)
            a_tag.replace_with(a_tag_text)
        except ValidationError:
            a_tag.replace_with(f"{a_tag_text} ({a_tag['href']})")

    for ul_tag in content.find_all("ul"):
        for li_tag in ul_tag.find_all("li"):
            li_tag.replace_with("- " + li_tag.get_text() + "\n")

    for ol_tag in content.find_all("ol"):
        for i, li_tag in enumerate(ol_tag.find_all("li")):
            li_tag.replace_with(str(i + 1) + ". " + li_tag.get_text() + "\n")

    for p_tag in content.find_all("p"):
        p_tag.replace_with(p_tag.get_text() + "\n")

    return content.get_text()
