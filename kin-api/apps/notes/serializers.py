import logging
import json
import os
import uuid
from PIL import Image

from django.core.files.images import get_image_dimensions
from django.core.files.storage import default_storage
from rest_framework import serializers
from django.core.files.base import File
from django.conf import settings

import magic
import reversion
from django_enum_choices.serializers import EnumChoiceField, EnumChoiceModelSerializerMixin
from reversion.models import Version
from storages.backends.s3boto3 import S3Boto3Storage

from apps.activity_log.choices import ActivityTypeEnum
from apps.activity_log.models import LogActivity
from apps.integrations.mixpanel.tasks import track_note_created
from apps.kin.models import Kin
from apps.notes.choices import NoteSourceEnum
from apps.notes.models import Note, NoteMedia
from apps.notes.tasks import transcode_video, transcode_audio
from apps.notes.utils import markup_text_to_plain_text, markup_text_to_raw_text
from apps.notifications.utils import send_notification


logger = logging.getLogger(__name__)


class NoteMediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = NoteMedia
        fields = "__all__"
        read_only_fields = (
            "id",
            "mime_type",
            "width",
            "height",
            "is_cover",
            "is_processing",
            "created",
            "note",
        )

    def convert_image(self, note_media_id):
        note_media = NoteMedia.objects.get(pk=note_media_id)
        original_file_name = note_media.media.name
        user_id = note_media.note.owner.id

        storage = S3Boto3Storage()

        file_path, ext = os.path.splitext(original_file_name)
        base_file_name = os.path.basename(file_path)
        temp_filename = uuid.uuid4()
        transcoding_file_path = f"/tmp/{temp_filename}_processing{ext}"
        transcoded_file_path = f"/tmp/{temp_filename}.jpg"

        storage.bucket.meta.client.download_file(
            settings.AWS_STORAGE_BUCKET_NAME, original_file_name, transcoding_file_path
        )

        try:
            im = Image.open(transcoding_file_path)
            rgb_im = im.convert("RGB")
            rgb_im.save(transcoded_file_path, quality=95)

        except Exception as e:
            send_notification(
                "Unfortunately we were unable to process your uploaded image.", user_id
            )
            logger.error(
                "Error processing image %s. %s", note_media.id, str(e), extra={"user_id": user_id}
            )
            note_media.delete()
            return

        fh = open(transcoded_file_path, "rb")
        out_file = File(fh)

        note_media.is_processing = False
        note_media.mime_type = "image/jpeg"
        note_media.media.save(f"{base_file_name}.jpg", out_file)

        # cleanup
        os.remove(transcoding_file_path)
        os.remove(transcoded_file_path)
        default_storage.delete(original_file_name)

    def create(self, validated_data):
        mime_type = magic.from_buffer(validated_data["media"].read(), mime=True)
        validated_data["mime_type"] = mime_type

        width, height = get_image_dimensions(validated_data["media"].file)
        validated_data["width"] = width
        validated_data["height"] = height

        if (
            mime_type.startswith("image/")
            and not validated_data["note"]
            .note_media.filter(mime_type__istartswith="image/")
            .exists()
        ):
            validated_data["is_cover"] = True
        elif mime_type.startswith("video/") or mime_type.startswith("audio/"):
            validated_data["is_processing"] = True

        instance = super().create(validated_data)

        if mime_type.startswith("video/"):
            transcode_video.delay(instance.id)
        elif mime_type.startswith("audio/"):
            transcode_audio.delay(instance.id)
        elif mime_type.startswith("image/"):
            self.convert_image(instance.id)
            image = NoteMedia.objects.get(pk=instance.id)
            return image

        return instance


class NoteMediaUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = NoteMedia
        fields = ("is_cover",)

    def update(self, instance, validated_data):

        if "is_cover" in validated_data:
            if validated_data["is_cover"]:
                if not instance.mime_type.startswith("image/"):
                    raise serializers.ValidationError(
                        {"is_cover": "Only images may be marked as a cover."}
                    )

                instance.note.note_media.update(is_cover=False)
                instance.is_cover = True

        return super().update(instance, validated_data)


class NoteMediaBulkDeleteSerializer(serializers.Serializer):
    note_media_ids = serializers.ListField(
        child=serializers.CharField(), help_text="An array of note media ids", required=True
    )

    def validate_note_media_ids(self, value):
        user = self._context["request"].user
        note = Note.objects.get(pk=self._context["note_pk"])

        for note_media_id in value:
            if not NoteMedia.objects.filter(pk=note_media_id, note=note, note__owner=user).exists():
                raise serializers.ValidationError("Invalid note media")

        return value


class InlineNoteKinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kin
        fields = ("id", "friendly_name", "profile_pic")


class NoteReadSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    kin = InlineNoteKinSerializer(required=False)
    media = NoteMediaSerializer(source="note_media", many=True, read_only=True)
    media_counts = serializers.DictField(read_only=True)
    source = EnumChoiceField(
        NoteSourceEnum,
        allow_null=True,
        default=None,
        help_text="Choices are <strong>STASH</strong>, <strong>KIN_DETAIL</strong>, <strong>SMS</strong>, <strong>SHARE</strong>, or <strong>SHARE_TOKEN</strong>. Default: <strong>STASH</strong>.",
    )

    class Meta:
        model = Note
        exclude = ("raw_text", "owner")
        read_only_fields = ("id", "plain_text", "kin", "media", "media_counts")


class NoteWriteSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    media_counts = serializers.DictField(read_only=True)
    source = EnumChoiceField(
        NoteSourceEnum,
        allow_null=True,
        default=None,
        help_text="Choices are <strong>STASH</strong>, <strong>KIN_DETAIL</strong>, <strong>SMS</strong>, <strong>SHARE</strong>, or <strong>SHARE_TOKEN</strong>. Default: <strong>STASH</strong>.",
    )

    class Meta:
        model = Note
        exclude = ("raw_text", "owner", "plain_text")

    def validate_kin(self, value):
        if value:
            user = self._context[
                "request"
            ].user  # TODO: need to try and not get user off of request object as it makes this flow not work outside of a request context

            try:
                # need to ensure the kin is owned by the logged in user
                Kin.objects.get(pk=value.id, owner=user)
            except Kin.DoesNotExist:
                raise serializers.ValidationError("Invalid kin")

        return value

    def create(self, validated_data):
        validated_data["raw_text"] = markup_text_to_raw_text(validated_data["text"])
        validated_data["plain_text"] = markup_text_to_plain_text(validated_data["text"])
        instance = super().create(validated_data)

        LogActivity.objects.generate_log_activity(ActivityTypeEnum.NUGGET_CREATE, obj=instance)

        track_note_created.delay(instance.id)

        return instance

    def update(self, instance, validated_data):
        kin = validated_data.get("kin", instance.kin)
        text = validated_data.get("text", instance.text)

        try:
            if instance.source:
                _ = validated_data.pop(
                    "source"
                )  # We don't want to change source if it's already been set.
        except KeyError:
            pass

        instance.raw_text = markup_text_to_raw_text(text)
        instance.plain_text = markup_text_to_plain_text(text)

        with reversion.create_revision():
            note = super().update(instance, validated_data)

        LogActivity.objects.generate_log_activity(ActivityTypeEnum.NUGGET_EDIT, obj=note)

        return note


class NoteVersionSerializer(serializers.ModelSerializer):
    text = serializers.SerializerMethodField()
    version = serializers.SerializerMethodField()
    created = serializers.SerializerMethodField()

    class Meta:
        model = Version
        exclude = (
            "id",
            "object_id",
            "db",
            "format",
            "serialized_data",
            "revision",
            "content_type",
            "object_repr",
        )

    def get_text(self, obj):
        version_data = json.loads(obj.serialized_data)
        return version_data[0]["fields"]["text"]

    def get_version(self, obj):
        return obj.revision_id

    def get_created(self, obj):
        return obj.revision.date_created
