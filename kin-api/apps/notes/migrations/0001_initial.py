# Generated by Django 3.0.3 on 2020-04-28 01:37

import apps.notes.models
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [("kin", "0001_initial")]

    operations = [
        migrations.CreateModel(
            name="Note",
            fields=[
                (
                    "order",
                    models.PositiveIntegerField(
                        db_index=True, editable=False, verbose_name="order"
                    ),
                ),
                (
                    "id",
                    django_extensions.db.fields.ShortUUIDField(
                        blank=True, editable=False, primary_key=True, serialize=False
                    ),
                ),
                ("text", models.TextField(blank=True)),
                ("raw_text", models.TextField(blank=True)),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("pinned", models.BooleanField(default=False)),
                ("modified", models.DateTimeField(auto_now=True)),
                (
                    "kin",
                    models.ForeignKey(
                        blank=True,
                        db_column="peep_id",
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="notes",
                        to="kin.Kin",
                    ),
                ),
            ],
            options={"db_table": "nuggets_nugget", "ordering": ("order",), "abstract": False},
        ),
        migrations.CreateModel(
            name="NoteMedia",
            fields=[
                (
                    "order",
                    models.PositiveIntegerField(
                        db_index=True, editable=False, verbose_name="order"
                    ),
                ),
                (
                    "id",
                    django_extensions.db.fields.ShortUUIDField(
                        blank=True, editable=False, primary_key=True, serialize=False
                    ),
                ),
                ("media", models.FileField(upload_to=apps.notes.models.note_media_path)),
                ("mime_type", models.CharField(blank=True, max_length=100)),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                (
                    "note",
                    models.ForeignKey(
                        db_column="nugget_id",
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="note_media",
                        to="notes.Note",
                    ),
                ),
            ],
            options={"db_table": "nuggets_nuggetmedia", "ordering": ("order",), "abstract": False},
        ),
    ]
