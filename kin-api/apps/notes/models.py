from django.db import models
from django.db.models import Q

import reversion
from django_enum_choices.fields import EnumChoiceField
from django_extensions.db.fields import ShortUUIDField

from apps.kin.models import Kin
from apps.kin_extensions.fields import LocationField
from apps.notes.choices import NoteSourceEnum
from apps.notes.managers import NoteManager
from apps.users.models import User


@reversion.register(exclude=["kin", "raw_text", "plain_text"], ignore_duplicates=True)
class Note(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="notes")
    kin = models.ForeignKey(
        Kin,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="notes",
        db_column="peep_id",
    )
    text = models.TextField(blank=True)
    plain_text = models.TextField(blank=True)
    raw_text = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    source = EnumChoiceField(NoteSourceEnum, null=True, blank=True, default=None)
    location = LocationField(null=True, blank=True)

    objects = NoteManager()

    class Meta:
        db_table = "nuggets_nugget"
        ordering = ("-created",)

    def save(self, *args, **kwargs):
        if self._state.adding:
            self.full_clean()
        return super().save(**kwargs)

    @property
    def media_counts(self):
        image_count = self.note_media.filter(mime_type__istartswith="image/").count()
        video_count = self.note_media.filter(mime_type__istartswith="video/").count()
        file_count = self.note_media.exclude(
            Q(mime_type__istartswith="image/") | Q(mime_type__istartswith="video/")
        ).count()

        return {"image_count": image_count, "video_count": video_count, "file_count": file_count}


def note_media_path(instance, filename):
    return f"nuggets/owner-{instance.note.owner.id}/nugget-{instance.note.id}/{filename}"


class NoteMedia(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    note = models.ForeignKey(
        Note, on_delete=models.CASCADE, related_name="note_media", db_column="nugget_id"
    )
    media = models.FileField(upload_to=note_media_path)
    mime_type = models.CharField(max_length=100, blank=True)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    is_cover = models.BooleanField(default=False)
    is_processing = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "nuggets_nuggetmedia"
        ordering = ("-created",)
