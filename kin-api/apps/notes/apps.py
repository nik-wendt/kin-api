from django.apps import AppConfig


class NotesConfig(AppConfig):
    name = "apps.notes"
    verbose_name = "Notes"

    def ready(self):
        import apps.notes.signals
