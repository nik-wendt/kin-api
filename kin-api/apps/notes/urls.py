from rest_framework_nested import routers

from apps.notes.views import NoteMediaViewSet, NoteViewSet


router = routers.DefaultRouter()
router.register("", NoteViewSet, basename="note")

note_media_router = routers.NestedDefaultRouter(router, "", lookup="note")
note_media_router.register("media", NoteMediaViewSet, basename="note-media")

urlpatterns = router.urls + note_media_router.urls
