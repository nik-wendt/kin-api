from django_enum_choices.filters import EnumChoiceFilter, EnumChoiceFilterSetMixin
from django_filters import rest_framework as filters

from apps.notes.choices import MediaTypeEnum
from apps.notes.models import Note


class NoteFilter(filters.FilterSet):
    unassigned = filters.BooleanFilter(method="unassigned_notes", help_text="true or false")
    sort = filters.OrderingFilter(
        fields=(("kin", "kin"), ("created", "created")),
        help_text="Choices are kin, or created. Prefix with a - for descending. Default: -created.",
    )

    class Meta:
        model = Note
        fields = ["kin"]

    def unassigned_notes(self, queryset, _, value):
        if value:
            queryset = queryset.filter(kin__isnull=True)
        else:
            queryset = queryset.exclude(kin__isnull=True)

        return queryset


class NoteMediaFilter(EnumChoiceFilterSetMixin, filters.FilterSet):
    media_type = EnumChoiceFilter(
        MediaTypeEnum, method="get_by_media_type", help_text="Choices are IMAGE or FILE."
    )

    def get_by_media_type(self, queryset, _, value):
        if value == MediaTypeEnum.IMAGE:
            queryset = queryset.filter(mime_type__istartswith="image/")
        elif value == MediaTypeEnum.FILE:
            queryset = queryset.exclude(mime_type__istartswith="image/")

        return queryset
