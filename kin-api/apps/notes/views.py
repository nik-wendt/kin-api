from django.shortcuts import get_object_or_404
from rest_framework import mixins, permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.parsers import FormParser, JSONParser, MultiPartParser
from rest_framework.response import Response

from drf_yasg.utils import swagger_auto_schema
from reversion.models import Version

from apps.kin_auth.permissions import IsPaid, IsVerified
from apps.notes.filters import NoteFilter, NoteMediaFilter
from apps.notes.models import Note, NoteMedia
from apps.notes.serializers import (
    NoteMediaBulkDeleteSerializer,
    NoteMediaSerializer,
    NoteMediaUpdateSerializer,
    NoteReadSerializer,
    NoteVersionSerializer,
    NoteWriteSerializer,
)


class NoteViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    filterset_class = NoteFilter

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Note.objects.none()

        qs = Note.objects.filter(owner=self.request.user)

        if self.action == "list":
            qs = qs.exclude(note_media__isnull=True, raw_text="")

        return qs

    def get_serializer_class(self):
        if self.action in ["create", "update", "partial_update"]:
            return NoteWriteSerializer

        return NoteReadSerializer

    def perform_create(self, serializer):
        note = serializer.save(owner=self.request.user)
        return note

    @swagger_auto_schema(
        request_body=NoteWriteSerializer, responses={status.HTTP_201_CREATED: NoteReadSerializer}
    )
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        note = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        output_serializer = NoteReadSerializer(note)
        return Response(output_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_update(self, serializer):
        note = serializer.save()
        return note

    @swagger_auto_schema(
        request_body=NoteWriteSerializer, responses={status.HTTP_200_OK: NoteReadSerializer}
    )
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        note = self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        output_serializer = NoteReadSerializer(note)
        return Response(output_serializer.data)

    @swagger_auto_schema(
        request_body=NoteWriteSerializer, responses={status.HTTP_200_OK: NoteReadSerializer}
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @swagger_auto_schema(responses={status.HTTP_200_OK: NoteReadSerializer(many=True)})
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(responses={status.HTTP_200_OK: NoteReadSerializer})
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={status.HTTP_200_OK: NoteVersionSerializer(many=True)},
        operation_id="notes_history",
    )
    @action(detail=True, methods=["GET"])
    def history(self, request, pk=None):
        note = self.get_object()
        versions = Version.objects.filter(object_id=note.id)

        output_serializer = NoteVersionSerializer(versions, many=True)

        return Response(output_serializer.data, status=status.HTTP_200_OK)


class NoteMediaViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = NoteMediaSerializer
    filterset_class = NoteMediaFilter
    parser_classes = [FormParser, MultiPartParser]

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return NoteMedia.objects.none()

        return NoteMedia.objects.filter(
            note__owner=self.request.user, note=self.kwargs["note_pk"]
        ).exclude(is_processing=True)

    def get_serializer_class(self):
        if self.action in ["update", "partial_update"]:
            return NoteMediaUpdateSerializer

        return super().get_serializer_class()

    def get_parsers(self):
        if self.request.method in ["PATCH", "PUT", "DELETE"]:
            return [JSONParser()]

        return super().get_parsers()

    def perform_create(self, serializer):
        note = get_object_or_404(Note, pk=self.kwargs["note_pk"], owner=self.request.user)
        serializer.save(note=note)

    def perform_update(self, serializer):
        return serializer.save()

    @swagger_auto_schema(
        request_body=NoteMediaUpdateSerializer, responses={status.HTTP_200_OK: NoteMediaSerializer}
    )
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        updated_instance = self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        success_serializer = NoteMediaSerializer(updated_instance)

        return Response(success_serializer.data)

    @swagger_auto_schema(
        request_body=NoteMediaUpdateSerializer, responses={status.HTTP_200_OK: NoteMediaSerializer}
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    def perform_destroy(self, instance):
        note = instance.note
        was_cover = instance.is_cover

        instance.delete()

        if was_cover:
            new_cover_image = note.note_media.filter(mime_type__istartswith="image/").first()

            if new_cover_image:
                new_cover_image.is_cover = True
                new_cover_image.save()

    @swagger_auto_schema(
        request_body=NoteMediaBulkDeleteSerializer,
        responses={status.HTTP_204_NO_CONTENT: []},
        operation_id="notes_media_bulk_delete",
    )
    @action(detail=False, methods=["DELETE"], url_path="bulk-delete")
    def bulk_destroy(self, request, note_pk=None):
        serializer = NoteMediaBulkDeleteSerializer(
            data=request.data, context={"request": request, "note_pk": note_pk}
        )
        serializer.is_valid(raise_exception=True)

        note_media = self.get_queryset().filter(pk__in=serializer.validated_data["note_media_ids"])
        note_media.delete()

        if not self.get_queryset().filter(is_cover=True).exists():
            new_cover_image = self.get_queryset().filter(mime_type__istartswith="image/").first()

            if new_cover_image:
                new_cover_image.is_cover = True
                new_cover_image.save()

        return Response(status=status.HTTP_204_NO_CONTENT)
