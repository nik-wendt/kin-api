from django.conf import settings
from django.db import models

import redis
from django_extensions.db.fields import ShortUUIDField

from apps.kin_extensions.utils import display_time
from apps.promo_codes.models import PromoCode


class Conference(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    name = models.CharField(max_length=100)
    promo_code = models.OneToOneField(
        PromoCode, on_delete=models.SET_NULL, related_name="conference", null=True
    )
    event_date = models.DateField()
    channel = models.CharField(max_length=25, blank=True)
    campaign = models.CharField(max_length=25, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-event_date", "-created"]

    def __str__(self):
        return self.name

    @property
    def number_of_attendees(self):
        return self.attendees.count()

    @property
    def share_token_ttl(self):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        ttl_seconds = redis_client.ttl(self.id)

        if ttl_seconds <= 0:
            return 0

        return display_time(ttl_seconds)


class ConferenceAttendee(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    conference = models.ForeignKey(Conference, on_delete=models.CASCADE, related_name="attendees")
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(blank=True)
    phone_number = models.CharField(max_length=25, blank=True)
    job_title = models.CharField(max_length=100, blank=True)
    company_name = models.CharField(max_length=100, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.last_name}, {self.first_name}"
