from django.db.models import Q
from django_filters import rest_framework as filters

from apps.conferences.models import Conference, ConferenceAttendee
from apps.promo_codes.models import PromoCode


class ConferenceFilter(filters.FilterSet):
    search = filters.CharFilter(method="search_conference")

    class Meta:
        model = Conference
        fields = ["search"]

    def search_conference(self, queryset, _, value):
        for term in value.split():
            queryset = Conference.objects.filter(
                Q(promo_code__code=term) | Q(id=term) | Q(name=term)
            )
        return queryset


class ConferenceAttendeeFilter(filters.FilterSet):
    search = filters.CharFilter(method="search_attendee")
    conference = filters.CharFilter(method="search_conference")

    class Meta:
        model = ConferenceAttendee
        fields = ["search", "conference"]

    def search_attendee(self, queryset, _, value):
        if value:
            for term in value.split():
                queryset = queryset.filter(
                    Q(email__icontains=term)
                    | Q(first_name__icontains=term)
                    | Q(last_name__icontains=term)
                    | Q(id=term)
                )
        return queryset

    def search_conference(self, queryset, _, value):
        if value:
            queryset = queryset.filter(conference__id=value)
        return queryset


class PromoCodeFilter(filters.FilterSet):
    search = filters.CharFilter(method="search_promo_code")

    class Meta:
        model = PromoCode
        fields = ["search"]

    def search_promo_code(self, queryset, _, value):
        if value:
            queryset = PromoCode.objects.filter(Q(code=value) | Q(id=value))
        return queryset
