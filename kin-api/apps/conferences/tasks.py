import json
from datetime import timedelta

from celery import shared_task
from django.conf import settings
from django.utils import timezone

import redis

from apps.conferences.models import Conference
from apps.facts.choices import FactTypeEnum


@shared_task
def generate_share_token(conference_id):
    conference = Conference.objects.get(pk=conference_id)
    share_dicts = []

    for attendee in conference.attendees.all():
        share_dict = {
            "friendly_name": f"{attendee.first_name} {attendee.last_name}",
            "first_name": attendee.first_name,
            "last_name": attendee.last_name,
            "facts": [],
        }

        if attendee.email:
            share_dict["facts"].append(
                {"key": "Email", "value": attendee.email, "fact_type": FactTypeEnum.EMAIL.value}
            )

        if attendee.phone_number:
            share_dict["facts"].append(
                {
                    "key": "Phone",
                    "value": attendee.phone_number,
                    "fact_type": FactTypeEnum.PHONE.value,
                }
            )

        if attendee.job_title:
            share_dict["facts"].append(
                {
                    "key": "Job Title",
                    "value": attendee.job_title,
                    "fact_type": FactTypeEnum.CUSTOM.value,
                }
            )

        if attendee.company_name:
            share_dict["facts"].append(
                {
                    "key": "Company",
                    "value": attendee.company_name,
                    "fact_type": FactTypeEnum.CUSTOM.value,
                }
            )

        share_dicts.append(share_dict)

    conference_token_expiration_date = conference.event_date + timedelta(seconds=60 * 60 * 24 * 4)
    conference_token_expiration_seconds = int(
        (conference_token_expiration_date - timezone.now().date()).total_seconds()
    )

    if conference_token_expiration_seconds > 0:
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.setex(
            conference.id,
            conference_token_expiration_seconds,
            json.dumps(
                {
                    "group_name": conference.name,
                    "kin": share_dicts,
                    "sharer_name": conference.name,
                }
            ),
        )
