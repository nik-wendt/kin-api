from django.apps import AppConfig


class ConferencesConfig(AppConfig):
    name = "apps.conferences"
    verbose_name = "Conferences"

    def ready(self):
        import apps.conferences.signals
