from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.conferences.models import Conference
from apps.conferences.tasks import generate_share_token


@receiver(post_save, sender=Conference)
def update_conference_share_token(sender, instance, **kwargs):
    generate_share_token.delay(instance.id)
