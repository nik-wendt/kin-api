import logging

from django.conf import settings
from django.utils import timezone
from rest_framework import serializers, status
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

import requests
from drf_yasg.utils import swagger_auto_schema
from google.oauth2 import id_token
from google.auth.transport import requests as google_requests

from apps.integrations.mixpanel.tasks import update_authentication_method
from apps.kin_auth.serializers import LoginSerializer
from apps.kin_auth.utils import create_user_kin
from apps.promo_codes.serializers import RedemptionSerializer
from apps.users.models import User, UserPreferences
from apps.kin_auth.tasks import add_sample_data_to_new_user


logger = logging.getLogger(__name__)


def decode_id_token(token):
    try:
        id_info = id_token.verify_oauth2_token(
            token, google_requests.Request(), settings.GOOGLE_AUTH["client_id"]
        )
        return id_info
    except ValueError:
        raise AuthenticationFailed("Token validation failed")


class GoogleAuthLoginSerializer(serializers.Serializer):
    user_id = serializers.CharField(help_text="Google: user -> id")
    email = serializers.EmailField(allow_null=True, help_text="Google: user -> email")
    first_name = serializers.CharField(allow_null=True, help_text="Google: user -> givenName")
    last_name = serializers.CharField(allow_null=True, help_text="Google: user -> familyName")
    identity_token = serializers.CharField(help_text="Google: idToken")
    authorization_code = serializers.CharField(allow_null=True, help_text="Google: serverAuthCode")
    promo_code = serializers.CharField(required=False, allow_null=True, allow_blank=True)


class GoogleAuthView(GenericAPIView):
    @swagger_auto_schema(
        request_body=GoogleAuthLoginSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="auth_social_google",
    )
    def post(self, request, *args, **kwargs):
        serializer = GoogleAuthLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        payload = {
            "client_id": settings.GOOGLE_AUTH["client_id"],
            "client_secret": settings.GOOGLE_AUTH["client_secret"],
            "code": serializer.data["authorization_code"],
            "grant_type": "authorization_code",
        }

        try:
            r = requests.post("https://oauth2.googleapis.com/token", data=payload)
            google_auth_data = r.json()
            logger.info("Getting token from Google. Here is the auth data %s", google_auth_data)

            decoded_jwt = decode_id_token(google_auth_data["id_token"])
        except KeyError as e:
            logger.error("There was a problem logging in with Google. Error data: %s", str(e))
            raise AuthenticationFailed("Sign in with Google failed.")

        if decoded_jwt["email"] and decoded_jwt["email_verified"]:
            try:
                user = User.objects.get(email=decoded_jwt["email"])
                user.last_login = timezone.now()
                user.save()
            except User.DoesNotExist:
                try:
                    first_name = serializer.data["first_name"]
                    last_name = serializer.data["last_name"]

                    user = User(
                        email=decoded_jwt["email"],
                        password="*",
                        first_name=first_name,
                        last_name=last_name,
                        last_login=timezone.now(),
                        is_verified=True,
                        is_active=True,
                    )
                    user.save()
                    UserPreferences.objects.create(user=user)  # initialize user preferences
                    user = create_user_kin(user.id)
                except Exception as e:
                    logger.error(
                        "There was a problem creating an account with Google. Error data: %s",
                        str(e),
                    )
                    raise

                if serializer.validated_data.get("promo_code", None):
                    redemption_serializer = RedemptionSerializer(
                        data={"promo_code": serializer.validated_data["promo_code"]}
                    )
                    try:
                        redemption_serializer.is_valid(raise_exception=True)
                    except serializers.ValidationError as e:  # We need to do this so validation errors for fields only on the redemption serializer don't leak out to this serializer. For this one, only promo code matters.
                        logger.error(
                            "There was a problem logging in with Google due to a promo code. Error data: %s",
                            str(e),
                        )
                        raise serializers.ValidationError(
                            {"promo_code": "The promo code is invalid or expired."}
                        )

                    redemption_serializer.save(redeemer=user)

                add_sample_data_to_new_user.delay(user.id)
        else:
            logger.error("There was a problem logging in with Google due to no verified account.")
            raise AuthenticationFailed("You must have a verified Google account to login.")

        refresh_token = LoginSerializer.get_token(user)

        update_authentication_method.delay(user.id, "GOOGLE")

        return Response(
            {"access": str(refresh_token.access_token), "refresh": str(refresh_token)},
            status=status.HTTP_200_OK,
        )
