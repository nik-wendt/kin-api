import json
import logging
import time

from django.conf import settings
from django.utils import timezone
from rest_framework import serializers, status
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

import jwt
import requests
from drf_yasg.utils import swagger_auto_schema
from jwt.algorithms import RSAAlgorithm
from jwt.exceptions import PyJWTError

from apps.integrations.mixpanel.tasks import update_authentication_method
from apps.kin_auth.serializers import LoginSerializer
from apps.kin_auth.tasks import add_sample_data_to_new_user
from apps.kin_auth.utils import create_user_kin
from apps.promo_codes.models import PromoCode
from apps.promo_codes.serializers import RedemptionSerializer
from apps.users.models import User, UserPreferences


logger = logging.getLogger(__name__)


def generate_apple_client_secret(private_key):
    now = int(time.time())

    headers = {"kid": settings.APPLE_AUTH["key_id"]}
    payload = {
        "iss": settings.APPLE_AUTH["team_id"],
        "iat": now,
        "exp": now + int(settings.APPLE_AUTH["client_secret_ttl_seconds"]),
        "aud": settings.APPLE_AUTH["token_audience"],
        "sub": settings.APPLE_AUTH["client_id"],
    }

    return jwt.encode(payload, key=private_key, algorithm="ES256", headers=headers)


def get_apple_jwk(kid=None):
    try:
        r = requests.get("https://appleid.apple.com/auth/keys")
        keys = r.json().get("keys")

        if not isinstance(keys, list) or not keys:
            raise AuthenticationFailed("Invalid jwk response")

        if kid:
            return json.dumps([key for key in keys if key["kid"] == kid][0])
        else:
            return (json.dumps(key) for key in keys)
    except KeyError as e:
        raise AuthenticationFailed(str(e))


def decode_id_token(id_token):
    if not id_token:
        raise AuthenticationFailed("Missing id_token parameter")

    try:
        kid = jwt.get_unverified_header(id_token).get("kid")
        public_key = RSAAlgorithm.from_jwk(get_apple_jwk(kid))
        decoded = jwt.decode(
            id_token,
            key=public_key,
            audience=[settings.APPLE_AUTH["client_id"]],
            algorithms=["RS256"],
        )
    except PyJWTError as e:
        logger.info("Apple token validation failed: %s", str(e))
        raise AuthenticationFailed("Token validation failed")

    return decoded


class AppleAuthLoginSerializer(serializers.Serializer):
    user_id = serializers.CharField(help_text="Apple: user")
    user_status = serializers.IntegerField(help_text="Apple: realUserStatus")
    email = serializers.EmailField(allow_null=True, help_text="Apple: email")
    first_name = serializers.CharField(allow_null=True, help_text="Apple: fullName -> givenName")
    last_name = serializers.CharField(allow_null=True, help_text="Apple: fullName -> familyName")
    identity_token = serializers.CharField(help_text="Apple: identityToken")
    authorization_code = serializers.CharField(help_text="Apple: authorizationCode")
    nonce = serializers.CharField(help_text="Apple: nonce")
    promo_code = serializers.CharField(required=False, allow_null=True, allow_blank=True)


class AppleAuthView(GenericAPIView):
    @swagger_auto_schema(
        request_body=AppleAuthLoginSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="auth_social_apple",
    )
    def post(self, request, *args, **kwargs):
        serializer = AppleAuthLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        payload = {
            "client_id": settings.APPLE_AUTH["client_id"],
            "client_secret": settings.APPLE_AUTH["client_secret"],
            "code": serializer.data["authorization_code"],
            "grant_type": "authorization_code",
        }

        try:
            r = requests.post("https://appleid.apple.com/auth/token", data=payload)
            apple_auth_data = r.json()
            decoded_jwt = decode_id_token(apple_auth_data["id_token"])
        except KeyError:
            raise AuthenticationFailed(
                "Sign in with Apple failed. Please go to your device's password & security settings, remove sign in with Apple for Kinship, and try again."
            )

        if (decoded_jwt["email"]) and (decoded_jwt["email_verified"] == "true"):
            try:
                user = User.objects.get(email=decoded_jwt["email"])
                user.last_login = timezone.now()
                user.save()
            except User.DoesNotExist:
                first_name = serializer.data["first_name"] or decoded_jwt["email"].split("@")[0]
                last_name = serializer.data["last_name"] or decoded_jwt["email"].split("@")[1]

                user = User(
                    email=decoded_jwt["email"],
                    password="*",
                    first_name=first_name,
                    last_name=last_name,
                    last_login=timezone.now(),
                    is_verified=True,
                    is_active=True,
                )
                user.save()
                UserPreferences.objects.create(user=user)  # initialize user preferences
                user = create_user_kin(user.id)

                if serializer.validated_data.get("promo_code", None):
                    redemption_serializer = RedemptionSerializer(
                        data={"promo_code": serializer.validated_data["promo_code"]}
                    )
                    try:
                        redemption_serializer.is_valid(raise_exception=True)
                    except serializers.ValidationError:  # We need to do this so validation errors for fields only on the redemption serializer don't leak out to this serializer. For this one, only promo code matters.
                        raise serializers.ValidationError(
                            {"promo_code": "The promo code is invalid or expired."}
                        )

                    redemption_serializer.save(redeemer=user)

                add_sample_data_to_new_user.delay(user.id)
        else:
            raise AuthenticationFailed("You must have a verified Apple account to login.")

        refresh_token = LoginSerializer.get_token(user)

        update_authentication_method.delay(user.id, "APPLE")

        return Response(
            {"access": str(refresh_token.access_token), "refresh": str(refresh_token)},
            status=status.HTTP_200_OK,
        )
