from django.utils import timezone
from rest_framework import serializers, status
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

import requests
from drf_yasg.utils import swagger_auto_schema

from apps.integrations.mixpanel.tasks import update_authentication_method
from apps.kin_auth.serializers import LoginSerializer
from apps.kin_auth.tasks import add_sample_data_to_new_user
from apps.kin_auth.utils import create_user_kin
from apps.promo_codes.models import PromoCode
from apps.promo_codes.serializers import RedemptionSerializer
from apps.users.models import User, UserPreferences


def valid_access_token(access_token, user_id):
    r = requests.get("https://graph.facebook.com/v9.0/me", params={"access_token": access_token})
    user_data = r.json()

    if r.status_code != 200 or user_data.get("id", "") != user_id:
        return False

    return True


class FacebookAuthLoginSerializer(serializers.Serializer):
    user_id = serializers.CharField()
    email = serializers.EmailField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    identity_token = serializers.CharField()
    promo_code = serializers.CharField(required=False, allow_null=True, allow_blank=True)


class FacebookAuthView(GenericAPIView):
    @swagger_auto_schema(
        request_body=FacebookAuthLoginSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="auth_social_facebook",
    )
    def post(self, request, *args, **kwargs):
        serializer = FacebookAuthLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if not valid_access_token(serializer.data["identity_token"], serializer.data["user_id"]):
            raise AuthenticationFailed("Token validation failed")

        try:
            user = User.objects.get(email=serializer.data["email"])
            user.last_login = timezone.now()
            user.save()
        except User.DoesNotExist:
            user = User(
                email=serializer.data["email"],
                password="*",
                first_name=serializer.data["first_name"],
                last_name=serializer.data["last_name"],
                last_login=timezone.now(),
                is_verified=True,
                is_active=True,
            )
            user.save()
            UserPreferences.objects.create(user=user)  # initialize user preferences
            user = create_user_kin(user.id)

            if serializer.validated_data.get("promo_code", None):
                redemption_serializer = RedemptionSerializer(
                    data={"promo_code": serializer.validated_data["promo_code"]}
                )
                try:
                    redemption_serializer.is_valid(raise_exception=True)
                except serializers.ValidationError:  # We need to do this so validation errors for fields only on the redemption serializer don't leak out to this serializer. For this one, only promo code matters.
                    raise serializers.ValidationError(
                        {"promo_code": "The promo code is invalid or expired."}
                    )

                redemption_serializer.save(redeemer=user)

            add_sample_data_to_new_user.delay(user.id)

        refresh_token = LoginSerializer.get_token(user)

        update_authentication_method.delay(user.id, "FACEBOOK")

        return Response(
            {"access": str(refresh_token.access_token), "refresh": str(refresh_token)},
            status=status.HTTP_200_OK,
        )
