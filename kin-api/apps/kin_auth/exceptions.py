from rest_framework.exceptions import APIException


class TooManyOTPAttempts(APIException):
    status_code = 400
    default_detail = "Too many attempts using an invalid verification code."
    default_code = "too_many_otp_attempts"


class RequiresPayment(APIException):
    status_code = 402
    default_detail = "Payment is required to continue using Kinship."
    default_code = "payment_required"
