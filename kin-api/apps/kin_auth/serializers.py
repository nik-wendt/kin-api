import logging
import time

from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.password_validation import validate_password
from django.db import transaction
from rest_framework import exceptions, serializers

import redis
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import RefreshToken

from apps.data_export.models import DataExport
from apps.integrations.mailchimp.utils import mailchimp_verified
from apps.kin.models import UserKin
from apps.kin_auth.tasks import (
    add_sample_data_to_new_user,
    send_email_change_alert,
    send_password_reset_code,
)
from apps.kin_auth.utils import (
    cleanup_account_delete_data,
    cleanup_otp_data,
    cleanup_password_reset_data,
    create_user_kin,
    get_issued_account_delete_code,
    get_issued_otp,
    get_issued_password_reset_code,
    get_original_email,
    increment_account_delete_retry,
    increment_otp_retry,
    increment_password_reset_retry,
    send_verification_code,
    set_email_change_redis,
)
from apps.promo_codes.models import PromoCode
from apps.promo_codes.serializers import RedemptionSerializer
from apps.users.models import User, UserPreferences


logger = logging.getLogger(__name__)


class AuthInlineUserPreferencesSetupSerializer(serializers.Serializer):
    add_kin_complete = serializers.BooleanField()
    import_contacts_complete = serializers.BooleanField()
    calendar_connect_complete = serializers.BooleanField()
    create_note_complete = serializers.BooleanField()
    text_to_kinship_complete = serializers.BooleanField()


class AuthInlineUserPreferencesSerializer(serializers.Serializer):
    setup = AuthInlineUserPreferencesSetupSerializer()
    mode = serializers.CharField()
    use_biometrics = serializers.BooleanField()
    receive_push_notifications = serializers.BooleanField(allow_null=True)
    sync_calendar_data = serializers.BooleanField(allow_null=True)
    access_contacts = serializers.BooleanField(allow_null=True)
    show_onboarding = serializers.BooleanField()
    show_setup = serializers.BooleanField()
    timezone_setting = serializers.CharField()
    app_settings = serializers.JSONField(allow_null=True)


class LoginSerializer(TokenObtainPairSerializer):
    default_error_messages = {"no_active_account": "No account found with the given credentials"}

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token["iat"] = int(time.time())
        token["email"] = user.email
        token["first_name"] = user.first_name
        token["last_name"] = user.last_name
        token["is_verified"] = user.is_verified
        token["is_active"] = user.is_active
        token["is_staff"] = user.is_staff
        token["payment_status"] = user.payment_status.value
        token["email_to_kinship_address"] = user.email_to_kinship_address
        token["user_kin"] = user.user_kin.id
        token["preferences"] = {
            "setup": {
                "add_kin_complete": user.preferences.setup_add_kin_complete,
                "import_contacts_complete": user.preferences.setup_import_contacts_complete,
                "calendar_connect_complete": user.preferences.setup_calendar_connect_complete,
                "create_note_complete": user.preferences.setup_create_note_complete,
                "text_to_kinship_complete": user.preferences.setup_text_to_kinship_complete,
            },
            "mode": user.preferences.mode.value,
            "use_biometrics": user.preferences.use_biometrics,
            "receive_push_notifications": user.preferences.receive_push_notifications,
            "sync_calendar_data": user.preferences.sync_calendar_data,
            "access_contacts": user.preferences.access_contacts,
            "timezone_setting": user.preferences.timezone_setting.zone,
            "show_onboarding": user.preferences.show_onboarding,
            "show_setup": user.preferences.show_setup,
            "app_settings": user.preferences.app_settings,
        }

        if getattr(user, "payment_provider"):
            token["payment_provider"] = user.payment_provider.value
        else:
            token["payment_provider"] = None

        if getattr(user, "payment_expiration"):
            token["payment_expiration"] = user.payment_expiration.isoformat()
        else:
            token["payment_expiration"] = None

        if user.phone_numbers.exists():
            token["phone_numbers"] = {
                "phone": user.phone_numbers.order_by("created").first().phone,
                "phone_country": user.phone_numbers.order_by("created").first().phone_country,
                "is_verified": user.phone_numbers.order_by("created").first().is_verified,
            }

            token["phone_numbers_list"] = [
                {
                    "phone": up.phone,
                    "phone_country": up.phone_country,
                    "is_verified": up.is_verified,
                }
                for up in user.phone_numbers.all()
            ]
        else:
            token["phone_numbers"] = None
            token["phone_numbers_list"] = []

        return token

    def validate(self, attrs):
        """
        Overridden from rest_framework_simplejwt TokenObtainSerializer and
        TokenObtainPairSerializer so that we can allow inactive users
        to still login. Will handle inactive user logic in the rest of
        the app flows.
        """

        authenticate_kwargs = {
            self.username_field: attrs[self.username_field],
            "password": attrs["password"],
        }
        try:
            authenticate_kwargs["request"] = self.context["request"]
        except KeyError:
            pass

        self.user = authenticate(**authenticate_kwargs)

        if self.user is None:
            raise exceptions.AuthenticationFailed(
                self.error_messages["no_active_account"], "no_active_account"
            )

        data = {}
        refresh = self.get_token(self.user)

        data["refresh"] = str(refresh)
        data["access"] = str(refresh.access_token)

        return data

    def validate_email(self, value):
        return value.lower()


class LoginSuccessSerializer(serializers.Serializer):
    id = serializers.CharField()
    access = serializers.CharField()
    refresh = serializers.CharField()
    expires_in = serializers.IntegerField()
    email = serializers.EmailField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    is_verified = serializers.BooleanField()
    is_active = serializers.BooleanField()
    is_staff = serializers.BooleanField()
    payment_status = serializers.CharField()
    payment_provider = serializers.CharField(allow_null=True)
    payment_expiration = serializers.DateTimeField(allow_null=True)
    email_to_kinship_address = serializers.EmailField()
    user_kin = serializers.CharField()
    preferences = AuthInlineUserPreferencesSerializer()
    phone_numbers = serializers.DictField(allow_null=True)
    phone_numbers_list = serializers.ListField(
        child=serializers.DictField(allow_null=True), allow_empty=True
    )


class LogoutSerializer(serializers.Serializer):
    refresh_token = serializers.CharField()

    def save(self, **kwargs):
        try:
            refresh_token = RefreshToken(self.validated_data["refresh_token"])
            refresh_token.blacklist()
        except TokenError:
            raise serializers.ValidationError("Invalid token")


class RefreshSuccessSerializer(serializers.Serializer):
    access = serializers.CharField()
    refresh = serializers.CharField()


class RegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={"input_type": "password"}, write_only=True)
    promo_code = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    source = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    campaign = serializers.CharField(required=False, allow_null=True, allow_blank=True)

    class Meta:
        model = User
        fields = [
            "id",
            "email",
            "first_name",
            "last_name",
            "password",
            "promo_code",
            "source",
            "campaign",
        ]

    def validate_promo_code(self, value):
        if not PromoCode.objects.filter(code=value).exists():
            raise serializers.ValidationError("The promo code is invalid or expired.")

        return value

    def validate_email(self, value):
        email = value.lower()
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        email_exists = redis_client.sismember("kin:emailchange_unverified_list", email)

        if User.objects.filter(email=email).exists() or email_exists:
            raise serializers.ValidationError("A user with this email address already exists.")

        return email

    def validate_password(self, value):
        if validate_password(value) is not None:
            raise serializers.ValidationError("Invalid password")

        return value

    def create(self, validated_data):
        user_data = {
            "email": validated_data["email"],
            "first_name": validated_data["first_name"],
            "last_name": validated_data["last_name"],
        }

        with transaction.atomic():
            user = User(**user_data)
            user.set_password(validated_data["password"])
            user.save()
            UserPreferences.objects.create(user=user)  # initialize user preferences
            user = create_user_kin(user.id)

            if validated_data.get("promo_code", None):
                redemption_serializer = RedemptionSerializer(
                    data={"promo_code": validated_data["promo_code"]}
                )
                try:
                    redemption_serializer.is_valid(raise_exception=True)
                except serializers.ValidationError:  # We need to do this so validation errors for fields only on the redemption serializer don't leak out to this serializer. For this one, only promo code matters.
                    raise serializers.ValidationError(
                        {"promo_code": "The promo code is invalid or expired."}
                    )

                redemption_serializer.save(redeemer=user)

        send_verification_code(user)

        add_sample_data_to_new_user.delay(user.id)

        return user


class VerifyEmailSerializer(serializers.Serializer):
    email = serializers.EmailField()
    otp = serializers.CharField()

    def validate(self, attrs):
        try:
            issued_otp = get_issued_otp(attrs["email"])

            if attrs["otp"] != issued_otp:
                _ = increment_otp_retry(attrs["email"])
                logger.info(
                    "Provided email verification code doesn't match issued code: %s", attrs["email"]
                )
                raise serializers.ValidationError("Invalid verification code.")
        except AttributeError as e:
            logger.info("Error validating email verification code: %s. %s", attrs["email"], str(e))
            raise serializers.ValidationError("Invalid verification code.")

        return attrs

    def validate_email(self, value):
        email = value.lower()

        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            raise serializers.ValidationError("No user found with that email address.")

        return email

    def save(self, **kwargs):
        user = User.objects.get(email=self.validated_data["email"])
        user.is_verified = True
        user.is_active = True
        user.save()
        mailchimp_verified(user)

        cleanup_otp_data(self.validated_data["email"])

        return user


class RegistrationSuccessSerializer(serializers.Serializer):
    id = serializers.CharField()
    email = serializers.EmailField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    access = serializers.CharField()
    refresh = serializers.CharField()
    expires_in = serializers.IntegerField()
    is_verified = serializers.BooleanField()
    is_active = serializers.BooleanField()
    is_staff = serializers.BooleanField()
    payment_status = serializers.CharField()
    payment_provider = serializers.CharField(allow_null=True)
    payment_expiration = serializers.DateTimeField(allow_null=True)
    email_to_kinship_address = serializers.EmailField()
    user_kin = serializers.CharField()
    preferences = AuthInlineUserPreferencesSerializer()


class ForgotPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate(self, attrs):
        if not User.objects.filter(email=attrs["email"]).exists():
            raise serializers.ValidationError("No user found with that email address.")

        return attrs

    def validate_email(self, value):
        return value.lower()

    def save(self, **kwargs):
        user = User.objects.get(email=self.validated_data["email"])
        send_password_reset_code.delay(user.email, user.first_name, user.last_name)

        return user


class ResendEmailVerificationSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate(self, attrs):
        if not User.objects.filter(email=attrs["email"]).exists():
            raise serializers.ValidationError("No user found with that email address.")

        if User.objects.filter(email=attrs["email"], is_verified=True).exists():
            raise serializers.ValidationError("User is already verified.")

        return attrs

    def validate_email(self, value):
        return value.lower()

    def save(self, **kwargs):
        user = User.objects.get(email=self.validated_data["email"])
        send_verification_code(user)

        return user


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()
    reset_code = serializers.CharField()

    def validate(self, attrs):
        try:
            issued_code = get_issued_password_reset_code(attrs["email"])

            if attrs["reset_code"] != issued_code:
                _ = increment_password_reset_retry(attrs["email"])
                logger.info(
                    "Provided password reset code doesn't match issued code: %s", attrs["email"]
                )
                raise serializers.ValidationError("Invalid password reset code.")
        except AttributeError as e:
            logger.info("Error validating password reset code: %s. %s", attrs["email"], str(e))
            raise serializers.ValidationError("Invalid password reset code.")

        return attrs

    def validate_email(self, value):
        email = value.lower()

        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            raise serializers.ValidationError("No user found with that email address.")

        return email

    def validate_password(self, value):
        if validate_password(value) is not None:
            raise serializers.ValidationError("Invalid password")

        return value

    def save(self, **kwargs):
        user = User.objects.get(email=self.validated_data["email"])
        user.set_password(self.validated_data["password"])
        user.save()

        cleanup_password_reset_data(self.validated_data["email"])

        return user


class ChangePasswordSerializer(serializers.Serializer):
    new_password = serializers.CharField(required=True)
    old_password = serializers.CharField(required=True)

    def validate_old_password(self, value):
        user = self._context["request"].user

        if not user.check_password(value):
            raise serializers.ValidationError(
                "Oops! The old password looks incorrect. Please try again."
            )

        return value

    def validate_new_password(self, value):
        user = self._context["request"].user

        if validate_password(value, user) is not None:
            raise serializers.ValidationError("invalid password")

        return value

    def save(self, **kwargs):
        user = self._context["request"].user
        user.set_password(self.validated_data["new_password"])
        user.save()
        return user


class ChangeEmailSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate_email(self, value):
        if User.objects.filter(email=value).exists():
            raise serializers.ValidationError("A user with this email address already exists.")

        return value

    def save(self, **kwargs):
        user = self._context["request"].user
        original_email = user.email
        was_verified = user.is_verified
        user.is_verified = False
        user.email = self.validated_data["email"]
        user.save()

        set_email_change_redis(user.email, original_email)

        if was_verified:
            send_email_change_alert.delay(original_email, user.first_name, user.last_name)

        send_verification_code(user)

        return user


class CancelEmailChangeSerializer(serializers.Serializer):
    def save(self, **kwargs):
        user = self._context["request"].user
        new_email = user.email
        original_email = get_original_email(new_email)

        if not original_email:
            raise exceptions.NotFound

        user.email = original_email
        user.is_verified = True
        user.save()
        user.refresh_from_db()

        cleanup_otp_data(new_email)

        return user


class PusherAuthSerializer(serializers.Serializer):
    channel_name = serializers.CharField()
    socket_id = serializers.CharField()


class PusherAuthResponseSerializer(serializers.Serializer):
    auth = serializers.CharField()


class AccountDeleteSerializer(serializers.Serializer):
    email = serializers.EmailField()
    delete_code = serializers.CharField()

    def validate(self, attrs):
        try:
            issued_code = get_issued_account_delete_code(attrs["email"])

            if attrs["delete_code"] != issued_code:
                _ = increment_account_delete_retry(attrs["email"])
                logger.info(
                    "Provided account delete code doesn't match issued code: %s", attrs["email"]
                )
                raise serializers.ValidationError("Invalid account delete code.")
        except AttributeError as e:
            logger.info("Error validating account delete code: %s. %s", attrs["email"], str(e))
            raise serializers.ValidationError("Invalid account delete code.")

        return attrs

    def save(self, **kwargs):
        user = User.objects.get(email=self.validated_data["email"])

        if DataExport.objects.filter(owner=user).exists():
            user.is_verified = False
            user.is_active = False
            user.save()
            return

        user_kin = UserKin.objects.get(user=user)
        cleanup_account_delete_data(user.email)
        user_kin.delete()
        user.delete()

        return
