from django.urls import path
from rest_framework.routers import DefaultRouter

from rest_framework_simplejwt.views import TokenVerifyView

from apps.kin_auth.social.apple import AppleAuthView
from apps.kin_auth.social.facebook import FacebookAuthView
from apps.kin_auth.social.google import GoogleAuthView
from apps.kin_auth.views import (
    AccountDeleteView,
    CancelEmailChangeView,
    ChangeEmailView,
    ChangePasswordView,
    ForgotPasswordView,
    StartAccountDeleteView,
    LoginView,
    LogoutView,
    PasswordResetView,
    PusherAuthView,
    RefreshView,
    ResendEmailVerificationView,
    RegistrationViewSet,
    VerifyEmailView,
)

router = DefaultRouter()
router.register(r"register", RegistrationViewSet, basename="register")


urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("refresh/", RefreshView.as_view(), name="refresh_token"),
    path("verify-email/", VerifyEmailView.as_view(), name="verify_email"),
    path("forgot-password/", ForgotPasswordView.as_view(), name="forgot_password"),
    path("password-reset/", PasswordResetView.as_view(), name="password_reset"),
    path("change-password/", ChangePasswordView.as_view(), name="change_password"),
    path("change-email/", ChangeEmailView.as_view(), name="change_email"),
    path("change-email-cancel/", CancelEmailChangeView.as_view(), name="change_email_cancel"),
    path(
        "resend-verification-email/",
        ResendEmailVerificationView.as_view(),
        name="resend_verification_email",
    ),
    path("jwt/verify/", TokenVerifyView.as_view(), name="verify_jwt"),
    path("social/apple/", AppleAuthView.as_view(), name="social_auth_apple"),
    path("social/google/", GoogleAuthView.as_view(), name="social_auth_google"),
    path("social/facebook/", FacebookAuthView.as_view(), name="social_auth_facebook"),
    path("pusher/", PusherAuthView.as_view(), name="pusher_auth"),
    path("start-account-delete/", StartAccountDeleteView.as_view(), name="start_account_delete",),
    path("account-delete/", AccountDeleteView.as_view(), name="account_delete"),
]

urlpatterns += router.urls
