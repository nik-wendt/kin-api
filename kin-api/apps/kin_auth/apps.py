from django.apps import AppConfig


class KinAuthConfig(AppConfig):
    name = "apps.kin_auth"
    verbose_name = "Kinship Auth"
