from django.utils import timezone
from rest_framework import mixins, serializers, status, viewsets, permissions
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.throttling import AnonRateThrottle
from rest_framework.views import APIView

from drf_yasg.utils import swagger_auto_schema
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework_simplejwt.serializers import TokenRefreshSerializer
from rest_framework_simplejwt.state import token_backend
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from apps.integrations.mixpanel.tasks import (
    update_authentication_method,
    mixpanel_user_app_version,
    mixpanel_user_campaign_and_source,
    mixpanel_start_account_delete_event,
    mixpanel_account_delete_event,
)
from apps.kin_auth.serializers import (
    AccountDeleteSerializer,
    CancelEmailChangeSerializer,
    ChangeEmailSerializer,
    ChangePasswordSerializer,
    ForgotPasswordSerializer,
    LoginSerializer,
    LoginSuccessSerializer,
    LogoutSerializer,
    PasswordResetSerializer,
    PusherAuthResponseSerializer,
    PusherAuthSerializer,
    RefreshSuccessSerializer,
    RegistrationSerializer,
    RegistrationSuccessSerializer,
    ResendEmailVerificationSerializer,
    VerifyEmailSerializer,
)
from apps.kin_auth.utils import send_account_delete_code
from apps.notifications.utils import get_pusher_client
from apps.spotlight.tasks import update_special_occasions_cache_task
from apps.users.models import User
from apps.users.serializers import UserSerializer


class LoginView(TokenObtainPairView):
    serializer_class = LoginSerializer
    throttle_classes = [AnonRateThrottle]

    @swagger_auto_schema(
        request_body=LoginSerializer,
        responses={status.HTTP_200_OK: LoginSuccessSerializer},
        operation_id="auth_login",
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])

        refresh_token = serializer.validated_data["refresh"]
        access_token = serializer.validated_data["access"]
        decoded_access_token = token_backend.decode(str(access_token), verify=False)

        response_data = {
            "id": decoded_access_token["user_id"],
            "access": access_token,
            "refresh": refresh_token,
            "expires_in": decoded_access_token["exp"] - decoded_access_token["iat"],
            "email": decoded_access_token["email"],
            "first_name": decoded_access_token["first_name"],
            "last_name": decoded_access_token["last_name"],
            "is_verified": decoded_access_token["is_verified"],
            "is_active": decoded_access_token["is_active"],
            "is_staff": decoded_access_token["is_staff"],
            "payment_status": decoded_access_token["payment_status"],
            "payment_provider": decoded_access_token["payment_provider"],
            "payment_expiration": decoded_access_token["payment_expiration"],
            "email_to_kinship_address": decoded_access_token["email_to_kinship_address"],
            "user_kin": decoded_access_token["user_kin"],
            "preferences": {
                "setup": {
                    "add_kin_complete": decoded_access_token["preferences"]["setup"][
                        "add_kin_complete"
                    ],
                    "import_contacts_complete": decoded_access_token["preferences"]["setup"][
                        "import_contacts_complete"
                    ],
                    "calendar_connect_complete": decoded_access_token["preferences"]["setup"][
                        "calendar_connect_complete"
                    ],
                    "create_note_complete": decoded_access_token["preferences"]["setup"][
                        "create_note_complete"
                    ],
                    "text_to_kinship_complete": decoded_access_token["preferences"]["setup"][
                        "text_to_kinship_complete"
                    ],
                },
                "mode": decoded_access_token["preferences"]["mode"],
                "use_biometrics": decoded_access_token["preferences"]["use_biometrics"],
                "receive_push_notifications": decoded_access_token["preferences"][
                    "receive_push_notifications"
                ],
                "sync_calendar_data": decoded_access_token["preferences"]["sync_calendar_data"],
                "access_contacts": decoded_access_token["preferences"]["access_contacts"],
                "timezone_setting": decoded_access_token["preferences"]["timezone_setting"],
                "show_onboarding": decoded_access_token["preferences"]["show_onboarding"],
                "show_setup": decoded_access_token["preferences"]["show_setup"],
                "app_settings": decoded_access_token["preferences"]["app_settings"],
            },
            "phone_numbers": decoded_access_token["phone_numbers"],
            "phone_numbers_list": decoded_access_token["phone_numbers_list"],
        }

        success_serializer = LoginSuccessSerializer(data=response_data)
        success_serializer.is_valid(raise_exception=True)

        User.objects.filter(id=decoded_access_token["user_id"]).update(last_login=timezone.now())

        update_special_occasions_cache_task.delay(decoded_access_token["user_id"])

        update_authentication_method.delay(decoded_access_token["user_id"], "KINSHIP")

        app_build_version = request.META.get("HTTP_X_APP_BUILD_VERSION", "")
        if app_build_version:
            mixpanel_user_app_version.delay(decoded_access_token["user_id"], app_build_version)
        return Response(success_serializer.validated_data, status=status.HTTP_200_OK)


class LogoutView(APIView):
    authentication_classes = []
    permission_classes = []

    @swagger_auto_schema(
        request_body=LogoutSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="auth_logout",
    )
    def post(self, request, *args, **kwargs):
        serializer = LogoutSerializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
            serializer.save()
        except serializers.ValidationError:
            pass  # A user should always be able to successfully logout, regardless of what happens with the token

        return Response(status=status.HTTP_200_OK)


class RefreshView(TokenRefreshView):
    throttle_classes = [AnonRateThrottle]

    @swagger_auto_schema(
        request_body=TokenRefreshSerializer,
        responses={status.HTTP_200_OK: RefreshSuccessSerializer},
        operation_id="auth_refresh",
    )
    def post(self, request, *args, **kwargs):
        resp = super().post(request, *args, **kwargs)

        access_token = resp.data["access"]
        decoded_access_token = token_backend.decode(str(access_token), verify=False)
        user = User.objects.get(pk=decoded_access_token["user_id"])
        user.last_login = timezone.now()
        user.save()

        update_special_occasions_cache_task.delay(decoded_access_token["user_id"])

        return resp


class RegistrationViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = RegistrationSerializer
    throttle_classes = [AnonRateThrottle]

    def perform_create(self, serializer):
        user = serializer.save()
        return user

    @swagger_auto_schema(
        request_body=RegistrationSerializer,
        responses={status.HTTP_200_OK: UserSerializer},
        operation_id="auth_register",
    )
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        user = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        success_serializer = UserSerializer(user, context={"request": request})

        campaign = serializer.validated_data.get("campaign")
        source = serializer.validated_data.get("source")
        if campaign or source:
            mixpanel_user_campaign_and_source.delay(user.id, campaign, source)

        return Response(success_serializer.data, status=status.HTTP_200_OK, headers=headers)


class VerifyEmailView(GenericAPIView):
    authentication_classes = ()
    throttle_classes = [AnonRateThrottle]
    serializer_class = VerifyEmailSerializer

    @swagger_auto_schema(
        request_body=VerifyEmailSerializer,
        responses={status.HTTP_200_OK: RegistrationSuccessSerializer},
        operation_id="auth_verify_email",
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()

        refresh_token = LoginSerializer.get_token(user)
        access_token = refresh_token.access_token
        decoded_access_token = token_backend.decode(str(access_token), verify=False)

        response_data = {
            "id": user.id,
            "email": user.email,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "access": str(access_token),
            "refresh": str(refresh_token),
            "expires_in": decoded_access_token["exp"] - decoded_access_token["iat"],
            "is_verified": user.is_verified,
            "is_active": user.is_active,
            "is_staff": user.is_staff,
            "payment_status": user.payment_status.value,
            "payment_expiration": user.payment_expiration,
            "email_to_kinship_address": user.email_to_kinship_address,
            "user_kin": user.user_kin.id,
            "preferences": {
                "setup": {
                    "add_kin_complete": user.preferences.setup_add_kin_complete,
                    "import_contacts_complete": user.preferences.setup_import_contacts_complete,
                    "calendar_connect_complete": user.preferences.setup_calendar_connect_complete,
                    "create_note_complete": user.preferences.setup_create_note_complete,
                    "text_to_kinship_complete": user.preferences.setup_text_to_kinship_complete,
                },
                "mode": user.preferences.mode.value,
                "use_biometrics": user.preferences.use_biometrics,
                "receive_push_notifications": user.preferences.receive_push_notifications,
                "sync_calendar_data": user.preferences.sync_calendar_data,
                "access_contacts": user.preferences.access_contacts,
                "timezone_setting": user.preferences.timezone_setting.zone,
                "show_onboarding": user.preferences.show_onboarding,
                "show_setup": user.preferences.show_setup,
                "app_settings": user.preferences.app_settings,
            },
            "phone_numbers": None,
            "phone_numbers_list": [],
        }

        if getattr(user, "payment_provider"):
            response_data["payment_provider"] = user.payment_provider.value
        else:
            response_data["payment_provider"] = None

        success_serializer = RegistrationSuccessSerializer(data=response_data)
        success_serializer.is_valid(raise_exception=True)

        User.objects.filter(id=user.id).update(last_login=timezone.now())

        return Response(success_serializer.validated_data, status=status.HTTP_200_OK)


class ForgotPasswordView(GenericAPIView):
    throttle_classes = [AnonRateThrottle]
    serializer_class = ForgotPasswordSerializer

    @swagger_auto_schema(
        request_body=ForgotPasswordSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="auth_forgot_password",
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {"detail": "Reset password code has been sent to the user's email"},
            status=status.HTTP_200_OK,
        )


class PasswordResetView(GenericAPIView):
    throttle_classes = [AnonRateThrottle]
    serializer_class = PasswordResetSerializer

    @swagger_auto_schema(
        request_body=PasswordResetSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="auth_password_reset",
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            {"detail": "Password has been reset successfully"}, status=status.HTTP_200_OK
        )


class ChangePasswordView(GenericAPIView):
    serializer_class = ChangePasswordSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(
        request_body=ChangePasswordSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="auth_change_password",
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"detail": "Password change successful."}, status=status.HTTP_200_OK)


class ChangeEmailView(GenericAPIView):
    serializer_class = ChangeEmailSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(
        request_body=ChangeEmailSerializer,
        responses={status.HTTP_200_OK: UserSerializer},
        operation_id="auth_change_email",
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        user = serializer.save()

        success_serializer = UserSerializer(user, context={"request": request})
        return Response(success_serializer.data, status=status.HTTP_200_OK)


class CancelEmailChangeView(GenericAPIView):
    serializer_class = CancelEmailChangeSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(
        request_body=CancelEmailChangeSerializer,
        responses={status.HTTP_200_OK: UserSerializer},
        operation_id="auth_cancel_change_email",
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()

        success_serializer = UserSerializer(user, context={"request": request})
        return Response(success_serializer.data, status=status.HTTP_200_OK)


class ResendEmailVerificationView(GenericAPIView):
    serializer_class = ResendEmailVerificationSerializer
    authentication_classes = ()
    throttle_classes = [AnonRateThrottle]

    @swagger_auto_schema(
        request_body=ResendEmailVerificationSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="auth_resend_verification_email",
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({"detail": "Verification email sent."}, status=status.HTTP_200_OK)


class PusherAuthView(GenericAPIView):
    serializer_class = PusherAuthSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(
        request_body=PusherAuthSerializer,
        responses={status.HTTP_200_OK: PusherAuthResponseSerializer},
        operation_id="auth_pusher",
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        pc = get_pusher_client()
        auth = pc.authenticate(
            channel=serializer.data["channel_name"], socket_id=serializer.data["socket_id"]
        )

        return Response(auth, status=status.HTTP_200_OK)


class StartAccountDeleteView(GenericAPIView):
    serializer_class = None
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(
        request_body=None,
        responses={status.HTTP_204_NO_CONTENT: None},
        operation_id="auth_start_account_delete",
    )
    def post(self, request, *args, **kwargs):
        send_account_delete_code(request.user)
        mixpanel_start_account_delete_event.delay(request.user.id)
        return Response(status=status.HTTP_204_NO_CONTENT)


class AccountDeleteView(GenericAPIView):
    serializer_class = AccountDeleteSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(
        request_body=AccountDeleteSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="auth_account_delete",
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        mixpanel_account_delete_event.delay(request.user.id)
        return Response({"detail": "Account successfully deleted"}, status=status.HTTP_200_OK)
