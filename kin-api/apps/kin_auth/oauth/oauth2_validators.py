from oauth2_provider.oauth2_validators import OAuth2Validator


class KinshipOAuth2Validator(OAuth2Validator):
    def get_additional_claims(self, request):
        claims = {
            "user_id": request.user.id,
            "email": request.user.email,
            "first_name": request.user.first_name,
            "last_name": request.user.last_name,
            "is_verified": request.user.is_verified,
            "is_active": request.user.is_active,
            "is_staff": request.user.is_staff,
            "payment_status": request.user.payment_status.value,
            "email_to_kinship_address": request.user.email_to_kinship_address,
            "user_kin": request.user.user_kin,
            "preferences": {
                "setup": {
                    "add_kin_complete": request.user.preferences.setup_add_kin_complete,
                    "import_contacts_complete": request.user.preferences.setup_import_contacts_complete,
                    "calendar_connect_complete": request.user.preferences.setup_calendar_connect_complete,
                    "create_note_complete": request.user.preferences.setup_create_note_complete,
                    "text_to_kinship_complete": request.user.preferences.setup_text_to_kinship_complete,
                },
                "mode": request.user.preferences.mode.value,
                "use_biometrics": request.user.preferences.use_biometrics,
                "receive_push_notifications": request.user.preferences.receive_push_notifications,
                "sync_calendar_data": request.user.preferences.sync_calendar_data,
                "access_contacts": request.user.preferences.access_contacts,
                "timezone_setting": request.user.preferences.timezone_setting.zone,
                "show_onboarding": request.user.preferences.show_onboarding,
                "show_setup": request.user.preferences.show_setup,
                "app_settings": request.user.preferences.app_settings,
            },
        }

        if getattr(request.user, "payment_provider"):
            claims["payment_provider"] = request.user.payment_provider.value
        else:
            claims["payment_provider"] = None

        if getattr(request.user, "payment_expiration"):
            claims["payment_expiration"] = request.user.payment_expiration.isoformat()
        else:
            claims["payment_expiration"] = None

        if request.user.phone_numbers.exists():
            claims["phone_numbers"] = {
                "phone": request.user.phone_numbers.order_by("created").first().phone,
                "phone_country": request.user.phone_numbers.order_by("created")
                .first()
                .phone_country,
                "is_verified": request.user.phone_numbers.order_by("created").first().is_verified,
            }

            claims["phone_numbers_list"] = [
                {
                    "phone": up.phone,
                    "phone_country": up.phone_country,
                    "is_verified": up.is_verified,
                }
                for up in request.user.phone_numbers.all()
            ]
        else:
            claims["phone_numbers"] = None
            claims["phone_numbers_list"] = []

        return claims
