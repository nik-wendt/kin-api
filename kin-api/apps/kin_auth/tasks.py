import logging
import os
import random

from django.core.files.base import File
from django.core.mail import EmailMessage
from django.http import HttpRequest

from celery import shared_task

from apps.facts.serializers import FactSerializer
from apps.groups.models import Group
from apps.groups.serializers import GroupSerializer
from apps.kin.sample_data import SAMPLE_GROUPS, SAMPLE_KIN_NOTES_AND_FACTS
from apps.kin.serializers import KinImportSerializer
from apps.kin_auth.utils import generate_password_reset_code
from apps.notes.serializers import NoteWriteSerializer
from apps.users.models import User


logger = logging.getLogger(__name__)


@shared_task
def add_sample_data_to_new_user(user_id):
    user = User.objects.get(pk=user_id)
    faux_request = HttpRequest()
    faux_request.user = user

    sample_group_data = [{"name": g} for g in SAMPLE_GROUPS]
    group_serializer = GroupSerializer(
        data=sample_group_data, many=True, context={"request": faux_request}
    )
    group_serializer.is_valid(raise_exception=True)
    group_serializer.save(owner=user)

    for sk in SAMPLE_KIN_NOTES_AND_FACTS:
        kin_serializer = KinImportSerializer(
            data={
                "friendly_name": sk["friendly_name"],
                "first_name": sk["first_name"],
                "middle_name": sk["middle_name"],
                "last_name": sk["last_name"],
                "source": "SYSTEM",
            }
        )
        kin_serializer.is_valid(raise_exception=True)
        kin = kin_serializer.save(owner=user)

        cwd = os.getcwd()
        fh = open(f"{cwd}/apps/kin/sample_data_images/{sk['profile_pic']}", "rb")
        out_file = File(fh)
        kin.profile_pic.save(sk["profile_pic"], out_file)

        for sf in sk.get("facts", []):
            fact_serializer = FactSerializer(
                data={
                    "key": sf["key"],
                    "value": sf["value"],
                    "fact_type": sf["fact_type"],
                    "metadata": sf["metadata"],
                    "kin": kin.id,
                },
                context={"request": faux_request},
            )
            fact_serializer.is_valid(raise_exception=True)
            fact_serializer.save()

        for sn in sk.get("notes", []):
            note_serializer = NoteWriteSerializer(
                data={"text": sn, "source": "SYSTEM", "kin": kin.id},
                context={"request": faux_request},
            )
            note_serializer.is_valid(raise_exception=True)
            note_serializer.save(owner=user)

        num_groups = random.randint(2, 4)
        random_group_ids = (
            Group.objects.filter(owner=user)
            .order_by("?")
            .values_list("id", flat=True)[0:num_groups]
        )
        kin.groups.add(*random_group_ids)


@shared_task
def send_email_change_alert(user_email, user_first_name, user_last_name):
    message = EmailMessage(
        subject="Your Kinship account was updated",
        to=[f"{user_first_name} {user_last_name} <{user_email}>"],
    )
    message.template_id = "Security alert (change account email) template"
    message.merge_data = {user_email: {"FNAME": user_first_name}}
    message.send()


@shared_task
def send_password_reset_code(user_email, user_first_name, user_last_name):
    code = generate_password_reset_code(user_email)

    message = EmailMessage(
        subject="Reset your Kinship password",
        to=[f"{user_first_name} {user_last_name} <{user_email}>"],
    )
    message.template_id = "Reset password template"
    message.merge_data = {user_email: {"FNAME": user_first_name, "OTP": code}}
    message.send()
