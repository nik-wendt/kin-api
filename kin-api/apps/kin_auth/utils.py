import logging
import random

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMessage
from django.db import transaction

import redis

from apps.facts.choices import FactTypeEnum
from apps.facts.models import Fact
from apps.kin.models import Kin, UserKin
from apps.kin_auth.exceptions import TooManyOTPAttempts
from apps.users.models import User


logger = logging.getLogger(__name__)


def get_random_code():
    code = f"{random.randrange(1, 10 ** 6):06}"
    return code


def generate_otp(user_email):
    otp = get_random_code()

    redis_key = f"kin:otp:{user_email}"
    redis_client = redis.Redis.from_url(settings.REDIS_URL)

    # delete any outstanding verfication codes before issuing a new one
    redis_client.delete(redis_key)
    redis_client.set(redis_key, str(otp), 600)

    return otp


def get_issued_otp(user_email):
    redis_key = f"kin:otp:{user_email}"
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    saved_otp = redis_client.get(redis_key).decode(
        "utf-8"
    )  # redis returns bytes, and we need to compare strings
    return saved_otp


def cleanup_otp_data(user_email):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_client.delete(f"kin:otpretry:{user_email}")
    redis_client.delete(f"kin:otp:{user_email}")

    original_email_key = f"kin:originalemail:{user_email}"
    original_email = redis_client.get(original_email_key)
    if original_email:
        redis_client.srem(f"kin:emailchange_unverified_list", original_email)
        redis_client.delete(original_email_key)


def increment_otp_retry(user_email):
    otp_retry_redis_key = f"kin:otpretry:{user_email}"
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    retry_count = redis_client.incr(otp_retry_redis_key)

    if retry_count >= 5:
        original_email_key = f"kin:originalemail:{user_email}"
        original_email = redis_client.get(original_email_key)
        if original_email:
            user = User.objects.get(email=user_email)
            user.email = original_email.decode("utf-8")
            user.is_verified = True
            user.save()

        cleanup_otp_data(user_email)

        logger.info("Too many attempts validating email verification code: %s.", user_email)
        raise TooManyOTPAttempts

    return retry_count


def generate_password_reset_code(user_email):
    code = get_random_code()

    redis_key = f"kin:passwordresettoken:{user_email}"
    redis_client = redis.Redis.from_url(settings.REDIS_URL)

    # delete any outstanding reset codes before issuing a new one
    redis_client.delete(redis_key)
    redis_client.set(redis_key, str(code), 600)

    return code


def get_issued_password_reset_code(user_email):
    redis_key = f"kin:passwordresettoken:{user_email}"
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    saved_code = redis_client.get(redis_key).decode(
        "utf-8"
    )  # redis returns bytes, and we need to compare strings
    return saved_code


def cleanup_password_reset_data(user_email):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_client.delete(f"kin:passwordresettoken:{user_email}")
    redis_client.delete(f"kin:passwordresetretry:{user_email}")


def increment_password_reset_retry(user_email):
    password_reset_retry_redis_key = f"kin:passwordresetretry:{user_email}"
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    retry_count = redis_client.incr(password_reset_retry_redis_key)

    if retry_count >= 5:
        cleanup_password_reset_data(user_email)

        logger.info("Too many attempts trying password reset code: %s.", user_email)
        raise TooManyOTPAttempts

    return retry_count


def set_email_change_redis(new_email, original_email):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_client.set(f"kin:originalemail:{new_email}", original_email)
    redis_client.sadd(f"kin:emailchange_unverified_list", original_email)


def get_original_email(new_email):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    original_email = redis_client.get(f"kin:originalemail:{new_email}")

    if not original_email:
        return ""

    return original_email.decode("utf-8")


def create_user_kin(user_id):
    with transaction.atomic():
        user = User.objects.get(pk=user_id)

        kin = Kin.objects.create(
            friendly_name=f"{user.first_name} {user.last_name}",
            first_name=user.first_name,
            last_name=user.last_name,
            owner=user,
        )
        _ = Fact.objects.create(
            key="Email", value=user.email, fact_type=FactTypeEnum.EMAIL, kin=kin
        )

        try:
            if user.phone_numbers.exists():
                user_phone = user.phone_numbers.order_by("created").first().phone
                _ = Fact.objects.create(
                    key="Phone", value=user_phone, fact_type=FactTypeEnum.PHONE, kin=kin
                )
        except ObjectDoesNotExist:
            pass

        UserKin.objects.create(user=user, kin=kin)
        user.refresh_from_db()
        return user


def send_verification_code(user):
    otp = generate_otp(user.email)
    message = EmailMessage(
        subject=f"You're *almost* good to go, {user.first_name}",
        to=[f"{user.first_name} {user.last_name} <{user.email}>"],
    )
    message.template_id = "Verify account Transactional Template"
    message.merge_data = {user.email: {"FNAME": user.first_name, "OTP": otp}}
    message.send()


def generate_account_delete_code(user_email):
    code = get_random_code()

    redis_key = f"kin:accountdeletetoken:{user_email}"
    redis_client = redis.Redis.from_url(settings.REDIS_URL)

    # delete any outstanding account delete codes before issuing a new one
    redis_client.delete(redis_key)
    redis_client.set(redis_key, str(code), 600)

    return code


def get_issued_account_delete_code(user_email):
    redis_key = f"kin:accountdeletetoken:{user_email}"
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    saved_code = redis_client.get(redis_key).decode(
        "utf-8"
    )  # redis returns bytes, and we need to compare strings
    return saved_code


def cleanup_account_delete_data(user_email):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_client.delete(f"kin:accountdeletetoken:{user_email}")
    redis_client.delete(f"kin:passwordresetretry:{user_email}")


def increment_account_delete_retry(user_email):
    account_delete_retry_redis_key = f"kin:accountdeleteretry:{user_email}"
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    retry_count = redis_client.incr(account_delete_retry_redis_key)

    if retry_count >= 5:
        cleanup_account_delete_data(user_email)

        logger.info("Too many attempts trying account delete code: %s.", user_email)
        raise TooManyOTPAttempts

    return retry_count


def send_account_delete_code(user):
    code = generate_account_delete_code(user.email)

    message = EmailMessage(
        subject=f"Ready to delete your account, {user.first_name}",
        to=[f"{user.first_name} {user.last_name} <{user.email}>"],
    )
    message.template_id = "Verify before deleting account - Transactional"
    message.merge_data = {user.email: {"FNAME": user.first_name, "CODE": code}}
    message.send()
