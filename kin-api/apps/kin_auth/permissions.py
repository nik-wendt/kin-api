from django.utils import timezone
from rest_framework.permissions import BasePermission

from apps.kin_auth.exceptions import RequiresPayment
from apps.users.choices import PaymentStatusEnum


class IsVerified(BasePermission):
    message = "Email verification required."

    def has_permission(self, request, view):

        return request.user and request.user.is_verified


class IsPaid(BasePermission):
    def has_permission(self, request, view):

        if request.user.payment_status == PaymentStatusEnum.PAST_DUE:
            raise RequiresPayment(
                detail={
                    "message": "Payment is required to continue using Kinship.",
                    "payment_status": request.user.payment_status.value,
                    "payment_expiration": request.user.payment_expiration,
                }
            )

        if (request.user.payment_status == PaymentStatusEnum.CANCELLED) and (
            request.user.payment_expiration < timezone.now()
        ):
            raise RequiresPayment(
                detail={
                    "message": "Payment is required to continue using Kinship.",
                    "payment_status": request.user.payment_status.value,
                    "payment_expiration": request.user.payment_expiration,
                }
            )

        return True
