from django.apps import AppConfig


class DevScriptsConfig(AppConfig):
    name = "apps.dev_scripts"
    verbose_name = "Development Scripts"
