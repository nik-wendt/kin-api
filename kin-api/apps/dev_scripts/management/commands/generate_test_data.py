import logging

from django.core.management.base import BaseCommand

import shortuuid
from faker import Faker

from apps.facts.choices import FactTypeEnum
from apps.facts.models import Fact
from apps.groups.models import Group
from apps.kin.models import Kin
from apps.kin_auth.utils import create_user_kin
from apps.kin_extensions.utils import DisableSignals
from apps.notes.models import Note
from apps.users.models import User, UserPreferences


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = """
        Generate a test account with data populated. Meant for development purposes only. Should only be run on a fresh database. 
        --user-count int for number of users to create 
        --dup true to create kin with duplicate data for smart merge testing.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fake = None
        self.user = None

    def _get_or_create_user(self, user_count=None):
        try:
            user = User.objects.get(email=f"test{user_count or ''}@example.com")
            return user
        except User.DoesNotExist:
            user = User.objects.create(
                id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                email=f"test{user_count or ''}@example.com",
                first_name="Test",
                last_name="Developer",
                is_verified=True,
                is_active=True,
            )
            user.set_password("Password123!")
            user.save()
            UserPreferences.objects.create(user=user)
            user = create_user_kin(user.id)

            return user

    def _create_groups(self):
        Group.objects.bulk_create(
            [
                Group(
                    id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                    name="Family",
                    owner=self.user,
                ),
                Group(
                    id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                    name="Coworkers",
                    owner=self.user,
                ),
                Group(
                    id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                    name="Friends",
                    owner=self.user,
                ),
                Group(
                    id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                    name="Close",
                    owner=self.user,
                ),
            ]
        )

    def _convert_to_shortuuid(self, uuid_string):
        sid = shortuuid.encode(uuid_string)
        return sid

    def _create_kin_notes(self, index, kin):
        if index % 3 == 0 and index % 5 == 0:
            paragraph = self.fake.paragraph()

            Note.objects.create(
                id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                text=f"<p>{paragraph}</p>",
                raw_text=paragraph,
                kin=kin,
                owner=self.user,
            )
        elif index % 3 == 0:
            sentences = self.fake.sentences()

            for sentence in sentences:
                Note.objects.create(
                    id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                    text=f"<p>{sentence}</p>",
                    raw_text=sentence,
                    kin=kin,
                    owner=self.user,
                )
        elif index % 5 == 0:
            paragraphs = self.fake.paragraphs(nb=6)

            for i, paragraph in enumerate(paragraphs):
                Note.objects.create(
                    id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                    text=f"<p>{paragraph}</p>",
                    raw_text=paragraph,
                    kin=kin,
                    owner=self.user,
                )

    def _create_kin_facts(self, index, kin):
        facts = []
        profile = self.fake.profile()
        phone = self.fake.phone_number()

        if index % 3 == 0 and index % 5 == 0:
            facts = [
                {"key": "Job Title", "value": profile["job"]},
                {"key": "Company", "value": profile["company"]},
                {"key": "Address", "value": profile["residence"].replace("\n", " ")},
                {"key": "Email", "value": profile["mail"], "fact_type": FactTypeEnum.EMAIL},
                {"key": "Birthdate", "value": profile["birthdate"].strftime("%m-%d-%Y")},
                {"key": "Phone", "value": phone, "fact_type": FactTypeEnum.PHONE},
            ]
        elif index % 3 == 0:
            facts = [
                {"key": "Email", "value": profile["mail"], "fact_type": FactTypeEnum.EMAIL},
                {"key": "Phone", "value": phone, "fact_type": FactTypeEnum.PHONE},
            ]
        elif index % 5 == 0:
            facts = []

        for fact in facts:
            Fact.objects.create(
                id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                key=fact["key"],
                value=fact["value"],
                fact_type=fact.get("fact_type", FactTypeEnum.CUSTOM),
                kin=kin,
            )

    def _add_kin_groups(self, index, kin):
        if index % 3 == 0 and index % 5 == 0:
            kin.groups.add(Group.objects.get(name="Coworkers", owner=self.user))
        elif index % 3 == 0:
            kin.groups.add(Group.objects.get(name="Family", owner=self.user))
            kin.groups.add(Group.objects.get(name="Close", owner=self.user))
        elif index % 5 == 0:
            kin.groups.add(Group.objects.get(name="Friends", owner=self.user))
            kin.groups.add(Group.objects.get(name="Close", owner=self.user))

    def _delete_kin_data(self):
        self.user.notes.all().delete()
        self.user.kin_groups.all().delete()
        self.user.kin.exclude(userkin__isnull=False).delete()

    def _create_kin_data(self, dup):
        self._delete_kin_data()
        self._create_groups()

        for i in range(2000):
            first_name = self.fake.first_name()
            last_name = self.fake.last_name()
            friendly_name = f"{first_name}{last_name[0].upper()}"
            middle_name = self.fake.first_name() if i % 6 == 0 else ""
            suffix = self.fake.suffix() if i % 7 == 0 else ""

            kin = Kin.objects.create(
                id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                friendly_name=friendly_name,
                first_name=first_name,
                middle_name=middle_name,
                last_name=last_name,
                suffix=suffix,
                owner=self.user,
            )

            self._create_kin_notes(i, kin)
            self._create_kin_facts(i, kin)
            self._add_kin_groups(i, kin)

        # scatter some duplicate data amongst the kin.
        if dup:
            for idx in range(0, 10):
                # Duplicate names
                if idx < 5:
                    first_name = self.fake.first_name()
                    last_name = self.fake.last_name()
                    suffix = self.fake.suffix() if idx % 3 == 0 else ""
                    friendly_name = f"{first_name}{last_name[0].upper()}"
                    Kin.objects.create(
                        id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                        friendly_name=friendly_name,
                        first_name=first_name,
                        last_name=last_name,
                        suffix=suffix,
                        owner=self.user,
                    )
                    Kin.objects.create(
                        id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                        friendly_name=friendly_name,
                        first_name=first_name,
                        last_name=last_name,
                        suffix=suffix,
                        owner=self.user,
                    )
                # Duplicate Emails
                elif idx < 8:
                    k1, k2 = self._two_unique_kin(idx)
                    self._duplicate_email_kin(k1, k2)
                # Duplicate Phone
                else:
                    k1, k2 = self._two_unique_kin(idx)
                    self._duplicate_phone_kin(k1, k2)

    def _two_unique_kin(self, idx):
        first_name_1 = self.fake.first_name()
        last_name_1 = self.fake.last_name()
        suffix_1 = self.fake.suffix() if idx % 3 == 0 else ""
        friendly_name_1 = f"{first_name_1}{last_name_1[0].upper()}"
        first_name_2 = self.fake.first_name()
        last_name_2 = self.fake.last_name()
        suffix_2 = self.fake.suffix() if idx % 3 == 0 else ""
        friendly_name_2 = f"{first_name_1}{last_name_1[0].upper()}"
        k1 = Kin.objects.create(
            id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
            friendly_name=friendly_name_1,
            first_name=first_name_1,
            last_name=last_name_1,
            suffix=suffix_1,
            owner=self.user,
        )

        k2 = Kin.objects.create(
            id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
            friendly_name=friendly_name_2,
            first_name=first_name_2,
            last_name=last_name_2,
            suffix=suffix_2,
            owner=self.user,
        )
        return k1, k2

    def _duplicate_email_kin(self, k1, k2):
        profile = self.fake.profile()
        fact = {"key": "Email", "value": profile["mail"], "fact_type": FactTypeEnum.EMAIL}
        Fact.objects.create(
            id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
            kin=k1,
            key=fact["key"],
            value=fact["value"],
            fact_type=fact["fact_type"],
        )
        Fact.objects.create(
            id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
            kin=k2,
            key=fact["key"],
            value=fact["value"],
            fact_type=fact["fact_type"],
        )

    def _duplicate_phone_kin(self, k1, k2):
        phone = self.fake.phone_number()
        fact = {"key": "Phone", "value": phone, "fact_type": FactTypeEnum.PHONE}
        Fact.objects.create(
            id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
            kin=k1,
            key=fact["key"],
            value=fact["value"],
            fact_type=fact["fact_type"],
        )
        Fact.objects.create(
            id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
            kin=k2,
            key=fact["key"],
            value=fact["value"],
            fact_type=fact["fact_type"],
        )

    def _create_unassigned_notes(self):
        paragraphs = self.fake.paragraphs(nb=7)

        for paragraph in paragraphs:
            Note.objects.create(
                id=self.fake.uuid4(cast_to=self._convert_to_shortuuid),
                text=f"<p>{paragraph}</p>",
                raw_text=paragraph,
                owner=self.user,
            )

    def add_arguments(self, parser):
        parser.add_argument("--user-count", nargs="?", type=int)
        parser.add_argument("--dup", nargs="+", type=str)

    def handle(self, *args, **options):
        logger.info("Starting test account data generation...")

        with DisableSignals():
            count = options.get("user_count", 0)
            dup = options.get("dup", None)
            if count:
                for idx in range(1, count):
                    self.fake = Faker()
                    self.fake.seed_instance(3645 + idx)
                    self.user = self._get_or_create_user(idx)
                    self._create_kin_data(dup)
                    self._create_unassigned_notes()
            else:
                self.fake = Faker()
                self.fake.seed_instance(3645)
                self.user = self._get_or_create_user()
                self._create_kin_data(dup)
                self._create_unassigned_notes()

        logger.info("Finshed test account data generation.")
