from django.urls import path
from rest_framework import routers

from apps.payments.views import AppleAppStoreNotificationView, AppleReceiptValidationView


router = routers.DefaultRouter()

urlpatterns = [
    path(
        "apple/validation/",
        AppleReceiptValidationView.as_view(),
        name="payments_apple_receipt_validation",
    ),
    path(
        "apple/app-store-notification/",
        AppleAppStoreNotificationView.as_view(),
        name="payments_apple_app_store_notification",
    ),
]

urlpatterns += router.urls
