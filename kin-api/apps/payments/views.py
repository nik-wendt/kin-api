import logging

from django.conf import settings
from django.db import transaction
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from drf_yasg.utils import swagger_auto_schema

from apps.kin_auth.permissions import IsVerified
from apps.payments.models import AppleAppStoreReceipt, AppleAppStoreNotification
from apps.payments.serializers import (
    AppleReceiptValidationResultSerializer,
    AppleReceiptValidationSerializer,
)
from apps.spotlight.tasks import send_all_spotlight_notifications

logger = logging.getLogger(__name__)


class AppleReceiptValidationView(APIView):
    permission_classes = (permissions.IsAuthenticated, IsVerified)

    @swagger_auto_schema(
        request_body=AppleReceiptValidationSerializer,
        responses={status.HTTP_200_OK: AppleReceiptValidationResultSerializer},
        operation_id="payments_apple_receipt_validation",
    )
    def post(self, request, *args, **kwargs):
        serializer = AppleReceiptValidationSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        output_serializer = AppleReceiptValidationResultSerializer(request.user)

        send_all_spotlight_notifications.delay(request.user.id)

        return Response(output_serializer.data, status=status.HTTP_200_OK)


class AppleAppStoreNotificationView(APIView):
    authentication_classes = ()
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        notification = AppleAppStoreNotification.objects.create(notification_data=request.data)

        if not request.data["password"] == settings.APPLE_APP_STORE["shared_secret"]:
            return Response(
                {"detail": "Invalid App Store notification"}, status=status.HTTP_400_BAD_REQUEST
            )

        with transaction.atomic():
            notification.valid = True
            notification.save()

            receipt = AppleReceiptValidationSerializer(
                data={"receipt_base64": request.data["unified_receipt"]["latest_receipt"]},
                context={"request": request},
            )

            if receipt.is_valid(raise_exception=True):
                owner_receipt = AppleAppStoreReceipt.objects.filter(
                    receipt_data__latest_receipt_info__0__original_transaction_id=notification.notification_data[
                        "unified_receipt"
                    ][
                        "pending_renewal_info"
                    ][
                        0
                    ][
                        "original_transaction_id"
                    ]
                ).first()

                if not owner_receipt:
                    logger.info(
                        "APP STORE NOTIFICATION %s (%s): No original transaction found",
                        notification.id,
                        request.data["notification_type"],
                    )
                    return Response(
                        status=status.HTTP_200_OK
                    )  # We always need to send Apple a 200 response to confirm receipt of the notification so they don't resend; doesn't mean it's valid.

                receipt = receipt.save(notification=notification, owner=owner_receipt.owner)

        logger.info("APP STORE NOTIFICATION: %s", request.data["notification_type"])
        return Response(status=status.HTTP_200_OK)
