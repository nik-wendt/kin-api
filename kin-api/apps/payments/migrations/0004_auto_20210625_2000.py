# Generated by Django 3.0.13 on 2021-06-25 20:00

from django.db import migrations
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [("payments", "0003_auto_20210625_1955")]

    operations = [
        migrations.AlterField(
            model_name="appleappstorenotification",
            name="id",
            field=django_extensions.db.fields.ShortUUIDField(
                blank=True, editable=False, primary_key=True, serialize=False
            ),
        )
    ]
