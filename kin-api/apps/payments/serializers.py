import logging

from django.conf import settings
from django.core.mail import send_mail
from django.db import transaction
from django.utils import timezone
from rest_framework import serializers

import arrow
from django_enum_choices.serializers import EnumChoiceModelSerializerMixin
from inapppy import AppStoreValidator, InAppPyValidationError

from apps.payments.models import AppleAppStoreReceipt
from apps.users.choices import PaymentProviderEnum, PaymentStatusEnum, PaymentSubscriptionTypeEnum
from apps.users.models import User


logger = logging.getLogger(__name__)


class AppleReceiptValidationSerializer(serializers.ModelSerializer):
    owner = serializers.HiddenField(default=serializers.CurrentUserDefault())
    notification = serializers.HiddenField(default=None)

    class Meta:
        model = AppleAppStoreReceipt
        fields = ("receipt_base64", "owner", "notification")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.receipt_data = None

    def validate_receipt_base64(self, value):
        bundle_id = settings.APPLE_APP_STORE["bundle_id"]
        auto_retry_wrong_env_request = True  # if True, automatically query sandbox endpoint if
        # validation fails on production endpoint

        validator = AppStoreValidator(
            bundle_id, auto_retry_wrong_env_request=auto_retry_wrong_env_request
        )

        try:
            exclude_old_transactions = True  # if True, include only the latest renewal transaction
            validation_result = validator.validate(
                value,
                settings.APPLE_APP_STORE["shared_secret"],
                exclude_old_transactions=exclude_old_transactions,
            )
            self.receipt_data = validation_result
        except InAppPyValidationError as ex:
            response_from_apple = f"{ex.raw_response['status']}: {ex.message}"
            logger.info(
                "Error validating Apple App Store receipt for user %s. REASON: %s",
                self._context["request"].user.id,
                response_from_apple,
            )
            raise serializers.ValidationError(response_from_apple)

        return value

    def create(self, validated_data):
        with transaction.atomic():
            if not AppleAppStoreReceipt.objects.filter(
                receipt_data__latest_receipt_info__0__original_transaction_id=self.receipt_data[
                    "latest_receipt_info"
                ][-1]["original_transaction_id"]
            ).exists():
                instance = AppleAppStoreReceipt(
                    receipt_base64=validated_data["receipt_base64"],
                    owner=validated_data["owner"],
                    notification=validated_data["notification"],
                    receipt_data=self.receipt_data,
                )
                instance.save()
            else:
                if not AppleAppStoreReceipt.objects.filter(
                    receipt_data__latest_receipt_info__0__original_transaction_id=self.receipt_data[
                        "latest_receipt_info"
                    ][0]["original_transaction_id"],
                    owner=validated_data["owner"],
                ).exists():
                    raise serializers.ValidationError(
                        "This App Store subscription already belongs to another Kinship account."
                    )

                instance, _ = AppleAppStoreReceipt.objects.update_or_create(
                    receipt_base64=validated_data["receipt_base64"],
                    owner=validated_data["owner"],
                    defaults={
                        "receipt_data": self.receipt_data,
                        "notification": validated_data["notification"],
                    },
                )

            user = instance.owner

            if (
                user.payment_status == PaymentStatusEnum.FOUNDING_MEMBER
                or user.payment_status == PaymentStatusEnum.LIFETIME_SUBSCRIPTION
            ):  # The app should prevent this from ever happening, but need to account for in the API just in case
                send_mail(
                    "FOUNDING MEMBER PAID ALERT",
                    f"Oh no! The user {self._context['request'].user.id} paid even though they're a founding member or lifetime subscriber. Contact them to resolve immediately",
                    settings.DEFAULT_FROM_EMAIL,
                    ["glenn@kinshipsystems.com"],
                )

                logger.info(
                    "Received payment from founding member. Contact user %s to issue refund.",
                    self._context["request"].user.id,
                )

                return instance

            user.payment_provider = PaymentProviderEnum.APPLE
            receipt_expiration = arrow.get(
                int(instance.receipt_data["latest_receipt_info"][0]["expires_date_ms"])
            ).datetime
            user.payment_expiration = receipt_expiration

            pending_renewal_info = instance.receipt_data.get("pending_renewal_info", None)
            pending_renewal_info_item = next(iter(pending_renewal_info), None)

            if pending_renewal_info_item["auto_renew_status"] == "0":
                user.payment_status = PaymentStatusEnum.CANCELLED
            elif receipt_expiration > timezone.now():
                user.payment_status = PaymentStatusEnum.PAID
            else:
                user.payment_status = PaymentStatusEnum.PAST_DUE

            if pending_renewal_info_item["auto_renew_product_id"] == "kinship_monthly_sub_1":
                user.payment_subscription_type = PaymentSubscriptionTypeEnum.MONTHLY
            elif pending_renewal_info_item["auto_renew_product_id"] == "kinship_annual_sub_1":
                user.payment_subscription_type = PaymentSubscriptionTypeEnum.ANNUAL
            else:
                user.payment_subscription_type = None

            user.save()

            return instance


class AppleReceiptValidationResultSerializer(
    EnumChoiceModelSerializerMixin, serializers.ModelSerializer
):
    class Meta:
        model = User
        fields = (
            "id",
            "email",
            "first_name",
            "last_name",
            "payment_provider",
            "payment_status",
            "payment_expiration",
        )
