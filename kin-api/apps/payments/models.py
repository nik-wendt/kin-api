from django.db.models import JSONField
from django.db import models

from django_extensions.db.fields import ShortUUIDField

from apps.users.models import User


class AppleAppStoreNotification(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    notification_data = JSONField(null=True)
    valid = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)


class AppleAppStoreReceipt(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="apple_app_store_receipts"
    )
    receipt_base64 = models.TextField()
    receipt_data = JSONField(null=True)
    notification = models.ForeignKey(
        AppleAppStoreNotification,
        on_delete=models.CASCADE,
        null=True,
        related_name="apple_app_store_receipts",
    )
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
