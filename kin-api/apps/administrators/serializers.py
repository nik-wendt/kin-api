import csv
import io

import shortuuid
from django_enum_choices.serializers import EnumChoiceModelSerializerMixin
from rest_framework import serializers

from apps.conferences.models import Conference, ConferenceAttendee
from apps.facts.choices import FactTypeEnum
from apps.facts.models import Fact
from apps.kin.choices import KinSourceEnum
from apps.kin.models import Kin
from apps.promo_codes.models import PromoCode
from apps.users.models import User


class PromoCodeAdminSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    used_count = serializers.ReadOnlyField()

    class Meta:
        model = PromoCode
        fields = "__all__"


class ConferenceAdminWriteSerializer(serializers.ModelSerializer):
    promo_code_id = serializers.CharField()

    def update(self, instance, validated_data):
        return super().update(instance, validated_data)

    class Meta:
        model = Conference
        fields = "__all__"


class ConferenceAdminSerializer(serializers.ModelSerializer):
    promo_code = PromoCodeAdminSerializer()
    attendees = serializers.SerializerMethodField()
    share_token_ttl = serializers.SerializerMethodField()

    def get_attendees(self, obj):
        return obj.number_of_attendees

    def get_share_token_ttl(self, obj):
        return obj.share_token_ttl

    class Meta:
        model = Conference
        fields = "__all__"


class ConferenceAttendeeAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConferenceAttendee
        fields = "__all__"


class UsersAdminSerializer(EnumChoiceModelSerializerMixin, serializers.HyperlinkedModelSerializer):
    masked_email = serializers.SerializerMethodField()

    def get_masked_email(self, obj):
        email = obj.email
        addr, domain = email.split("@")
        stars = ""
        if len(addr) < 4:
            first_chars, star_chars, last_chars = addr[:1], addr[1:], ""
        elif len(addr) < 5:
            first_chars, star_chars, last_chars = addr[:1], addr[1:-1], addr[-1:]
        else:
            first_chars, star_chars, last_chars = addr[:2], addr[2:], addr[-1:]
        for x in star_chars:
            stars += "*"
        return f"{first_chars}{stars}{last_chars}@{domain}"

    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "is_superuser",
            "is_active",
            "is_staff",
            "is_verified",
            "payment_status",
            "payment_expiration",
            "payment_subscription_type",
            "date_joined",
            "masked_email",
        )


class SingleUserCsvSerializer(serializers.Serializer):
    file = serializers.FileField(required=True)
    user_id = serializers.CharField(required=True)

    class Meta:
        fields = ("file",)

    def validate(self, attrs):
        errors = {}
        file = attrs["file"]
        try:
            User.objects.get(pk=attrs["user_id"])
        except User.DoesNotExist:
            errors["user_id"] = f"id: {id} does not match existing user"

        if file.content_type == "text/csv":
            pass
        else:
            errors["file"] = "Not a valid CSV file"
            raise serializers.ValidationError(errors)

        decoded_file = file.read().decode()
        io_string = io.StringIO(decoded_file)
        reader = csv.DictReader(io_string)
        for row in reader:
            if "First Name" in row and row["First Name"]:
                pass
            else:
                errors[
                    "First Name"
                ] = "Field `First Name` is a required filed. Please download example csv file for proper field names."
        if errors:
            raise serializers.ValidationError(errors)
        file.seek(0)
        return attrs

    def save(self, **kwargs):
        file = self.validated_data["file"]
        user_id = self.validated_data["user_id"]
        decoded_file = file.read().decode()
        io_string = io.StringIO(decoded_file)
        reader = csv.DictReader(io_string)
        headers = list(reader.fieldnames)
        kin_fields = ["First Name", "Last Name", "Middle Name"]
        new_kin = []
        new_facts = []
        for f in kin_fields:
            try:
                headers.remove(f)
            except ValueError:
                pass
        for row in reader:
            kin_id = shortuuid.uuid()
            new_kin.append(
                Kin(
                    id=kin_id,
                    first_name=row["First Name"],
                    friendly_name=f"{row.get('First Name')} {row.get('Last Name')}",
                    middle_name=row.get("Middle Name"),
                    last_name=row.get("Last Name"),
                    source=KinSourceEnum.CSV,
                    owner_id=user_id,
                )
            )
            for field in headers:
                socials = ["twitter", "facebook", "linkedin", "instagram"]
                if "email" in field.lower():
                    fact_type_enum = FactTypeEnum.EMAIL
                elif "phone" in field.lower():
                    fact_type_enum = FactTypeEnum.PHONE
                elif any(x in field.lower() for x in ["day", "date"]):
                    fact_type_enum = FactTypeEnum.DATE
                elif "address" in field.lower():
                    fact_type_enum = FactTypeEnum.ADDRESS
                elif any(x in field.lower() for x in socials):
                    fact_type_enum = FactTypeEnum.SOCIAL_PROFILE
                else:
                    fact_type_enum = FactTypeEnum.CUSTOM
                new_facts.append(
                    Fact(
                        id=shortuuid.uuid(),
                        key=field,
                        value=row.get(field),
                        kin_id=kin_id,
                        fact_type=fact_type_enum,
                    )
                )
        Kin.objects.bulk_create(new_kin)
        Fact.objects.bulk_create(new_facts)
        return {"kin_imported": new_kin.__len__()}


class CSVAttendeeSerializer(serializers.Serializer):
    csv_file = serializers.FileField(required=True)

    def save(self, **kwargs):
        conference = kwargs["conference"]
        file = self.validated_data["csv_file"]

        decoded_file = file.read().decode()
        io_string = io.StringIO(decoded_file)
        reader = csv.DictReader(io_string)

        attendees = []
        for row in reader:
            attendees.append(ConferenceAttendee(**row, conference=conference, id=shortuuid.uuid()))

        try:
            ca = ConferenceAttendee.objects.bulk_create(attendees)
            return {"new_attendees_created": ca.__len__()}
        except Exception as e:
            raise e
