from django.apps import AppConfig


class AdministratorsConfig(AppConfig):
    name = "apps.administrators"
    verbose_name = "Administrators"
