from django.urls import path, include
from rest_framework import routers

from apps.administrators.views import (
    hello_admins,
    UserModelViewset,
    SingleUserCsvUploadView,
    PromoCodeModelViewset,
    ConferenceModelViewset,
    ConferenceAttendeeModelViewset,
)
from apps.kin_admin.views import (
    RemoveOrphanUsersView,
    FlushSpotlightCaches,
    FlushGroupMailtoCaches,
    Analytics,
    TriggerNotificationView,
    SetPaymentStatusView,
    MaintenanceModeViewSet,
)
from apps.kin_auth.views import LogoutView

router = routers.DefaultRouter()
router.register(r"users", UserModelViewset, basename="user")
router.register(r"promo-code", PromoCodeModelViewset, basename="promo-code")
router.register(r"conference", ConferenceModelViewset, basename="conference")
router.register(r"conference-attendee", ConferenceAttendeeModelViewset, basename="attendee")
router.register("maintenance", MaintenanceModeViewSet, basename="maintenance_mode")

urlpatterns = [
    path("", hello_admins),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("users/remove-orphan-users/", RemoveOrphanUsersView.as_view(), name="remove_orphan_users"),
    path(
        "set-user-payment-status/", SetPaymentStatusView.as_view(), name="set_user_payment_status"
    ),
    path("flush-spotlight-caches/", FlushSpotlightCaches.as_view(), name="flush_spotlight_caches"),
    path(
        "flush-group-mailto-caches/",
        FlushGroupMailtoCaches.as_view(),
        name="flush_group_mailto_caches",
    ),
    path("analytics/", Analytics.as_view(), name="analytics"),
    path("trigger-notification/", TriggerNotificationView.as_view(), name="trigger_notification"),
    path("promo-codes/", include("apps.promo_codes.urls")),
    path("users/csv-single-upload/", SingleUserCsvUploadView.as_view(), name="single-user-upload"),
]
urlpatterns += router.urls
