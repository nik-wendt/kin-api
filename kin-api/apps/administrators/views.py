import os

from django.core.files import File
from django.db.models import Q
from django.http import HttpResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, serializers, permissions, viewsets
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response

from apps.administrators.serializers import (
    UsersAdminSerializer,
    SingleUserCsvSerializer,
    PromoCodeAdminSerializer,
    ConferenceAdminSerializer,
    ConferenceAdminWriteSerializer,
    ConferenceAttendeeAdminSerializer,
    CSVAttendeeSerializer,
)
from apps.conferences.filters import ConferenceAttendeeFilter, ConferenceFilter, PromoCodeFilter
from apps.conferences.models import Conference, ConferenceAttendee
from apps.promo_codes.models import PromoCode
from apps.users.filters import UserAdminFilter
from apps.users.models import User


def hello_admins(request):
    return HttpResponse("Hello Administrators!")


class UserModelViewset(viewsets.ModelViewSet):
    serializer_class = UsersAdminSerializer
    permission_classes = [
        permissions.IsAuthenticated,
        permissions.IsAdminUser,
    ]
    queryset = User.objects.all()
    filterset_class = UserAdminFilter

    def list(self, request, *args, **kwargs):
        ret = super().list(request, *args, **kwargs)

        return Response({"users": ret.data})

    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)


class SingleUserCsvUploadView(GenericAPIView):
    serializer_class = SingleUserCsvSerializer
    parser_classes = [FormParser, MultiPartParser]
    permission_classes = [
        permissions.IsAuthenticated,
        permissions.IsAdminUser,
    ]

    def get(self, request):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        f = open(f"{dir_path}/example.csv", "r")
        csvFile = File(f)
        response = HttpResponse(csvFile.read(), content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="example.csv"'

        return response

    @swagger_auto_schema(
        request_body=SingleUserCsvSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="single_user_csv",
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        ret = serializer.save()
        return Response(ret, status=status.HTTP_200_OK)


class PromoCodeModelViewset(viewsets.ModelViewSet):
    serializer_class = PromoCodeAdminSerializer
    permission_classes = [
        permissions.IsAuthenticated,
        permissions.IsAdminUser,
    ]
    queryset = PromoCode.objects.all()
    filterset_class = PromoCodeFilter

    def list(self, request, *args, **kwargs):
        ret = super().list(request, *args, **kwargs)

        return Response(ret.data)

    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)


class ConferenceModelViewset(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
        permissions.IsAdminUser,
    ]
    filterset_class = ConferenceFilter
    queryset = Conference.objects.all()

    def get_serializer_class(self):
        if self.action in ["update", "partial_update", "create"]:
            return ConferenceAdminWriteSerializer
        return ConferenceAdminSerializer

    def list(self, request, *args, **kwargs):
        ret = super().list(request, *args, **kwargs)

        return Response(ret.data)

    def update(self, request, *args, **kwargs):

        return super().update(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @action(detail=True, methods=["GET"])
    def unassigned_promo_codes(self, request, *args, **kwargs):
        queryset = PromoCode.objects.filter(
            Q(conference__id__isnull=True) | Q(conference__id=kwargs["pk"])
        )
        serializer = PromoCodeAdminSerializer

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = serializer(queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=CSVAttendeeSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
        operation_id="import_attendees_csv",
    )
    @action(detail=True, methods=["POST"], parser_classes=(FormParser, MultiPartParser))
    def import_attendees_csv(self, request, *args, **kwargs):
        serializer = CSVAttendeeSerializer(data=request.data)

        serializer.is_valid(raise_exception=True)

        ret = serializer.save(conference=Conference.objects.get(pk=kwargs["pk"]))

        return Response(ret)


class ConferenceAttendeeModelViewset(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
        permissions.IsAdminUser,
    ]
    queryset = ConferenceAttendee.objects.all()
    filterset_class = ConferenceAttendeeFilter

    def get_serializer_class(self):
        # if self.action in ["update", "partial_update", "create"]:
        #     return ConferenceAdminWriteSerializer
        return ConferenceAttendeeAdminSerializer

    def list(self, request, *args, **kwargs):
        ret = super().list(request, *args, **kwargs)
        return Response(ret.data)

    def update(self, request, *args, **kwargs):

        return super().update(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)
