from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string


def send_email(template_name, subject, email_data):

    send_mail(
        subject,
        render_to_string(template_name, email_data),
        settings.DEFAULT_FROM_EMAIL,
        [email_data["email"]],
        html_message=render_to_string(template_name, email_data),
    )
