from collections import OrderedDict

from django.core.exceptions import FieldError
from django.core.paginator import InvalidPage
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.utils.urls import replace_query_param


class CustomPageNumberPagination(PageNumberPagination):
    page_size_query_param = "page_size"
    max_page_size = 100

    def get_paginated_response(self, data):
        total_count = None

        try:
            total_count = (
                self.page.paginator.object_list.model.objects.filter(owner=self.request.user)
                .exclude(id=self.request.user.user_kin.id)
                .count()
            )
        except FieldError:
            try:
                total_count = self.page.paginator.object_list.model.objects.filter(
                    kin__owner=self.request.user
                ).count()
            except FieldError:
                total_count = self.page.paginator.object_list.model.objects.filter(
                    note__owner=self.request.user
                ).count()

        return Response(
            OrderedDict(
                [
                    ("total_count", total_count),
                    ("count", self.page.paginator.count),
                    ("next", self.get_next_link()),
                    ("previous", self.get_previous_link()),
                    ("results", data),
                ]
            )
        )

    def get_paginated_response_schema(self, schema):
        return {
            "type": "object",
            "properties": {
                "total_count": {"type": "integer", "example": 123},
                "count": {"type": "integer", "example": 123},
                "next": {"type": "string", "nullable": True},
                "previous": {"type": "string", "nullable": True},
                "results": schema,
            },
        }


class ESPaginator(CustomPageNumberPagination):
    page_size_query_param = "page_size"
    max_page_size = 100

    def paginate_queryset(self, queryset, request, view=None):

        """
        Paginate a queryset if required, either returning a
        page object, or `None` if pagination is not configured for this view.
        """
        page_size = self.get_page_size(request)
        if not page_size:
            return None

        paginator = self.django_paginator_class(queryset, page_size)
        page_number = self.get_page_number(request, paginator)

        try:
            self.page = paginator.page(page_number)
        except InvalidPage:
            return None

        if paginator.num_pages > 1 and self.template is not None:
            # The browsable API should display pagination controls.
            self.display_page_controls = True

        self.request = request
        return list(self.page)

    def get_paginated_response(self, data, object_name=None):
        total_count = None

        try:
            total_count = (
                self.page.paginator.object_list.model.objects.filter(owner=self.request.user)
                .exclude(id=self.request.user.user_kin.id)
                .count()
            )
        except FieldError:
            try:
                total_count = self.page.paginator.object_list.model.objects.filter(
                    kin__owner=self.request.user
                ).count()
            except FieldError:
                total_count = self.page.paginator.object_list.model.objects.filter(
                    note__owner=self.request.user
                ).count()

        return Response(
            OrderedDict(
                [
                    ("total_count", total_count),
                    ("count", self.page.paginator.count),
                    ("next", self.get_next_link(object_name)),
                    ("previous", self.get_previous_link()),
                    ("results", data),
                ]
            )
        )

    def get_next_link(self, object_name=""):
        if not self.page.has_next():
            return None
        url = self.request.build_absolute_uri()
        page_number = self.page.next_page_number()
        final_url = replace_query_param(url, self.page_query_param, page_number)
        if object_name:
            final_url = f"{final_url}&only_search={object_name}"
        return final_url
