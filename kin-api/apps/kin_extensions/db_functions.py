from django.db.models import Aggregate, CharField, Value


class GroupConcat(Aggregate):
    function = "GROUP_CONCAT"
    template = "%(function)s(%(expressions)s)"

    def __init__(self, expression, delimiter, **extra):
        output_field = extra.pop("output_field", CharField())
        delimiter = Value(delimiter)
        super(GroupConcat, self).__init__(expression, delimiter, output_field=output_field, **extra)

    def as_postgresql(self, compiler, connection):
        self.function = "STRING_AGG"
        return super(GroupConcat, self).as_sql(compiler, connection)
