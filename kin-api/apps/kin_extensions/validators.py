from rest_framework import serializers

from jsonschema import Draft7Validator


def json_schema_to_drf_validate(instance, field, schema):
    validation_errors = []

    v = Draft7Validator(schema)

    if not v.is_valid(instance):
        for error in v.iter_errors(instance):
            try:
                validation_errors.append(f"'{error.absolute_path.pop()}' {error.message}")
            except IndexError:
                validation_errors.append(error.message)

        raise serializers.ValidationError({field: validation_errors})
