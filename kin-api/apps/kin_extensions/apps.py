from django.apps import AppConfig


class KinExtensionsConfig(AppConfig):
    name = "apps.kin_extensions"
    verbose_name = "Kinship Extensions"
