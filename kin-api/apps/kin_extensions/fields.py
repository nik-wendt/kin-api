import logging

from django.db.models import JSONField
from django.core.exceptions import ValidationError

from jsonschema import Draft7Validator

from apps.kin_extensions.geo import GeocodingError, google_reverse_geocoding, osm_reverse_geocoding


logger = logging.getLogger(__name__)


LOCATION_SCHEMA = {
    "type": "object",
    "properties": {
        "lat": {"type": "number", "minimum": -90, "maximum": 90},
        "lon": {"type": "number", "minimum": -180, "maximum": 180},
    },
    "required": ["lat", "lon"],
    "additionalProperties": False,
}


def validate_location(value):
    validation_errors = []

    v = Draft7Validator(LOCATION_SCHEMA)

    if not v.is_valid(value):
        for error in v.iter_errors(value):
            try:
                validation_errors.append(f"'{error.absolute_path.pop()}' {error.message}")
            except IndexError:
                validation_errors.append(error.message)

        raise ValidationError(validation_errors)


class LocationField(JSONField):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.validators.append(validate_location)

    def clean(self, value, model_instance):
        value = super().clean(value, model_instance)

        if value:
            try:
                geo_address_dict = google_reverse_geocoding(value["lat"], value["lon"])
                value.update(geo_address_dict)
            except GeocodingError as ge:
                try:
                    logger.warning("Google gecode fail, attempting OSM: %s", str(ge))
                    geo_address_dict = osm_reverse_geocoding(value["lat"], value["lon"])
                    value.update(geo_address_dict)
                except Exception as e:
                    logger.warning(
                        "Unable to reverse gecode lat %s, lon %s. REASON: %s",
                        value["lat"],
                        value["lon"],
                        str(e),
                    )
                    value.update(
                        {
                            "street_number": "",
                            "street_name": "",
                            "city": "",
                            "state": "",
                            "country": "",
                            "postal_code": "",
                            "natural_feature": "",
                        }
                    )

        return value
