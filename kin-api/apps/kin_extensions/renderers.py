from django.template.loader import render_to_string
from rest_framework import renderers


class QRCodeRenderer(renderers.BaseRenderer):
    media_type = "image/png"
    format = "qrcode"
    charset = None
    render_style = "binary"

    def render(self, data, media_type=None, renderer_context=None):
        return data


class vCardRenderer(renderers.BaseRenderer):
    media_type = "text/x-vcard"
    format = "vcard"
    charset = "UTF-8"
    render_style = "binary"

    def render(self, data, media_type=None, renderer_context=None):
        return data


class PkpassRenderer(renderers.BaseRenderer):
    media_type = "file/pkpass"
    format = "pkpass"
    charset = "UTF-8"
    render_style = "binary"

    def render(self, data, accepted_media_type=None, renderer_context=None):
        return data
