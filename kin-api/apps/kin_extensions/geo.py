from django.conf import settings

import requests


class GeocodingError(Exception):
    pass


def google_reverse_geocoding(lat, lon):
    params = {"latlng": f"{lat},{lon}", "key": settings.GOOGLE_API_KEY}

    resp = requests.get("https://maps.googleapis.com/maps/api/geocode/json", params=params)

    if "error_message" in resp.json():
        raise GeocodingError(resp.json()["error_message"])

    ret_address_dict = {
        "street_number": "",
        "street_name": "",
        "city": "",
        "state": "",
        "country": "",
        "postal_code": "",
        "natural_feature": "",
    }

    address_components = resp.json()["results"][0]["address_components"]

    for component in address_components:
        if "street_number" in component["types"]:
            ret_address_dict["street_number"] = component["short_name"]
        elif "route" in component["types"]:
            ret_address_dict["street_name"] = component["short_name"]
        elif "sublocality" in component["types"]:
            ret_address_dict["city"] = component["short_name"]
        elif "locality" in component["types"]:
            if not ret_address_dict.get("city", ""):
                ret_address_dict["city"] = component["short_name"]
        elif "administrative_area_level_1" in component["types"]:
            ret_address_dict["state"] = component["short_name"]
        elif "country" in component["types"]:
            ret_address_dict["country"] = component["short_name"]
        elif "postal_code" in component["types"]:
            ret_address_dict["postal_code"] = component["short_name"]
        elif "natural_feature" in component["types"]:
            ret_address_dict["natural_feature"] = component["short_name"]
        else:
            continue

    return ret_address_dict


def osm_reverse_geocoding(lat, lon):
    params = {"lat": lat, "lon": lon, "format": "json", "zoom": 18, "addressdetails": 1}

    resp = requests.get("https://nominatim.openstreetmap.org/reverse", params=params)

    ret_address_dict = {"natural_feature": ""}

    try:
        address_components = resp.json()["address"]
    except KeyError:
        raise GeocodingError(resp.text)

    ret_address_dict["street_number"] = address_components.get("house_number", "")
    ret_address_dict["street_name"] = address_components.get("road", "")
    ret_address_dict["city"] = address_components.get("suburb", "") or address_components.get(
        "city", ""
    )
    ret_address_dict["state"] = address_components.get("state", "")
    ret_address_dict["country"] = address_components.get("country_code", "").upper()
    ret_address_dict["postal_code"] = address_components.get("postcode", "")

    return ret_address_dict
