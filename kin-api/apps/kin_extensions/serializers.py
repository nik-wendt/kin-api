from rest_framework import serializers

import phonenumbers
from django_enum_choices.serializers import EnumChoiceField
from django_enum_choices.utils import value_from_built_choice


NO_KEY_MSG = "Key {failing_key} is not a valid {enum_class_name}"


class CustomEnumChoiceField(EnumChoiceField):
    default_error_messages = {"non_existent_key": NO_KEY_MSG}

    def to_internal_value(self, value):
        if isinstance(value, self.enum_class):
            return value

        for choice in self.enum_class:
            if value_from_built_choice(self.choice_builder(choice)) == value:
                return choice

        self.fail("non_existent_key", failing_key=value, enum_class_name=self.enum_class.__name__)


class ChoicesItemSerializer(serializers.Serializer):
    value = serializers.CharField()
    option = serializers.CharField()


class PhoneNumber:
    def __init__(self, phone, country_code):
        self.phone, self.country_code = phone, country_code


class InternationalPhoneNumberField(serializers.Field):
    def to_representation(self, value):
        return phonenumbers.format_number(
            phonenumbers.parse(value.phone, value.country_code),
            phonenumbers.PhoneNumberFormat.INTERNATIONAL,
        )

    def to_internal_value(self, data):
        return data
