# Kinship API Websocket

Kinship Websocket Notifications

## Client Library

[Pusher (JS/React Native)](https://github.com/pusher/pusher-js)

## Authentication & Channel Subscription

### Example (web)
```javascript
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>
  // Enable pusher logging - don't include this in production
  Pusher.logToConsole = true;

  // The first param is the app id, this will be different per environment, dev is shown
  var pusher = new Pusher('d366d77ccea00074a4af', {
    cluster: 'us2',
    forceTLS: true,
    authEndpoint: 'https://{api_domain}/api/v2/auth/pusher/',
    auth: {
        headers: {
            'Authorization': 'Bearer {access_token}'
        }
    }
  });

  var channel = pusher.subscribe('private-encrypted-{user_id}');
  channel.bind('spotlight-recent-kin-cache', function(data) {
    alert(JSON.stringify(data));
  });
</script>
```

## Notification Types

### Spotlight Task Names

+ spotlight-recent-kin-cache
+ spotlight-vintage-kin-cache
+ spotlight-popular-kin-cache
+ spotlight-upcoming-reminders-cache
+ spotlight-upcoming-events-cache
+ spotlight-unassigned-notes-cache
+ spotlight-special-occasions-cache

> All of the spotlight cache notifications will have a status_code of 200, and the message field will contain the new cached payload accept for spotlight-unassigned-notes-cache & spotlight-upcoming-events-cache
which have too large of payloads to send over a websocket, so request to the endpoint will need to be made on receipt of that event

### SMS Task Names

+ sms-verification

  > will have a status_code of 200 if phone number has been verified, otherwise it'll be a 400. The message field will be a string with the success or fail message.

### Kin Import Task Names

+ kin-import

  > will have a status_code of 200 if kin validated and saved successfully, otherwise it'll be a 400.


### All Kin Task Names

+ all-kin-cache

  > will have a status_code of 200, message will be empty (payload is too large to send over socket) so a request to the endpoint will need to be made on receipt of this event
