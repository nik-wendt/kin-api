import json

from django.conf import settings

import redis


class FlashNotificationMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        self.redis_client = redis.StrictRedis.from_url(
            settings.REDIS_URL, charset="utf-8", decode_responses=True
        )

    def __call__(self, request):
        response = self.get_response(request)

        user = request.user
        if user.is_authenticated:
            redis_key = f"notifications:{user.id}"
            notifications = self.redis_client.lrange(redis_key, 0, -1)
            response["X-KIN-NOTIFICATIONS"] = json.dumps(notifications)
            self.redis_client.delete(redis_key)

        return response
