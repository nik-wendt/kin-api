import json
import logging

from django.conf import settings

import pusher
import redis
import requests

from apps.users.models import User


logger = logging.getLogger(__name__)


def _send_user_push_notification(message, user_id, tz_aware=False):
    headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": f"Basic {settings.ONE_SIGNAL_API_KEY}",
    }

    payload = {
        "app_id": settings.ONE_SIGNAL_APP_ID,
        "include_external_user_ids": [user_id],
        "channel_for_external_user_ids": "push",
        "contents": {"en": message},
    }

    if tz_aware:
        payload["delayed_option"] = "timezone"
        payload["delivery_time_of_day"] = settings.ONE_SIGNAL_PUSH_NOTIFICATION_TIME

    resp = requests.post(
        "https://onesignal.com/api/v1/notifications", headers=headers, data=json.dumps(payload)
    )

    if resp.status_code != 200:
        error_msg = f"Failed to send push notification to {user_id}: {message}"
        logger.error(error_msg, extra={"user_id": user_id, "results": resp.text})
        return

    success_msg = f"Sent push notification to {user_id}: {message}"
    logger.info(success_msg, extra={"user_id": user_id, "results": resp.text})


def _set_flash_notification(message, user_id):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_client.lpush(f"notifications:{user_id}", message)


def send_notification(message, user_id, tz_aware=False):
    try:
        user = User.objects.get(pk=user_id, is_active=True)

        if user.preferences.receive_push_notifications:
            _send_user_push_notification(message, user_id, tz_aware)
        else:
            _set_flash_notification(message, user_id)
    except User.DoesNotExist:
        logger.error(
            "Couldn't send notification. User %s doesn't exist or is inactive",
            user_id,
            extra={"user_id": user_id},
        )


def get_pusher_client():
    client = pusher.Pusher(**settings.PUSHER)

    return client
