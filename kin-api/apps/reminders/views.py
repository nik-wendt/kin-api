from rest_framework import mixins, permissions, serializers, status, viewsets
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from drf_yasg.utils import swagger_auto_schema

from apps.kin_auth.permissions import IsPaid, IsVerified
from apps.reminders.choices import FrequencyEnum, RecencyEnum
from apps.reminders.models import Reminder
from apps.reminders.serializers import ReminderChoicesSerializer, ReminderSerializer


class ReminderViewSet(mixins.CreateModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = ReminderSerializer

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Reminder.objects.none()

        return Reminder.objects.filter(kin__owner=self.request.user)

    @swagger_auto_schema(
        request_body=serializers.Serializer, responses={status.HTTP_200_OK: ReminderSerializer}
    )
    @action(detail=True, methods=["POST"])
    def dismiss(self, request, pk=None):
        instance = self.get_object()
        instance.set_next_reminder_date(save=False)
        instance.notification_sent = False
        instance.dismissed = True
        instance.save()

        output_serializer = ReminderSerializer(instance)
        return Response(output_serializer.data, status=status.HTTP_200_OK)


class ReminderChoicesViewSet(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(responses={status.HTTP_200_OK: ReminderChoicesSerializer})
    def get(self, request):
        frequency_choices = [{"value": e.name, "option": e.value} for e in FrequencyEnum]
        recency_choices = [{"value": e.name, "option": e.value} for e in RecencyEnum]

        serializer = ReminderChoicesSerializer(
            data={"frequency_choices": frequency_choices, "recency_choices": recency_choices}
        )
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
