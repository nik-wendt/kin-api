from rest_framework import serializers

from django_enum_choices.choice_builders import attribute_value
from django_enum_choices.serializers import EnumChoiceModelSerializerMixin

from apps.kin.models import Kin
from apps.kin_extensions.serializers import ChoicesItemSerializer, CustomEnumChoiceField
from apps.reminders.choices import FrequencyEnum, RecencyEnum
from apps.reminders.models import Reminder


class KinPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        user = self.context["request"].user
        queryset = Kin.objects.filter(owner=user)
        return queryset


class ReminderSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    kin = KinPrimaryKeyRelatedField()
    frequency = CustomEnumChoiceField(FrequencyEnum, choice_builder=attribute_value)
    recency = CustomEnumChoiceField(RecencyEnum, choice_builder=attribute_value, read_only=True)

    class Meta:
        model = Reminder
        fields = "__all__"
        read_only_fields = ("id", "created", "recency", "next_notification")

    def create(self, validated_data):
        try:
            validated_data["kin"].reminder.delete()
        except Reminder.DoesNotExist:
            pass

        reminder = super().create(validated_data)
        reminder.set_next_reminder_date(save=True)

        return reminder


class ReminderChoicesSerializer(serializers.Serializer):
    frequency_choices = ChoicesItemSerializer(many=True)
    recency_choices = ChoicesItemSerializer(many=True)
