from django.apps import AppConfig


class RemindersConfig(AppConfig):
    name = "apps.reminders"
    verbose_name = "Reminders"
