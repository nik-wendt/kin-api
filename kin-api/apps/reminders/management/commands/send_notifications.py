import logging
from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils import timezone

from apps.notifications.utils import send_notification
from apps.reminders.models import Reminder
from apps.users.models import User

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Sends out notifications for all users' timely kin reminders."

    def handle(self, *args, **options):
        logger.info("Starting reminder notification sending...")

        users = User.objects.filter(
            kin__reminder__next_notification__lte=timezone.now(),
            kin__reminder__notification_sent=False,
        ).distinct()

        for user in users:
            reminders = Reminder.objects.filter(
                kin__owner=user, next_notification__lte=timezone.now(), notification_sent=False
            ).exclude(dismissed=True)
            reminders_count = reminders.count()
            reminder_kin_names = reminders.values_list("kin__friendly_name", flat=True)

            message = f"Good morning! Today is a great day to contact {', '.join(reminder_kin_names[0:3])}"

            if reminders_count > 3:
                message += ", and others!"
            else:
                message += "!"

            send_notification(message, user.id, tz_aware=True)

            reminders.update(notification_sent=True)

        logger.info("Finshed reminder notification sending.")
