import logging

from django.core.management.base import BaseCommand

from apps.reminders.models import Reminder


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Updates next notification date on reminders that are passed."

    def handle(self, *args, **options):
        logger.info("Starting reminder notification schedule updater...")

        sent_reminders = Reminder.objects.filter(notification_sent=True)
        for reminder in sent_reminders:
            reminder.set_next_reminder_date(save=False)
            reminder.notification_sent = False
            reminder.save()

        dismissed_reminders = Reminder.objects.filter(dismissed=True)
        dismissed_reminders.update(dismissed=False)

        logger.info("Finished reminder notification schedule updater.")
