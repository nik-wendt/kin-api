import random
from datetime import timedelta

from django.db import models
from django.utils import timezone
from django.utils.functional import cached_property

from django_enum_choices.choice_builders import attribute_value
from django_enum_choices.fields import EnumChoiceField
from django_extensions.db.fields import ShortUUIDField

from apps.kin.models import Kin
from apps.reminders.choices import FrequencyEnum, RecencyEnum


class Reminder(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    kin = models.OneToOneField(
        Kin, on_delete=models.CASCADE, db_column="peep_id", related_name="reminder"
    )
    frequency = EnumChoiceField(FrequencyEnum, choice_builder=attribute_value)
    next_notification = models.DateTimeField(null=True, blank=True)
    notification_sent = models.BooleanField(default=False)
    dismissed = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    @cached_property
    def recency(self):
        if not self.next_notification:
            return None

        delta = self.next_notification - timezone.now()

        if (delta.days >= 0) and (delta.days < 4):
            return RecencyEnum.SOON

        return RecencyEnum.LATER

    @cached_property
    def far_away(self):
        if not self.next_notification:
            return None

        delta = self.next_notification - timezone.now()

        if delta.days >= 30:
            return True

        return False

    def set_next_reminder_date(self, save=True):
        frequency = self.frequency

        starting_date = timezone.now()

        differential = 0
        num_days = 0

        if frequency == FrequencyEnum.WEEKLY:
            differential = random.randint(1, 2)
            num_days = 6 + differential
        elif frequency == FrequencyEnum.MONTHLY:
            differential = random.randint(1, 11)
            num_days = 25 + differential
        elif frequency == FrequencyEnum.QUARTERLY:
            differential = random.randint(1, 21)
            num_days = 80 + differential
        elif frequency == FrequencyEnum.ANNUALLY:
            differential = random.randint(1, 61)
            num_days = 335 + differential
        elif frequency == FrequencyEnum.SEMIANNUALLY:
            differential = random.randint(1, 61)
            num_days = 700 + differential

        self.next_notification = starting_date + timedelta(days=num_days)

        if save:
            self.save()
