from enum import Enum


class FrequencyEnum(Enum):
    WEEKLY = "Week"
    MONTHLY = "Month"
    QUARTERLY = "Quarter"
    ANNUALLY = "Year"
    SEMIANNUALLY = "Two years"


class RecencyEnum(Enum):
    SOON = "SOON"
    LATER = "LATER"
