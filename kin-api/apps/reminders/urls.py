from django.urls import path
from rest_framework import routers

from apps.reminders.views import ReminderChoicesViewSet, ReminderViewSet


router = routers.DefaultRouter()
router.register("", ReminderViewSet, basename="reminder")


urlpatterns = [path("choices/", ReminderChoicesViewSet.as_view())]


urlpatterns += router.urls
