from django.apps import AppConfig


class CMSConfig(AppConfig):
    name = "apps.cms"
    verbose_name = "CMS"
