from django.db import models

from django_extensions.db.fields import ShortUUIDField


class LoadingText(models.Model):
    id = ShortUUIDField(primary_key=True)
    headline = models.TextField(blank=True)
    body = models.TextField(blank=True)

    class Meta:
        db_table = "loading_loadingtext"
