from rest_framework import viewsets

from apps.cms.models import LoadingText
from apps.cms.serializers import LoadingTextSerializer


class LoadingTextViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = LoadingTextSerializer

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return LoadingText.objects.none()
        return LoadingText.objects.all().order_by("?")[:1]
