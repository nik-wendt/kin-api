from rest_framework import serializers

from apps.cms.models import LoadingText


class LoadingTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = LoadingText
        fields = ("headline", "body")
