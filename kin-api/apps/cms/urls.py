from rest_framework import routers

from apps.cms.views import LoadingTextViewSet


router = routers.DefaultRouter()
router.register("loading", LoadingTextViewSet, basename="loading-text")

urlpatterns = router.urls
