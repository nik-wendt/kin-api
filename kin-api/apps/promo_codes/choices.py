from enum import Enum


class PromoCodeLifeSpanTypeEnum(Enum):
    LIMITED_USES = "LIMITED_USES"
    UNLIMITED_USES = "UNLIMITED_USES"
    LIMITED_TIME = "LIMITED_TIME"


class PromoCodeTypeEnum(Enum):
    TRIAL_EXTENSION = "TRIAL_EXTENSION"
    LIFETIME_SUBSCRIPTION = "LIFETIME_SUBSCRIPTION"
