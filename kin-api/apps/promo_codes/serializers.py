from datetime import timedelta

from django.db import transaction
from django.utils import timezone
from rest_framework import serializers

from django_enum_choices.serializers import EnumChoiceModelSerializerMixin

from apps.promo_codes.choices import PromoCodeLifeSpanTypeEnum, PromoCodeTypeEnum
from apps.promo_codes.models import PromoCode, Redemption
from apps.users.choices import PaymentStatusEnum


class RedemptionSerializer(serializers.ModelSerializer):
    promo_code = serializers.CharField(max_length=15)

    class Meta:
        model = Redemption
        read_only_fields = ("id", "created")
        exclude = ("redeemer",)

    def validate_promo_code(self, value):
        try:
            promo_code_instance = PromoCode.objects.get(code=value)

            if promo_code_instance.expiration_date < timezone.now().date():
                raise serializers.ValidationError("The promo code is invalid or expired.")

            if promo_code_instance.life_span_type == PromoCodeLifeSpanTypeEnum.LIMITED_USES:
                num_redemptions = Redemption.objects.filter(promo_code=promo_code_instance).count()
                if num_redemptions >= promo_code_instance.limited_use_count:
                    raise serializers.ValidationError("The promo code is invalid or expired.")
        except PromoCode.DoesNotExist:
            raise serializers.ValidationError("The promo code is invalid or expired.")

        return promo_code_instance

    def validate_redeemer(self, value):
        if value.payment_status != PaymentStatusEnum.TRIAL:
            raise serializers.ValidationError(
                {"promo_code": ["Promo codes may only be used during the trial period."]}
            )

        if Redemption.objects.filter(redeemer=value).exists():
            raise serializers.ValidationError(
                {"promo_code": ["This user has already redeemed a promo code."]}
            )

        return value

    def save(self, **kwargs):
        with transaction.atomic():
            _ = self.validate_redeemer(kwargs["redeemer"])

            redemption = super().save(**kwargs)

            if redemption.promo_code.promo_code_type == PromoCodeTypeEnum.TRIAL_EXTENSION:
                redemption.redeemer.payment_expiration = (
                    redemption.redeemer.date_joined
                    + timedelta(days=redemption.promo_code.num_trial_days)
                )
                redemption.redeemer.save()

            if redemption.promo_code.promo_code_type == PromoCodeTypeEnum.LIFETIME_SUBSCRIPTION:
                redemption.redeemer.payment_status = PaymentStatusEnum.LIFETIME_SUBSCRIPTION
                redemption.redeemer.payment_provider = None
                redemption.redeemer.payment_expiration = None
                redemption.redeemer.payment_subscription_type = None
                redemption.redeemer.save()

            return redemption


class PromoCodeReadSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    user = serializers.CharField(required=False, allow_null=True, allow_blank=True)

    class Meta:
        model = PromoCode
        exclude = ("creator",)
