from django.apps import AppConfig


class PromoCodesConfig(AppConfig):
    name = "apps.promo_codes"
    verbose_name = "Promo Codes"
