import random
import string


def generate_random_promo_code(length=8):
    promo_code = "".join(random.choices(string.ascii_uppercase + string.octdigits, k=length))
    return promo_code
