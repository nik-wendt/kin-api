from django.db import models

from django_enum_choices.fields import EnumChoiceField
from django_extensions.db.fields import ShortUUIDField

from apps.promo_codes.choices import PromoCodeLifeSpanTypeEnum, PromoCodeTypeEnum
from apps.promo_codes.utils import generate_random_promo_code
from apps.users.models import User


class PromoCode(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    code = models.CharField(max_length=15, unique=True, default=generate_random_promo_code)
    description = models.TextField(blank=True)
    promo_code_type = EnumChoiceField(
        PromoCodeTypeEnum,
        help_text="Choices are <strong>TRIAL_EXTENSION</strong> or <strong>LIFETIME_SUBSCRIPTION</strong>. Default: <strong>TRIAL_EXTENSION</strong>.",
    )
    num_trial_days = models.PositiveIntegerField(
        blank=True,
        null=True,
        help_text="The number of days the user's free trial period should last. Required if promo code type is TRIAL_EXTENSION. Leave null for other promo code types.",
    )
    life_span_type = EnumChoiceField(
        PromoCodeLifeSpanTypeEnum,
        help_text="Choices are <strong>LIMITED_USES</strong>, <strong>UNLIMITED_USES</strong>, or <strong>LIMITED_TIME</strong>.",
    )
    limited_use_count = models.PositiveIntegerField(
        blank=True, null=True, help_text="Set for life span type of LIMITED_USES only."
    )
    expiration_date = models.DateField(
        help_text="The date the promo code is no longer able to be redeemed."
    )
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="owned_promo_codes",
        limit_choices_to={"is_staff": True},
    )
    creator = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="created_promo_codes",
        limit_choices_to={"is_staff": True},
    )
    created = models.DateTimeField(auto_now_add=True)

    @property
    def used_count(self):
        return Redemption.objects.filter(promo_code=self).count()

    class Meta:
        ordering = ["-created"]

    def __str__(self):
        return self.code


class Redemption(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    promo_code = models.ForeignKey(PromoCode, on_delete=models.PROTECT, related_name="redemptions")
    redeemer = models.ForeignKey(User, on_delete=models.CASCADE, related_name="redemptions")
    created = models.DateTimeField(auto_now_add=True)
