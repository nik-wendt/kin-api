from rest_framework import routers

from apps.promo_codes.views import PromoCodeReadViewSet, PromoCodeRedemptionViewSet


router = routers.DefaultRouter()
router.register("promo-codes", PromoCodeReadViewSet, basename="promo-codes")
router.register("redemptions", PromoCodeRedemptionViewSet, basename="redemptions")


urlpatterns = router.urls
