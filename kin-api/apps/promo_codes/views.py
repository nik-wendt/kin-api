from rest_framework import mixins, permissions, status, viewsets
from rest_framework.response import Response

from drf_yasg.utils import swagger_auto_schema

from apps.kin_auth.permissions import IsPaid, IsVerified
from apps.promo_codes.models import PromoCode
from apps.promo_codes.serializers import PromoCodeReadSerializer, RedemptionSerializer
from apps.users.serializers import UserSerializer


class PromoCodeRedemptionViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = RedemptionSerializer

    @swagger_auto_schema(
        request_body=RedemptionSerializer,
        responses={status.HTTP_200_OK: UserSerializer},
        operation_id="promo_codes_redemption",
    )
    def create(self, request, *args, **kwargs):
        user = request.user
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(redeemer=user)
        user.refresh_from_db()

        headers = self.get_success_headers(serializer.data)

        success_serializer = UserSerializer(user, context={"request": request})
        return Response(success_serializer.data, status=status.HTTP_200_OK, headers=headers)


class PromoCodeReadViewSet(
    mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    permission_classes = [permissions.IsAuthenticated, permissions.IsAdminUser, IsVerified]
    serializer_class = PromoCodeReadSerializer

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return PromoCode.objects.none()
        is_super = self.request.user.is_superuser
        if is_super:
            qs = PromoCode.objects.all()
        else:
            qs = PromoCode.objects.filter(owner=self.request.user)
        return qs
