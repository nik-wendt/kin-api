from rest_framework import serializers, status
from rest_framework.response import Response
from rest_framework.views import APIView

from drf_yasg.utils import swagger_auto_schema

from apps.client_logging.serializers import ClientLoggingSerializer


class ClientLoggingView(APIView):
    permission_classes = []

    @swagger_auto_schema(
        request_body=ClientLoggingSerializer,
        responses={status.HTTP_201_CREATED: serializers.Serializer},
        operation_id="client_logging_create",
    )
    def post(self, request, *args, **kwargs):
        serializer = ClientLoggingSerializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_201_CREATED)
