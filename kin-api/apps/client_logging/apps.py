from django.apps import AppConfig


class ClientLoggingConfig(AppConfig):
    name = "apps.client_logging"
    verbose_name = "Client Logging"
