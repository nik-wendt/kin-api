import logging

from rest_framework import serializers

from django_enum_choices.serializers import EnumChoiceField

from apps.client_logging.choices import ClientLogLevelEnum


logger = logging.getLogger(__name__)


class ClientLoggingSerializer(serializers.Serializer):
    message = serializers.CharField()
    client_os_name = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    client_os_version = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    log_level = EnumChoiceField(
        ClientLogLevelEnum,
        default=ClientLogLevelEnum.ERROR,
        help_text="Choices are <strong>DEBUG</strong>, <strong>INFO</strong>, <strong>WARNING</strong>, or <strong>ERROR</strong>. Default: <strong>ERROR</strong>.",
    )

    def create(self, validated_data):
        user = self._context["request"].user
        message = validated_data["message"]
        client_os_name = validated_data.get("client_os_name", None)
        client_os_version = validated_data.get("client_os_version", None)
        app_build_version = self._context["request"].META.get("HTTP_X_APP_BUILD_VERSION", "")

        extra = {
            "client": True,
            "user_id": user.id,
            "client_os_name": client_os_name,
            "client_os_version": client_os_version,
            "app_build_version": app_build_version,
        }

        if validated_data["log_level"] == ClientLogLevelEnum.DEBUG:
            logger.debug(message, extra=extra)
        elif validated_data["log_level"] == ClientLogLevelEnum.INFO:
            logger.info(message, extra=extra)
        elif validated_data["log_level"] == ClientLogLevelEnum.WARNING:
            logger.warning(message, extra=extra)
        elif validated_data["log_level"] == ClientLogLevelEnum.ERROR:
            logger.error(message, extra=extra)

        return {}
