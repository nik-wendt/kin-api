from django.urls import path

from apps.client_logging.views import ClientLoggingView


urlpatterns = [path("", ClientLoggingView.as_view(), name="client-logging")]
