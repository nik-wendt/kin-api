from django.apps import AppConfig


class SESIntegrationManagementConfig(AppConfig):
    name = "apps.integrations.ses_management"
    verbose_name = "SES Management"
