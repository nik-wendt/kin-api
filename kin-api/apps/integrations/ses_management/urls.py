from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from apps.integrations.ses_management.views import email_to_note, handle_bounce


urlpatterns = [
    url(r"^bounce/$", csrf_exempt(handle_bounce)),
    url(r"^email-note/$", csrf_exempt(email_to_note)),
]
