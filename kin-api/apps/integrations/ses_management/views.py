import json

from django.http import HttpResponse
from django.views.decorators.http import require_POST

from apps.integrations.ses_management.tasks import process_note_message


@require_POST
def handle_bounce(request):
    raw_json = request.body

    # TODO: add logging of bounces

    return HttpResponse()


@require_POST
def email_to_note(request):
    incoming_data = json.loads(request.body.decode("utf-8"))

    records = incoming_data["Records"]

    for record in records:
        bucket_name = record["s3"]["bucket"]["name"]
        object_key = record["s3"]["object"]["key"]

        process_note_message.delay(bucket_name, object_key)

    return HttpResponse()
