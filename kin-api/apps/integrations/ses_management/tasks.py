import base64
import logging
import os
import re

from celery import shared_task
from django.conf import settings
from django.core.files.base import File
from django.db import transaction
from django.http import HttpRequest

import boto3
import mailparser

from apps.kin_extensions.constants import EMAIL_REGEX
from apps.notes.choices import NoteSourceEnum
from apps.notes.serializers import NoteMediaSerializer, NoteWriteSerializer
from apps.users.models import User


logger = logging.getLogger(__name__)


def get_s3_client():
    s3_client_params = {
        "aws_access_key_id": settings.AWS_ACCESS_KEY_ID,
        "aws_secret_access_key": settings.AWS_SECRET_ACCESS_KEY,
        "region_name": "us-east-2",
    }

    s3_client = boto3.client("s3", **s3_client_params)

    return s3_client


@shared_task
def process_note_message(bucket_name, object_key):
    client = get_s3_client()

    raw_email = client.get_object(Bucket=bucket_name, Key=object_key)["Body"].read().decode("utf-8")

    email_message = mailparser.parse_from_string(raw_email)

    try:
        to_address = next(r for r in email_message.to[0] if re.match(EMAIL_REGEX, r))
        user_id = re.search(r"\+(.*?)\@", to_address).group(1)
        user = User.objects.get(pk=user_id)

        with transaction.atomic():
            faux_request = HttpRequest()
            faux_request.user = user
            note_serializer = NoteWriteSerializer(
                data={
                    "text": email_message.body.split("--- mail_boundary ---", 1)[0],
                    "source": NoteSourceEnum.EMAIL.value,
                },
                context={"request": faux_request},
            )
            note_serializer.is_valid(raise_exception=True)
            note = note_serializer.save(owner=user)

            for attachment in email_message.attachments:
                filename = f"/tmp/{attachment['filename']}"
                open(filename, "wb").write(base64.b64decode(attachment["payload"]))
                fh = open(filename, "rb")
                save_file = File(fh)

                note_media_serializer = NoteMediaSerializer(data={"media": save_file})
                note_media_serializer.is_valid(raise_exception=True)
                note_media_serializer.save(note=note)

                os.remove(filename)
    except Exception as e:
        logger.error("Unfortunately we couldn't save a note to Kinship. %s", str(e))
