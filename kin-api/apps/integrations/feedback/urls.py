from django.urls import path

from apps.integrations.feedback.views import (
    CustomerFeedbackSubmissionView,
)


urlpatterns = [
    path("", CustomerFeedbackSubmissionView.as_view(), name="submit_feedback"),
]
