from rest_framework import serializers, status
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from drf_yasg.utils import swagger_auto_schema

from apps.integrations.feedback.serializers import CustomerFeedbackCreateSerializer


class CustomerFeedbackSubmissionView(APIView):
    parser_classes = [FormParser, MultiPartParser]

    @swagger_auto_schema(
        request_body=CustomerFeedbackCreateSerializer,
        responses={status.HTTP_201_CREATED: serializers.Serializer},
    )
    def post(self, request, *args, **kwargs):
        serializer = CustomerFeedbackCreateSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_201_CREATED)
