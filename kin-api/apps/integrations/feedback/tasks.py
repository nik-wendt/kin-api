from celery import shared_task
from django.core.mail import EmailMessage


@shared_task
def task_send_mail(subject, payload):
    message = EmailMessage(
        subject=subject,
        to=["support@kinshipsystems.com"],
    )
    if payload.get("attachments"):
        for attachment in payload.get("attachments"):
            message.attach_file(attachment)
    message.body = f"""
    Name: {payload.get('name')}
    Email: {payload.get('email')}
    Description: {payload.get('description')}
    
    """
    message.send()
