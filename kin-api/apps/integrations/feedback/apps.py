from django.apps import AppConfig


class FeedbackIntegrationConfig(AppConfig):
    name = "apps.integrations.feedback"
    verbose_name = "Feedback Integration"
