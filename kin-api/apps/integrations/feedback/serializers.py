import logging
import os
import uuid

from rest_framework import serializers

from apps.integrations.feedback.tasks import task_send_mail
from apps.users.models import User

logger = logging.getLogger(__name__)


class CustomerFeedbackCreateSerializer(serializers.Serializer):
    email = serializers.EmailField(
        required=False, help_text="Required when user is not authenticated."
    )
    description = serializers.CharField()
    attachment = serializers.ImageField(required=False, allow_null=True)
    name = serializers.CharField(
        required=False, help_text="Required when user is not authenticated."
    )

    def validate(self, attrs):
        user = self._context["request"].user

        if not user.is_authenticated:
            if not attrs.get("email", ""):
                raise serializers.ValidationError(
                    {"email": "Email is required for a non-authenticated user."}
                )

            if not attrs.get("name", ""):
                raise serializers.ValidationError(
                    {"name": "Name is required for a non-authenticated user."}
                )

        if user.is_authenticated:
            attrs["email"] = user.email
            attrs["name"] = f"{user.first_name} {user.last_name}"
        else:
            attrs["email"] = attrs["email"].lower()

            if User.objects.filter(email=attrs["email"]).exists():
                unauthenticated_user = User.objects.get(email=attrs["email"])
                attrs[
                    "name"
                ] = f"{unauthenticated_user.first_name} {unauthenticated_user.last_name}"

        return attrs

    def create(self, validated_data):
        subject = "In-app Customer Issue"
        payload = {"email": validated_data["email"], "description": validated_data["description"]}

        if validated_data.get("name", ""):
            payload["name"] = validated_data["name"]

        if validated_data.get("attachment", None):
            file_extension = os.path.splitext(str(validated_data["attachment"]))[1]
            temp_file_path = f"/tmp/{uuid.uuid4()}{file_extension}"

            with open(temp_file_path, "wb+") as temp_file:
                for chunk in validated_data["attachment"].chunks():
                    temp_file.write(chunk)

            payload["attachments"] = [temp_file_path]

        task_send_mail(subject, payload)

        return {}


class CustomerFeedbackRequesterSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)
