from django.urls import path

from apps.integrations.twilio.views import GetVerificationSMSView, TwilioSMSView


urlpatterns = [
    path("", TwilioSMSView.as_view(), name="sms"),
    path("get-verification-sms/", GetVerificationSMSView.as_view(), name="get-verification-sms"),
]
