from django.apps import AppConfig


class TwilioIntegrationConfig(AppConfig):
    name = "apps.integrations.twilio"
    verbose_name = "Twilio"
