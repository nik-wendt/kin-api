from django.conf import settings
from django.template.loader import render_to_string
from rest_framework import exceptions, permissions, serializers, status
from rest_framework.response import Response
from rest_framework.views import APIView

from drf_yasg.utils import swagger_auto_schema
from itsdangerous.url_safe import URLSafeTimedSerializer
from twilio.rest import Client

from apps.integrations.twilio.serializers import (
    TwilioIncomingSMSSerializer,
    VerificationSMSSerializer,
)
from apps.integrations.twilio.tasks import process_verification_message, process_note_message_twilio
from apps.kin_auth.permissions import IsPaid, IsVerified
from apps.kin_extensions.serializers import PhoneNumber
from apps.users.models import User


class TwilioSMSView(APIView):
    def _process_verification_message(self, data):
        process_verification_message.delay(
            body=data["Body"], from_phone=data["From"], from_country=data["FromCountry"]
        )

    def _process_note_message(self, user, data):
        num_media = int(data["NumMedia"])
        protocol = "whatsapp" if data["From"].startswith("whatsapp") else "sms"
        media_urls = []

        for i in range(num_media):
            media_urls.append(data[f"MediaUrl{i}"])

        serializer = TwilioIncomingSMSSerializer(
            data={"body": data["Body"], "media_urls": media_urls}
        )
        serializer.is_valid(raise_exception=True)

        process_note_message_twilio.delay(
            user_id=user.id,
            from_phone=data["From"],
            note_text=serializer.data["body"],
            media_urls=media_urls,
            protocol=protocol,
        )

    @swagger_auto_schema(
        request_body=TwilioIncomingSMSSerializer,
        responses={status.HTTP_200_OK: serializers.Serializer},
    )
    def post(self, request, *args, **kwargs):
        if "+Code+" in request.data["Body"]:
            self._process_verification_message(request.data)
        else:
            try:
                from_phone_only = request.data["From"]
                from_phone_only = from_phone_only.replace("whatsapp:", "")
                user = User.objects.get(phone_numbers__phone=from_phone_only, is_verified=True)
                self._process_note_message(user, request.data)
            except User.DoesNotExist:
                client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
                client.messages.create(
                    body="You must verify your phone number in the Kinship app before using this feature.",
                    from_=request.data["To"],
                    to=request.data["From"],
                )
                raise exceptions.AuthenticationFailed("No such user")

        return Response(status=status.HTTP_200_OK)


class GetVerificationSMSView(APIView):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)

    @swagger_auto_schema(
        responses={status.HTTP_200_OK: VerificationSMSSerializer},
        operation_id="sms_get_verification_sms",
    )
    def get(self, request, *args, **kwargs):
        s = URLSafeTimedSerializer(settings.SECRET_KEY)
        token = s.dumps({"user_id": request.user.id})

        sms = render_to_string("verification_sms.txt", {"token": token})

        to_obj = PhoneNumber(settings.TWILIO_SMS_PHONE, "US")

        serializer = VerificationSMSSerializer(data={"to": to_obj, "message": sms})
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data, status=status.HTTP_200_OK)
