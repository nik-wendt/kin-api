import logging
import mimetypes
import os

from celery import shared_task
from django.conf import settings
from django.core.files.base import File
from django.db import IntegrityError, transaction
from django.http import HttpRequest
from django.utils import timezone
from rest_framework import status

import requests
from itsdangerous.exc import BadSignature
from itsdangerous.url_safe import URLSafeTimedSerializer
from twilio.rest import Client

from apps.notes.choices import NoteSourceEnum
from apps.notes.serializers import NoteMediaSerializer, NoteWriteSerializer
from apps.notifications.utils import get_pusher_client
from apps.users.models import User, UserPhone


logger = logging.getLogger(__name__)


@shared_task
def process_verification_message(*, body, from_phone, from_country):
    client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)

    try:
        token = body.split("+", 2)[2].strip()
        s = URLSafeTimedSerializer(settings.SECRET_KEY)
        decoded_token = s.loads(token, max_age=10 * 60)

        user = User.objects.get(id=decoded_token["user_id"])
    except (User.DoesNotExist, BadSignature) as e:
        client.messages.create(
            body="Invalid code. Please request phone number verification in the Kinship app again.",
            from_=settings.TWILIO_SMS_PHONE,
            to=from_phone,
        )
        return

    try:
        UserPhone.objects.create(
            user=user, phone=from_phone, phone_country=from_country, is_verified=True,
        )
        client.messages.create(
            body="Your phone number is now verified and connected to your Kinship account.",
            from_=settings.TWILIO_SMS_PHONE,
            to=from_phone,
        )

        pc = get_pusher_client()
        pc.trigger(
            f"private-encrypted-{user.id}",
            "sms-verification",
            {
                "status_code": status.HTTP_200_OK,
                "message": "Phone number verified",
                "timestamp": str(timezone.now()),
            },
        )
    except IntegrityError:
        client.messages.create(
            body="Turns out this phone number is already connected to a Kinship account.",
            from_=settings.TWILIO_SMS_PHONE,
            to=from_phone,
        )

        pc = get_pusher_client()
        pc.trigger(
            f"private-encrypted-{user.id}",
            "sms-verification",
            {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": "Duplicate phone number",
                "timestamp": str(timezone.now()),
            },
        )


@shared_task
def process_note_message_twilio(*, user_id, note_text, media_urls, protocol, from_phone):

    user = User.objects.get(pk=user_id)

    to_phone = from_phone
    from_phone = (
        f"whatsapp:{settings.TWILIO_WHATSAPP_PHONE}"
        if protocol == "whatsapp"
        else settings.TWILIO_SMS_PHONE
    )

    client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)

    try:
        with transaction.atomic():
            faux_request = HttpRequest()
            faux_request.user = user
            note_serializer = NoteWriteSerializer(
                data={"text": note_text, "source": NoteSourceEnum.SMS.value},
                context={"request": faux_request},
            )
            note_serializer.is_valid(raise_exception=True)
            note = note_serializer.save(owner=user)

            for i, media_url in enumerate(media_urls):
                r = requests.get(media_url, allow_redirects=True)
                mime_type = r.headers.get("content-type")
                extension = mimetypes.guess_extension(mime_type)
                filename = f"/tmp/{note.id}_{i}{extension}"
                open(filename, "wb").write(r.content)
                fh = open(filename, "rb")
                save_file = File(fh)

                note_media_serializer = NoteMediaSerializer(data={"media": save_file})
                note_media_serializer.is_valid(raise_exception=True)
                note_media_serializer.save(note=note)

                os.remove(filename)

        client.messages.create(
            body="Your note has been safely added to your unassigned notes in the Kinship app!",
            from_=from_phone,
            to=to_phone,
        )
    except Exception as e:
        client.messages.create(
            body="Unfortunately we couldn't save your note to Kinship. Please try again or file a bug report.",
            from_=from_phone,
            to=to_phone,
        )
