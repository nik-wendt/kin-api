from django.template.defaultfilters import linebreaks_filter
from rest_framework import serializers

from apps.kin_extensions.serializers import InternationalPhoneNumberField


class TwilioIncomingSMSSerializer(serializers.Serializer):
    body = serializers.CharField(allow_blank=True)
    media_urls = serializers.ListField(allow_empty=True)

    def validate_body(self, value):
        html_body = linebreaks_filter(value)
        html_body = html_body.replace("\n", "<br>")

        if not html_body:
            return "<p></p>"

        return html_body


class VerificationSMSSerializer(serializers.Serializer):
    to = InternationalPhoneNumberField()
    message = serializers.CharField(max_length=160)
