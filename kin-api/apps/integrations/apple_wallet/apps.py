from django.apps import AppConfig


class AppleWalletIntegrationConfig(AppConfig):
    name = "apps.integrations.apple_wallet"
    verbose_name = "Apple Wallet Integration"

    def ready(self):
        import apps.integrations.apple_wallet.signals
