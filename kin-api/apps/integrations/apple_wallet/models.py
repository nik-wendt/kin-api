import hashlib
import json
import logging
import os
import secrets
import tempfile
import uuid
import zipfile
from glob import glob
from io import BytesIO

import redis
from PIL import Image, ImageDraw, ImageOps
from django.core.exceptions import ValidationError
from django.conf import settings
from django.db import models
from django.utils.module_loading import import_string
from django.utils.translation import gettext_lazy as _
from shortuuid.django_fields import ShortUUIDField

from apps.facts.choices import FactTypeEnum
from apps.integrations.apple_wallet import crypto
from apps.integrations.apple_wallet.files import WalletpassContentFile
from apps.integrations.apple_wallet.settings import dwpconfig as WALLETPASS_CONF
from apps.integrations.apple_wallet.storage import WalletPassStorage
from apps.integrations.apple_wallet.wallet import Generic, Barcode
from apps.users.models import User

logger = logging.getLogger(__name__)


def kin_pass_path(instance, filename):
    return f"{WALLETPASS_CONF['UPLOAD_TO']}/owner-{instance.owner_id}/{filename}"


class PassBuilder:
    def __init__(self, directory=None):
        self.pass_data = {}
        self.pass_data_required = {
            "passTypeIdentifier": WALLETPASS_CONF["PASS_TYPE_ID"],
            "serialNumber": None,
            "teamIdentifier": WALLETPASS_CONF["TEAM_ID"],
            "webServiceURL": WALLETPASS_CONF["SERVICE_URL"],
            "authenticationToken": None,
        }
        # directory = None
        self.extra_files = {}
        self.manifest_dict = {}
        self.builded_pass_content = None
        self.directory = directory
        if directory is not None:
            self._load_pass_json_file_if_exists(directory)
        self.pass_data_required.update(
            {
                "serialNumber": secrets.token_urlsafe(20),
                "authenticationToken": crypto.gen_random_token(),
            }
        )

    def _copy_dir_files(self, tmp_pass_dir):
        """Copy files from provided base dir to temporal dir

        Args:
            tmp_pass_dir (str): temporal dir path
        """
        for absolute_filepath in glob(os.path.join(self.directory, "**"), recursive=True):
            filename = os.path.basename(absolute_filepath)
            relative_file_path = os.path.relpath(absolute_filepath, self.directory)
            if filename == ".DS_Store":
                continue
            if not os.path.isfile(absolute_filepath):
                continue
            filecontent = open(absolute_filepath, "rb").read()
            # Add files to manifest
            self.manifest_dict[relative_file_path] = hashlib.sha1(filecontent).hexdigest()
            dest_abs_filepath = os.path.join(tmp_pass_dir, relative_file_path)
            dest_abs_dirpath = os.path.dirname(dest_abs_filepath)
            if not os.path.exists(dest_abs_dirpath):
                os.makedirs(dest_abs_dirpath)
            ff = open(dest_abs_filepath, "wb")
            ff.write(filecontent)
            ff.close()

    def _write_extra_files(self, tmp_pass_dir):
        """Write extra files contained in self.extra_files into tmp dir

        Args:
            tmp_pass_dir (str): temporal dir path
        """
        for relative_file_path, filecontent in self.extra_files.items():
            # Add files to manifest
            self.manifest_dict[relative_file_path] = hashlib.sha1(filecontent).hexdigest()
            dest_abs_filepath = os.path.join(tmp_pass_dir, relative_file_path)
            dest_abs_dirpath = os.path.dirname(dest_abs_filepath)
            if not os.path.exists(dest_abs_dirpath):
                os.makedirs(dest_abs_dirpath)
            ff = open(dest_abs_filepath, "wb")
            ff.write(filecontent)
            ff.close()

    def _write_pass_json(self, tmp_pass_dir):
        """Write content of self.pass_data to pass.json (in JSON format)

        Args:
            tmp_pass_dir (str): temporal dir path where pass.json will be saved
        """
        pass_json = json.dumps(self.pass_data)
        pass_json_bytes = bytes(pass_json, "utf8")
        # Add pass.json to manifest
        self.manifest_dict["pass.json"] = hashlib.sha1(pass_json_bytes).hexdigest()
        ff = open(os.path.join(tmp_pass_dir, "pass.json"), "wb")
        ff.write(pass_json_bytes)
        ff.close()

    def _write_manifest_json_and_signature(self, tmp_pass_dir):
        """Write the content of self.manifest_dict into manifest.json

        Args:
            tmp_pass_dir (str): temporal dir path
        """
        manifest_json = json.dumps(self.manifest_dict)
        manifest_json_bytes = bytes(manifest_json, "utf8")
        ff = open(os.path.join(tmp_pass_dir, "manifest.json"), "wb")
        ff.write(manifest_json_bytes)
        ff.close()
        signature_content = crypto.pkcs7_sign(
            certcontent=WALLETPASS_CONF["CERT_CONTENT"],
            keycontent=WALLETPASS_CONF["KEY_CONTENT"],
            wwdr_certificate=WALLETPASS_CONF["WWDRCA_CONTENT"],
            data=manifest_json_bytes,
            key_password=WALLETPASS_CONF["KEY_PASSWORD"],
        )
        ff = open(os.path.join(tmp_pass_dir, "signature"), "wb")
        ff.write(signature_content)
        ff.close()

    def _zip_all(self, directory):
        zip_file_path = os.path.join(directory, "..", "walletcard.pkpass")
        zip_pkpass = zipfile.ZipFile(zip_file_path, "w", zipfile.ZIP_DEFLATED)
        for filepath in glob(os.path.join(directory, "**"), recursive=True):
            relative_file_path = os.path.relpath(filepath, directory)
            zip_pkpass.write(filepath, arcname=relative_file_path)
        zip_pkpass.close()
        return open(zip_file_path, "rb").read()

    def _load_pass_json_file_if_exists(self, directory):
        """Call self.load_pass_json_file if pass.json exist

        Args:
            directory (str): directory where pass.json resides
        """
        if os.path.isfile(os.path.join(directory, "pass.json")):
            self.load_pass_json_file(directory)

    def _attach_cropped_pic(self, pic):
        buffer = BytesIO()
        size = (512, 512)
        mask = Image.new("L", size, 0)
        draw = ImageDraw.Draw(mask)
        draw.ellipse((0, 0) + size, fill=255)

        antialiasing_modifier = 8
        working_border_size = [x * antialiasing_modifier for x in size]
        border = Image.new("RGBA", working_border_size, color=(0, 0, 0, 0))
        draw = ImageDraw.Draw(border)
        draw.ellipse(
            (0, 0) + tuple(working_border_size),
            fill=(0, 0, 0, 0),
            outline=(255, 255, 255, 255),
            width=15 * antialiasing_modifier,
        )
        border = border.resize(size, Image.LANCZOS)

        pil_pfp = Image.open(pic).convert("RGBA")

        output = ImageOps.fit(pil_pfp, mask.size, centering=(0.5, 0.5))

        output.putalpha(mask)
        output.paste(border, mask=border)
        output.save(fp=buffer, format="PNG")

        self.add_file("thumbnail.png", buffer.getvalue())
        buffer = None

    def clean(self):
        self.manifest_dict = {}
        self.builded_pass_content = None
        if not self.pass_data:
            raise ValidationError(_("Cannot obtain data for pass.json."))

    def load_pass_json_file(self, dir):
        """Load json file without test if exists.

        Args:
            dir (str): path where resides the pass.json
        """
        json_data = open(os.path.join(dir, "pass.json"), "r").read()
        self.pass_data = json.loads(json_data)

    def pre_build_pass_data(self):
        """Update self.pass_data with self.pass_data_required content"""
        self.pass_data.update(self.pass_data_required)

    def build(self, user):
        """Build .pkpass file"""
        self.clean()
        with tempfile.TemporaryDirectory() as tmpdirname:
            os.mkdir(os.path.join(tmpdirname, "data.pass"))
            tmp_pass_dir = os.path.join(tmpdirname, "data.pass")
            if self.directory:
                self._copy_dir_files(tmp_pass_dir)
            if user.user_kin.profile_pic:
                self._attach_cropped_pic(user.user_kin.profile_pic)
            self._write_extra_files(tmp_pass_dir)
            self.pre_build_pass_data()
            self._write_pass_json(tmp_pass_dir)
            self._write_manifest_json_and_signature(tmp_pass_dir)
            self.builded_pass_content = self._zip_all(tmp_pass_dir)
        return self.builded_pass_content

    def write_to_model(self, user, instance=None):
        """Saves the content of builded and zipped pass into Pass model.

        Args:
            user (User, required) : A user instance to be used as the owner of the pass.

            instance (Pass, optional): Pass instance, a new one will be created
                if none provided. Defaults to None.

        Returns:
            Pass: instance of Pass (already saved)
        """
        if instance is None:
            instance = Pass()

        setattr(instance, "owner", user)
        setattr(instance, "pass_type_identifier", WALLETPASS_CONF["PASS_TYPE_ID"])
        setattr(instance, "serial_number_id", self.pass_data_required.get("serialNumber"))
        setattr(
            instance, "authentication_token", self.pass_data_required.get("authenticationToken")
        )

        if instance.data.name:
            filename = os.path.basename(instance.data.name)
        else:
            filename = f"{uuid.uuid1()}.pkpass"

        content = WalletpassContentFile(self.builded_pass_content)
        instance.data.delete()
        instance.data.save(filename, content)

        return instance

    def add_file(self, path, content):
        self.extra_files[path] = content


class CardInfoBuilder:
    def __init__(self, share_dict, serial_number):
        self.other_phone_numbers_and_emails = []
        self.social_facts = []
        self.secondary_count = 0
        self.secondary_fields: []
        self.serial_number = serial_number
        self.template_name = share_dict["meta"].get("template_name")
        self.friendly_name = share_dict.get("friendly_name")
        self.facts = share_dict["facts"]
        self.primary_facts = [
            fact
            for fact in share_dict["facts"]
            if fact["metadata"].get("phone_primary") or fact["metadata"].get("email_primary")
        ]
        self.back_card_fields = [f["key"] for f in self.facts]
        self.card_info = Generic()
        self.card_info_dict = self._get_card_info_dict()

    def _append_secondary_field(self, facts_iterable):
        for fact in facts_iterable:
            if self.secondary_count < 2:
                self.card_info.add_secondary_field(
                    key=fact["key"], value=fact["value"], label=fact["key"]
                )
                self.secondary_count += 1

    def _get_other_phone_numbers_and_emails(self):
        self.other_phone_numbers_and_emails = [
            fact
            for fact in self.facts
            if (
                fact["fact_type"] == FactTypeEnum.PHONE.value
                or fact["fact_type"] == FactTypeEnum.EMAIL.value
            )
            and not (fact["metadata"].get("phone_primary") or fact["metadata"].get("email_primary"))
        ]

    def _get_social_facts(self):
        self.social_facts = [
            fact for fact in self.facts if fact["fact_type"] == FactTypeEnum.SOCIAL_PROFILE.value
        ]

    def _get_remainder_facts(self):
        self.facts = [
            fact
            for fact in self.facts
            if fact not in self.social_facts
            or fact not in self.primary_facts
            or fact not in self.other_phone_numbers_and_emails
        ]

    def _get_card_info_dict(self):
        self.card_info.add_primary_field(key="name", value=self.friendly_name, label="Name")

        self.card_info.add_back_field(
            key="included_fields",
            value="\n".join(self.back_card_fields),
            label="Fields included in this share card",
        )
        self.card_info.add_back_field(key="token_id", value=self.serial_number, label="Token ID")

        if self.template_name:
            self.card_info.add_header_field(
                key="template_name",
                value=self.template_name,
                label="Pass Name",
            )

        # Continue through a chain of grabbing data until we have enough to fill the front of the card.
        # Stop as soon as we're good.
        if self.primary_facts:
            self._append_secondary_field(self.primary_facts)
        if self.secondary_count >= 2:
            return self.card_info.json_dict()

        self._get_other_phone_numbers_and_emails()
        if self.other_phone_numbers_and_emails:
            self._append_secondary_field(self.other_phone_numbers_and_emails)
        if self.secondary_count >= 2:
            return self.card_info.json_dict()

        self._get_social_facts()
        if self.social_facts:
            self._append_secondary_field(self.social_facts)
        if self.secondary_count >= 2:
            return self.card_info.json_dict()

        self._get_remainder_facts()
        if self.facts:
            self._append_secondary_field(self.facts)

        return self.card_info.json_dict()


class Pass(models.Model):
    """
    Pass instance
    """

    id = ShortUUIDField(primary_key=True, editable=False)
    pass_type_identifier = models.CharField(max_length=150)
    serial_number = models.CharField(max_length=24, blank=False, null=False)
    authentication_token = models.CharField(max_length=150)
    data = models.FileField(
        upload_to=kin_pass_path,
        storage=WalletPassStorage(),
    )
    updated_at = models.DateTimeField(auto_now=True)
    # THINK OF A BETTER NAME FOR THIS AT SOME POINT
    associated_ids = models.JSONField(default={})
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="wallet_pass", default=None
    )

    @classmethod
    def get_or_create_pass(cls, serial_number, token, user):
        """Using two libaries to generate the file:
        https://github.com/Bastian-Kuhn/wallet
        and https://github.com/Develatio/django-walletpass

        walletpass creates models for ease of use and updating.
        However, the class based creation method of pass data is easier to read and follow
        """

        # TODO: in the future we may want to split this based on the user in order to do custom styling per client org.
        share_dict = token["kin"][0]
        if cls.objects.filter(serial_number=serial_number).exists():
            pass_instance = cls.objects.get(
                serial_number=serial_number,
                pass_type_identifier="pass.com.heykinship.kin",
            )
            # Handle passes.
            builder = pass_instance.get_pass_builder()
            card_info = CardInfoBuilder(share_dict, serial_number).card_info_dict
            builder.pass_data.update({"generic": card_info})
            builder.pass_data.update({"token_id": serial_number})

            builder.build(user)
            pass_instance = builder.write_to_model(user, pass_instance)
            pass_instance.serial_number = serial_number

            # handle redis tokens
            redis_client = redis.Redis.from_url(settings.REDIS_URL)
            redis_client.persist(serial_number)

        else:
            pass_instance = cls._create_user_kin_pass(share_dict, serial_number, user)
        pass_instance.save()

        return pass_instance

    @classmethod
    def _create_user_kin_pass(cls, share_dict, serial_number, user):
        builder = PassBuilder()
        builder.pass_data_required["serialNumber"] = serial_number
        friendly_name = share_dict.get("friendly_name")
        url = f"https://kinship.app.link/qrcode?kin_token={serial_number}"
        builder.pass_data["token_id"] = serial_number

        # Files
        builder.add_file(
            "icon.png", open("apps/integrations/apple_wallet/images/icon.png", "rb").read()
        )
        builder.add_file(
            "logo.png", open("apps/integrations/apple_wallet/images/logo.png", "rb").read()
        )
        builder.add_file(
            "background.png",
            open("apps/integrations/apple_wallet/images/background.png", "rb").read(),
        )
        builder.add_file(
            "icon@2x.png", open("apps/integrations/apple_wallet/images/icon@2x.png", "rb").read()
        )

        #  Kin data
        card_info = CardInfoBuilder(share_dict, serial_number).card_info_dict

        # Lame needed stuff
        builder.pass_data["generic"] = card_info
        builder.pass_data["barcodes"] = [Barcode(message=url, format="qr").json_dict()]
        builder.pass_data["description"] = f"Wallet pass for {friendly_name}"
        builder.pass_data["formatVersion"] = 1
        builder.pass_data["organizationName"] = "Kinship"

        # Color
        builder.pass_data["backgroundColor"] = "rgb(15,11,57)"
        builder.pass_data["foregroundColor"] = "rgb(255,255,255)"
        builder.pass_data["labelColor"] = "rgb(96,95,177)"

        # Build
        builder.build(user)
        pass_instance = builder.write_to_model(user)
        pass_instance.serial_number = serial_number

        pass_instance.associated_ids = {
            "fact_ids": share_dict["meta"].get("fact_ids"),
            "note_ids": share_dict["meta"].get("note_ids"),
            "kin_id": user.user_kin.id,
        }

        # Redis
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        redis_client.expire(
            serial_number, 60 * 60 * 24 * 365
        )  # set the new token to expire in a year.

        return pass_instance

    def push_notification(self):
        push_class = import_string(WALLETPASS_CONF["WALLETPASS_PUSH_CLASS"])
        push_module = push_class()
        for registration in self.registrations.all():
            push_module.push_notification_from_instance(registration)

    def new_pass_builder(self, directory=None):
        builder = PassBuilder(directory)
        builder.pass_data_required.update(
            {
                "passTypeIdentifier": self.pass_type_identifier,
                "serialNumber": self.serial_number,
                "authenticationToken": self.authentication_token,
            }
        )
        return builder

    def get_pass_builder(self):
        builder = PassBuilder()
        with tempfile.TemporaryDirectory() as tmpdirname:
            os.mkdir(os.path.join(tmpdirname, "data.pass"))
            tmp_pass_dir = os.path.join(tmpdirname, "data.pass")
            # Put zip file into tmp dir
            zip_path = os.path.join(tmpdirname, "walletcard.pkpass")
            zip_pkpass = open(zip_path, "wb")
            zip_pkpass.write(self.data.read())
            zip_pkpass.close()
            # Extract zip file to tmp dir
            with zipfile.ZipFile(zip_path, "r") as zip_ref:
                zip_ref.extractall(tmp_pass_dir)
            # Populate builder with zip content
            for filepath in glob(os.path.join(tmp_pass_dir, "**"), recursive=True):
                filename = os.path.basename(filepath)
                relative_file_path = os.path.relpath(filepath, tmp_pass_dir)
                if filename == "pass.json":
                    builder.load_pass_json_file(tmp_pass_dir)
                    continue
                if relative_file_path in ["signature", "manifest.json", ".", ".."]:
                    continue
                if not os.path.isfile(filepath):
                    continue
                if filename == "thumbnail.png":
                    os.remove(filepath)
                    continue
                builder.add_file(relative_file_path, open(filepath, "rb").read())
        # Load of these fields due to that those fields are ignored on pass.json loading
        builder.pass_data_required.update(
            {
                "passTypeIdentifier": self.pass_type_identifier,
                "serialNumber": self.serial_number,
                "authenticationToken": self.authentication_token,
            }
        )
        return builder

    def __unicode__(self):
        return self.serial_number

    class Meta:
        verbose_name_plural = "passes"
        unique_together = (
            (
                "pass_type_identifier",
                "serial_number",
            ),
        )


class Registration(models.Model):
    """
    Registration of a Pass on a device
    """

    id = ShortUUIDField(primary_key=True, editable=False)
    device_library_identifier = models.CharField(max_length=150)
    push_token = models.CharField(max_length=150)
    pazz = models.ForeignKey(
        Pass,
        on_delete=models.CASCADE,
        related_name="registrations",
    )

    def __unicode__(self):
        return self.device_library_identifier


class Log(models.Model):
    """
    Log message sent by a device
    """

    message = models.TextField()

    def __unicode__(self):
        return self.message
