from rest_framework.renderers import JSONRenderer

from apps.integrations.apple_wallet.models import Pass
from rest_framework import viewsets, permissions
from rest_framework import status
from rest_framework.response import Response

from apps.integrations.apple_wallet.serializers import PassSerializer
from apps.kin_auth.permissions import IsVerified, IsPaid
from apps.kin_extensions.renderers import PkpassRenderer


class PkpassViewSet(viewsets.mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    queryset = Pass.objects.all()
    serializer_class = PassSerializer
    renderer_classes = [JSONRenderer, PkpassRenderer]

    def create(self, request, *args, **kwargs):
        renderer_format = request.accepted_renderer.format

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        serializer.save(user=self.request.user)
        headers = self.get_success_headers(serializer.encoded_data)

        if renderer_format and renderer_format == "pkpass":
            return Response(serializer.pass_url, status=status.HTTP_201_CREATED, headers=headers)
        return Response(serializer.encoded_data, status=status.HTTP_201_CREATED, headers=headers)
