from django.contrib import admin
from apps.integrations.apple_wallet.models import Pass, Registration, Log

admin.site.register(Pass)
admin.site.register(Registration)
admin.site.register(Log)
