from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.facts.models import Fact
from apps.kin.models import Kin
from apps.notes.models import Note
from apps.integrations.apple_wallet.models import Pass
from apps.integrations.apple_wallet.tasks import update_wallet_pass_task
from apps.kin.tasks import delete_userkin_templates


@receiver(post_save, sender=Pass)
def send_push_notification(instance=None, **_kwargs):
    instance.push_notification()


@receiver(post_save, sender=Kin)
@receiver(post_save, sender=Fact)
@receiver(post_save, sender=Note)
def update_wallet_pass(sender, instance, **kwargs):
    kin = None
    if sender == Fact or sender == Note:
        kin = instance.kin
    elif sender == Kin:
        kin = instance
    # once we're done dealing with the instance and need to talk to other DBs, let's throw it to the celery worker.
    if hasattr(kin, "userkin"):
        # For wallet pass stuff
        update_wallet_pass_task.delay(instance.id, sender._meta.object_name)
        # Clear userkinshare records.
        delete_userkin_templates.delay(kin.owner_id)
