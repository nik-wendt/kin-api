import json

import redis
from celery import shared_task

from apps.facts.models import Fact
from apps.integrations.apple_wallet.models import Pass
from apps.integrations.apple_wallet.serializers import PassSerializer
from apps.kin.models import Kin
from apps.kin.serializers import generate_kin_share_dict
from apps.notes.models import Note
from kin_api_project import settings


@shared_task
def update_wallet_pass_task(instance_id, instance_type):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    if instance_type == "Kin":
        kin = Kin.objects.get(id=instance_id)
        wallet_passes = Pass.objects.filter(associated_ids__kin_id=instance_id)
    elif instance_type == "Fact":
        fact = Fact.objects.get(id=instance_id)
        kin = fact.kin
        wallet_passes = Pass.objects.filter(associated_ids__fact_ids__contains=instance_id)
    elif instance_type == "Note":
        note = Note.objects.get(id=instance_id)
        kin = note.kin
        wallet_passes = Pass.objects.get(associated_ids__note_ids__contains=note.kin.owner_id)
    else:
        # How did you get here?
        raise Exception

    for wallet_pass in wallet_passes:
        token = json.loads(redis_client.get(wallet_pass.serial_number))
        token_kin = token["kin"][0]

        share_facts = Fact.objects.filter(id__in=token_kin["meta"]["fact_ids"])
        share_notes = Note.objects.filter(id__in=token_kin["meta"]["note_ids"])

        share_dict = generate_kin_share_dict(
            kin,
            token_kin["meta"]["kin_fields"],
            token_kin["friendly_name"],
            list(share_facts.values_list("id", flat=True)),
            list(share_notes.values_list("id", flat=True)),
            token["meta"].get("template_name"),
        )

        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        value = json.dumps(
            {
                "group_name": "",
                "sharer_name": f"{kin.owner.first_name} {kin.owner.last_name}",
                "kin": [share_dict],
            }
        )
        redis_client.set(wallet_pass.serial_number, value)
        serializer = PassSerializer(data={"token": wallet_pass.serial_number})
        serializer.is_valid(raise_exception=True)
        serializer.save(user=kin.owner)
