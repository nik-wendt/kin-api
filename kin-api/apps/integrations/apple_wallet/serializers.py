import base64
import json

import redis
from rest_framework import serializers

from apps.integrations.apple_wallet.models import Pass
from apps.kin.models import UserKin
from kin_api_project import settings


class PassSerializer(serializers.Serializer):
    token = serializers.CharField(required=True)

    def validate_token(self, value):
        redis_client = redis.Redis.from_url(settings.REDIS_URL)
        decoded_token = redis_client.get(value)

        if not decoded_token:
            raise serializers.ValidationError(
                "The token is expired or invalid. Please request a new one."
            )

        token_data = json.loads(decoded_token)
        kin_id = token_data["kin"][0]["meta"].get("kin_id")

        if not UserKin.objects.filter(kin_id=kin_id).exists():
            raise serializers.ValidationError(
                "This token does not contain user Kin data. Please use only tokens sharing the user's Kin."
            )

        return token_data

    def save(self, **kwargs):
        """
        Both create and edit are handled in the same serializer. This makes it a single endpoint for "getting" a pkpass
        created by a token. Always request a new token before "creating" a pkpass to ensure most recent data.
        """

        token = self.validated_data["token"]
        token_id = self.root.initial_data["token"]
        user = kwargs["user"]

        pass_instance = Pass.get_or_create_pass(token_id, token, user)

        self.pass_url = pass_instance.data.url
        self.encoded_data = base64.b64encode(pass_instance.data.read())
