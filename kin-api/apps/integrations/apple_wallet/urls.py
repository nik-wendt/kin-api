from rest_framework import routers

from apps.integrations.apple_wallet.views import PkpassViewSet

router = routers.DefaultRouter()
router.register("", PkpassViewSet, basename="pkpass")

urlpatterns = []
urlpatterns += router.urls
