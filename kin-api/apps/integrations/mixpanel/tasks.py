import logging

from celery import shared_task
from django.conf import settings

from mixpanel import Mixpanel

from apps.facts.models import Fact
from apps.groups.models import Group
from apps.kin.models import Kin
from apps.notes.models import Note
from apps.reminders.models import Reminder
from apps.users.models import User


logger = logging.getLogger(__name__)


@shared_task
def update_stats(user_id):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)

    try:
        user = User.objects.get(pk=user_id)

        kin_count = user.kin.count()
        fact_count = Fact.objects.filter(kin__owner=user).count()
        reminder_count = Reminder.objects.filter(kin__owner=user).count()
        group_count = user.kin_groups.count()
        note_count = user.notes.count()

        mp.people_set(
            user.id,
            {
                "Date Joined": user.date_joined,
                "Num Kin": kin_count,
                "Num Facts": fact_count,
                "Num Reminders": reminder_count,
                "Num Groups": group_count,
                "Num Notes": note_count,
            },
        )
    except Exception as e:
        logger.info(
            "Unable to update Mixpanel status for user %s. User does not exist. %s",
            user_id,
            str(e),
            extra={"user_id": user_id},
        )


@shared_task
def update_payment_info(user_id):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)

    try:
        user = User.objects.get(pk=user_id)

        mp.people_set(
            user.id,
            {
                "payment_status": user.payment_status.value if user.payment_status else None,
                "payment_provider": user.payment_provider.value if user.payment_provider else None,
                "payment_expiration": user.payment_expiration if user.payment_expiration else None,
                "payment_subscription_type": user.payment_subscription_type.value
                if user.payment_subscription_type
                else None,
            },
        )
    except Exception as e:
        logger.info(
            "Unable to update Mixpanel status for user %s. User does not exist. %s",
            user_id,
            str(e),
            extra={"user_id": user_id},
        )


@shared_task
def update_authentication_method(user_id, auth_method):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)

    try:
        user = User.objects.get(pk=user_id)
        mp.people_set(user.id, {"auth_method": auth_method})
    except Exception as e:
        logger.info(
            "Unable to update Mixpanel status for user %s. User does not exist. %s",
            user_id,
            str(e),
            extra={"user_id": user_id},
        )


@shared_task
def add_or_remove_kin_to_group_mixpanel_event(group_id: str, kin_added: int, source: str):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)

    if kin_added < 0:
        key = "Kin Removed"
    if kin_added > -1:
        key = "Kin Added"

    try:
        group = Group.objects.get(pk=group_id)
        mp.track(
            group.owner.id,
            "Group Updated",
            {key: abs(kin_added), "Source": source},
        )
    except Group.DoesNotExist as e:
        logger.info(
            "Unable to update Mixpanel status for user %s. User does not exist. %s",
            group_id,
            str(e),
            extra={"group_id": group_id},
        )


@shared_task
def track_note_created(note_id):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)

    try:
        note = Note.objects.get(pk=note_id)

        mp.track(
            note.owner.id,
            "Note Created",
            {
                "Text Length": len(note.plain_text),
                "Note Source": note.source.value,
                "Is Note Unassigned": bool(note.kin),
            },
        )
    except Exception as e:
        logger.info(
            "Unable to track Mixpanel event for note creation %s. Note does not exist. %s",
            note_id,
            str(e),
            extra={"note_id": note_id},
        )


# TODO: this is not implemented yet. Waiting for FE to remove their tracking first.
@shared_task
def track_note_updated(note_id):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)

    try:
        note = Note.objects.get(pk=note_id)

        mp.track(
            note.owner.id,
            "Note Updated",
            {
                "Text Length": len(note.plain_text),
                "Note Source": note.source.value,
                "Is Note Unassigned": bool(note.kin),
            },
        )
    except Exception as e:
        logger.info(
            "Unable to track Mixpanel event for note creation %s. Note does not exist. %s",
            note_id,
            str(e),
            extra={"note_id": note_id},
        )


@shared_task
def mixpanel_shared_kin(kin_id, is_user, sharing_method, content_type, **kwargs):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)
    kin_card_type = "User Kin" if is_user else "Contact Kin"

    try:
        kin = Kin.objects.get(pk=kin_id)
        message_dict = {
            "Kin Card Type": kin_card_type,
            "Sharing Method": sharing_method,
            "Content Type": content_type.capitalize() if content_type else "",
        }
        if content_type == "CUSTOM":
            message_dict["Details Shared"] = kwargs.get("number_details_shared")
            message_dict["Notes Shared"] = kwargs.get("number_notes_shared")
        number_kin_share = kwargs.get("number_kin_shared")
        if number_kin_share:
            message_dict["Kin Shared"] = number_kin_share

        mp.track(kin.owner_id, "Kin Shared", message_dict)

    except Exception as e:
        logger.info(
            "Unable to track Mixpanel event for kin share %s. -- %s",
            kin_id,
            str(e),
            extra={"kin_id": kin_id},
        )


@shared_task
def mixpanel_user_app_version(user_id, app_version):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)
    try:
        user = User.objects.get(pk=user_id)
        mp.people_set(user.id, {"App Version": app_version})
    except Exception as e:
        logger.info(
            "Unable to update Mixpanel status for user %s. User does not exist. %s",
            user_id,
            str(e),
            extra={"user_id": user_id},
        )


@shared_task
def mixpanel_user_campaign_and_source(user_id, campaign, source):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)
    try:
        user = User.objects.get(pk=user_id)
        message = {}
        if campaign:
            message["Campaign"] = campaign
        if source:
            message["Source"] = source
        mp.people_set(user.id, message)
    except Exception as e:
        logger.info(
            "Unable to update Mixpanel status for user %s. User does not exist. %s",
            user_id,
            str(e),
            extra={"user_id": user_id},
        )


@shared_task
def mixpanel_data_export_event(user_id):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)
    try:
        user = User.objects.get(pk=user_id)
        mp.track(user_id, "Data Exported")
    except Exception as e:
        logger.info(
            "Unable to update Mixpanel status for user %s. User does not exist. %s",
            user_id,
            str(e),
            extra={"user_id": user_id},
        )


@shared_task
def mixpanel_start_account_delete_event(user_id):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)
    try:
        user = User.objects.get(pk=user_id)
        mp.track(
            user_id,
            "Account Delete Requested",
        )
    except Exception as e:
        logger.info(
            "Unable to update Mixpanel event for user %s. User does not exist. %s",
            user_id,
            str(e),
            extra={"user_id": user_id},
        )


@shared_task
def mixpanel_account_delete_event(user_id):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)
    try:
        user = User.objects.get(pk=user_id)
        mp.track(
            user_id,
            "Account Deleted",
        )
    except Exception as e:
        logger.info(
            "Unable to update Mixpanel event for user %s. User does not exist. %s",
            user_id,
            str(e),
            extra={"user_id": user_id},
        )


@shared_task
def mixpanel_fact_created_event(user_id):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)
    try:
        user = User.objects.get(pk=user_id)
        mp.track(
            user_id,
            "Fact Created",
        )
    except Exception as e:
        logger.info(
            "Unable to update Mixpanel event for user %s. User does not exist. %s",
            user_id,
            str(e),
            extra={"user_id": user_id},
        )


@shared_task
def mixpanel_fact_deleted_event(user_id, is_bulk=False, quantity=1):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)
    try:
        user = User.objects.get(pk=user_id)
        properties = {
            "Bulk Delete": is_bulk,
            "Facts Deleted": quantity,
        }
        mp.track(user_id, "Fact Deleted", properties)
    except Exception as e:
        logger.info(
            "Unable to update Mixpanel event for user %s. User does not exist. %s",
            user_id,
            str(e),
            extra={"user_id": user_id},
        )


@shared_task
def mixpanel_fact_modified_event(user_id):
    mp = Mixpanel(settings.MIXPANEL_TOKEN)
    try:
        user = User.objects.get(pk=user_id)
        mp.track(
            user_id,
            "Fact Modified",
        )
    except Exception as e:
        logger.info(
            "Unable to update Mixpanel event for user %s. User does not exist. %s",
            user_id,
            str(e),
            extra={"user_id": user_id},
        )
