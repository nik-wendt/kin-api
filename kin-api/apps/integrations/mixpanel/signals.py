from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from apps.groups.models import Group
from apps.integrations.mixpanel.tasks import update_stats, update_payment_info
from apps.kin.models import Kin
from apps.notes.models import Note
from apps.users.models import User


@receiver(post_save, sender=Kin)
@receiver(post_delete, sender=Kin)
@receiver(post_save, sender=Note)
@receiver(post_delete, sender=Note)
@receiver(post_save, sender=Group)
@receiver(post_delete, sender=Group)
def update_mixpanel_user_stats(sender, instance, **kwargs):
    update_stats.delay(instance.owner.id)


@receiver(post_save, sender=User)
def update_mixpanel_user_payment_info(sender, instance, **kwargs):
    update_payment_info.delay(instance.id)
