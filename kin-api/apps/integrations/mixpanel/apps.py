from django.apps import AppConfig


class MixPanelIntegrationConfig(AppConfig):
    name = "apps.integrations.mixpanel"
    verbose_name = "Mixpanel"

    def ready(self):
        import apps.integrations.mixpanel.signals
