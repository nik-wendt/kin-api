from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from drf_yasg.utils import swagger_auto_schema

from apps.integrations.zapier.serializers import KinIntegrationSerializer
from apps.kin.models import Kin
from apps.kin_auth.permissions import IsPaid, IsVerified


class NewKinTriggerView(APIView):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)

    @swagger_auto_schema(
        responses={status.HTTP_200_OK: KinIntegrationSerializer(many=True)},
        operation_id="zapier_new_kin_trigger",
    )
    def get(self, request, *args, **kwargs):
        qs = (
            Kin.objects.filter(owner=request.user.id)
            .prefetch_related("facts")
            .order_by("-created")[:25]
        )
        kin = KinIntegrationSerializer(qs, many=True)
        return Response(kin.data, status=status.HTTP_200_OK)
