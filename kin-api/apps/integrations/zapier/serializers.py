from rest_framework import serializers

from apps.kin.models import Kin


class KinIntegrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kin
        fields = (
            "id",
            "friendly_name",
            "first_name",
            "middle_name",
            "last_name",
            "suffix",
            "primary_email",
            "primary_phone",
            "created",
            "modified",
        )
