from django.urls import path

from apps.integrations.zapier.views import NewKinTriggerView


urlpatterns = [path("new-kin-trigger/", NewKinTriggerView.as_view(), name="new-kin-trigger")]
