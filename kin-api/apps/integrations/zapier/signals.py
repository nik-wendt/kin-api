from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.kin.models import Kin


@receiver(post_save, sender=Kin)
def new_kin_trigger_cache(sender, instance, created, **kwargs):
    # TODO: This is where we will handle webhooks once available
    pass
