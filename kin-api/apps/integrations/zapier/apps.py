from django.apps import AppConfig


class ZapierConfig(AppConfig):
    name = "apps.integrations.zapier"
    verbose_name = "Zapier Integration"

    def ready(self):
        import apps.integrations.zapier.signals
