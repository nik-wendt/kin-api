from django.apps import AppConfig


class MailchimpConfig(AppConfig):
    name = "apps.integrations.mailchimp"
    verbose_name = "Mailchimp"

    def ready(self):
        import apps.integrations.mailchimp.signals
