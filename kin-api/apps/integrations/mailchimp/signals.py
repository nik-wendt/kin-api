from django.db.models.signals import pre_save
from django.dispatch import receiver

from apps.integrations.mailchimp.utils import mailchimp_add_paid_tag
from apps.users.models import User
from apps.users.choices import PaymentStatusEnum


@receiver(pre_save, sender=User)
def send_feature_email_for_paid_user(sender, instance, **kwargs):
    new_payment_status = instance.payment_status
    if new_payment_status == PaymentStatusEnum.PAID:
        old_payment_status = User.objects.get(pk=instance.id).payment_status
        if new_payment_status != old_payment_status:
            mailchimp_add_paid_tag(instance)
