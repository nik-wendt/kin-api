import hashlib
import logging

import mailchimp_marketing as mm
from mailchimp_marketing.api_client import ApiClientError

from apps.users.models import User
from kin_api_project.settings import (
    MAILCHIMP_MARKETING_API_KEY,
    MAILCHIMP_DATACENTER,
    MAILCHIMP_LIST_ID,
)

logger = logging.getLogger(__name__)


def get_mailchimp_client():
    mailchimp = mm.Client()
    mailchimp.set_config({"api_key": MAILCHIMP_MARKETING_API_KEY, "server": MAILCHIMP_DATACENTER})
    return mailchimp


def mc_set_list_member(mailchimp_client: mm.Client, user: User, email_md5):
    """a PUT on the create or update endpoint"""
    response = mailchimp_client.lists.set_list_member(
        MAILCHIMP_LIST_ID,
        email_md5,
        {
            "email_address": user.email,
            "status_if_new": "subscribed",
            "merge_fields": {
                "FNAME": user.first_name,
                "LNAME": user.last_name,
                "EXPIRES": user.payment_expiration.strftime("%Y-%m-%d"),
            },
        },
    )
    return response


def mc_add_tag(mailchimp_client: mm.Client, tag_name: str, email_md5, previous_tags=[]):
    response = mailchimp_client.lists.update_list_member_tags(
        MAILCHIMP_LIST_ID,
        email_md5,
        {"tags": previous_tags + [{"name": tag_name, "status": "active"}]},
    )
    return response


def mailchimp_add_paid_tag(user):

    mailchimp = get_mailchimp_client()
    email = user.email
    email_md5 = hashlib.md5(email.encode()).hexdigest()
    try:
        response = mc_set_list_member(mailchimp, user, email_md5)
        tags = response["tags"]
        response = mc_add_tag(mailchimp, "Phase: Paid", email_md5, tags)
        logger.info(response)
    except ApiClientError as e:
        logger.error(f"Status:{e.status_code}\n msg: {e.text}")


def mailchimp_verified(user):
    mailchimp = get_mailchimp_client()
    email = user.email
    email_md5 = hashlib.md5(email.encode()).hexdigest()
    try:
        response = mc_set_list_member(mailchimp, user, email_md5)
        tags = response["tags"]
        response = mc_add_tag(mailchimp, "Phase: Verified", email_md5, tags)
        logger.info(response)
    except ApiClientError as e:
        logger.error(f"Status:{e.status_code}\n msg: {e.text}")
