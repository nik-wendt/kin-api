import hashlib
import logging

from django.core.management.base import BaseCommand
from django.utils import timezone
from mailchimp_marketing.api_client import ApiClientError

from apps.integrations.mailchimp.utils import mc_set_list_member, mc_add_tag, get_mailchimp_client
from apps.users.choices import PaymentStatusEnum
from apps.users.models import User

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Command for triggering the farewell and feedback email"

    def handle(self, *args, **options):
        mailchimp = get_mailchimp_client()

        _1_days_ago = timezone.now() - timezone.timedelta(days=1)
        now = timezone.now()
        qs = User.objects.filter(
            payment_status__in=[PaymentStatusEnum.CANCELLED, PaymentStatusEnum.TRIAL],
            payment_expiration__range=[_1_days_ago, now],
        )

        for user in qs:
            email = user.email
            email_md5 = hashlib.md5(email.encode()).hexdigest()
            try:
                response = mc_set_list_member(mailchimp, user, email_md5)
                tags = response["tags"]
                response = mc_add_tag(mailchimp, "Phase: Farewell", email_md5, tags)
                logger.info(response)
            except ApiClientError as e:
                logger.error(f"Status:{e.status_code}\n msg: {e.text}")
