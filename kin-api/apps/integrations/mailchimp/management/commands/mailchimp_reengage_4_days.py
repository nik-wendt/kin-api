import hashlib
import logging

from django.core.management.base import BaseCommand
from django.utils import timezone

from mailchimp_marketing.api_client import ApiClientError

from apps.integrations.mailchimp.utils import mc_set_list_member, mc_add_tag, get_mailchimp_client
from apps.users.models import User

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "command for triggering re-engagement email after 4 days of inactivity"

    def handle(self, *args, **options):
        mailchimp = get_mailchimp_client()

        _4_days_ago = timezone.now() - timezone.timedelta(days=4)
        _6_days_ago = timezone.now() - timezone.timedelta(days=6)
        qs = User.objects.filter(
            is_active=True,
            is_verified=True,
            last_login__lte=_4_days_ago,
            date_joined__range=[_6_days_ago, _4_days_ago],
        )

        for user in qs:
            email = user.email
            email_md5 = hashlib.md5(email.encode()).hexdigest()
            try:
                response = mc_set_list_member(mailchimp, user, email_md5)
                tags = response["tags"]
                response = mc_add_tag(mailchimp, "Phase: Inactive 4", email_md5, tags)
                logger.info(response)
            except ApiClientError as e:
                logger.error(f"Status:{e.status_code}\n msg: {e.text}")
