from rest_framework import routers

from apps.activity_log.views import LogActivityViewSet


router = routers.DefaultRouter()
router.register("", LogActivityViewSet, basename="activity-log")

urlpatterns = router.urls
