from django.db.models import Q

from django_enum_choices.filters import EnumChoiceFilterSetMixin
from django_filters import rest_framework as filters

from apps.activity_log.models import LogActivity


class LogActivityFilter(EnumChoiceFilterSetMixin, filters.FilterSet):
    kin = filters.CharFilter(method="filter_by_kin")

    class Meta:
        model = LogActivity
        fields = ["activity_type", "kin"]

    def filter_by_kin(self, queryset, _, value):

        if value:
            queryset = queryset.filter(
                Q(objs__contains=[{"id": value, "type": "peep"}])
                | Q(targets__contains=[{"id": value, "type": "peep"}])
            )
        return queryset
