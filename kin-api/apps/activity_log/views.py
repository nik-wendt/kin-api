from rest_framework import mixins, permissions, viewsets

from apps.activity_log.filters import LogActivityFilter
from apps.activity_log.models import LogActivity
from apps.activity_log.serializers import LogActivitySerializer
from apps.kin_auth.permissions import IsPaid, IsVerified


class LogActivityViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = LogActivitySerializer
    filterset_class = LogActivityFilter

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return LogActivity.objects.none()

        return LogActivity.objects.filter(owner=self.request.user)
