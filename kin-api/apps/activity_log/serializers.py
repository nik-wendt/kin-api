from rest_framework import serializers

from django_enum_choices.serializers import EnumChoiceModelSerializerMixin

from apps.activity_log.models import LogActivity


class LogActivitySerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = LogActivity
        fields = ("id", "activity_type", "objs", "targets", "snippet", "owner", "created")
