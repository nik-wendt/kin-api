from django.db.models import JSONField
from django.db import models

from django_enum_choices.fields import EnumChoiceField
from django_extensions.db.fields import ShortUUIDField

from apps.activity_log.choices import ActivityTypeEnum
from apps.activity_log.managers import LogActivityManager
from apps.users.models import User


class LogActivity(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    activity_type = EnumChoiceField(ActivityTypeEnum)
    snippet = models.TextField(blank=True)
    objs = JSONField(blank=True, null=True)
    targets = JSONField(blank=True, null=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="log_messages")
    created = models.DateTimeField(auto_now_add=True)

    objects = LogActivityManager()

    class Meta:
        db_table = "activity_log_logactivity"
        ordering = ["-created"]
