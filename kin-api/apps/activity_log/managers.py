from django.db import models

from apps.activity_log.choices import ActivityTypeEnum


class LogActivityManager(models.Manager):
    def generate_log_activity(self, activity_type, obj=None, target=None):
        try:
            log_activity = self.create(activity_type=activity_type, owner=obj.owner)
        except AttributeError:
            log_activity = self.create(activity_type=activity_type, owner=obj.kin.owner)

        if activity_type == ActivityTypeEnum.PEEP_CREATE:
            log_activity.objs = [{"id": obj.id, "value": obj.friendly_name, "type": "peep"}]
            log_activity.targets = [{"id": "", "value": "My Kin", "type": ""}]
        elif activity_type == ActivityTypeEnum.PEEP_MADE_DORMANT:
            log_activity.objs = [{"id": obj.id, "value": obj.friendly_name, "type": "peep"}]
            log_activity.targets = [{"id": "", "value": "Dormant", "type": ""}]
        elif (activity_type == ActivityTypeEnum.NUGGET_CREATE) or (
            activity_type == ActivityTypeEnum.NUGGET_EDIT
        ):
            log_activity.objs = [{"id": obj.id, "value": "Note", "type": "nugget"}]
            try:
                log_activity.targets = [
                    {"id": obj.peep.id, "value": obj.peep.friendly_name, "type": "peep"}
                ]
            except AttributeError:
                log_activity.targets = None

            log_activity.snippet = obj.raw_text
        elif activity_type == ActivityTypeEnum.TAG_CREATE:
            log_activity.objs = [{"id": obj.name, "value": obj.name, "type": "tag"}]
        elif activity_type == ActivityTypeEnum.PEEP_ADD_TAG:
            log_activity.objs = [{"id": obj.id, "value": obj.friendly_name, "type": "peep"}]
            log_activity.targets = [{"id": target.name, "value": target.name, "type": "tag"}]
        elif activity_type == ActivityTypeEnum.PEEP_ADD_FACT:
            log_activity.objs = [{"id": obj.id, "value": "Fact", "type": "fact"}]
            log_activity.targets = [
                {"id": target.id, "value": target.friendly_name, "type": "peep"}
            ]
            log_activity.snippet = f"{obj.key}: {obj.value}"
        elif activity_type == ActivityTypeEnum.REMINDER_DONE:
            log_activity.objs = [{"id": obj.id, "value": obj.completed, "type": "reminder"}]
            log_activity.targets = [{"id": "", "value": "Reminder Done", "type": ""}]
        elif activity_type == ActivityTypeEnum.SPREADSHEET_IMPORT:
            log_activity.objs = [{"id": "", "value": target["peep_count"], "type": ""}]
            log_activity.targets = [{"id": "", "value": target["file"], "type": ""}]

        return log_activity.save()
