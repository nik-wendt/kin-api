from rest_framework_nested import routers

from apps.spotlight.views import SpotlightViewSet


router = routers.DefaultRouter()
router.register("", SpotlightViewSet, basename="spotlight")


urlpatterns = router.urls
