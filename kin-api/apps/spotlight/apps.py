from django.apps import AppConfig


class SpotlightConfig(AppConfig):
    name = "apps.spotlight"
    verbose_name = "Spotlight"

    def ready(self):
        import apps.spotlight.signals
