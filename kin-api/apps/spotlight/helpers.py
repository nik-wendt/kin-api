import datetime
import json
import operator
from functools import reduce
from operator import or_

from django.conf import settings
from django.db.models import Count, OuterRef, Prefetch, Q, Subquery
from django.utils import timezone
from rest_framework import status

import arrow
import redis

from apps.calendar.models import Event
from apps.connections.models import Connection
from apps.facts.choices import FactTypeEnum
from apps.facts.models import Fact
from apps.groups.models import Group
from apps.kin.models import Kin
from apps.notes.models import Note, NoteMedia
from apps.notifications.utils import get_pusher_client
from apps.reminders.models import Reminder
from apps.spotlight.serializers import (
    PopularKinResultsSerializer,
    RecentKinResultsSerializer,
    SpecialOccasionResultsSerializer,
    UnassignedNotesResultsSerializer,
    UpcomingEventResultsSerializer,
    UpcomingReminderResultsSerializer,
)

# Recent Kin


def _get_recent_kin_results(user):
    latest_note_cover_images = NoteMedia.objects.filter(note=OuterRef("pk"), is_cover=True)

    latest_notes = (
        Note.objects.annotate(cover_image=Subquery(latest_note_cover_images.values("media")[:1]))
        .order_by("kin__id", "-modified")
        .distinct("kin__id")
    )

    popular_groups = Subquery(
        Group.objects.filter(kin=OuterRef("kin"))
        .annotate(kin_count=Count("kin"))
        .order_by("-kin_count")
        .values_list("id", flat=True)[:2]
    )

    kin = (
        Kin.objects.filter(owner=user)
        .exclude(userkin__user=user)
        .order_by("-modified")
        .prefetch_related(
            Prefetch("notes", queryset=latest_notes, to_attr="latest_notes"),
            Prefetch(
                "groups",
                queryset=Group.objects.filter(id__in=popular_groups)
                .annotate(kin_count=Count("kin"))
                .order_by("-kin_count"),
                to_attr="popular_groups",
            ),
        )[:10]
    )

    results = [
        {
            "id": k.id,
            "friendly_name": k.friendly_name,
            "profile_pic": k.profile_pic,
            "latest_note": next(iter(k.latest_notes), None),
            "popular_groups": [g.name for g in k.popular_groups],
            "reminder": getattr(k, "reminder", None),
        }
        for k in kin
    ]

    return results


def _get_and_cache_recent_kin_serialized(user):
    raw_results = _get_recent_kin_results(user)
    serializer = RecentKinResultsSerializer(
        {"count": len(raw_results), "cached_at": timezone.now(), "results": raw_results}
    )

    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_client.set(f"spotlight:recent_kin:{user.id}", json.dumps(serializer.data))

    pc = get_pusher_client()
    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-recent-kin-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    return serializer.data


def get_recent_kin(user):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_key = f"spotlight:recent_kin:{user.id}"

    serialized_results = redis_client.get(redis_key)

    if serialized_results:
        return json.loads(serialized_results)

    return _get_and_cache_recent_kin_serialized(user)


# Vintage Kin


def _get_vintage_kin_results(user):
    latest_note_cover_images = NoteMedia.objects.filter(note=OuterRef("pk"), is_cover=True)

    latest_notes = (
        Note.objects.annotate(cover_image=Subquery(latest_note_cover_images.values("media")[:1]))
        .order_by("kin__id", "-modified")
        .distinct("kin__id")
    )

    popular_groups = Subquery(
        Group.objects.filter(kin=OuterRef("kin"))
        .annotate(kin_count=Count("kin"))
        .order_by("-kin_count")
        .values_list("id", flat=True)[:2]
    )
    # 15 days ago
    _15_days_ago = timezone.now() - timezone.timedelta(days=15)

    kin = (
        Kin.objects.filter(owner=user)
        .exclude(userkin__user=user)
        .exclude(connections__connection_date__gte=_15_days_ago)
        .order_by("modified")
        .prefetch_related(
            Prefetch("notes", queryset=latest_notes, to_attr="latest_notes"),
            Prefetch(
                "groups",
                queryset=Group.objects.filter(id__in=popular_groups)
                .annotate(kin_count=Count("kin"))
                .order_by("-kin_count"),
                to_attr="popular_groups",
            ),
        )[:10]
    )

    results = [
        {
            "id": k.id,
            "friendly_name": k.friendly_name,
            "profile_pic": k.profile_pic,
            "latest_note": next(iter(k.latest_notes), None),
            "popular_groups": [g.name for g in k.popular_groups],
            "reminder": getattr(k, "reminder", None),
        }
        for k in kin
    ]

    return results


def _get_and_cache_vintage_kin_serialized(user):
    raw_results = _get_vintage_kin_results(user)
    serializer = RecentKinResultsSerializer(
        {"count": len(raw_results), "cached_at": timezone.now(), "results": raw_results}
    )

    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_client.set(f"spotlight:vintage_kin:{user.id}", json.dumps(serializer.data))

    pc = get_pusher_client()
    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-vintage-kin-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    return serializer.data


def get_vintage_kin(user):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_key = f"spotlight:vintage_kin:{user.id}"

    serialized_results = redis_client.get(redis_key)

    if user.date_joined > timezone.now() - datetime.timedelta(days=15):
        return []
    if serialized_results:
        return json.loads(serialized_results)

    return _get_and_cache_vintage_kin_serialized(user)


# Popular Kin


def _get_popular_kin_results(user):
    latest_note_cover_images = NoteMedia.objects.filter(note=OuterRef("pk"), is_cover=True)

    latest_notes = (
        Note.objects.annotate(cover_image=Subquery(latest_note_cover_images.values("media")[:1]))
        .order_by("kin__id", "-modified")
        .distinct("kin__id")
    )

    popular_groups = Subquery(
        Group.objects.filter(kin=OuterRef("kin"))
        .annotate(kin_count=Count("kin"))
        .order_by("-kin_count")
        .values_list("id", flat=True)[:2]
    )

    kin = (
        Kin.objects.filter(owner=user)
        .exclude(userkin__user=user)
        .order_by("-modified")
        .prefetch_related(
            Prefetch("notes", queryset=latest_notes, to_attr="latest_notes"),
            Prefetch(
                "groups",
                queryset=Group.objects.filter(id__in=popular_groups)
                .annotate(kin_count=Count("kin"))
                .order_by("-kin_count"),
                to_attr="popular_groups",
            ),
        )
        .popular()[:10]
    )

    results = [
        {
            "id": k.id,
            "friendly_name": k.friendly_name,
            "profile_pic": k.profile_pic,
            "latest_note": next(iter(k.latest_notes), None),
            "popular_groups": [g.name for g in k.popular_groups],
            "reminder": getattr(k, "reminder", None),
        }
        for k in kin
    ]

    return results


def _get_and_cache_popular_kin_serialized(user):
    raw_results = _get_popular_kin_results(user)
    serializer = PopularKinResultsSerializer(
        {"count": len(raw_results), "cached_at": timezone.now(), "results": raw_results}
    )

    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_client.set(f"spotlight:popular_kin:{user.id}", json.dumps(serializer.data))

    pc = get_pusher_client()
    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-popular-kin-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    return serializer.data


def get_popular_kin(user):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_key = f"spotlight:popular_kin:{user.id}"

    serialized_results = redis_client.get(redis_key)

    if serialized_results:
        return json.loads(serialized_results)

    return _get_and_cache_popular_kin_serialized(user)


# Upcoming Reminders


def _get_upcoming_reminders_results(user):
    popular_groups = Subquery(
        Group.objects.filter(kin=OuterRef("kin"))
        .annotate(kin_count=Count("kin"))
        .order_by("-kin_count")
        .values_list("id", flat=True)[:2]
    )

    upcoming_reminders = (
        Reminder.objects.filter(
            kin__owner=user,
            next_notification__lt=datetime.datetime.today() + datetime.timedelta(days=10),
        )
        .exclude(dismissed=True)
        .select_related("kin")
        .order_by("next_notification")
        .prefetch_related(
            Prefetch(
                "kin__groups",
                queryset=Group.objects.filter(id__in=popular_groups)
                .annotate(kin_count=Count("kin"))
                .order_by("-kin_count"),
                to_attr="popular_groups",
            )
        )
    )

    results = [
        {
            "id": r.kin.id,
            "friendly_name": r.kin.friendly_name,
            "profile_pic": r.kin.profile_pic,
            "primary_email": r.kin.primary_email,
            "primary_phone": r.kin.primary_phone,
            "primary_sms": r.kin.primary_sms,
            "last_connection_date": r.kin.last_connection_date,
            "popular_groups": [g.name for g in r.kin.popular_groups],
            "reminder": {
                "id": r.id,
                "recency": r.recency,
                "next_notification": r.next_notification,
            },
        }
        for r in upcoming_reminders
    ]

    return results


def _get_and_cache_upcoming_reminders_serialized(user):
    raw_results = _get_upcoming_reminders_results(user)
    serializer = UpcomingReminderResultsSerializer(
        {"count": len(raw_results), "cached_at": timezone.now(), "results": raw_results}
    )

    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_client.set(f"spotlight:upcoming_reminders:{user.id}", json.dumps(serializer.data))

    pc = get_pusher_client()
    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-upcoming-reminders-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    return serializer.data


def get_upcoming_reminders(user):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_key = f"spotlight:upcoming_reminders:{user.id}"

    serialized_results = redis_client.get(redis_key)

    if serialized_results:
        return json.loads(serialized_results)

    return _get_and_cache_upcoming_reminders_serialized(user)


# Upcoming Events


def _get_upcoming_events_results(user):
    upcoming_events = (
        Event.objects.filter(calendar__owner=user, start_date__gte=timezone.now())
        .order_by("start_date")
        .select_related("calendar")
        .prefetch_related(Prefetch("attendees"))
    )

    results = []

    for e in upcoming_events:
        kin_attendees = []
        event_dict = {
            "source_id": e.source_id,
            "calendar_source_id": e.calendar.source_id,
            "title": e.title,
            "start_date": e.start_date,
            "end_date": e.end_date,
            "all_day": e.all_day,
            "location": e.location,
        }

        for k in e.attendees.all():
            email_fact = (
                k.facts.filter(fact_type=FactTypeEnum.EMAIL)
                .order_by("-metadata__email_primary")
                .first()
            )
            kin_attendees.append(
                {
                    "id": k.id,
                    "email": email_fact.value,
                    "friendly_name": k.friendly_name,
                    "profile_pic": k.profile_pic,
                }
            )

        non_kin_attendees = [
            {"id": None, "email": nk["email"], "friendly_name": nk["name"], "profile_pic": None}
            for nk in e.non_kin_attendees
        ]

        event_dict["attendees"] = kin_attendees + non_kin_attendees

        results.append(event_dict)

    return results


def _get_and_cache_upcoming_events_serialized(user):
    raw_results = _get_upcoming_events_results(user)
    serializer = UpcomingEventResultsSerializer(
        {"count": len(raw_results), "cached_at": timezone.now(), "results": raw_results}
    )

    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_client.set(f"spotlight:upcoming_events:{user.id}", json.dumps(serializer.data))

    pc = get_pusher_client()
    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-upcoming-events-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    return serializer.data


def get_upcoming_events(user):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_key = f"spotlight:upcoming_events:{user.id}"

    serialized_results = redis_client.get(redis_key)

    if serialized_results:
        return json.loads(serialized_results)

    return _get_and_cache_upcoming_events_serialized(user)


# Unassigned Notes


def _get_unassigned_notes_results(user):
    unassigned_note_cover_images = NoteMedia.objects.filter(note=OuterRef("pk"), is_cover=True)

    unassigned_notes = Note.objects.filter(owner=user, kin__isnull=True).annotate(
        cover_image=Subquery(unassigned_note_cover_images.values("media")[:1])
    )
    return unassigned_notes


def _get_and_cache_unassigned_notes_serialized(user):
    raw_results = _get_unassigned_notes_results(user)
    serializer = UnassignedNotesResultsSerializer(
        {"count": raw_results.count(), "cached_at": timezone.now(), "results": raw_results}
    )

    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_client.set(f"spotlight:unassigned_notes:{user.id}", json.dumps(serializer.data))

    pc = get_pusher_client()
    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-unassigned-notes-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    return serializer.data


def get_unassigned_notes(user):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_key = f"spotlight:unassigned_notes:{user.id}"

    serialized_results = redis_client.get(redis_key)

    if serialized_results:
        return json.loads(serialized_results)

    return _get_and_cache_unassigned_notes_serialized(user)


# Special Occasions


def _get_special_occasions_results(user):
    results = []

    current_user_date = datetime.datetime.now(user.preferences.timezone_setting)
    upcoming_month_days = [
        f'{d.strftime("%m-%d")}'
        for d in (current_user_date + datetime.timedelta(n) for n in range(10))
    ]

    value_query = reduce(or_, (Q(value__endswith=d) for d in upcoming_month_days))

    special_occasions = Fact.objects.filter(
        fact_type=FactTypeEnum.DATE, metadata__date_notify=True, kin__owner=user
    )
    special_occasions = special_occasions.filter(value_query)

    for occasion in special_occasions:
        occasion_date = arrow.get(occasion.value)

        if occasion_date.year != 9999:
            if occasion_date.year > current_user_date.year:
                continue

        if current_user_date.date() > datetime.date(
            current_user_date.year, occasion_date.month, occasion_date.day
        ):
            next_occasion_date = datetime.date(
                current_user_date.year + 1, occasion_date.month, occasion_date.day
            )
        else:
            next_occasion_date = datetime.date(
                current_user_date.year, occasion_date.month, occasion_date.day
            )

        results.append(
            {
                "id": occasion.kin.id,
                "friendly_name": occasion.kin.friendly_name,
                "profile_pic": occasion.kin.profile_pic,
                "occasion": occasion.key,
                "date": occasion.value,
                "sort_date": datetime.date(
                    next_occasion_date.year, next_occasion_date.month, next_occasion_date.day
                ),
            }
        )

    results.sort(key=operator.itemgetter("sort_date"))

    return results


def _get_and_cache_special_occasions_serialized(user):
    raw_results = _get_special_occasions_results(user)
    has_future_occasions = Fact.objects.filter(
        fact_type=FactTypeEnum.DATE, metadata__date_notify=True, kin__owner=user
    ).exists()
    serializer = SpecialOccasionResultsSerializer(
        {
            "count": len(raw_results),
            "has_future_occasions": has_future_occasions,
            "cached_at": timezone.now(),
            "results": raw_results,
        }
    )

    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_client.set(f"spotlight:special_occasions:{user.id}", json.dumps(serializer.data))

    pc = get_pusher_client()
    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-special-occasions-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    return serializer.data


def get_special_occasions(user):
    redis_client = redis.Redis.from_url(settings.REDIS_URL)
    redis_key = f"spotlight:special_occasions:{user.id}"

    serialized_results = redis_client.get(redis_key)

    if serialized_results:
        return json.loads(serialized_results)

    return _get_and_cache_special_occasions_serialized(user)


def _send_all_spotlight_notifications(user):
    pc = get_pusher_client()

    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-recent-kin-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-vintage-kin-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-popular-kin-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-upcoming-reminders-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-upcoming-events-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-unassigned-notes-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )

    pc.trigger(
        f"private-encrypted-{user.id}",
        "spotlight-special-occasions-cache",
        {"status_code": status.HTTP_200_OK, "timestamp": str(timezone.now())},
    )
