from django.core.files.storage import default_storage
from rest_framework import serializers

from django_enum_choices.choice_builders import attribute_value

from apps.kin_extensions.serializers import CustomEnumChoiceField
from apps.reminders.choices import RecencyEnum


class SpotlightNoteSerializer(serializers.Serializer):
    text = serializers.CharField()
    plain_text = serializers.CharField()
    cover_image = serializers.SerializerMethodField()
    media_counts = serializers.DictField()
    created = serializers.DateTimeField()
    modified = serializers.DateTimeField()

    def get_cover_image(self, obj):
        if not obj.cover_image:
            return None

        return default_storage.url(obj.cover_image)


class RecentKinSerializer(serializers.Serializer):
    id = serializers.CharField()
    friendly_name = serializers.CharField()
    profile_pic = serializers.ImageField()
    latest_note = SpotlightNoteSerializer()
    popular_groups = serializers.ListField(child=serializers.CharField())
    reminder_recency = CustomEnumChoiceField(
        RecencyEnum, choice_builder=attribute_value, source="reminder.recency", default=""
    )


class RecentKinResultsSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    cached_at = serializers.DateTimeField()
    results = RecentKinSerializer(many=True)


class PopularKinSerializer(serializers.Serializer):
    id = serializers.CharField()
    friendly_name = serializers.CharField()
    profile_pic = serializers.ImageField()
    latest_note = SpotlightNoteSerializer()
    popular_groups = serializers.ListField(child=serializers.CharField())
    reminder_recency = CustomEnumChoiceField(
        RecencyEnum, choice_builder=attribute_value, source="reminder.recency", default=""
    )


class PopularKinResultsSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    cached_at = serializers.DateTimeField()
    results = PopularKinSerializer(many=True)


class UpcomingReminderSerializer(serializers.Serializer):
    id = serializers.CharField()
    recency = CustomEnumChoiceField(RecencyEnum, choice_builder=attribute_value, default="")
    next_notification = serializers.DateTimeField()


class UpcomingKinReminderSerializer(serializers.Serializer):
    id = serializers.CharField()
    friendly_name = serializers.CharField()
    profile_pic = serializers.ImageField()
    primary_email = serializers.EmailField(allow_null=True)
    primary_phone = serializers.CharField(allow_null=True)
    primary_sms = serializers.CharField(allow_null=True)
    last_connection_date = serializers.DateField(read_only=True, allow_null=True)
    popular_groups = serializers.ListField(child=serializers.CharField())
    reminder = UpcomingReminderSerializer()


class UpcomingReminderResultsSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    cached_at = serializers.DateTimeField()
    results = UpcomingKinReminderSerializer(many=True)


class UpcomingEventAttendeeSerializer(serializers.Serializer):
    id = serializers.CharField(allow_null=True)
    email = serializers.EmailField()
    friendly_name = serializers.CharField()
    profile_pic = serializers.ImageField(allow_null=True)


class UpcomingEventSerializer(serializers.Serializer):
    source_id = serializers.CharField()
    calendar_source_id = serializers.CharField()
    title = serializers.CharField()
    start_date = serializers.DateTimeField(allow_null=True)
    end_date = serializers.DateTimeField(allow_null=True)
    all_day = serializers.BooleanField()
    location = serializers.CharField()
    attendees = UpcomingEventAttendeeSerializer(many=True, allow_null=True)


class UpcomingEventResultsSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    cached_at = serializers.DateTimeField()
    results = UpcomingEventSerializer(many=True)


class UnassignedNoteSerializer(SpotlightNoteSerializer):
    id = serializers.CharField()


class UnassignedNotesResultsSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    cached_at = serializers.DateTimeField()
    results = UnassignedNoteSerializer(many=True)


class SpecialOccasionSerializer(serializers.Serializer):
    id = serializers.CharField()
    friendly_name = serializers.CharField()
    profile_pic = serializers.ImageField()
    occasion = serializers.CharField()
    date = serializers.CharField()


class SpecialOccasionResultsSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    has_future_occasions = serializers.BooleanField()
    cached_at = serializers.DateTimeField()
    results = SpecialOccasionSerializer(many=True)
