import logging

from celery import shared_task

from apps.spotlight.helpers import (
    _get_and_cache_popular_kin_serialized,
    _get_and_cache_recent_kin_serialized,
    _get_and_cache_special_occasions_serialized,
    _get_and_cache_vintage_kin_serialized,
    _get_and_cache_unassigned_notes_serialized,
    _get_and_cache_upcoming_events_serialized,
    _get_and_cache_upcoming_reminders_serialized,
    _send_all_spotlight_notifications,
)
from apps.users.models import User


logger = logging.getLogger(__name__)


@shared_task
def update_recent_kin_cache_task(user_id):
    try:
        user = User.objects.get(pk=user_id)
        _ = _get_and_cache_recent_kin_serialized(user)
        _ = _get_and_cache_vintage_kin_serialized(user)
    except User.DoesNotExist:
        logger.info("Failed to update_recent_kin_cache. No such user: %s", user_id)


@shared_task
def update_popular_kin_cache_task(user_id):
    try:
        user = User.objects.get(pk=user_id)
        _ = _get_and_cache_popular_kin_serialized(user)
    except User.DoesNotExist:
        logger.info("Failed to update_popular_kin_cache. No such user: %s", user_id)


@shared_task
def update_upcoming_reminders_cache_task(user_id):
    try:
        user = User.objects.get(pk=user_id)
        _ = _get_and_cache_upcoming_reminders_serialized(user)
    except User.DoesNotExist:
        logger.info("Failed to update_upcoming_reminders_cache. No such user: %s", user_id)


@shared_task
def update_upcoming_events_cache_task(user_id):
    try:
        user = User.objects.get(pk=user_id)
        _ = _get_and_cache_upcoming_events_serialized(user)
    except User.DoesNotExist:
        logger.info("Failed to update_upcoming_events_cache. No such user: %s", user_id)


@shared_task
def update_unassigned_notes_cache_task(user_id):
    try:
        user = User.objects.get(pk=user_id)
        _ = _get_and_cache_unassigned_notes_serialized(user)
    except User.DoesNotExist:
        logger.info("Failed to update_unassigned_notes_cache. No such user: %s", user_id)


@shared_task
def update_special_occasions_cache_task(user_id):
    try:
        user = User.objects.get(pk=user_id)
        _ = _get_and_cache_special_occasions_serialized(user)
    except User.DoesNotExist:
        logger.info("Failed to update_special_occasions_cache. No such user: %s", user_id)


@shared_task
def send_all_spotlight_notifications(user_id):
    try:
        user = User.objects.get(pk=user_id)
        _send_all_spotlight_notifications(user)
    except User.DoesNotExist:
        logger.info("Failed to _send_all_spotlight_notifications. No such user: %s", user_id)
