import logging

from django.core.management.base import BaseCommand

from apps.spotlight.helpers import _get_and_cache_special_occasions_serialized
from apps.users.models import User

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Refreshes spotlight special occasion data for all users."

    def handle(self, *args, **options):
        logger.info("Starting spotlight special occasion generation...")

        users = User.objects.all()
        for user in users:
            _ = _get_and_cache_special_occasions_serialized(user)

        logger.info("Finshed spotlight special occasion generation.")
