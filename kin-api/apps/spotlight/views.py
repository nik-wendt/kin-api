from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from drf_yasg.utils import swagger_auto_schema

from apps.kin_auth.permissions import IsPaid, IsVerified
from apps.spotlight.helpers import (
    get_popular_kin,
    get_recent_kin,
    get_special_occasions,
    get_unassigned_notes,
    get_upcoming_events,
    get_upcoming_reminders,
    get_vintage_kin,
)
from apps.spotlight.serializers import (
    PopularKinResultsSerializer,
    RecentKinResultsSerializer,
    SpecialOccasionResultsSerializer,
    UnassignedNotesResultsSerializer,
    UpcomingEventResultsSerializer,
    UpcomingReminderResultsSerializer,
)


class SpotlightViewSet(viewsets.ViewSet):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)

    @swagger_auto_schema(responses={status.HTTP_200_OK: RecentKinResultsSerializer})
    @action(detail=False, methods=["GET"], url_path="recent-kin")
    def recent_kin(self, request):
        recent_kin = get_recent_kin(request.user)

        return Response(recent_kin, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={status.HTTP_200_OK: RecentKinResultsSerializer})
    @action(detail=False, methods=["GET"], url_path="vintage-kin")
    def vintage_kin(self, request):
        vintage_kin = get_vintage_kin(request.user)

        return Response(vintage_kin, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={status.HTTP_200_OK: PopularKinResultsSerializer})
    @action(detail=False, methods=["GET"], url_path="popular-kin")
    def popular_kin(self, request):
        popular_kin = get_popular_kin(request.user)

        return Response(popular_kin, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={status.HTTP_200_OK: UpcomingReminderResultsSerializer})
    @action(detail=False, methods=["GET"], url_path="upcoming-reminders")
    def upcoming_reminders(self, request):
        upcoming_reminders = get_upcoming_reminders(request.user)

        return Response(upcoming_reminders, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={status.HTTP_200_OK: UpcomingEventResultsSerializer})
    @action(detail=False, methods=["GET"], url_path="upcoming-events")
    def upcoming_events(self, request):
        upcoming_events = get_upcoming_events(request.user)

        return Response(upcoming_events, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={status.HTTP_200_OK: UnassignedNotesResultsSerializer})
    @action(detail=False, methods=["GET"], url_path="unassigned-notes")
    def unassigned_notes(self, request):
        unassigned_notes = get_unassigned_notes(request.user)

        return Response(unassigned_notes, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={status.HTTP_200_OK: SpecialOccasionResultsSerializer})
    @action(detail=False, methods=["GET"], url_path="special-occasions")
    def special_occasions(self, request):
        special_occasions = get_special_occasions(request.user)

        return Response(special_occasions, status=status.HTTP_200_OK)
