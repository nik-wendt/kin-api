from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from apps.facts.choices import FactTypeEnum
from apps.facts.models import Fact
from apps.kin.models import Kin
from apps.notes.models import Note, NoteMedia
from apps.reminders.models import Reminder
from apps.spotlight.tasks import (
    update_recent_kin_cache_task,
    update_popular_kin_cache_task,
    update_upcoming_reminders_cache_task,
    update_unassigned_notes_cache_task,
    update_special_occasions_cache_task,
)


@receiver(post_save, sender=Kin)
@receiver(post_delete, sender=Kin)
@receiver(post_save, sender=Fact)
@receiver(post_delete, sender=Fact)
@receiver(post_save, sender=Note)
@receiver(post_delete, sender=Note)
def update_recent_kin_cache(sender, instance, **kwargs):
    try:
        user_id = instance.owner.id
    except AttributeError:
        user_id = instance.kin.owner.id

    update_recent_kin_cache_task.delay(user_id)


@receiver(post_save, sender=Kin)
@receiver(post_delete, sender=Kin)
def update_popular_kin_cache(sender, instance, **kwargs):
    update_popular_kin_cache_task.delay(instance.owner.id)


@receiver(post_save, sender=Kin)
@receiver(post_delete, sender=Kin)
@receiver(post_save, sender=Fact)
@receiver(post_delete, sender=Fact)
@receiver(post_save, sender=Reminder)
@receiver(post_delete, sender=Reminder)
def update_upcoming_reminders_cache(sender, instance, **kwargs):
    try:
        user_id = instance.owner.id
    except AttributeError:
        user_id = instance.kin.owner.id

    update_upcoming_reminders_cache_task.delay(user_id)


@receiver(post_save, sender=Note)
@receiver(post_save, sender=NoteMedia)
@receiver(post_delete, sender=Note)
@receiver(post_delete, sender=NoteMedia)
def update_unassigned_notes_cache(sender, instance, **kwargs):
    try:
        user_id = instance.owner.id  # Sender is a note
    except AttributeError:
        user_id = instance.note.owner.id  # Sender is note media

    update_unassigned_notes_cache_task.delay(user_id)


@receiver(post_save, sender=Kin)
@receiver(post_delete, sender=Kin)
@receiver(post_save, sender=Fact)
@receiver(post_delete, sender=Fact)
def update_special_occasions_cache(sender, instance, **kwargs):
    try:
        user_id = instance.owner.id
    except AttributeError:
        user_id = instance.kin.owner.id
        if instance.fact_type != FactTypeEnum.DATE:
            return

    update_special_occasions_cache_task.delay(user_id)
