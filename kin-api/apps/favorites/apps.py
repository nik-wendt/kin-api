from django.apps import AppConfig


class FavoritesConfig(AppConfig):
    name = "apps.favorites"
    verbose_name = "Favorites"

    def ready(self):
        import apps.favorites.signals
