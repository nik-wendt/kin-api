from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

from django_extensions.db.fields import ShortUUIDField


class Favorite(models.Model):
    id = ShortUUIDField(primary_key=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = ShortUUIDField()
    content_object = GenericForeignKey("content_type", "object_id")
    is_favorite = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ("content_type", "object_id")
