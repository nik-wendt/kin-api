from rest_framework import permissions, mixins
from rest_framework.pagination import PageNumberPagination

from rest_framework.viewsets import GenericViewSet

from apps.favorites.models import Favorite
from apps.favorites.serializers import FavoriteSerializer
from apps.kin.models import Kin
from apps.kin_auth.permissions import IsPaid, IsVerified


class FavoriteViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    GenericViewSet,
):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    serializer_class = FavoriteSerializer
    pagination_class = PageNumberPagination

    def get_queryset(self):
        kin_ids = Kin.objects.filter(owner=self.request.user).values_list("id", flat=True)
        return Favorite.objects.filter(object_id__in=kin_ids)
