import logging

from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers

from apps.favorites.models import Favorite
from apps.groups.models import Group
from apps.groups.serializers import GroupSerializer
from apps.kin.models import Kin
from apps.kin.serializers import KinSimpleSerializer


logger = logging.getLogger(__name__)


class FavoritedObjectRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        if isinstance(value, Kin):
            serializer = KinSimpleSerializer(value)
        if isinstance(value, Group):
            serializer = GroupSerializer(value)
        return serializer.data


class FavoriteSerializer(serializers.ModelSerializer):
    object_id = serializers.CharField(required=True)
    content_type = serializers.CharField(
        required=True, help_text="Only `kin` or `group` at the moment"
    )
    content_object = FavoritedObjectRelatedField(read_only=True)

    class Meta:
        model = Favorite
        fields = ("id", "object_id", "content_type", "content_object")

    def validate_content_type(self, content_type):
        if content_type.lower() not in ["kin", "group"]:
            raise serializers.ValidationError(
                f"`{content_type}` is not a valid content_type. Try, `kin` or `group`"
            )
        return content_type

    def validate(self, attrs):
        id = attrs["object_id"]
        content_type = attrs["content_type"].lower()

        try:
            if content_type == "kin":
                app_label = "kin"
            else:
                app_label = f"{content_type}s"
            ct = ContentType.objects.get(model=content_type, app_label=app_label)
        except Exception as e:
            raise serializers.ValidationError(e)

        model_class = ct.model_class()
        if not model_class.objects.filter(id=id).exists():
            raise serializers.ValidationError(f"invalid id '{id}' for objet type {content_type}")

        if Favorite.objects.filter(content_type=ct, object_id=id):
            raise serializers.ValidationError(
                f"Favorite already exists for {content_type} with id '{id}'"
            )

        return super().validate(attrs)

    def create(self, validated_data):
        content_type = validated_data["content_type"].lower()
        if content_type == "kin":
            app_label = "kin"
        else:
            app_label = f"{content_type}s"
        ct = ContentType.objects.get(model=content_type, app_label=app_label)
        model_class = ct.model_class()
        content_object = model_class.objects.get(pk=validated_data["object_id"])
        return super().create({"content_object": content_object})
