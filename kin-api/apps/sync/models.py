from django.db.models import JSONField
from django.db import models
from django_extensions.db.fields import ShortUUIDField

from apps.users.models import User


class SyncContactHistory(models.Model):
    id = ShortUUIDField(primary_key=True, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="contact_history")
    contact_record = JSONField(null=False, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    # TODO 4798: Source field? In case the data is in different shapes? Probably not.
