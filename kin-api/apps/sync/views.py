from datetime import datetime, timedelta

import pytz
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import permissions
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.response import Response

from apps.kin_auth.permissions import IsVerified, IsPaid
from apps.sync.models import SyncContactHistory
from apps.sync.serializers import SyncContactHistorySerializer


class SyncContactHistoryView(CreateAPIView, ListAPIView):
    permission_classes = (permissions.IsAuthenticated, IsVerified, IsPaid)
    queryset = SyncContactHistory.objects.all()
    serializer_class = SyncContactHistorySerializer

    def get(self, request, *args, **kwargs):
        try:
            most_recent_record = SyncContactHistory.objects.filter(user=request.user).latest(
                "created"
            )
            last_record_date = most_recent_record.created
        except ObjectDoesNotExist:
            return Response(data=None, status=status.HTTP_404_NOT_FOUND)
        now = datetime.utcnow()
        now = now.replace(tzinfo=pytz.utc)
        if last_record_date < now - timedelta(hours=24):
            return Response(data=None, status=status.HTTP_404_NOT_FOUND)
        elif last_record_date > now - timedelta(hours=24):
            return Response(data={"created": last_record_date}, status=status.HTTP_200_OK)
        else:
            raise Exception

    def post(self, request, *args, **kwargs):
        serializer = SyncContactHistorySerializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        serializer.save(user=self.request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)
