from django.urls import path
from rest_framework.routers import DefaultRouter

from apps.sync.views import SyncContactHistoryView  #  SyncView

router = DefaultRouter()
# router.register(r"", SyncView, basename="sync")

urlpatterns = [path("contact-history/", SyncContactHistoryView.as_view(), name="contact_history")]

urlpatterns += router.urls
