from rest_framework import serializers

from apps.kin_extensions.validators import json_schema_to_drf_validate
from apps.sync.models import SyncContactHistory
from apps.users.models import User

CONTACT_HISTORY_SCHEMA = {
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {"type": "string"},
            "firstName": {"type": "string"},
            "lastName": {"type": "string"},
            "phoneNumbers": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "id": {"type": "string"},
                        "countryCode": {"type": "string"},
                        "number": {"type": "string"},
                        "digits": {"type": "string"},
                        "label": {"type": "string"},
                    },
                    "additionalProperties": False,
                    "required": ["id", "digits", "countryCode"],
                },
            },
            "emails": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "email": {"type": "string"},
                        "label": {"type": "string"},
                        "id": {"type": "id"},
                    },
                    "additionalProperties": False,
                    "required": ["id", "email"],
                },
            },
            "addresses": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "id": {"type": "string"},
                        "region": {"type": "string"},
                        "street": {"type": "string"},
                        "city": {"type": "string"},
                        "country": {"type": "string"},
                        "postalCode": {"type": "string"},
                        "isoCountryCode": {"type": "string"},
                        "label": {"type": "string"},
                    },
                    "additionalProperties": False,
                    "required": ["id", "street", "city", "isoCountryCode"],
                },
            },
            "additionalProperties": True,
            "required": ["id"],
        },
    },
}


class UserPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        user = self.context["request"].user
        queryset = User.objects.get(pk=user)
        return queryset


class SyncContactHistorySerializer(serializers.ModelSerializer):
    user = UserPrimaryKeyRelatedField(required=False)
    contact_record = serializers.JSONField()

    def validate(self, attrs):
        json_schema_to_drf_validate(
            attrs["contact_record"], "contact_record", CONTACT_HISTORY_SCHEMA
        )
        return super().validate(attrs)

    def save(self, **kwargs):
        kwargs["contact_record"] = self.validated_data

        return super().save(**kwargs)

    class Meta:
        model = SyncContactHistory
        fields = "__all__"
