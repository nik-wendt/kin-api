from django.apps import AppConfig


class SyncConfig(AppConfig):
    name = "apps.sync"
    verbose_name = "Sync"
