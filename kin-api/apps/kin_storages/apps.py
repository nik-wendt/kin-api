from django.apps import AppConfig


class KinStoragesConfig(AppConfig):
    name = "apps.kin_storages"
    verbose_name = "Kinship Storages"
