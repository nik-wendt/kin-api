from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

schema_view = get_schema_view(
    openapi.Info(
        title="Kinship API",
        default_version="v2",
        description="The Kinship Admin API",
        terms_of_service="",
        contact=openapi.Contact(email="nik@kinshipsystems.com"),
    ),
    public=True,
    urlconf="admin_portal_project.urls",
)


urlpatterns = [
    path("", include("apps.administrators.urls")),
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="v2-schema-swagger-ui",
    ),
]
