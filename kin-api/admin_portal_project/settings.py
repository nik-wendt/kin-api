import os

import dj_database_url
from corsheaders.defaults import default_headers
from envparse import env

from kin_api_project.settings import SWAGGER_SETTINGS


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# aws general
AWS_APP_ACCESS_KEY_ID = env("AWS_APP_ACCESS_KEY_ID")
AWS_APP_SECRET_ACCESS_KEY = env("AWS_APP_SECRET_ACCESS_KEY")

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env("DJANGO_SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env("DJANGO_DEBUG", cast=bool, default=False)

ALLOWED_HOSTS = env("DJANGO_ALLOWED_HOSTS", cast=list)

# Application definition

EXTERNAL_APPS = [
    "corsheaders",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_countries",
    "django_extensions",
    "rest_framework",
    "drf_yasg",
    "oauth2_provider",
    "timezone_field",
    "health_check",
    "health_check.db",
    "health_check.contrib.redis",
    "anymail",
]

KINSHIP_APPS = [
    "apps.kin_storages.apps.KinStoragesConfig",
    "apps.users.apps.UsersConfig",
    "apps.kin_admin.apps.KinAdminConfig",
    "apps.promo_codes.apps.PromoCodesConfig",
    "apps.conferences.apps.ConferencesConfig",
    "apps.kin_auth.apps.KinAuthConfig",
    "apps.groups.apps.GroupsConfig",
    "apps.kin.apps.KinConfig",
    "apps.facts.apps.FactsConfig",
    "apps.reminders.apps.RemindersConfig",
    "apps.connections.apps.ConnectionsConfig",
    "apps.activity_log.apps.ActivityLogConfig",
    "apps.notes.apps.NotesConfig",
    "apps.calendar.apps.CalendarConfig",
    "apps.data_export.apps.DataExportConfig",
]

INSTALLED_APPS = EXTERNAL_APPS + KINSHIP_APPS

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "apps.notifications.middleware.FlashNotificationMiddleware",
]

ROOT_URLCONF = "admin_portal_project.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

ASGI_APPLICATION = "admin_portal_project.asgi.application"

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "oauth2_provider.contrib.rest_framework.OAuth2Authentication",
    ),
    "DEFAULT_FILTER_BACKENDS": ("django_filters.rest_framework.DjangoFilterBackend",),
    # TODO: make sure this gets updated to the custom pagination class once it's fixed.
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.PageNumberPagination",
    "PAGE_SIZE": 25,
    "DEFAULT_THROTTLE_RATES": {"anon": "20/minute"},
}


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {"default": dj_database_url.parse(env("DJANGO_DB_URL", default="sqlite:///tmp"))}

REDIS_URL = env("REDIS_URL")

# Auth

AUTH_USER_MODEL = "users.User"

AUTHENTICATION_BACKENDS = ["django.contrib.auth.backends.AllowAllUsersModelBackend"]

LOGIN_REDIRECT_URL = ""

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"},
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = "/api/static/"
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"
STATIC_ROOT = os.path.join(BASE_DIR, "static")

# Uploaded files
DEFAULT_FILE_STORAGE = env(
    "DJANGO_DEFAULT_FILE_STORAGE", default="apps.kin_storages.backends.CustomS3Boto3Storage"
)
AWS_ACCESS_KEY_ID = AWS_APP_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY = AWS_APP_SECRET_ACCESS_KEY
AWS_STORAGE_BUCKET_NAME = env(
    "AWS_STORAGE_BUCKET_NAME", default=None
)  # TODO: take away default and configure in ci/cd to use minio for test phase, only defaulting to pass testing phase
AWS_DEFAULT_ACL = None
AWS_BUCKET_ACL = None
AWS_S3_FILE_OVERWRITE = False
AWS_AUTO_CREATE_BUCKET = True
AWS_S3_ENDPOINT_URL = env("AWS_S3_ENDPOINT_URL", default=None)
AWS_S3_CUSTOM_DOMAIN = env("AWS_S3_CUSTOM_DOMAIN")  # e.g. media-dev.heykinship.com/media
DATA_UPLOAD_MAX_MEMORY_SIZE = 10_485_760  # 10MB
FILE_UPLOAD_MAX_MEMORY_SIZE = 10_485_760  # 10MB


# ssl
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

# security headers
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_SECONDS = 3600
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True
X_FRAME_OPTIONS = "DENY"
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = list(default_headers) + ["x-app-build-version"]

# anymail
ANYMAIL = {"MANDRILL_API_KEY": env("MAILCHIMP_TRANSACTIONAL_API_KEY", default=None)}

# email settings
EMAIL_BACKEND = env(
    "DJANGO_EMAIL_BACKEND", default="django.core.mail.backends.console.EmailBackend"
)
DEFAULT_FROM_EMAIL = env("DJANGO_DEFAULT_FROM_EMAIL")
EMAIL_TO_KINSHIP_ADDRESS = env("EMAIL_TO_KINSHIP_ADDRESS", default=None)

# mailchimp
MAILCHIMP_MARKETING_API_KEY = env("MAILCHIMP_MARKETING_API_KEY", default=None)
MAILCHIMP_TRANSACTIONAL_API_KEY = env("MAILCHIMP_TRANSACTIONAL_API_KEY", default=None)
MAILCHIMP_DATACENTER = (
    MAILCHIMP_MARKETING_API_KEY.split("-")[1] if MAILCHIMP_MARKETING_API_KEY else None
)  # e.g. `us3`
MAILCHIMP_LIST_ID = env("MAILCHIMP_LIST_ID", default=None)

# logging
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {"format": "%(levelname)s - %(module)s - %(message)s - %(asctime)s"},
        "json": {"()": "pythonjsonlogger.jsonlogger.JsonFormatter"},
    },
    "handlers": {
        "console": {"level": "DEBUG", "class": "logging.StreamHandler", "formatter": "default"}
    },
    "loggers": {"": {"handlers": ["console"], "propagate": True, "level": "DEBUG"}},
}

# Celery
CELERY_BROKER_URL = env("REDIS_URL")
CELERY_RESULT_BACKEND = env("REDIS_URL")

# Swagger
SWAGGER_SETTINGS = SWAGGER_SETTINGS

# OAUTH
OAUTH2_PROVIDER = {
    "OIDC_ENABLED": True,
    "OIDC_RSA_PRIVATE_KEY": env("OIDC_RSA_PRIVATE_KEY", default=None),
    "SCOPES": {
        "read": "Read",
        "write": "Write",
        "openid": "OpenID Connect",
        "profile": "Profile",
        "offline_access": "Offline Access",
    },
    "PKCE_REQUIRED": False,
    "OAUTH2_VALIDATOR_CLASS": "apps.kin_auth.oauth.oauth2_validators.KinshipOAuth2Validator",
    "ACCESS_TOKEN_EXPIRE_SECONDS": int(env("ACCESS_TOKEN_LIFETIME_MINUTES", default=10)) * 60,
    "REFRESH_TOKEN_EXPIRE_SECONDS": int(env("REFRESH_TOKEN_LIFETIME_MINUTES", default=10080)) * 60,
    "REFRESH_TOKEN_GRACE_PERIOD_SECONDS": 120,
}
