from django.urls import include, path

from drf_yasg import openapi
from drf_yasg.views import get_schema_view


schema_view = get_schema_view(
    openapi.Info(
        title="Kinship Developer API",
        default_version="v1",
        description="The Kinship Developer API",
        terms_of_service="",
        contact=openapi.Contact(email="glenn@kinshipsystems.com"),
    ),
    public=True,
    urlconf="dev_portal_project.urls",
)


urlpatterns = [
    path("swagger/", schema_view.with_ui("swagger", cache_timeout=0), name="v2-schema-swagger-ui",),
    path("", include("health_check.urls")),
    path("", include("apps.developers.urls")),
]
