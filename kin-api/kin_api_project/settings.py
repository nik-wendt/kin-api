from datetime import timedelta
import os

import dj_database_url
from corsheaders.defaults import default_headers
from envparse import env

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# Celery
CELERY_BROKER_URL = env("REDIS_URL")
CELERY_RESULT_BACKEND = env("REDIS_URL")

# aws general
AWS_APP_ACCESS_KEY_ID = env("AWS_APP_ACCESS_KEY_ID")
AWS_APP_SECRET_ACCESS_KEY = env("AWS_APP_SECRET_ACCESS_KEY")

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env("DJANGO_SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env("DJANGO_DEBUG", cast=bool, default=False)

ALLOWED_HOSTS = env("DJANGO_ALLOWED_HOSTS", cast=list)

# Application definition

EXTERNAL_APPS = [
    "corsheaders",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_countries",
    "django_extensions",
    "oauth2_provider",
    "rest_framework",
    "drf_yasg",
    "rest_framework_simplejwt.token_blacklist",
    "ordered_model",
    "reversion",
    "timezone_field",
    "health_check",
    "health_check.db",
    "health_check.contrib.redis",
    "anymail",
]

# this env var is set in the entrypoint script 'test' for local and in the .gitlab-ci.yml for cicd
TEST_ENV = env("TEST_ENV", default=False)

if not TEST_ENV:
    EXTERNAL_APPS += [
        "django_elasticsearch_dsl",
        "health_check.contrib.celery",
        "health_check.contrib.celery_ping",
    ]

KINSHIP_APPS = [
    "apps.kin_storages.apps.KinStoragesConfig",
    "apps.users.apps.UsersConfig",
    "apps.kin_auth.apps.KinAuthConfig",
    "apps.kin_extensions.apps.KinExtensionsConfig",
    "apps.cms.apps.CMSConfig",
    "apps.healthcheck.apps.HealthCheckConfig",
    "apps.activity_log.apps.ActivityLogConfig",
    "apps.groups.apps.GroupsConfig",
    "apps.kin.apps.KinConfig",
    "apps.facts.apps.FactsConfig",
    "apps.notes.apps.NotesConfig",
    "apps.reminders.apps.RemindersConfig",
    "apps.notifications.apps.NotificationsConfig",
    "apps.integrations.ses_management.apps.SESIntegrationManagementConfig",
    "apps.integrations.twilio.apps.TwilioIntegrationConfig",
    "apps.integrations.mixpanel.apps.MixPanelIntegrationConfig",
    "apps.integrations.feedback.apps.FeedbackIntegrationConfig",
    "apps.integrations.mailchimp.apps.MailchimpConfig",
    "apps.integrations.zapier.apps.ZapierConfig",
    "apps.dev_scripts.apps.DevScriptsConfig",
    "apps.client_logging.apps.ClientLoggingConfig",
    "apps.data_export.apps.DataExportConfig",
    "apps.spotlight.apps.SpotlightConfig",
    "apps.calendar.apps.CalendarConfig",
    "apps.connections.apps.ConnectionsConfig",
    "apps.payments.apps.PaymentsConfig",
    "apps.sync.apps.SyncConfig",
    "apps.promo_codes.apps.PromoCodesConfig",
    "apps.conferences.apps.ConferencesConfig",
    "apps.developers.apps.DevelopersConfig",
    "apps.integrations.apple_wallet.apps.AppleWalletIntegrationConfig",
    "apps.smart_merge.apps.SmartMergeConfig",
    "apps.kin_admin.apps.KinAdminConfig",
    "apps.favorites.apps.FavoritesConfig",
]

INSTALLED_APPS = EXTERNAL_APPS + KINSHIP_APPS

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "apps.notifications.middleware.FlashNotificationMiddleware",
]

ROOT_URLCONF = "kin_api_project.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

ASGI_APPLICATION = "kin_api_project.asgi.application"


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {"default": dj_database_url.parse(env("DJANGO_DB_URL", default="sqlite:///tmp"))}

REDIS_URL = env("REDIS_URL")

# Auth

AUTH_USER_MODEL = "users.User"

AUTHENTICATION_BACKENDS = ["django.contrib.auth.backends.AllowAllUsersModelBackend"]

LOGIN_REDIRECT_URL = ""

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"},
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

APPLE_AUTH = {
    "client_id": env("APPLE_AUTH_CLIENT_ID", default=None),
    "client_secret": env("APPLE_AUTH_CLIENT_SECRET", default=None),
    "client_secret_ttl_seconds": env("APPLE_AUTH_CLIENT_SECRET_TTL_SECONDS", default=None),
    "team_id": env("APPLE_AUTH_TEAM_ID", default=None),
    "key_id": env("APPLE_AUTH_KEY_ID", default=None),
    "token_audience": env("APPLE_AUTH_TOKEN_AUDIENCE", default=None),
}

GOOGLE_AUTH = {
    "client_id": env("GOOGLE_AUTH_CLIENT_ID", default=None),
    "client_secret": env("GOOGLE_AUTH_CLIENT_SECRET", default=None),
}


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = "/api/static/"
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"
STATIC_ROOT = os.path.join(BASE_DIR, "static")

# Uploaded files
DEFAULT_FILE_STORAGE = env(
    "DJANGO_DEFAULT_FILE_STORAGE", default="apps.kin_storages.backends.CustomS3Boto3Storage"
)
AWS_ACCESS_KEY_ID = AWS_APP_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY = AWS_APP_SECRET_ACCESS_KEY
AWS_STORAGE_BUCKET_NAME = env(
    "AWS_STORAGE_BUCKET_NAME", default=None
)  # TODO: take away default and configure in ci/cd to use minio for test phase, only defaulting to pass testing phase
AWS_DEFAULT_ACL = None
AWS_BUCKET_ACL = None
AWS_S3_FILE_OVERWRITE = False
AWS_AUTO_CREATE_BUCKET = True
AWS_S3_ENDPOINT_URL = env("AWS_S3_ENDPOINT_URL", default=None)
AWS_S3_CUSTOM_DOMAIN = env("AWS_S3_CUSTOM_DOMAIN")  # e.g. media-dev.heykinship.com/media
DATA_UPLOAD_MAX_MEMORY_SIZE = 10_485_760  # 10MB
FILE_UPLOAD_MAX_MEMORY_SIZE = 10_485_760  # 10MB

# channels
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {"hosts": [env("REDIS_URL", default="redis://redis:6379/0")]},
    }
}

# drf
REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework_simplejwt.authentication.JWTAuthentication",
        "oauth2_provider.contrib.rest_framework.OAuth2Authentication",
    ),
    "DEFAULT_FILTER_BACKENDS": ("django_filters.rest_framework.DjangoFilterBackend",),
    "DEFAULT_PAGINATION_CLASS": "apps.kin_extensions.pagination.CustomPageNumberPagination",
    "PAGE_SIZE": 25,
    "DEFAULT_THROTTLE_RATES": {"anon": "20/minute"},
}

# swagger
SWAGGER_SETTINGS = {
    "SECURITY_DEFINITIONS": {"Jwt": {"type": "apiKey", "in": "header", "name": "Authorization"}},
    "USE_SESSION_AUTH": False,
    "PERSIST_AUTH": True,
    "DEFAULT_PAGINATOR_INSPECTORS": [
        "apps.kin_extensions.openapi_schema.CustomDjangoRestResponsePagination",
        "drf_yasg.inspectors.CoreAPICompatInspector",
    ],
}

# simple jwt
SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": timedelta(
        minutes=int(env("ACCESS_TOKEN_LIFETIME_MINUTES", default=10))
    ),
    "REFRESH_TOKEN_LIFETIME": timedelta(
        minutes=int(env("REFRESH_TOKEN_LIFETIME_MINUTES", default=10080))
    ),
    "ROTATE_REFRESH_TOKENS": True,
    "BLACKLIST_AFTER_ROTATION": True,
    "ALGORITHM": "HS256",
    "SIGNING_KEY": SECRET_KEY,
    "AUTH_HEADER_TYPES": ("JWT",),
}

# ssl
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

# security headers
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_SECONDS = 3600
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True
X_FRAME_OPTIONS = "DENY"
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = list(default_headers) + ["x-app-build-version"]

# anymail
ANYMAIL = {"MANDRILL_API_KEY": env("MAILCHIMP_TRANSACTIONAL_API_KEY", default=None)}

# email settings

EMAIL_BACKEND = env(
    "DJANGO_EMAIL_BACKEND", default="django.core.mail.backends.console.EmailBackend"
)
DEFAULT_FROM_EMAIL = env("DJANGO_DEFAULT_FROM_EMAIL")
EMAIL_TO_KINSHIP_ADDRESS = env("EMAIL_TO_KINSHIP_ADDRESS", default=None)

# ses
AWS_SES_ACCESS_KEY_ID = AWS_APP_ACCESS_KEY_ID
AWS_SES_SECRET_ACCESS_KEY = AWS_APP_SECRET_ACCESS_KEY
AWS_SES_REGION_NAME = env("AWS_SES_REGION_NAME")
AWS_SES_REGION_ENDPOINT = env("AWS_SES_REGION_ENDPOINT")

# mailchimp
MAILCHIMP_MARKETING_API_KEY = env("MAILCHIMP_MARKETING_API_KEY", default=None)
MAILCHIMP_TRANSACTIONAL_API_KEY = env("MAILCHIMP_TRANSACTIONAL_API_KEY", default=None)
MAILCHIMP_DATACENTER = (
    MAILCHIMP_MARKETING_API_KEY.split("-")[1] if MAILCHIMP_MARKETING_API_KEY else None
)  # e.g. `us3`
MAILCHIMP_LIST_ID = env("MAILCHIMP_LIST_ID", default=None)

# logging
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {"format": "%(levelname)s - %(module)s - %(message)s - %(asctime)s"},
        "json": {"()": "pythonjsonlogger.jsonlogger.JsonFormatter"},
    },
    "handlers": {
        "console": {"level": "DEBUG", "class": "logging.StreamHandler", "formatter": "default"}
    },
    "loggers": {"": {"handlers": ["console"], "propagate": True, "level": "DEBUG"}},
}

# OneSignal
ONE_SIGNAL_APP_ID = env("ONE_SIGNAL_APP_ID", default=None)
ONE_SIGNAL_API_KEY = env("ONE_SIGNAL_API_KEY", default=None)
ONE_SIGNAL_PUSH_NOTIFICATION_TIME = env("ONE_SIGNAL_PUSH_NOTIFICATION_TIME", default="11:00AM")

# Twilio
TWILIO_ACCOUNT_SID = env("TWILIO_ACCOUNT_SID", default=None)
TWILIO_AUTH_TOKEN = env("TWILIO_AUTH_TOKEN", default=None)
TWILIO_SMS_PHONE = env("TWILIO_SMS_PHONE", default=None)
TWILIO_WHATSAPP_PHONE = env("TWILIO_WHATSAPP_PHONE", default=TWILIO_SMS_PHONE)

# Mixpanel
MIXPANEL_TOKEN = env("MIXPANEL_TOKEN", default=None)

# Pusher
PUSHER = {
    "app_id": env("PUSHER_APP_ID", default=None),
    "key": env("PUSHER_KEY", default=None),
    "secret": env("PUSHER_SECRET", default=None),
    "cluster": "us2",
    "ssl": True,
    "encryption_master_key_base64": env("PUSHER_ENCRYPTION_MASTER", default=None),
}

# Apple App Store Payments
APPLE_APP_STORE = {
    "bundle_id": env("APPLE_APP_STORE_BUNDLE_ID", default=None),
    "shared_secret": env("APPLE_APP_STORE_SHARED_SECRET", default=None),
}

# Google APIs
GOOGLE_API_KEY = env("GOOGLE_API_KEY", default=None)


OAUTH2_PROVIDER = {
    "OIDC_ENABLED": True,
    "OIDC_RSA_PRIVATE_KEY": env("OIDC_RSA_PRIVATE_KEY", default=None),
    "SCOPES": {
        "read": "Read",
        "write": "Write",
        "openid": "OpenID Connect",
        "profile": "Profile",
        "offline_access": "Offline Access",
    },
    "PKCE_REQUIRED": False,
    "OAUTH2_VALIDATOR_CLASS": "apps.kin_auth.oauth.oauth2_validators.KinshipOAuth2Validator",
    "ACCESS_TOKEN_EXPIRE_SECONDS": int(env("ACCESS_TOKEN_LIFETIME_MINUTES", default=10)) * 60,
    "REFRESH_TOKEN_EXPIRE_SECONDS": int(env("REFRESH_TOKEN_LIFETIME_MINUTES", default=10080)) * 60,
    "REFRESH_TOKEN_GRACE_PERIOD_SECONDS": 120,
}

# Apple Wallet
PASS_TYPE_ID = env("PASS_TYPE_ID", default=None)
APPLE_KEY_PEM_PASS = env("APPLE_KEY_PEM_PASS", None)


WALLETPASS = {
    "CERT_PATH": "apps/integrations/apple_wallet/certs/certificate.pem",
    "KEY_PATH": "apps/integrations/apple_wallet/certs/key.pem",
    # (None if isn't protected)
    # MUST be in bytes-like
    "KEY_PASSWORD": bytes(APPLE_KEY_PEM_PASS, "utf-8"),
    "PASS_TYPE_ID": env("PASS_TYPE_ID", default=None),
    "TEAM_ID": env("APPLE_AUTH_TEAM_ID", default=None),
    "SERVICE_URL": env("WALLET_SERVICE_URL", default=None),
}

# Elasticsearch
ELASTICSEARCH_DSL_SIGNAL_PROCESSOR = "apps.search.tasks.CelerySignalProcessor"

ELASTICSEARCH_DSL = {"default": {"hosts": env("ELASTIC_HOST", default=None)}}
