"""kin_api_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth import views as auth_views
from django.urls import include, path

from drf_yasg import openapi
from drf_yasg.views import get_schema_view

schema_view = get_schema_view(
    openapi.Info(
        title="Kinship API",
        default_version="v2",
        description="The Kinship API v2",
        terms_of_service="",
        contact=openapi.Contact(email="glenn@truthandsystems.com"),
    ),
    public=True,
    urlconf="kin_api_project.urls",
)


urlpatterns = [
    path(
        "api/v2/swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="v2-schema-swagger-ui",
    ),
    path("o/", include("apps.kin_auth.oauth.urls", namespace="oauth2_provider")),
    path("api/v2/auth/", include("apps.kin_auth.urls")),
    path("api/v2/users/", include("apps.users.urls")),
    path("api/v2/cms/", include("apps.cms.urls")),
    path("api/v2/groups/", include("apps.groups.urls")),
    path("api/v2/kin/", include("apps.kin.urls")),
    path("api/v2/facts/", include("apps.facts.urls")),
    path("api/v2/notes/", include("apps.notes.urls")),
    path("api/v2/log/", include("apps.activity_log.urls")),
    path("api/v2/reminders/", include("apps.reminders.urls")),
    path("api/v2/search/", include("apps.search.urls")),
    path("api/v2/ses/", include("apps.integrations.ses_management.urls")),
    path("api/v2/sms/", include("apps.integrations.twilio.urls")),
    path("api/v2/healthchecks/", include("apps.healthcheck.urls")),
    path("api/v2/client-logging/", include("apps.client_logging.urls")),
    path("api/v2/export/", include("apps.data_export.urls")),
    path("api/v2/admin/", include("apps.kin_admin.urls")),
    path("api/v2/spotlight/", include("apps.spotlight.urls")),
    path("api/v2/calendars/", include("apps.calendar.urls")),
    path("api/v2/connections/", include("apps.connections.urls")),
    path("api/v2/feedback/", include("apps.integrations.feedback.urls")),
    path("api/v2/payments/", include("apps.payments.urls")),
    path("api/v2/sync/", include("apps.sync.urls")),
    path("api/v2/promo-codes/", include("apps.promo_codes.urls")),
    path("api/v2/zapier/", include("apps.integrations.zapier.urls")),
    path("accounts/login/", auth_views.LoginView.as_view(template_name="oauth/login.html")),
    path("api/v2/passes/", include("apps.integrations.apple_wallet.passes-urls")),
    path("api/v2/pass/", include("apps.integrations.apple_wallet.urls")),
    path("api/v2/merge/", include("apps.smart_merge.urls")),
    path("api/v2/favorite/", include("apps.favorites.urls")),
    path("", include("health_check.urls")),
]
