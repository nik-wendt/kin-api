"""
ASGI config for kin_api_project project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/asgi/
"""

import os
import threading

from django.core.asgi import get_asgi_application
import django

THREAD_STACK_SIZE_IN_BYTES = 4 * 80 * 1024  # 320kb
threading.stack_size(THREAD_STACK_SIZE_IN_BYTES)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "kin_api_project.settings")
django.setup()
application = get_asgi_application()
