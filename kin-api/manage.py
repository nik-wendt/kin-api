#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""

import os
import sys
import threading

THREAD_STACK_SIZE_IN_BYTES = 4 * 80 * 1024  # 320kb
threading.stack_size(THREAD_STACK_SIZE_IN_BYTES)


def main():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "kin_api_project.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == "__main__":
    main()
