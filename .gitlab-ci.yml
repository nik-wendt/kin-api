image: docker:latest

stages:
  - build
  - test
  - deploy

services:
    - name: docker:dind
      alias: thedockerhost
variables:
    DOCKER_HOST: tcp://thedockerhost:2375/
    DOCKER_DRIVER: overlay2
    # See https://github.com/docker-library/docker/pull/166
    DOCKER_TLS_CERTDIR: ""
    KUBE_NAMESPACE: "apps-$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME"
    PROJECT_NAME: $CI_PROJECT_NAME
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_BRANCH

build-dev:
  stage: build
  rules:
    - if: $CI_COMMIT_BRANCH == "development"
    - if: $CI_COMMIT_BRANCH == "uat"
    - if: $CI_COMMIT_BRANCH == "master"
  tags:
    - digitalocean
  script:
    - echo $CI_REGISTRY_IMAGE
    - echo "building dev image $CI_REGISTRY_IMAGE"
    - docker build . --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH
    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH


test:
  variables:
    POSTGRES_VERSION: "13"
    POSTGRES_DB: test
    POSTGRES_USER_TEST: postgres
    POSTGRES_PASSWORD_TEST: postgres
    POSTGRES_HOST_AUTH_METHOD: trust
    REDIS_URL: redis://redis:6379/0
    AWS_APP_ACCESS_KEY_ID: abcdef123
    AWS_APP_SECRET_ACCESS_KEY: ghiklm456
    AWS_SES_REGION_NAME: us-east-1
    AWS_SES_REGION_ENDPOINT: email.us-east-1.amazonaws.com
    AWS_S3_CUSTOM_DOMAIN: minio:9000
    DJANGO_SECRET_KEY: "12345"
    DJANGO_ALLOWED_HOSTS: "*"
    DJANGO_DEFAULT_FROM_EMAIL: Kinship <noreply@heykinship.com>
    TEST_ENV: 'true'
  services:
    - "postgres:${POSTGRES_VERSION}"
    - redis:5
  stage: test
  image: python:3.8-slim
  needs: []
  before_script:
    - export POSTGRES_USER=$POSTGRES_USER_TEST
    - export POSTGRES_PASSWORD=$POSTGRES_PASSWORD_TEST
    - apt-get update
    - apt-get install -y libpq-dev libffi-dev libjpeg-dev zlib1g-dev libpng-dev libmagic-dev
    - DB_HOST="postgres"
    - export DATABASE_URL="postgres://${POSTGRES_USER_TEST}:${POSTGRES_PASSWORD_TEST}@${DB_HOST}:5432/${POSTGRES_DB}"
    - export DJANGO_DB_URL="postgres://${POSTGRES_USER_TEST}:${POSTGRES_PASSWORD_TEST}@${DB_HOST}:5432/${POSTGRES_DB}"
    - cd kin-api
    - pip install -r requirements.txt
    - pip install -r requirements-test.txt
    - python manage.py migrate
  script:
    - pytest .
  rules:
    - if: '$TEST_DISABLED'
      when: never
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
  tags:
    - digitalocean


deploy-dev:
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == "development"
  tags:
    - digitalocean
  image: dtzar/helm-kubectl
  variables:
    API_ROOT_URL: $API_ROOT_URL_DEV
    AWS_STORAGE_BUCKET_NAME: $AWS_STORAGE_BUCKET_NAME_DEV
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID_DEV
    AWS_APP_ACCESS_KEY_ID: $AWS_APP_ACCESS_KEY_ID_DEV
    AWS_APP_SECRET_ACCESS_KEY: $AWS_APP_SECRET_ACCESS_KEY_DEV
    AWS_S3_CUSTOM_DOMAIN: $AWS_S3_CUSTOM_DOMAIN_DEV
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY_DEV
    AWS_TASK_DEFINITION_NAME: $AWS_TASK_DEFINITION_NAME_DEV
    ACCESS_TOKEN_LIFETIME_MINUTES: $ACCESS_TOKEN_LIFETIME_MINUTES_DEV
    EMAIL_TO_KINSHIP_ADDRESS: $EMAIL_TO_KINSHIP_ADDRESS_DEV
    DJANGO_DEBUG: $DJANGO_DEBUG_DEV
    DJANGO_SECRET_KEY: $DJANGO_SECRET_KEY_DEV
    FLASK_DEBUG: $FLASK_DEBUG_DEV
    POSTGRES_DB: $POSTGRES_DB_DEV
    POSTGRES_HOST: $POSTGRES_HOST_DEV
    POSTGRES_PASSWORD: $POSTGRES_PASSWORD_DEV
    REDIS_URL: $REDIS_URL_DO
    SIGNING_KEY: $SIGNING_KEY_DEV
    MIXPANEL_TOKEN: $MIXPANEL_TOKEN_DEV
    APPLE_AUTH_CLIENT_ID: $APPLE_AUTH_CLIENT_ID_DEV
    APPLE_AUTH_CLIENT_SECRET: $APPLE_AUTH_CLIENT_SECRET_DEV
    APPLE_AUTH_TEAM_ID: $APPLE_AUTH_TEAM_ID_DEV
    APPLE_AUTH_KEY_ID: $APPLE_AUTH_KEY_ID_DEV
    GOOGLE_AUTH_CLIENT_ID: $GOOGLE_AUTH_CLIENT_ID_DEV
    GOOGLE_AUTH_CLIENT_SECRET: $GOOGLE_AUTH_CLIENT_SECRET_DEV
    PUSHER_APP_ID: $PUSHER_APP_ID_DEV
    PUSHER_KEY: $PUSHER_KEY_DEV
    PUSHER_SECRET: $PUSHER_SECRET_DEV
    PUSHER_ENCRYPTION_MASTER: $PUSHER_ENCRYPTION_MASTER_DEV
    APPLE_APP_STORE_BUNDLE_ID: $APPLE_APP_STORE_BUNDLE_ID_DEV
    APPLE_APP_STORE_SHARED_SECRET: $APPLE_APP_STORE_SHARED_SECRET_DEV
    TWILIO_ACCOUNT_SID: $TWILIO_ACCOUNT_SID_DEV
    TWILIO_AUTH_TOKEN: $TWILIO_AUTH_TOKEN_DEV
    TWILIO_SMS_PHONE: $TWILIO_SMS_PHONE_DEV
    TWILIO_WHATSAPP_PHONE: $TWILIO_WHATSAPP_PHONE_DEV
    GOOGLE_API_KEY: $GOOGLE_API_KEY_DEV
    MAILCHIMP_LIST_ID: $MAILCHIMP_LIST_ID_DEV
    MAILCHIMP_TRANSACTIONAL_API_KEY: $MAILCHIMP_TRANSACTIONAL_API_KEY
    MAILCHIMP_MARKETING_API_KEY: $MAILCHIMP_MARKETING_API_KEY
    OIDC_RSA_PRIVATE_KEY: $OIDC_RSA_PRIVATE_KEY_DEV
    WALLET_SERVICE_URL: $WALLET_SERVICE_URL_DEV
    ELASTIC_HOST: $ELASTIC_HOST_DEV


    # Kubernetes variables
    KUBE_NAMESPACE: "apps-$CI_PROJECT_NAME-development"
    KUBE_DEPLOYMENT_REPLICA_SIZE: 1
    KUBE_CLUSTER_ID: $KUBE_CLUSTER_ID_DEV

  before_script:
    - apk update
    - apk add -u gettext
    - apk add doctl --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
    - doctl auth init -t $DO_TOKEN
    - doctl kubernetes cluster kubeconfig save $KUBE_CLUSTER_ID
    - kubectl describe namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
    - kubectl create secret docker-registry gitlab-ci-secret -n "$KUBE_NAMESPACE" --docker-server=$CI_REGISTRY --docker-username="$CI_DEPLOY_USER" --docker-password="$CI_DEPLOY_PASSWORD" --docker-email="$GITLAB_USER_EMAIL" --dry-run=client -o yaml | kubectl apply -f -
    - envsubst < kube/django/env-var-config-map.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/env-var-config-map.yaml | kubectl create -f - -n "$KUBE_NAMESPACE"
  script:
    - envsubst < kube/redis/redis-service.yml | kubectl apply -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/redis/redis-stateful-set.yaml | kubectl apply -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/redis/redis-config.yaml | kubectl apply -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/django-service.yml | kubectl apply -f - -n "$KUBE_NAMESPACE"
    - kubectl apply -f kube/es/infrastructure/es-stateful-set.yaml -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/django-api.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/django-api.yaml |kubectl create -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/celery.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/celery.yaml |kubectl create -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/django-admin.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/django-admin.yaml |kubectl create -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/django-dev-portal.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/jango-dev-portal.yaml |kubectl create -f - -n "$KUBE_NAMESPACE"
    - bash kube/cron/setup_crons.sh $IMAGE $KUBE_NAMESPACE || exit 1

deploy-uat:
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == "uat"
  tags:
    - digitalocean
  image: dtzar/helm-kubectl
  variables:
    API_ROOT_URL: $API_ROOT_URL_UAT
    AWS_STORAGE_BUCKET_NAME: $AWS_STORAGE_BUCKET_NAME_UAT
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID_UAT
    AWS_APP_ACCESS_KEY_ID: $AWS_APP_ACCESS_KEY_ID_UAT
    AWS_APP_SECRET_ACCESS_KEY: $AWS_APP_SECRET_ACCESS_KEY_UAT
    AWS_S3_CUSTOM_DOMAIN: $AWS_S3_CUSTOM_DOMAIN_UAT
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY_UAT
    AWS_TASK_DEFINITION_NAME: $AWS_TASK_DEFINITION_NAME_UAT
    ACCESS_TOKEN_LIFETIME_MINUTES: $ACCESS_TOKEN_LIFETIME_MINUTES_DEV
    EMAIL_TO_KINSHIP_ADDRESS: $EMAIL_TO_KINSHIP_ADDRESS_UAT
    DJANGO_DEBUG: $DJANGO_DEBUG_UAT
    DJANGO_SECRET_KEY: $DJANGO_SECRET_KEY_UAT
    FLASK_DEBUG: $FLASK_DEBUG_UAT
    POSTGRES_DB: $POSTGRES_DB_UAT
    POSTGRES_HOST: $POSTGRES_HOST_UAT
    POSTGRES_PASSWORD: $POSTGRES_PASSWORD_UAT
    REDIS_URL: $REDIS_URL_DO
    SIGNING_KEY: $SIGNING_KEY_UAT
    MIXPANEL_TOKEN: $MIXPANEL_TOKEN_UAT
    APPLE_AUTH_CLIENT_ID: $APPLE_AUTH_CLIENT_ID_UAT
    APPLE_AUTH_CLIENT_SECRET: $APPLE_AUTH_CLIENT_SECRET_UAT
    APPLE_AUTH_TEAM_ID: $APPLE_AUTH_TEAM_ID_UAT
    APPLE_AUTH_KEY_ID: $APPLE_AUTH_KEY_ID_UAT
    GOOGLE_AUTH_CLIENT_ID: $GOOGLE_AUTH_CLIENT_ID_UAT
    GOOGLE_AUTH_CLIENT_SECRET: $GOOGLE_AUTH_CLIENT_SECRET_UAT
    PUSHER_APP_ID: $PUSHER_APP_ID_UAT
    PUSHER_KEY: $PUSHER_KEY_UAT
    PUSHER_SECRET: $PUSHER_SECRET_UAT
    PUSHER_ENCRYPTION_MASTER: $PUSHER_ENCRYPTION_MASTER_UAT
    APPLE_APP_STORE_BUNDLE_ID: $APPLE_APP_STORE_BUNDLE_ID_UAT
    APPLE_APP_STORE_SHARED_SECRET: $APPLE_APP_STORE_SHARED_SECRET_UAT
    TWILIO_ACCOUNT_SID: $TWILIO_ACCOUNT_SID_UAT
    TWILIO_AUTH_TOKEN: $TWILIO_AUTH_TOKEN_UAT
    TWILIO_SMS_PHONE: $TWILIO_SMS_PHONE_UAT
    GOOGLE_API_KEY: $GOOGLE_API_KEY_UAT
    MAILCHIMP_LIST_ID: $MAILCHIMP_LIST_ID_UAT
    MAILCHIMP_TRANSACTIONAL_API_KEY: $MAILCHIMP_TRANSACTIONAL_API_KEY
    MAILCHIMP_MARKETING_API_KEY: $MAILCHIMP_MARKETING_API_KEY
    OIDC_RSA_PRIVATE_KEY: $OIDC_RSA_PRIVATE_KEY_UAT
    WALLET_SERVICE_URL: $WALLET_SERVICE_URL_UAT
    ELASTIC_HOST: $ELASTIC_HOST_UAT

    # Kubernetes variables
    KUBE_CLUSTER_ID: "55fd774d-6caa-472a-9786-98bbc64f24ff"
    KUBE_NAMESPACE: "apps-$CI_PROJECT_NAME-uat"
    KUBE_DEPLOYMENT_REPLICA_SIZE: 1

  before_script:
    - apk update
    - apk add -u gettext
    - apk add doctl --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
    - doctl auth init -t $DO_TOKEN
    - doctl kubernetes cluster kubeconfig save $KUBE_CLUSTER_ID
    - kubectl describe namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
    - kubectl create secret docker-registry gitlab-ci-secret -n "$KUBE_NAMESPACE" --docker-server=$CI_REGISTRY --docker-username="$CI_DEPLOY_USER" --docker-password="$CI_DEPLOY_PASSWORD" --docker-email="$GITLAB_USER_EMAIL" --dry-run=client -o yaml | kubectl apply -f -
    - envsubst < kube/django/env-var-config-map.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/env-var-config-map.yaml | kubectl create -f - -n "$KUBE_NAMESPACE"
  script:
    - envsubst < kube/redis/redis-service.yml | kubectl apply -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/redis/redis-stateful-set.yaml | kubectl apply -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/redis/redis-config.yaml | kubectl apply -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/django-service.yml | kubectl apply -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/django-api.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/django-api.yaml |kubectl create -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/celery.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/celery.yaml |kubectl create -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/django-admin.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/django-admin.yaml |kubectl create -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/django-dev-portal.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/jango-dev-portal.yaml |kubectl create -f - -n "$KUBE_NAMESPACE"
    - bash kube/cron/setup_crons.sh $IMAGE $KUBE_NAMESPACE || exit 1


deploy-prod:
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  tags:
    - digitalocean
  image: dtzar/helm-kubectl
  variables:
    API_ROOT_URL: $API_ROOT_URL_PROD
    AWS_STORAGE_BUCKET_NAME: $AWS_STORAGE_BUCKET_NAME_PROD
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID_PROD
    AWS_APP_ACCESS_KEY_ID: $AWS_APP_ACCESS_KEY_ID_PROD
    AWS_APP_SECRET_ACCESS_KEY: $AWS_APP_SECRET_ACCESS_KEY_PROD
    AWS_S3_CUSTOM_DOMAIN: $AWS_S3_CUSTOM_DOMAIN_PROD
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY_PROD
    AWS_TASK_DEFINITION_NAME: $AWS_TASK_DEFINITION_NAME_PROD
    ACCESS_TOKEN_LIFETIME_MINUTES: $ACCESS_TOKEN_LIFETIME_MINUTES_PROD
    EMAIL_TO_KINSHIP_ADDRESS: $EMAIL_TO_KINSHIP_ADDRESS_PROD
    DJANGO_DEBUG: $DJANGO_DEBUG_PROD
    DJANGO_SECRET_KEY: $DJANGO_SECRET_KEY_PROD
    FLASK_DEBUG: $FLASK_DEBUG_PROD
    POSTGRES_DB: $POSTGRES_DB_PROD
    POSTGRES_HOST: $POSTGRES_HOST_PROD
    POSTGRES_PASSWORD: $POSTGRES_PASSWORD_PROD
    REDIS_URL: $REDIS_URL_DO
    SIGNING_KEY: $SIGNING_KEY_PROD
    MIXPANEL_TOKEN: $MIXPANEL_TOKEN_PROD
    APPLE_AUTH_CLIENT_ID: $APPLE_AUTH_CLIENT_ID_PROD
    APPLE_AUTH_CLIENT_SECRET: $APPLE_AUTH_CLIENT_SECRET_PROD
    APPLE_AUTH_TEAM_ID: $APPLE_AUTH_TEAM_ID_PROD
    APPLE_AUTH_KEY_ID: $APPLE_AUTH_KEY_ID_PROD
    GOOGLE_AUTH_CLIENT_ID: $GOOGLE_AUTH_CLIENT_ID_PROD
    GOOGLE_AUTH_CLIENT_SECRET: $GOOGLE_AUTH_CLIENT_SECRET_PROD
    PUSHER_APP_ID: $PUSHER_APP_ID_PROD
    PUSHER_KEY: $PUSHER_KEY_PROD
    PUSHER_SECRET: $PUSHER_SECRET_PROD
    PUSHER_ENCRYPTION_MASTER: $PUSHER_ENCRYPTION_MASTER_PROD
    APPLE_APP_STORE_BUNDLE_ID: $APPLE_APP_STORE_BUNDLE_ID_PROD
    APPLE_APP_STORE_SHARED_SECRET: $APPLE_APP_STORE_SHARED_SECRET_PROD
    TWILIO_ACCOUNT_SID: $TWILIO_ACCOUNT_SID_PROD
    TWILIO_AUTH_TOKEN: $TWILIO_AUTH_TOKEN_PROD
    TWILIO_SMS_PHONE: $TWILIO_SMS_PHONE_PROD
    TWILIO_WHATSAPP_PHONE: $TWILIO_WHATSAPP_PHONE_PROD
    GOOGLE_API_KEY: $GOOGLE_API_KEY_PROD
    MAILCHIMP_LIST_ID: $MAILCHIMP_LIST_ID_PROD
    MAILCHIMP_TRANSACTIONAL_API_KEY: $MAILCHIMP_TRANSACTIONAL_API_KEY
    MAILCHIMP_MARKETING_API_KEY: $MAILCHIMP_MARKETING_API_KEY
    OIDC_RSA_PRIVATE_KEY: $OIDC_RSA_PRIVATE_KEY_PROD
    WALLET_SERVICE_URL: $WALLET_SERVICE_URL_PROD
    ELASTIC_HOST: $ELASTIC_HOST_PROD

    # Kubernetes variables
    KUBE_NAMESPACE: "apps-$CI_PROJECT_NAME-prod"
    KUBE_DEPLOYMENT_REPLICA_SIZE: 1
    KUBE_CLUSTER_ID: $KUBE_CLUSTER_ID_PROD

  before_script:
    - apk update
    - apk add -u gettext
    - apk add doctl --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
    - doctl auth init -t $DO_TOKEN
    - doctl kubernetes cluster kubeconfig save $KUBE_CLUSTER_ID
    - kubectl describe namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
    - kubectl create secret docker-registry gitlab-ci-secret -n "$KUBE_NAMESPACE" --docker-server=$CI_REGISTRY --docker-username="$CI_DEPLOY_USER" --docker-password="$CI_DEPLOY_PASSWORD" --docker-email="$GITLAB_USER_EMAIL" --dry-run=client -o yaml | kubectl apply -f -
    - envsubst < kube/django/env-var-config-map.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/env-var-config-map.yaml | kubectl create -f - -n "$KUBE_NAMESPACE"

  script:
    - envsubst < kube/redis/redis-service.yml | kubectl apply -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/redis/redis-stateful-set.yaml | kubectl apply -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/redis/redis-config.yaml | kubectl apply -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/django-service.yml | kubectl apply -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/django-api.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/django-api.yaml |kubectl create -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/celery.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/celery.yaml |kubectl create -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/django-admin.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/django-admin.yaml |kubectl create -f - -n "$KUBE_NAMESPACE"
    - envsubst < kube/django/django-dev-portal.yaml | kubectl replace -f - --force -n "$KUBE_NAMESPACE" || envsubst < kube/django/jango-dev-portal.yaml |kubectl create -f - -n "$KUBE_NAMESPACE"
    - bash kube/cron/setup_crons.sh $IMAGE $KUBE_NAMESPACE || exit 1
